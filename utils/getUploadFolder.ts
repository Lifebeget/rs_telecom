import crypto from 'crypto';
import fs from 'fs';
import path from 'path';

export const getUploadFolder = () => {
  const uploads = path.join(
    'uploads',
    crypto.pseudoRandomBytes(4).toString('hex')
  );

  if (!fs.existsSync(uploads)) {
    fs.mkdirSync(uploads);
  }

  return uploads;
};
