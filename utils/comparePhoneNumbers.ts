export function arePhoneNumbersEqual(ph1: string, ph2: string): boolean {
  return ph1.replace(/\s/g, '') === ph2.replace(/\s/g, '');
}
