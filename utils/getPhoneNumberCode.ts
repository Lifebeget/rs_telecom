import { parsePhoneNumber } from 'Utils';

type PhoneNumber = {
  ndc: string; // national destination code
  msisdn: string; // subscriber number
};

export const getPhoneNumberCode = (phoneNumber: string): PhoneNumber => {
  const parts = parsePhoneNumber(phoneNumber).formatInternational().split(' ');

  const ndc = parts[1];
  const msisdn = parts[2];

  return {
    msisdn,
    ndc
  };
};
