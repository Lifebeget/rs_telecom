import { Brackets, SelectQueryBuilder } from 'typeorm';

const MIN_SEARCH_LENGTH = 3;

export const searchQueryBuilder = <T>(
  qb: SelectQueryBuilder<T>,
  search = '',
  phoneNumberColumn: string,
  firstNameColumn: string,
  surnameColumn?: string,
  emailColumn?: string
): SelectQueryBuilder<T> => {
  if (search.length > MIN_SEARCH_LENGTH) {
    const parameters = {
      search: `%${search}%`
    };

    qb.andWhere(
      new Brackets(sqb => {
        sqb.where(
          `replace(${phoneNumberColumn}, ' ', '') LIKE replace(:search, ' ', '')`,
          parameters
        );

        if (surnameColumn) {
          sqb
            .orWhere(
              `lower(${surnameColumn}) || ' ' || lower(${firstNameColumn}) LIKE lower(:search)`,
              parameters
            )
            .orWhere(
              `lower(${firstNameColumn}) || ' ' || lower(${surnameColumn}) LIKE lower(:search)`,
              parameters
            );
        } else {
          sqb.orWhere(
            `lower(${firstNameColumn}) LIKE lower(:search)`,
            parameters
          );
        }

        if (emailColumn) {
          sqb.orWhere(`lower(${emailColumn}) LIKE lower(:search)`, parameters);
        }
      })
    );
  }

  return qb;
};
