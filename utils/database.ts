/* eslint-disable sonarjs/no-identical-functions -- refactor */
import faker from 'faker';
import { Connection, EntityManager } from 'typeorm';

import { ChecklistItemState, Permission, Role, TeamType } from 'Constants';
import { ContractChecklistStatusEntity } from 'Server/modules/contract/entities';
import {
  PermissionEntity,
  RoleEntity,
  RoleXPermissionEntity
} from 'Server/modules/role/entities';
import {
  TeamEntity,
  TeamSuggestedRoleEntity
} from 'Server/modules/team/entities';

type Manager = Connection | EntityManager;

faker.locale = 'de';

export async function findOneTeamSid(
  manager: Manager,
  team: string
): Promise<{ sid: string }> {
  const row = await manager
    .createQueryBuilder(TeamEntity, 't')
    .select('t.sid')
    .where('t.name = :name', { name: team })
    .getOne();

  if (!row) {
    throw new Error(`Team ${team} not found`);
  }

  return row;
}

export async function findOneTeamByParentSid(
  connection: Manager,
  parentTeam: string | null,
  team: string
): Promise<{ sid: string }> {
  const query = connection
    .createQueryBuilder(TeamEntity, 't')
    .select('t.sid')
    .where('t.name = :name', { name: team });

  if (parentTeam) {
    query.andWhere(
      't.parent_team_sid IN (SELECT t.sid FROM teams.team t WHERE t.name = :parent)',
      {
        parent: parentTeam
      }
    );
  } else {
    query.andWhere('t.parent_team_sid IS NULL');
  }

  const row = await query.getOne();

  if (!row) {
    throw new Error(`Team ${team} not found`);
  }

  return row;
}

export async function findOneContractChecklistStatus(
  connection: Manager,
  status: ChecklistItemState
): Promise<ContractChecklistStatusEntity> {
  const row = await connection
    .createQueryBuilder(ContractChecklistStatusEntity, 's')
    .where('s.name = :status', { status })
    .getOne();

  if (!row) {
    throw new Error(`Status ${status} not found`);
  }

  return row;
}

export async function findOneRole(
  connection: Manager,
  role: string
): Promise<RoleEntity> {
  const row = await connection
    .createQueryBuilder(RoleEntity, 'r')
    .where('r.name = :name', { name: role })
    .getOne();

  if (!row) {
    throw new Error(`Role ${role} not found`);
  }

  return row;
}

export async function findOnePermission(
  manager: Manager,
  permission: Permission
): Promise<PermissionEntity> {
  const row = await manager
    .createQueryBuilder(PermissionEntity, 'p')
    .where('p.name = :name', { name: permission })
    .getOne();

  if (!row) {
    throw new Error(`Permission ${permission} not found`);
  }

  return row;
}

export async function insertPermission(
  manager: EntityManager,
  role: string,
  permissions: Permission[]
) {
  const { sid: roleSid } = await findOneRole(manager, role);
  const permissionEntities = await Promise.all(
    permissions.map(permission => findOnePermission(manager, permission))
  );

  await manager
    .createQueryBuilder()
    .insert()
    .into(RoleXPermissionEntity, ['role', 'permission'])
    .values(
      permissionEntities.map(({ sid: permissionSid }) => ({
        permission: permissionSid,
        role: roleSid
      }))
    )
    .execute();
}

export async function removePermission(
  manager: EntityManager,
  role: string,
  permissions: Permission[]
) {
  const { sid: roleSid } = await findOneRole(manager, role);
  const permissionEntities = await Promise.all(
    permissions.map(permission => findOnePermission(manager, permission))
  );

  await Promise.all(
    permissionEntities.map(({ sid: permissionSid }) =>
      manager
        .createQueryBuilder()
        .delete()
        .from(RoleXPermissionEntity)
        .where('role_sid = :roleSid', { roleSid })
        .andWhere('permission_sid = :permissionSid', { permissionSid })
        .execute()
    )
  );
}

export async function insertTeam(
  manager: EntityManager,
  parentTeam: string,
  teams: Pick<TeamEntity, 'name'>[]
): Promise<void> {
  const { sid: parentTeamSid } = await findOneTeamSid(manager, parentTeam);

  await manager
    .createQueryBuilder()
    .insert()
    .into(TeamEntity, ['name', 'team'])
    .values(
      teams.map(({ name }) => ({
        name,
        team: parentTeamSid
      }))
    )
    .returning('')
    .execute();
}

export async function insertTeamAndType(
  manager: EntityManager,
  parentTeam: string,
  teams: Pick<TeamEntity, 'name' | 'type'>[]
): Promise<void> {
  const { sid: parentTeamSid } = await findOneTeamSid(manager, parentTeam);

  await manager
    .createQueryBuilder()
    .insert()
    .into(TeamEntity, ['name', 'parentTeam', 'type'])
    .values(
      teams.map(({ name, type }) => ({
        name,
        parentTeam: parentTeamSid,
        type
      }))
    )
    .returning('')
    .execute();
}

export async function removeTeam(
  manager: EntityManager,
  parentTeam: string,
  teams: Pick<TeamEntity, 'name'>[]
): Promise<void> {
  const { sid: parentTeamSid } = await findOneTeamSid(manager, parentTeam);

  await Promise.all(
    teams.map(({ name }) =>
      manager
        .createQueryBuilder()
        .delete()
        .from(TeamEntity)
        .where('team = :team', { team: parentTeamSid })
        .andWhere('name = :name', { name })
        .execute()
    )
  );
}

export async function addSuggestedRoleForTeamType(
  manager: EntityManager,
  teamType: TeamType,
  roleNames: Role[]
) {
  const roles = await Promise.all(
    roleNames.map(roleName => findOneRole(manager, roleName))
  );

  const entities = roles.map<TeamSuggestedRoleEntity>(
    role => new TeamSuggestedRoleEntity({ role: role.sid, teamType })
  );

  await manager.insert(TeamSuggestedRoleEntity, entities);
}
