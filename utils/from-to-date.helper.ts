import { DateTime } from 'luxon';

export interface FromToDateInterface {
  from: string;
  to: string;
}

export const fromToDate = (startPoint: string): FromToDateInterface => ({
  from: DateTime.fromISO(startPoint, { zone: 'utc' })
    .startOf('month')
    .startOf('week')
    .toISO(),
  to: DateTime.fromISO(startPoint, { zone: 'utc' })
    .endOf('month')
    .endOf('week')
    .toISO()
});
