export const isScheduleProcess = () => process.env.SCHEDULE_PROCESS || false;
export const isRabbitMQDisabled = () => process.env.RABBITMQ_DISABLE || false;
