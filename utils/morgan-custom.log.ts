import { IncomingMessage, ServerResponse } from 'http';

import morgan from 'morgan';

morgan.token('pid', () => `${process.pid}`);

export const morganToJson = (
  tokens: morgan.TokenIndexer,
  req: IncomingMessage,
  res: ServerResponse
) =>
  JSON.stringify({
    'content-length': tokens['res'](req, res, 'content-length'),
    'http-version': tokens['http-version'](req, res),
    method: tokens['method'](req, res),
    pid: tokens['pid'](req, res),
    referrer: tokens['referrer'](req, res),
    'remote-address': tokens['remote-addr'](req, res),
    'status-code': tokens['status'](req, res),
    time: tokens['date'](req, res, 'iso'),
    url: tokens['url'](req, res),
    'user-agent': tokens['user-agent'](req, res)
  });
