import { DateTime } from 'luxon';
import { range } from 'remeda';

export class SplitDateOnRange {
  private startRangeDate: DateTime;
  private stopRangeDate: DateTime;

  constructor(fromDate: string, toDate: string) {
    this.startRangeDate = DateTime.fromISO(fromDate, { zone: 'utc' });
    this.stopRangeDate = DateTime.fromISO(toDate, { zone: 'utc' }).endOf(
      'week'
    );
  }

  getDiff() {
    return Number(
      this.stopRangeDate
        .diff(this.startRangeDate, 'weeks')
        .as('weeks')
        .toFixed()
    );
  }

  days(fromWeekDay: DateTime) {
    return range(0, 7).map(i => {
      const weekDay = fromWeekDay.plus({ days: i }).startOf('day');

      return { date: weekDay.toUTC().toISO() };
    });
  }

  private getStatWeekDayIntoWeek(week: number): DateTime {
    return this.startRangeDate
      .plus({ weeks: week })
      .startOf('week')
      .startOf('day');
  }

  private getEndWeekDayIntoWeek(week: number): DateTime {
    return this.startRangeDate
      .plus({ weeks: week })
      .endOf('week')
      .startOf('day');
  }

  weeks(diffRange: number) {
    return range(0, diffRange).map(week => {
      const fromWeekDay = this.getStatWeekDayIntoWeek(week);
      const toWeekDate = this.getEndWeekDayIntoWeek(week);
      const days = this.days(fromWeekDay);

      return {
        days: [...days],
        range: {
          from: fromWeekDay.toUTC().toISO(),
          to: toWeekDate.toUTC().toISO()
        }
      };
    });
  }

  public static fromState(fromDate: string, toDate: string) {
    const inst = new this(fromDate, toDate);
    const diff = inst.getDiff();

    return inst.weeks(diff);
  }
}
