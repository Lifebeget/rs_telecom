import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateNewTablesForPromoter1629062143560
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE shops.promoter_schedule_state_status AS ENUM ('Draft', 'Confirm', 'Done')`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'from date',
            isNullable: false,
            name: 'from',
            type: 'timestamp with time zone'
          },
          {
            comment: 'to date',
            isNullable: false,
            name: 'to',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Promoter Schedule State status',
            isNullable: false,
            name: 'status',
            type: 'shops.promoter_schedule_state_status'
          },
          {
            comment: 'Datetime created',
            default: 'now()',
            isNullable: true,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.promoter_schedule_state'
      })
    );

    await queryRunner.query(
      `CREATE TYPE shops.promoter_schedule_status AS ENUM ('Available', 'Cancel', 'Pending')`
    );
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Promoter Schedule state SID',
            isNullable: false,
            name: 'promoter_schedule_state_sid',
            type: 'uuid'
          },
          {
            comment: 'Time block sid',
            default: null,
            isNullable: true,
            name: 'time_block_sid',
            type: 'uuid'
          },
          {
            comment: 'Promoter status',
            isNullable: false,
            name: 'promoter_status',
            type: 'shops.promoter_schedule_status'
          },
          {
            comment: 'Member sid',
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'Date and time of the working day',
            isNullable: false,
            name: 'worked_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Archive flag',
            default: false,
            isNullable: true,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'Date and time of the archive flag',
            isNullable: true,
            name: 'is_archived_flg_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Datetime created',
            default: 'now()',
            isNullable: true,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.promoter_schedule'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.promoter_schedule_state');
    await queryRunner.dropTable('shops.promoter_schedule');
  }
}
