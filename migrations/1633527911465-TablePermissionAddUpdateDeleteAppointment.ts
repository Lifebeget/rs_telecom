import { keys } from 'ramda';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission, PermissionMap } from 'Constants';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['addAppointment']
  | PermissionMap['updateAppointment']
  | PermissionMap['deleteAppointment']
>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.addAppointment]: 'add appointment',
  [Permission.updateAppointment]: 'update appointment',
  [Permission.deleteAppointment]: 'delete appointment'
};

const roles = [
  'supervisor system',
  'supervisor root',
  'supervisor regular',
  'callcenter teamlead',
  'qc teamlead'
];

export class TablePermissionAddUpdateDelete1633527911465
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }
}
