import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableClientXContract1610362899368
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'clients.client_x_contract'
      })
    );

    await queryRunner.createPrimaryKey('clients.client_x_contract', [
      'client_sid',
      'contract_sid'
    ]);

    await queryRunner.createForeignKeys('clients.client_x_contract', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),

      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "clients"."client_x_contract" IS 'list clients and contracts';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clients.client_x_contract');
  }
}
