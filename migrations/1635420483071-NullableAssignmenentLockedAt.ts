import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class NullableAssignmenentLockedAt1635420483071
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'contracts.contract',
      'is_assignment_locked_at',
      new TableColumn({
        comment: 'Assignment lock date',
        default: null,
        isNullable: true,
        name: 'is_assignment_locked_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
