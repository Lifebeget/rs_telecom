import { MigrationInterface, QueryRunner } from 'typeorm';

export class TableTeamDropCanHaveLeadFlgColumn1643806917624
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'can_have_lead_flg');
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
