import { MigrationInterface, QueryRunner } from 'typeorm';

import {
  // addSuggestedRole,
  // dropSuggestedRole,
  insertTeam,
  removeTeam
} from 'Server/utils';

export class AddTeamViaSales1625815047264 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertTeam(queryRunner.manager, 'Organization', [
      {
        name: 'Via Sales'
      }
    ]);

    // old role inserts were removed from here
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removeTeam(queryRunner.manager, 'Organization', [
      {
        name: 'Via Sales'
      }
    ]);

    // old role inserts were removed from here
  }
}
