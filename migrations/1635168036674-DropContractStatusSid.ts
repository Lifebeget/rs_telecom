import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropContractStatusSid1635168036674 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'status_sid');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'status_sid');
  }
}
