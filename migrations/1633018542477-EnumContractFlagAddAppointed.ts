import { MigrationInterface, QueryRunner } from 'typeorm';

const enumTypeName = 'contracts.contract_flag_enum';

export class EnumContractFlagAddAppointed1633018542477
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TYPE ${enumTypeName} ADD VALUE 'Appointed'`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Not revertible');
  }
}
