import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class RemovePermissionToSendFpEsignFromSupervisors1658901685550
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.fingerprintEdit
    ]);

    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.fingerprintEdit
    ]);

    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.fingerprintEdit
    ]);

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.fingerprintESignEdit
    ]);

    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.fingerprintESignEdit
    ]);

    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.fingerprintESignEdit
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.fingerprintEdit
    ]);

    await insertPermission(queryRunner.manager, 'supervisor system', [
      Permission.fingerprintEdit
    ]);

    await insertPermission(queryRunner.manager, 'supervisor regular', [
      Permission.fingerprintEdit
    ]);

    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.fingerprintESignEdit
    ]);

    await insertPermission(queryRunner.manager, 'supervisor system', [
      Permission.fingerprintESignEdit
    ]);

    await insertPermission(queryRunner.manager, 'supervisor regular', [
      Permission.fingerprintESignEdit
    ]);
  }
}
