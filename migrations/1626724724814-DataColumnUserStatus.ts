import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class DataColumnWorkingDay1626724724814 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.working_days',
      new TableColumn({
        comment: 'User data information',
        isNullable: true,
        name: 'user_data',
        type: 'jsonb'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user_status', 'user_data');
  }
}
