import {
  EntityManager,
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { ChecklistItemState } from 'Constants';
import { ContractChecklistTemplateEntity } from 'Server/modules/contract/entities';
import { findOneContractChecklistStatus } from 'Server/utils';

export class AlterTableContractCheckListTemplate1611999448401
  implements MigrationInterface {
  private async insertInto(
    manager: EntityManager,
    {
      index,
      question,
      comment,
      status
    }: {
      index: number;
      question: string;
      comment?: string;
      status: ChecklistItemState;
    }
  ) {
    const contractChecklistStatusEntity = await findOneContractChecklistStatus(
      manager,
      status
    );

    await manager
      .createQueryBuilder()
      .insert()
      .into(ContractChecklistTemplateEntity, [
        'index',
        'question',
        'comment',
        'status'
      ])
      .values({
        comment,
        index,
        question,
        status: contractChecklistStatusEntity.sid
      })
      .execute();
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'sort index',
            isNullable: false,
            isUnique: true,
            name: 'index',
            type: 'integer'
          },
          {
            comment: 'question text',
            isNullable: false,
            name: 'question',
            type: 'text'
          },
          {
            comment: 'comment text',
            default: null,
            isNullable: true,
            name: 'comment',
            type: 'text'
          },
          {
            isNullable: false,
            name: 'check_list_status_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_checklist_template'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_checklist_template',
      new TableForeignKey({
        columnNames: ['check_list_status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_checklist_status'
      })
    );

    await this.insertInto(queryRunner.manager, {
      index: 1,
      question: 'vorgestellt als Vodafone Vertriebspartner',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 2,
      question: 'bedankt für die Bestellung',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 3,
      question: 'Genehmigung zum Datenabgleich eingeholt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 4,
      question: 'KD-Name geprüft',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 5,
      question: 'Kennwort bei nicht VI gefragt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 6,
      question: 'Anschrift bzw. Lieferadresse geprüft',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 7,
      question: 'Iban eingeholt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 8,
      question: 'bei VVL mit TW -> Alt-Tarif erwähnt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 9,
      question:
        'bei VVL mit TW -> die Bestandteile des neuen Tarifes aufgezählt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 10,
      question: 'bei VVL ohne TW -> Tarifname genannt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 11,
      question:
        'bei VVL ohne TW -> Übernahme der zeitlich nicht limitierte Rabatte erwähnt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 12,
      question: 'GG gegannt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 13,
      question:
        'bei Prämie GS -> GS statt eines vergünstigten Telefons erwähnt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 14,
      question: 'bei Prämie HW -> Hersteller, Modell, Farbe, Preis genannt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 15,
      question:
        'bei Prämie HW -> Lieferung per Nachnahme mit der DHL und Barzahlung an dern Boten erwähnt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 16,
      question:
        'Liefer - bzw. Überweisungsdauer ca. 14 Tage nach Aktivierung erwähnt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 17,
      question: 'Liefer - bzw. Überweisung erfolgt durch den Logistikpartner',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 18,
      question: 'Einverstädnis zur VVL eingeholt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 19,
      question: 'Unterschriftsprozess erklärt',
      status: ChecklistItemState.Undefined
    });

    await this.insertInto(queryRunner.manager, {
      index: 20,
      question: 'Live-Unterschrift eingeholt',
      status: ChecklistItemState.Undefined
    });

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_checklist_template" IS 'list of contract check template';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_checklist_template');
  }
}
