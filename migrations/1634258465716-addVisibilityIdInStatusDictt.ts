import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class addVisibilityIdInStatusDictt1634258465716
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.dict_contract_status',
      new TableColumn({
        comment: 'Visibility ID (VisibilityID in contract visibility file)',
        isNullable: true,
        name: 'visibility_id',
        type: 'int'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.dict_contract_status',
      new TableColumn({
        comment: 'Visibility ID (VisibilityID in contract visibility file)',
        isNullable: true,
        name: 'visibility_id',
        type: 'int'
      })
    );
  }
}
