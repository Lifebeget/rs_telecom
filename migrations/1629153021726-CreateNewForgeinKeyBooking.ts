import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class CreateNewForgeinKeyBooking1629153021726
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createForeignKeys('shops.promoter_schedule', [
      new TableForeignKey({
        columnNames: ['promoter_schedule_state_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.promoter_schedule_state'
      }),
      new TableForeignKey({
        columnNames: ['time_block_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_time_block'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.createForeignKeys('shops.shop_schedule_state', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      })
    ]);

    await queryRunner.createForeignKeys('shops.shop_day', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      })
    ]);

    await queryRunner.createForeignKeys('shops.shop_schedule', [
      new TableForeignKey({
        columnNames: ['promoter_schedule_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.promoter_schedule'
      }),
      new TableForeignKey({
        columnNames: ['shop_schedule_state_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_schedule_state'
      }),
      new TableForeignKey({
        columnNames: ['day_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_day'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('shops.promoter_schedule', [
      new TableForeignKey({
        columnNames: ['promoter_schedule_state_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.promoter_schedule_state'
      }),
      new TableForeignKey({
        columnNames: ['time_block_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_time_block'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.dropForeignKeys('shops.shop_schedule_state', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      })
    ]);

    await queryRunner.dropForeignKeys('shops.shop_day', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      })
    ]);

    await queryRunner.dropForeignKeys('shops.shop_schedule', [
      new TableForeignKey({
        columnNames: ['promoter_schedule_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.promoter_schedule'
      }),
      new TableForeignKey({
        columnNames: ['shop_schedule_state_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_schedule_state'
      }),
      new TableForeignKey({
        columnNames: ['day_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_day'
      })
    ]);
  }
}
