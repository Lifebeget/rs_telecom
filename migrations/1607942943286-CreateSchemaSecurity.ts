import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaSecurity1607942943286 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('security');
    await queryRunner.query(
      `COMMENT ON SCHEMA "security" IS 'all security objects (roles, permissions)';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('security');
  }
}
