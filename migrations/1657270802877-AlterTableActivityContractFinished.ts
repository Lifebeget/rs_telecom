import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractFinished1657270802877
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            name: 'finish_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'activities.activity_contract_finished'
      })
    );

    await queryRunner.createForeignKeys(
      'activities.activity_contract_finished',
      [
        new TableForeignKey({
          columnNames: ['activity_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'activities.activity'
        })
      ]
    );
    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_finished" IS 'activity contract finished';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activity.activity_contract_finished');
  }
}
