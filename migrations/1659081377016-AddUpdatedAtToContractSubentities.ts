import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const column = new TableColumn({
  comment: 'update date',
  default: 'now()',
  isNullable: false,
  name: 'updated_at',
  type: 'timestamp with time zone'
});

export class AddUpdatedAtToContractSubentities1659081377016
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('contracts.contract_phone_number', column);
    await queryRunner.query(`
    UPDATE 
      contracts.contract_phone_number as t
    SET
      updated_at = t.created_at`);

    await queryRunner.addColumn('contracts.contract_next_best_action', column);
    await queryRunner.query(`
    UPDATE 
      contracts.contract_next_best_action as t
    SET
      updated_at = t.created_at`);

    await queryRunner.addColumn('contracts.contract_note', column);
    await queryRunner.query(`
    UPDATE 
      contracts.contract_note as t
    SET
      updated_at = t.created_at`);

    await queryRunner.addColumn(
      'contracts.contract_subsidy_eligibility',
      column
    );
    await queryRunner.query(`
      UPDATE 
      contracts.contract_subsidy_eligibility as t
      SET
        updated_at = t.created_at`);

    await queryRunner.addColumn('contracts.contract_address_mail', column);
    await queryRunner.query(`
      UPDATE 
      contracts.contract_address_mail as t
      SET
        updated_at = t.created_at`);

    await queryRunner.addColumn('contracts.contract_address_invoice', column);
    await queryRunner.query(`
      UPDATE 
      contracts.contract_address_invoice as t
      SET
        updated_at = t.created_at`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is not revertable');
  }
}
