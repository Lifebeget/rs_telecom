import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientPersonEvaluation1625191219422
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'person evaluation',
        default: null,
        isNullable: true,
        name: 'person_evaluation',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'person_evaluation');
  }
}
