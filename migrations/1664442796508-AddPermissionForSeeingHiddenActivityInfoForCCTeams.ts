import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  PermissionMap['canSeeHiddenActivityInfo']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.canSeeHiddenActivityInfo]: 'can see hidden activity info'
};

export class AddPermissionForSeeingHiddenActivityInfoForCCTeams1664442796508
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.canSeeHiddenActivityInfo
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.canSeeHiddenActivityInfo
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.canSeeHiddenActivityInfo
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.canSeeHiddenActivityInfo
    ]);
  }
}
