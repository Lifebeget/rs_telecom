import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientBanPassword1625191211158 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'ban password',
        default: null,
        isNullable: true,
        name: 'ban_password',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'ban_password');
  }
}
