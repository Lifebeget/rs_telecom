import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractMoved1614247903173
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'from_team_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'to_team_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_contract_moved'
      })
    );

    await queryRunner.createForeignKeys('activities.activity_contract_moved', [
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      }),
      new TableForeignKey({
        columnNames: ['from_team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['to_team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_moved" IS 'activity contract moved';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_moved');
  }
}
