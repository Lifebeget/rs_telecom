import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddNewColumnContractIsMainCard1664880549523
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        default: false,
        isNullable: true,
        name: 'is_main',
        type: 'boolean'
      })
    );

    await queryRunner.query(`
      WITH partnerContract AS (
        SELECT DISTINCT c.main_contract_sid 
          FROM contracts.contract AS c
            WHERE c.main_contract_sid IS NOT NULL)
      UPDATE contracts.contract 
        SET is_main = TRUE
        FROM partnerContract
        WHERE sid = partnerContract.main_contract_sid;
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract',
      new TableColumn({
        default: false,
        isNullable: false,
        name: 'is_main',
        type: 'boolean'
      })
    );
  }
}
