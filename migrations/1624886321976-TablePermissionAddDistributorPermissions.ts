import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

type PermissionKeys = Extract<Permission, PermissionMap['viewDistributor']>;

const permission: Record<PermissionKeys, string> = {
  [Permission.viewDistributor]: 'get distributor'
};

export class TablePermissionAddDistributorPermissions1624886321976
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();
  }
}
