import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';
import { EnumValues } from 'enum-values';

import { ScheduleStatus, ScheduleType } from 'Constants';

export class AlterTableShopSchedule1626681144633 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'time_block_sid',
            type: 'uuid'
          },
          {
            default: null,
            enum: EnumValues.getValues(ScheduleStatus),
            isNullable: true,
            name: 'schedule_status',
            type: 'enum'
          },
          {
            enum: EnumValues.getValues(ScheduleType),
            isNullable: false,
            name: 'schedule_type',
            type: 'enum'
          },
          {
            comment: 'week range',
            isNullable: false,
            name: 'week_range',
            type: 'jsonb'
          },
          {
            comment: 'is canceled schedule',
            default: false,
            isNullable: false,
            name: 'is_canceled_flg',
            type: 'boolean'
          },
          {
            comment: 'canceled date',
            default: null,
            isNullable: true,
            name: 'is_canceled_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'is archived schedule',
            default: false,
            isNullable: false,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'archived date',
            default: null,
            isNullable: true,
            name: 'is_archived_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_schedule'
      })
    );

    await queryRunner.createForeignKeys('shops.shop_schedule', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),
      new TableForeignKey({
        columnNames: ['time_block_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop_time_block'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "shops"."shop_schedule" IS 'shop schedule';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop_schedule');
  }
}
