import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaShops1625796000820 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('shops');
    await queryRunner.query(`COMMENT ON SCHEMA "shops" IS 'all shop objects';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('shops');
  }
}
