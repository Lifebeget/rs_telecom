import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableLead1612014271493 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'phone number',
            isNullable: false,
            isUnique: true,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'name',
            default: null,
            isNullable: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'password',
            default: null,
            isNullable: true,
            name: 'password',
            type: 'character varying(255)'
          },
          {
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'carrier',
            default: null,
            isNullable: true,
            name: 'carrier',
            type: 'character varying(255)'
          },
          {
            default: null,
            isNullable: true,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'leads.lead'
      })
    );

    await queryRunner.createForeignKeys('leads.lead', [
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.dict_lead_status'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),
      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      })
    ]);

    await queryRunner.query(`COMMENT ON TABLE "leads"."lead" IS 'lead';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.lead');
  }
}
