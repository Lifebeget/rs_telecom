import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTeamXRole1608016364312 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'role_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.team_x_role'
      })
    );

    await queryRunner.createPrimaryKey('teams.team_x_role', [
      'team_sid',
      'role_sid'
    ]);

    await queryRunner.createForeignKeys('teams.team_x_role', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),

      new TableForeignKey({
        columnNames: ['role_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.role'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "teams"."team_x_role" IS 'list of teams and roles';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.team_x_role');
  }
}
