import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAssignmentCreatedAt1648030839409
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      UPDATE contracts.assignment
      SET created_at = now()
      WHERE created_at is null;
    `);

    await queryRunner.changeColumn(
      'contracts.assignment',
      'created_at',
      new TableColumn({
        comment: 'create date',
        default: 'now()',
        isNullable: false,
        name: 'created_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'contracts.assignment',
      'created_at',
      new TableColumn({
        comment: 'create date',
        default: null,
        isNullable: true,
        name: 'created_at',
        type: 'timestamp with time zone'
      })
    );
  }
}
