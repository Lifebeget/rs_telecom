import { MigrationInterface, QueryRunner } from 'typeorm';

const tableName = 'contracts.contract_extend_request';

export class TableExtendRequestRemoveBankDataUniqueConstraint1635748930421
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      tableName,
      'UQ_72f323640319b1c10af64648412'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('not revertable');
  }
}
