import { MigrationInterface, QueryRunner, TableUnique } from 'typeorm';

export class AddUniqueConstraintOnTeamContractSort1659333154198
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createUniqueConstraint(
      'teams.team_x_contract_sort',
      new TableUnique({
        columnNames: ['team_sid', 'sort_sid']
      })
    );

    await queryRunner.createUniqueConstraint(
      'teams.team_x_contract_sort',
      new TableUnique({
        columnNames: ['team_sid', 'index']
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is irrevertable');
  }
}
