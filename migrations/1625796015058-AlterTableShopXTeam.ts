import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableShopXTeam1625796015058 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_x_team'
      })
    );

    await queryRunner.createPrimaryKey('shops.shop_x_team', [
      'shop_sid',
      'team_sid'
    ]);

    await queryRunner.createForeignKeys('shops.shop_x_team', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "shops"."shop_x_team" IS 'list shop and team';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop_x_team');
  }
}
