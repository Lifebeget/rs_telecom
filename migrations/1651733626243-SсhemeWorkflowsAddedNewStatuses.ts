import { MigrationInterface, QueryRunner } from 'typeorm';

export class SсhemeWorkflowsAddedNewStatuses1651733626243
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const manager = queryRunner.manager;

    const addedTypes = `INSERT INTO workflows.status_type (sid, name, is_deleted, created_at, deleted_at) VALUES ('ad347db9-882f-4b52-8889-1ca18ac34b78', 'System', false, now(), null);
    INSERT INTO workflows.status_type (sid, name, is_deleted, created_at, deleted_at) VALUES ('38d9ba4d-3490-472b-a160-dcbc49a0609e', 'Behavioral', false, now(), null);
    INSERT INTO workflows.status_type (sid, name, is_deleted, created_at, deleted_at) VALUES ('76978ab9-670a-4164-83aa-de1261a036d7', 'Additional', false, now(), null);
    `;

    const systemType = 'ad347db9-882f-4b52-8889-1ca18ac34b78';
    const behavioralType = '38d9ba4d-3490-472b-a160-dcbc49a0609e';
    const additionalType = '76978ab9-670a-4164-83aa-de1261a036d7';

    await manager.query(addedTypes);

    const addStatuses = `INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('ecde3764-0b5e-466c-921b-f8c57d537f78', 'QCConfirmation', 'Contract extending has been requested', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('b707b055-ac33-4fe0-bcc4-1deb59bdb996', 'CCClarification', 'Contract clarification has been requested', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('56f88599-6a40-4a25-bdad-65a8d57911fb', 'QCConfirmed', 'Contract extending has been confirmed', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('f0048478-fedc-4a90-afea-c87fcfeed48a', 'Idle', 'Contract Idle status', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('322ad388-dc9e-4de9-a697-2179392e8ea3', 'CCClarificationCancelled', 'Contract CCClarificationCancelled status',  now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('4b109555-d3f0-473b-bcee-75383499b288', 'NotInterested', 'Contract NotInterested status',  now(), '${behavioralType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('e092c260-e7e7-4cf8-908b-2f8f326277bf', 'NotReached', 'Contract NotReached status', now(), '${behavioralType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('1fa2a7e2-55f0-478b-beef-7a3dd98b17b7', 'QCConfirmationCancelled', 'Contract QCConfirmationCancelled status', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('0a84888f-134d-4755-81ee-5d84e52d164d', 'NotSigned', 'Contract NotReached status', now(), '${additionalType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('a9ed424b-2736-4e4b-a39a-da79bfae7aaf', 'WaitESign', 'Contract awaiting e-sign', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('b0047ed0-82e7-4fb9-ac3b-0d6c49a9ca72', 'QCTimeout', 'Contract QC Timeout', now(), '${systemType}');

    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('782db42c-7b6c-4173-acab-5f5b3bf89996', 'Activated', 'Setting the status with the activation command: Activated', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('5368976e-2a1e-439d-844a-e4a65abf986f', 'ActivationCancelled', 'Setting the status with the activation command: ActivationCancelled', now(), '${systemType}');
    INSERT INTO workflows.contract_status (sid, name, description, created_at, type_sid) VALUES ('12233e8d-f769-475f-9a69-32b987de5512', 'ActivationClarification', 'Setting the status with the activation command: ActivationClarification', now(), '${systemType}');
    `;

    await manager.query(addStatuses);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
