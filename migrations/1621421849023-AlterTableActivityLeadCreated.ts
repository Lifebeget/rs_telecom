import { EnumValues } from 'enum-values';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { LeadCreationMethod } from 'Constants/LeadCreationMethod';

export class AlterTableActivityLeadCreated1621421849023
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'creation method',
            enum: EnumValues.getValues(LeadCreationMethod),
            isNullable: false,
            name: 'method',
            type: 'enum'
          }
        ],
        name: 'activities.activity_lead_created'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_lead_created',
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_lead_created" IS 'activity lead created';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_lead_created');
  }
}
