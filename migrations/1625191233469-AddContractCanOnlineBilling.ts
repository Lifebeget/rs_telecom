import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractCanOnlineBilling1625191233469
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'contract can online billing',
        default: null,
        isNullable: true,
        name: 'can_online_billing',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'can_online_billing');
  }
}
