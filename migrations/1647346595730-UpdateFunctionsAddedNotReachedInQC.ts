import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateFunctionsAddedNotReachedInQC1647346595730
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    CREATE OR REPLACE function getcontract_one(member uuid, team_type text, teamSid uuid, isTeamLead boolean,
      contract_sid_in uuid)
RETURNS table
(
contract_sid_res uuid,
status_sid       uuid,
status_type      text
)
as
$$
DECLARE
DEFAULT_STATUS_SID uuid;
BEGIN
SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;


--Простой CC
IF team_type = 'CC' AND isTeamLead = false
--Видит только свои статусы
THEN
return query
SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                       (CASE
                            WHEN ccsl.status_sid IS NULL
                                THEN DEFAULT_STATUS_SID
                            ELSE ccsl.status_sid END)  as status_sid,
                       (CASE
                            WHEN ccsl.status_type IS NULL THEN 'System'
                            ELSE ccsl.status_type END) as status_type

FROM contracts.contract as ccontract
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
               first_value(ccsl.status_sid)
               over (
                   PARTITION BY (CASE
                                     WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                         THEN 'Behavioral'
                                     WHEN cdcs.visibility_id = 8
                                         THEN 'Additional'
                                     ELSE 'System' END)
                   order by ccsl.created_at DESC
                   RANGE BETWEEN
                       UNBOUNDED PRECEDING AND
                       UNBOUNDED FOLLOWING
                   )                 as status_sid,
               CASE
                   WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                       THEN 'Behavioral'
                   ELSE 'System' END as status_type
   FROM contracts.contract_status_log as ccsl
            INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
   WHERE (cdcs.visibility_id = 0
       OR cdcs.visibility_id = 1
       OR cdcs.visibility_id = 2
       OR cdcs.visibility_id = 3
       OR cdcs.visibility_id = 4
       OR cdcs.visibility_id = 5
       OR cdcs.visibility_id = 6
       OR cdcs.visibility_id = 8
       OR cdcs.visibility_id = 9
       OR cdcs.visibility_id = 10
       OR cdcs.visibility_id = 11
       OR cdcs.visibility_id = 12)
     AND ccsl.contract_sid = ccontract.sid
     AND ccsl.member_sid = member
) as ccsl ON TRUE
WHERE ccontract.sid = contract_sid_in;

end if;

--CC Сложный (тимлид)
IF team_type = 'CC' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract

LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                 as status_sid,
                        CASE
                            WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                THEN 'Behavioral'
                            ELSE 'System' END as status_type
            FROM contracts.contract_status_log as ccsl
                     INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 0
                OR cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 6
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid
              AND ccsl.team_sid = teamSid
) as ccsl ON TRUE
WHERE ccontract.sid = contract_sid_in;
end if;

--QC Простой
IF team_type = 'QC' AND isTeamLead = false
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 7
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid
AND ccontract.sid = contract_sid_in;
end if;

--QC СЛОЖНЫЙ
IF team_type = 'QC' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 7
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccontract.sid = contract_sid_in;
end if;

IF team_type = 'Organization'
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                  as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END) as status_sid,
                              (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 0
                OR cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 6
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccontract.sid = contract_sid_in;
end if;


--Activation Простой
IF team_type = 'Activation' AND isTeamLead = false
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid
AND ccontract.sid = contract_sid_in;
end if;

--Activation СЛОЖНЫЙ
IF team_type = 'Activation' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid
              AND ccsl.team_sid = teamSid) as ccsl
   ON TRUE
WHERE ccontract.sid = contract_sid_in;
end if;


END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE function getcontracts(member uuid, team_type text, teamSid uuid, isTeamLead boolean)
RETURNS table
(
contract_sid_res uuid,
status_sid       uuid,
status_type      text
)
as
$$
DECLARE

DEFAULT_STATUS_SID uuid;
BEGIN
SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;


--Простой CC
IF team_type = 'CC' AND isTeamLead = false
--Видит только свои статусы
THEN
return query
SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                       (CASE
                            WHEN ccsl.status_sid IS NULL
                                THEN DEFAULT_STATUS_SID
                            ELSE ccsl.status_sid END)  as status_sid,
                       (CASE
                            WHEN ccsl.status_type IS NULL THEN 'System'
                            ELSE ccsl.status_type END) as status_type

FROM contracts.contract as ccontract
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
               first_value(ccsl.status_sid)
               over (
                   PARTITION BY (CASE
                                     WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                         THEN 'Behavioral'
                                     WHEN cdcs.visibility_id = 8
                                         THEN 'Additional'
                                     ELSE 'System' END)
                   order by ccsl.created_at DESC
                   RANGE BETWEEN
                       UNBOUNDED PRECEDING AND
                       UNBOUNDED FOLLOWING
                   )                 as status_sid,
               CASE
                   WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                       THEN 'Behavioral'
                   ELSE 'System' END as status_type
   FROM contracts.contract_status_log as ccsl
            INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
   WHERE (cdcs.visibility_id = 0
       OR cdcs.visibility_id = 1
       OR cdcs.visibility_id = 2
       OR cdcs.visibility_id = 3
       OR cdcs.visibility_id = 4
       OR cdcs.visibility_id = 5
       OR cdcs.visibility_id = 6
       OR cdcs.visibility_id = 8
       OR cdcs.visibility_id = 9
       OR cdcs.visibility_id = 10
       OR cdcs.visibility_id = 11
       OR cdcs.visibility_id = 12)
     AND ccsl.contract_sid = ccontract.sid
     AND ccsl.member_sid = member
) as ccsl ON TRUE;
end if;

--CC Сложный (тимлид)
IF team_type = 'CC' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract

LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                 as status_sid,
                        CASE
                            WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                THEN 'Behavioral'
                            ELSE 'System' END as status_type
            FROM contracts.contract_status_log as ccsl
                     INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 0
                OR cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 6
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid
              AND ccsl.team_sid = teamSid
) as ccsl ON TRUE;
end if;

--QC Простой
IF team_type = 'QC' AND isTeamLead = false
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (
                        cdcs.visibility_id = 1
                    OR cdcs.visibility_id = 2
                    OR cdcs.visibility_id = 3
                    OR cdcs.visibility_id = 4
                    OR cdcs.visibility_id = 5
                    OR cdcs.visibility_id = 7
                    OR cdcs.visibility_id = 8
                    OR cdcs.visibility_id = 9
                    OR cdcs.visibility_id = 10
                    OR cdcs.visibility_id = 11
                    OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid;
end if;

--QC СЛОЖНЫЙ
IF team_type = 'QC' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 7
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.team_sid = teamSid
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid
AND ccxt.team_sid IS NOT NULL;
end if;

IF team_type = 'Organization'
--Видит только свои статусы
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 6
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE;
end if;

--Activation Простой
IF team_type = 'Activation' AND isTeamLead = false
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid;
end if;

--Activation СЛОЖНЫЙ
IF team_type = 'Activation' AND isTeamLead = true
--Видит только свои статусы
THEN
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
LEFT JOIN LATERAL
(SELECT DISTINCT ON ( status_type, ccsl.contract_sid) first_value(ccsl.status_sid)
                                          over (
                                              PARTITION BY (CASE
                                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                    THEN 'Behavioral'
                                                                WHEN cdcs.visibility_id = 8
                                                                    THEN 'Additional'
                                                                ELSE 'System' END)
                                              order by ccsl.created_at DESC
                                              RANGE BETWEEN
                                                  UNBOUNDED PRECEDING AND
                                                  UNBOUNDED FOLLOWING
                                              )                   as status_sid,
                                          (CASE
                                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                   THEN 'Behavioral'
                                               WHEN cdcs.visibility_id = 8
                                                   THEN 'Additional'
                                               ELSE 'System' END) as status_type
FROM contracts.contract_status_log as ccsl
LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
WHERE (cdcs.visibility_id = 2
OR cdcs.visibility_id = 3
OR cdcs.visibility_id = 4
OR cdcs.visibility_id = 5
OR cdcs.visibility_id = 10
OR cdcs.visibility_id = 11
OR cdcs.visibility_id = 12)
AND ccsl.team_sid = teamSid
AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON TRUE
WHERE ccxt.team_sid = teamSid
AND ccxt.team_sid IS NOT NULL;
end if;
END;
$$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS getcontracts_by_team(teamSids text, teamType varchar);

CREATE OR REPLACE function getcontracts_by_team(teamSids text, teamType varchar)
RETURNS table
(
contract_sid_res uuid,
status_sid       uuid,
status_type      text
)
as
$$
DECLARE
DEFAULT_STATUS_SID uuid;
team_sids_arr      uuid[];
cnt_team           int;
isQC               boolean;
BEGIN
SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
team_sids_arr = string_to_array(teamSids, ',');
cnt_team = array_length(team_sids_arr, 1);
isQC = false;

if cnt_team = 1
THEN
IF (SELECT COUNT(*)
FROM teams.team as tt
WHERE tt.sid = ANY (team_sids_arr)
AND tt.type::varchar = 'QC'
LIMIT 1) > 0
THEN
isQC = true;
end if;
end if;


IF isQC = true
THEN
return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract
LEFT JOIN contracts.contract_x_team as ccxt
   ON ccontract.sid = ccxt.contract_sid AND ccxt.team_sid = ANY (team_sids_arr)
LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type,
                        ccsl.contract_sid
            FROM contracts.contract_status_log as ccsl
                     LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 7
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid) as ccsl
   ON ccsl.contract_sid = ccontract.sid AND
      ccsl.status_type IS NOT NULL
WHERE ccxt.team_sid = ANY (team_sids_arr);
ELSE
return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                (CASE
                                     WHEN ccsl.status_sid IS NULL
                                         THEN DEFAULT_STATUS_SID
                                     ELSE ccsl.status_sid END)  as status_sid,
                                (CASE
                                     WHEN ccsl.status_type IS NULL THEN 'System'
                                     ELSE ccsl.status_type END) as status_type
FROM contracts.contract as ccontract

LEFT JOIN LATERAL (SELECT DISTINCT ON ( ccsl.contract_sid, status_type)
                        first_value(ccsl.status_sid)
                        over (
                            PARTITION BY (CASE
                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                  THEN 'Behavioral'
                                              WHEN cdcs.visibility_id = 8
                                                  THEN 'Additional'
                                              ELSE 'System' END)
                            order by ccsl.created_at DESC
                            RANGE BETWEEN
                                UNBOUNDED PRECEDING AND
                                UNBOUNDED FOLLOWING
                            )                   as status_sid,
                        (CASE
                             WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                 THEN 'Behavioral'
                             WHEN cdcs.visibility_id = 8
                                 THEN 'Additional'
                             ELSE 'System' END) as status_type
            FROM contracts.contract_status_log as ccsl
                     INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
            WHERE (cdcs.visibility_id = 0
                OR cdcs.visibility_id = 1
                OR cdcs.visibility_id = 2
                OR cdcs.visibility_id = 3
                OR cdcs.visibility_id = 4
                OR cdcs.visibility_id = 5
                OR cdcs.visibility_id = 6
                OR cdcs.visibility_id = 8
                OR cdcs.visibility_id = 9
                OR cdcs.visibility_id = 10
                OR cdcs.visibility_id = 11
                OR cdcs.visibility_id = 12)
              AND ccsl.contract_sid = ccontract.sid
) as ccsl ON TRUE;
end if;


END;
$$
LANGUAGE plpgsql;
  
  `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE OR REPLACE function getcontract_one(member uuid, team_type text, teamSid uuid, isTeamLead boolean,
        contract_sid_in uuid)
  RETURNS table
  (
  contract_sid_res uuid,
  status_sid       uuid,
  status_type      text
  )
  as
  $$
  DECLARE
  DEFAULT_STATUS_SID uuid;
  BEGIN
  SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
  
  
  --Простой CC
  IF team_type = 'CC' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query
  SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                         (CASE
                              WHEN ccsl.status_sid IS NULL
                                  THEN DEFAULT_STATUS_SID
                              ELSE ccsl.status_sid END)  as status_sid,
                         (CASE
                              WHEN ccsl.status_type IS NULL THEN 'System'
                              ELSE ccsl.status_type END) as status_type
  
  FROM contracts.contract as ccontract
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                 first_value(ccsl.status_sid)
                 over (
                     PARTITION BY (CASE
                                       WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                           THEN 'Behavioral'
                                       WHEN cdcs.visibility_id = 8
                                           THEN 'Additional'
                                       ELSE 'System' END)
                     order by ccsl.created_at DESC
                     RANGE BETWEEN
                         UNBOUNDED PRECEDING AND
                         UNBOUNDED FOLLOWING
                     )                 as status_sid,
                 CASE
                     WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                         THEN 'Behavioral'
                     ELSE 'System' END as status_type
     FROM contracts.contract_status_log as ccsl
              INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
     WHERE (cdcs.visibility_id = 0
         OR cdcs.visibility_id = 1
         OR cdcs.visibility_id = 2
         OR cdcs.visibility_id = 3
         OR cdcs.visibility_id = 4
         OR cdcs.visibility_id = 5
         OR cdcs.visibility_id = 6
         OR cdcs.visibility_id = 7
         OR cdcs.visibility_id = 8
         OR cdcs.visibility_id = 9
         OR cdcs.visibility_id = 10
         OR cdcs.visibility_id = 11
         OR cdcs.visibility_id = 12)
       AND ccsl.contract_sid = ccontract.sid
       AND ccsl.member_sid = member
  ) as ccsl ON TRUE
  WHERE ccontract.sid = contract_sid_in;
  
  end if;
  
  --CC Сложный (тимлид)
  IF team_type = 'CC' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                 as status_sid,
                          CASE
                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                  THEN 'Behavioral'
                              ELSE 'System' END as status_type
              FROM contracts.contract_status_log as ccsl
                       INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 0
                  OR cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 6
                  OR cdcs.visibility_id = 7
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid
                AND ccsl.team_sid = teamSid
  ) as ccsl ON TRUE
  WHERE ccontract.sid = contract_sid_in;
  end if;
  
  --QC Простой
  IF team_type = 'QC' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid
  AND ccontract.sid = contract_sid_in;
  end if;
  
  --QC СЛОЖНЫЙ
  IF team_type = 'QC' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccontract.sid = contract_sid_in;
  end if;
  
  IF team_type = 'Organization'
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                  as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END) as status_sid,
                                (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 0
                  OR cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 6
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccontract.sid = contract_sid_in;
  end if;
  
  
  --Activation Простой
  IF team_type = 'Activation' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid
  AND ccontract.sid = contract_sid_in;
  end if;
  
  --Activation СЛОЖНЫЙ
  IF team_type = 'Activation' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid
                AND ccsl.team_sid = teamSid) as ccsl
     ON TRUE
  WHERE ccontract.sid = contract_sid_in;
  end if;
  
  
  END;
  $$
  LANGUAGE plpgsql;
  
  
  CREATE OR REPLACE function getcontracts(member uuid, team_type text, teamSid uuid, isTeamLead boolean)
  RETURNS table
  (
  contract_sid_res uuid,
  status_sid       uuid,
  status_type      text
  )
  as
  $$
  DECLARE
  
  DEFAULT_STATUS_SID uuid;
  BEGIN
  SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
  
  
  --Простой CC
  IF team_type = 'CC' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query
  SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                         (CASE
                              WHEN ccsl.status_sid IS NULL
                                  THEN DEFAULT_STATUS_SID
                              ELSE ccsl.status_sid END)  as status_sid,
                         (CASE
                              WHEN ccsl.status_type IS NULL THEN 'System'
                              ELSE ccsl.status_type END) as status_type
  
  FROM contracts.contract as ccontract
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                 first_value(ccsl.status_sid)
                 over (
                     PARTITION BY (CASE
                                       WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                           THEN 'Behavioral'
                                       WHEN cdcs.visibility_id = 8
                                           THEN 'Additional'
                                       ELSE 'System' END)
                     order by ccsl.created_at DESC
                     RANGE BETWEEN
                         UNBOUNDED PRECEDING AND
                         UNBOUNDED FOLLOWING
                     )                 as status_sid,
                 CASE
                     WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                         THEN 'Behavioral'
                     ELSE 'System' END as status_type
     FROM contracts.contract_status_log as ccsl
              INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
     WHERE (cdcs.visibility_id = 0
         OR cdcs.visibility_id = 1
         OR cdcs.visibility_id = 2
         OR cdcs.visibility_id = 3
         OR cdcs.visibility_id = 4
         OR cdcs.visibility_id = 5
         OR cdcs.visibility_id = 6
         OR cdcs.visibility_id = 7
         OR cdcs.visibility_id = 8
         OR cdcs.visibility_id = 9
         OR cdcs.visibility_id = 10
         OR cdcs.visibility_id = 11
         OR cdcs.visibility_id = 12)
       AND ccsl.contract_sid = ccontract.sid
       AND ccsl.member_sid = member
  ) as ccsl ON TRUE;
  end if;
  
  --CC Сложный (тимлид)
  IF team_type = 'CC' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                 as status_sid,
                          CASE
                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                  THEN 'Behavioral'
                              ELSE 'System' END as status_type
              FROM contracts.contract_status_log as ccsl
                       INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 0
                  OR cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 6
                  OR cdcs.visibility_id = 7
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid
                AND ccsl.team_sid = teamSid
  ) as ccsl ON TRUE;
  end if;
  
  --QC Простой
  IF team_type = 'QC' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (
                          cdcs.visibility_id = 1
                      OR cdcs.visibility_id = 2
                      OR cdcs.visibility_id = 3
                      OR cdcs.visibility_id = 4
                      OR cdcs.visibility_id = 5
                      OR cdcs.visibility_id = 8
                      OR cdcs.visibility_id = 9
                      OR cdcs.visibility_id = 10
                      OR cdcs.visibility_id = 11
                      OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid;
  end if;
  
  --QC СЛОЖНЫЙ
  IF team_type = 'QC' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.team_sid = teamSid
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid
  AND ccxt.team_sid IS NOT NULL;
  end if;
  
  IF team_type = 'Organization'
  --Видит только свои статусы
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 6
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE;
  end if;
  
  --Activation Простой
  IF team_type = 'Activation' AND isTeamLead = false
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid;
  end if;
  
  --Activation СЛОЖНЫЙ
  IF team_type = 'Activation' AND isTeamLead = true
  --Видит только свои статусы
  THEN
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt ON ccontract.sid = ccxt.contract_sid
  LEFT JOIN LATERAL
  (SELECT DISTINCT ON ( status_type, ccsl.contract_sid) first_value(ccsl.status_sid)
                                            over (
                                                PARTITION BY (CASE
                                                                  WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                      THEN 'Behavioral'
                                                                  WHEN cdcs.visibility_id = 8
                                                                      THEN 'Additional'
                                                                  ELSE 'System' END)
                                                order by ccsl.created_at DESC
                                                RANGE BETWEEN
                                                    UNBOUNDED PRECEDING AND
                                                    UNBOUNDED FOLLOWING
                                                )                   as status_sid,
                                            (CASE
                                                 WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                     THEN 'Behavioral'
                                                 WHEN cdcs.visibility_id = 8
                                                     THEN 'Additional'
                                                 ELSE 'System' END) as status_type
  FROM contracts.contract_status_log as ccsl
  LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
  WHERE (cdcs.visibility_id = 2
  OR cdcs.visibility_id = 3
  OR cdcs.visibility_id = 4
  OR cdcs.visibility_id = 5
  OR cdcs.visibility_id = 10
  OR cdcs.visibility_id = 11
  OR cdcs.visibility_id = 12)
  AND ccsl.team_sid = teamSid
  AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON TRUE
  WHERE ccxt.team_sid = teamSid
  AND ccxt.team_sid IS NOT NULL;
  end if;
  END;
  $$
  LANGUAGE plpgsql;
  
  
  DROP FUNCTION IF EXISTS getcontracts_by_team(teamSids text, teamType varchar);
  
  CREATE OR REPLACE function getcontracts_by_team(teamSids text, teamType varchar)
  RETURNS table
  (
  contract_sid_res uuid,
  status_sid       uuid,
  status_type      text
  )
  as
  $$
  DECLARE
  DEFAULT_STATUS_SID uuid;
  team_sids_arr      uuid[];
  cnt_team           int;
  isQC               boolean;
  BEGIN
  SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
  team_sids_arr = string_to_array(teamSids, ',');
  cnt_team = array_length(team_sids_arr, 1);
  isQC = false;
  
  if cnt_team = 1
  THEN
  IF (SELECT COUNT(*)
  FROM teams.team as tt
  WHERE tt.sid = ANY (team_sids_arr)
  AND tt.type::varchar = 'QC'
  LIMIT 1) > 0
  THEN
  isQC = true;
  end if;
  end if;
  
  
  IF isQC = true
  THEN
  return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  LEFT JOIN contracts.contract_x_team as ccxt
     ON ccontract.sid = ccxt.contract_sid AND ccxt.team_sid = ANY (team_sids_arr)
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type,
                          ccsl.contract_sid
              FROM contracts.contract_status_log as ccsl
                       LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid) as ccsl
     ON ccsl.contract_sid = ccontract.sid AND
        ccsl.status_type IS NOT NULL
  WHERE ccxt.team_sid = ANY (team_sids_arr);
  ELSE
  return query SELECT distinct ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                  (CASE
                                       WHEN ccsl.status_sid IS NULL
                                           THEN DEFAULT_STATUS_SID
                                       ELSE ccsl.status_sid END)  as status_sid,
                                  (CASE
                                       WHEN ccsl.status_type IS NULL THEN 'System'
                                       ELSE ccsl.status_type END) as status_type
  FROM contracts.contract as ccontract
  
  LEFT JOIN LATERAL (SELECT DISTINCT ON ( ccsl.contract_sid, status_type)
                          first_value(ccsl.status_sid)
                          over (
                              PARTITION BY (CASE
                                                WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                    THEN 'Behavioral'
                                                WHEN cdcs.visibility_id = 8
                                                    THEN 'Additional'
                                                ELSE 'System' END)
                              order by ccsl.created_at DESC
                              RANGE BETWEEN
                                  UNBOUNDED PRECEDING AND
                                  UNBOUNDED FOLLOWING
                              )                   as status_sid,
                          (CASE
                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                   THEN 'Behavioral'
                               WHEN cdcs.visibility_id = 8
                                   THEN 'Additional'
                               ELSE 'System' END) as status_type
              FROM contracts.contract_status_log as ccsl
                       INNER JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
              WHERE (cdcs.visibility_id = 0
                  OR cdcs.visibility_id = 1
                  OR cdcs.visibility_id = 2
                  OR cdcs.visibility_id = 3
                  OR cdcs.visibility_id = 4
                  OR cdcs.visibility_id = 5
                  OR cdcs.visibility_id = 6
                  OR cdcs.visibility_id = 8
                  OR cdcs.visibility_id = 9
                  OR cdcs.visibility_id = 10
                  OR cdcs.visibility_id = 11
                  OR cdcs.visibility_id = 12)
                AND ccsl.contract_sid = ccontract.sid
  ) as ccsl ON TRUE;
  end if;
  
  
  END;
  $$
  LANGUAGE plpgsql;
  
  `);
  }
}
