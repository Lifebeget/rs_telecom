import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';
import { EnumValues } from 'enum-values';

import { ContractFingerprintType } from 'Constants';

export class AlterTableContractFingerprint1622007115056
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'fingerprint type',
            enum: EnumValues.getValues(ContractFingerprintType),
            isNullable: false,
            name: 'type',
            type: 'enum'
          },
          {
            comment: 'token',
            default: null,
            isNullable: true,
            name: 'token',
            type: 'character varying(255)'
          },
          {
            comment: 'document link',
            default: null,
            isNullable: true,
            name: 'document_link',
            type: 'character varying(255)'
          },
          {
            comment: 'fingerprint link',
            default: null,
            isNullable: true,
            name: 'fingerprint_link',
            type: 'character varying(255)'
          },
          {
            comment: 'sent date',
            default: null,
            isNullable: true,
            name: 'sent_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'approve date',
            default: null,
            isNullable: true,
            name: 'approved_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'view date',
            default: null,
            isNullable: true,
            name: 'viewed_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'complete date',
            default: null,
            isNullable: true,
            name: 'completed_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'fail date',
            default: null,
            isNullable: true,
            name: 'failed_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_fingerprint'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_fingerprint', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_fingerprint_status'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_fingerprint" IS 'contract fingerprint';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_fingerprint');
  }
}
