import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { ContractRequestStatus } from 'Constants';

export class AddContractProcessStatus1625191251294
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'process trigger method',
        default: null,
        enum: EnumValues.getValues(ContractRequestStatus),
        isNullable: true,
        name: 'process_status',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'process_status');
  }
}
