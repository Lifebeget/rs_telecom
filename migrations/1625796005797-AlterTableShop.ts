import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableShop1625796005797 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'contact phone number',
            isNullable: false,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'contact email',
            isNullable: false,
            name: 'email',
            type: 'character varying(255)'
          },
          {
            comment: 'address street',
            isNullable: false,
            name: 'street',
            type: 'character varying(255)'
          },
          {
            comment: 'address house',
            isNullable: false,
            name: 'house',
            type: 'character varying(255)'
          },
          {
            comment: 'address postcode',
            isNullable: false,
            name: 'postcode',
            type: 'character varying(255)'
          },
          {
            comment: 'address location',
            isNullable: false,
            name: 'location',
            type: 'character varying(255)'
          },
          {
            comment: 'address land',
            isNullable: false,
            name: 'land',
            type: 'character varying(255)'
          },
          {
            comment: 'delete flag',
            default: false,
            isNullable: false,
            name: 'is_deleted_flg',
            type: 'boolean'
          },
          {
            comment: 'delete date',
            default: null,
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop'
      })
    );

    await queryRunner.query(`COMMENT ON TABLE "shops"."shop" IS 'shop list';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop');
  }
}
