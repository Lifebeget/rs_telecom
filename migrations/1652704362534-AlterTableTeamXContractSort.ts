import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTeamXContractSort1652704362534
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          { isNullable: false, name: 'team_sid', type: 'uuid' },
          {
            isNullable: false,
            name: 'sort_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'index sort',
            default: 1,
            name: 'index',
            type: 'integer'
          }
        ],
        name: 'teams.team_x_contract_sort'
      })
    );

    await queryRunner.createForeignKeys('teams.team_x_contract_sort', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['sort_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_sort'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('teams.team_x_contract_sort', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['sort_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_sort'
      })
    ]);

    await queryRunner.dropTable('teams.team_x_contract_sort');
  }
}
