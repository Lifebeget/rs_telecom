import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveOldDistributorCode1647321588160
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      UPDATE contracts.contract
      SET vo_user_id    = NULL,
          vo_region     = NULL,
          vo_project_id = NULL
      WHERE contract.vo_user_id IS NOT NULL
        AND length(contract.vo_user_id) > 12;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration cannot be reverted');
  }
}
