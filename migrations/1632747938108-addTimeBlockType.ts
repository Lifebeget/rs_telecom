import { EnumValues } from 'enum-values';
import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { BookingType } from 'Constants';

export class addTimeBlockType1632747938108 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'shops.promoter_schedule',
      new TableColumn({
        comment: 'time block type',
        default: null,
        enum: EnumValues.getValues(BookingType),
        isNullable: true,
        name: 'time_block_type',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'shops.promoter_schedule',
      new TableColumn({
        comment: 'time block type',
        default: null,
        enum: EnumValues.getValues(BookingType),
        isNullable: true,
        name: 'time_block_type',
        type: 'enum'
      })
    );
  }
}
