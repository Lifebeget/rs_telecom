import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  PermissionMap['fingerprintESignViewTeam']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.fingerprintESignViewTeam]: 'view team esigns'
};

export class CreatePermissionForViewingTeamEsigns1656591954482
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintESignViewTeam
    ]);

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintESignViewTeam
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();

    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintESignViewTeam
    ]);

    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintESignViewTeam
    ]);
  }
}
