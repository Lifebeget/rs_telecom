import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableDictContractStatus1610362691460
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'status name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'status description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_contract_status'
      })
    );

    // await queryRunner.manager
    //   .createQueryBuilder()
    //   .insert()
    //   .into(ExtensionVariantEntity, ['name', 'description'])
    //   .values([
    //     {
    //       name: ExtensionVariant.normalExtension,
    //       description: 'Contract can be normally extended'
    //     },
    //     {
    //       name: ExtensionVariant.earlyExtension,
    //       description: 'Contract can be early extended'
    //     },
    //     {
    //       name: ExtensionVariant.cannotBeExtended,
    //       description: 'Contract cannot be extended'
    //     },
    //     {
    //       name: ExtensionVariant.extendingConfirmed,
    //       description: 'Contract extending has been confirmed'
    //     },
    //     {
    //       name: ExtensionVariant.extendingRequested,
    //       description: 'Contract extending has been requested'
    //     },
    //     {
    //       name: ExtensionVariant.clarificationRequested,
    //       description: 'Contract clarification has been requested'
    //     },
    //     {
    //       name: 'expired' as ExtensionVariant,
    //       description: 'Contract expired'
    //     }
    //   ])
    //   .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_contract_status" IS 'contract status';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.dict_contract_status');
  }
}
