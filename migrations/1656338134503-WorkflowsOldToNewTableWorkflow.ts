import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

import { ContractStatus } from 'Constants';

export class WorkflowsOldToNewTableWorkflow1656338134503
  implements MigrationInterface {
  async workflowAction({
    memberSid,
    contractSid,
    manager,
    isArchivedBool,
    createdAt,
    teamSid,
    typeSid,
    statusSid
  }: {
    memberSid: string;
    contractSid: string;
    manager: EntityManager;
    isArchivedBool: boolean;
    createdAt: string;
    teamSid: string;
    typeSid: string;
    statusSid: string;
  }): Promise<boolean> {
    const isArchived = String(isArchivedBool);

    let flowSid = null;
    const isStarted = 'true';

    const sqlAddFlow = `INSERT INTO workflows.contract_flow (sid, contract_sid, created_at, updated_at, is_archived, deleted_at) VALUES (DEFAULT, '${contractSid}'::uuid, '${createdAt}', '${createdAt}', '${isArchived}'::boolean, null::timestamp with time zone );`;
    await manager.query(sqlAddFlow);
    const getCreatedFlow = await manager.query(
      `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' AND wcf.is_archived = '${isArchived}'::boolean ORDER BY wcf.is_archived ASC, wcf.created_at DESC LIMIT 1`
    );
    flowSid = getCreatedFlow[0]['sid'] as string;

    const getMemberInActualFlow = await manager.query(
      `SELECT * FROM workflows.contract_flow_member as wcfm WHERE wcfm.flow_sid = '${flowSid}' AND wcfm.member_sid = '${memberSid}'`
    );

    const getTeamInActualFlow = await manager.query(
      `SELECT * FROM workflows.contract_flow_team as wcft WHERE wcft.flow_sid = '${flowSid}' AND wcft.team_sid = '${teamSid}' `
    );

    if (!getTeamInActualFlow || !getTeamInActualFlow[0]) {
      const sqlAddTeam = `INSERT INTO workflows.contract_flow_team (sid, flow_sid, team_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${teamSid}'::uuid, 'false'::boolean, '${isArchived}'::boolean, '${createdAt}'::timestamp with time zone, null::timestamp with time zone)`;
      await manager.query(sqlAddTeam);
    }

    if (!getMemberInActualFlow || !getMemberInActualFlow[0]) {
      const sqlAddTeam = `INSERT INTO workflows.contract_flow_member (sid, flow_sid, member_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${memberSid}'::uuid, '${isStarted}'::boolean, '${isArchived}'::boolean, '${createdAt}', null::timestamp with time zone)`;
      await manager.query(sqlAddTeam);

      const sqlAddStatus = `INSERT INTO workflows.contract_flow_status (sid, flow_sid, contract_sid, team_sid, member_sid, status_sid, is_deleted, created_at, deleted_at, type_sid) VALUES (DEFAULT, '${flowSid}'::uuid, '${contractSid}'::uuid, '${teamSid}'::uuid, '${memberSid}'::uuid, '${statusSid}'::uuid, '${isArchived}', '${createdAt}'::timestamp, null::timestamp with time zone, '${typeSid}'::uuid)`;
      await manager.query(sqlAddStatus);
    }

    return true;
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const assignmentRows = await queryRunner.manager.query(`SELECT a.member_sid,
    a.contract_sid,
    a.is_deleted,
    a.created_at,
    member.team_sid as team_sid
    FROM contracts.assignment  as a
    LEFT JOIN teams.member as member ON member.sid = a.member_sid
    ORDER BY a.created_at ASC`);

    const defaultStatus = await queryRunner.manager.query(
      `SELECT * FROM workflows.contract_status as status WHERE status.name = '${ContractStatus.Idle}' LIMIT 1`
    );
    if (!defaultStatus || !defaultStatus[0]) {
      throw new Error('Iddle not found');
    }
    const idleStatus = defaultStatus[0];
    for await (const data of assignmentRows) {
      const memberSid = data['member_sid'];
      const contractSid = data['contract_sid'];
      const isDeleted = data['is_deleted'];
      const teamSid = data['team_sid'];
      const createdAt = new Date(data['created_at']).toISOString();
      await this.workflowAction({
        contractSid,
        createdAt,
        isArchivedBool: isDeleted,
        manager: queryRunner.manager,
        memberSid,
        statusSid: idleStatus['sid'],
        teamSid,
        typeSid: idleStatus['type_sid']
      });
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
