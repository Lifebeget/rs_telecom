import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class addContractsLock1632422985171 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract', [
      new TableColumn({
        comment: 'Contract assignment lock',
        default: false,
        isNullable: false,
        name: 'is_assignment_locked',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'Assignment lock date',
        isNullable: true,
        name: 'is_assignment_locked_at',
        type: 'timestamp with time zone'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('contracts.contract', [
      new TableColumn({
        comment: 'Contract assignment lock',
        default: false,
        isNullable: false,
        name: 'is_assignment_locked',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'Assignment lock date',
        isNullable: true,
        name: 'is_assignment_locked_at',
        type: 'timestamp with time zone'
      })
    ]);
  }
}
