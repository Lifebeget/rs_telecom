import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission } from 'Constants';

export class AddPermissionUpdateContractInfoToQC1656579411070
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'qc agent', [
      Permission.updateContractInfo
    ]);
    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.updateContractInfo
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'qc agent', [
      Permission.updateContractInfo
    ]);
    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.updateContractInfo
    ]);
  }
}
