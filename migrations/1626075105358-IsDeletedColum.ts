import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class IsDeletedColum1625593535865 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'User is deleted',
        default: false,
        isNullable: false,
        name: 'is_deleted',
        type: 'boolean'
      })
    );
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'update time isDeleted',
        isNullable: true,
        name: 'updated_is_deleted_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', 'is_deleted');
    await queryRunner.dropColumn('users.user', 'updated_is_deleted_at');
  }
}
