import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropUniqueConstraintOnContractChecklist1637239413525
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'contracts.contract_checklist',
      'UQ_ca317c9355edfadc554512612ff'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
