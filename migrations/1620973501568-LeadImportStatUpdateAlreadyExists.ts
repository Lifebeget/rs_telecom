import { MigrationInterface, QueryRunner } from 'typeorm';

export class LeadImportStatUpdateAlreadyExists1620973501568
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'leads.lead_import_stat',
      'invalid_already_exists',
      'already_exists'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'leads.lead_import_stat',
      'already_exists',
      'invalid_already_exists'
    );
  }
}
