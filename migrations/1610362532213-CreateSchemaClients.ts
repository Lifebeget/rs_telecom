import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaClients1610362532213 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('clients');
    await queryRunner.query(
      `COMMENT ON SCHEMA "clients" IS 'all clients objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('clients');
  }
}
