import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateContractChecklist1638174925682
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(`
      UPDATE contracts.contract_checklist_template
      SET question = 'Kunden-Name abgeglichen'
      WHERE index = 4;

      UPDATE contracts.contract_checklist_template
      SET question = 'Kennwort abgefragt falls nicht Vertragsinhaber'
      WHERE index = 5;

      UPDATE contracts.contract_checklist_template
      SET question = 'Iban abgeglichen'
      WHERE index = 7;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Vertragsverlängerung mit Tarifwechsel -> Alt-Tarif erwähnt'
      WHERE index = 8;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Vertragsverlängerung mit Tarifwechsel -> die Bestandteile des neuen Tarifes erwähnt'
      WHERE index = 9;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Vertragsverlängerung ohne Tarifwechsel -> Tarifname genannt'
      WHERE index = 10;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Vertragsverlängerung ohne Tarifwechsel -> Übernahme der zeitlich nicht limitierte Rabatte erwähnt'
      WHERE index = 11;

      UPDATE contracts.contract_checklist_template
      SET question = 'Grundgebühr genannt'
      WHERE index = 12;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Prämie Gutschrift -> Gutschrift statt eines vergünstigten Telefons erwähnt'
      WHERE index = 13;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Prämie Hardware -> Hersteller, Modell, Farbe, Preis genannt'
      WHERE index = 14;

      UPDATE contracts.contract_checklist_template
      SET question = 'bei Prämie Hardware -> Lieferung per Nachname mit der DHL und Barzahlung an dem Boten erwähnt'
      WHERE index = 15;

      UPDATE contracts.contract_checklist_template
      SET question = 'Einverständnis zur Vertragsverlängerung eingeholt'
      WHERE index = 18;
    `);

    await queryRunner.manager.query(`
      WITH checklist AS (
        SELECT contract_checklist.*
        FROM contracts.contract_checklist
          LEFT JOIN contracts.contract ON contract.sid = contract_checklist.contract_sid
        WHERE contract_checklist.is_deleted = FALSE
          AND NOT (contract_checklist.contract_sid = ANY (
            SELECT contract_x_team.contract_sid
            FROM contracts.contract_x_team
              INNER JOIN teams.team ON contract_x_team.team_sid = team.sid
            WHERE team.type = 'QC' and contract_x_team.contract_sid = contract.sid
        ))
      )
      UPDATE contracts.contract_checklist
      SET is_deleted = TRUE
      FROM checklist
      WHERE contracts.contract_checklist.sid = checklist.sid
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
