import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class LeadUpdateClientColumn1620806922394 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'leads.lead',
      new TableColumn({
        default: null,
        isNullable: true,
        name: 'client_sid',
        type: 'uuid'
      }),
      new TableColumn({
        isNullable: false,
        name: 'client_sid',
        type: 'uuid'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'leads.lead',
      new TableColumn({
        isNullable: false,
        name: 'client_sid',
        type: 'uuid'
      }),
      new TableColumn({
        default: null,
        isNullable: true,
        name: 'client_sid',
        type: 'uuid'
      })
    );
  }
}
