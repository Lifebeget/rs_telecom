import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';
import { PermissionEntity } from 'Server/modules/role/entities';

type PermissionKeys = Extract<Permission, PermissionMap['deleteDistributor']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.deleteDistributor]: 'delete distributor'
};

export class AddDeleteDistributorPermission1664794680820
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permissions) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permissions) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permissions)
      })
      .execute();

    await removePermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permissions) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permissions) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );
  }
}
