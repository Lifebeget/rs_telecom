import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractFlag } from 'Constants';
import { ContractXFlagEntity } from 'Server/modules/contract/entities';

const columnIsNotReachedFlg = new TableColumn({
  comment: 'is not reached contract flag',
  default: false,
  isNullable: false,
  name: 'is_not_reached_flg',
  type: 'boolean'
});

const columnIsNotInterestedFlg = new TableColumn({
  comment: 'is not interested contract flag',
  default: false,
  isNullable: false,
  name: 'is_not_interested_flg',
  type: 'boolean'
});

export class TableContractReplaceFlagsWithContractXFlag1629206671117
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const contractsWithIsNotInterestedFlgSids = (await queryRunner.manager
      .createQueryBuilder()
      .select('contract.sid')
      .from('contracts.contract', 'contract')
      .where('contract.is_not_interested_flg = true')
      .getMany()) as string[];

    await queryRunner.manager.insert(
      ContractXFlagEntity,
      contractsWithIsNotInterestedFlgSids.map(sid => ({
        contract: sid,
        flag: 'NotInterested' as ContractFlag
      }))
    );

    const contractsWithIsNotReachedFlgSids = (await queryRunner.manager
      .createQueryBuilder()
      .select('contract.sid')
      .from('contracts.contract', 'contract')
      .where('contract.is_not_reached_flg = true')
      .getMany()) as string[];

    await queryRunner.manager.insert(
      ContractXFlagEntity,
      contractsWithIsNotReachedFlgSids.map(sid => ({
        contract: sid,
        flag: 'NotReached' as ContractFlag
      }))
    );

    await queryRunner.dropColumns('contracts.contract', [
      columnIsNotReachedFlg,
      columnIsNotInterestedFlg
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Is not revertable');
  }
}
