import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { ContractFingerprintStatus } from 'Constants';
import { ContractFingerprintStatusEntity } from 'Server/modules/contract/entities';

export class AlterTableDictContractFingerprintStatus1622006857057
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'status name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'status description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_contract_fingerprint_status'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ContractFingerprintStatusEntity, ['name', 'description'])
      .values([
        {
          description: 'Fingerprint request approved',
          name: ContractFingerprintStatus.requestApproved
        },
        {
          description: 'Fingerprint request viewed',
          name: ContractFingerprintStatus.requestViewed
        },
        {
          description: 'Fingerprint request completed',
          name: ContractFingerprintStatus.requestCompleted
        },
        {
          description: 'Fingerprint request failed',
          name: ContractFingerprintStatus.requestFailed
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_contract_fingerprint_status" IS 'list of contract fingerprint status';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.dict_contract_fingerprint_status');
  }
}
