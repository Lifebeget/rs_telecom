import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableShopXMember1625796020494 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_x_member'
      })
    );

    await queryRunner.createPrimaryKey('shops.shop_x_member', [
      'shop_sid',
      'member_sid'
    ]);

    await queryRunner.createForeignKeys('shops.shop_x_member', [
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "shops"."shop_x_member" IS 'list shop and member';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop_x_member');
  }
}
