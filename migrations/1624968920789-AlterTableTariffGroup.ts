import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableTariffGroup1624968920789 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'tariff name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'tariffs.tariff_group'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "tariffs"."tariff_group" IS 'tariff group';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff_group');
  }
}
