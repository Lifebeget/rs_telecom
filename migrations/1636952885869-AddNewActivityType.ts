import { MigrationInterface, QueryRunner } from 'typeorm';

import { ActivityType } from 'Constants';
import { ActivityTypeEntity } from 'Server/modules/activity/entities';

export class AddNewActivityType1636952885869 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const {
      identifiers: [{ sid }]
    } = await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ActivityTypeEntity, ['name', 'description'])
      .values({
        description: 'Contract extension variant change',
        name: ActivityType.ContractExtensionVariantChanged
      })
      .execute();

    await queryRunner.query(
      `
      WITH activities AS (
        SELECT activity.sid
        FROM activities.activity
          LEFT JOIN activities.dict_activity_type 
            ON activity.activity_type_sid = dict_activity_type.sid
          LEFT JOIN activities.activity_contract_status_changed
            ON activity.sid = activity_contract_status_changed.activity_sid
          LEFT JOIN activities.activity_extension_variant_changed
            ON activity.sid = activity_extension_variant_changed.activity_sid
          WHERE dict_activity_type.name = 'ContractStatusChanged'
            AND activity_contract_status_changed.activity_sid ISNULL
            AND activity_extension_variant_changed.activity_sid NOTNULL
      )
      UPDATE activities.activity
      SET activity_type_sid = $1
        FROM activities
      WHERE activities.sid = activity.sid
    `,
      [sid]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
