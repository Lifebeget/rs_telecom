import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class ActivityUpdateNoteForeignKey1620798959997
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['note_sid'],
        name: 'FK_faaaad3c1ba83a8c15359e7c500',
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client_note'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['note_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client_note'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['note_sid'],
        name: 'FK_faaaad3c1ba83a8c15359e7c500',
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client_note'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['note_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client_note'
      })
    );
  }
}
