import { MigrationInterface, QueryRunner } from 'typeorm';

export class TableTeamDropIsPromotionColumn1643806418915
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'is_promotion_flg');
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
