import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractImportFlag1645772313085 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'is imported flag',
        default: false,
        isNullable: false,
        name: 'is_imported',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
