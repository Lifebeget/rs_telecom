import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class WorkingDays1625208674589 implements MigrationInterface {
  private tableName = 'users.working_days';

  private foreignKey = 'user_sid';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'user identifier',
            isNullable: false,
            name: 'user_sid',
            type: 'uuid'
          },
          {
            comment: 'time the tracking started',
            default: 'now()',
            isNullable: false,
            name: 'started_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'time the tracking stopped',
            isNullable: true,
            name: 'ended_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: this.tableName
      })
    );

    await queryRunner.createForeignKey(
      this.tableName,
      new TableForeignKey({
        columnNames: ['user_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'users.user'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(this.tableName, this.foreignKey);
    await queryRunner.dropTable(this.tableName);
  }
}
