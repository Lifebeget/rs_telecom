import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnCreationType = new TableColumn({
  comment: 'contract is imported',
  default: false,
  isNullable: false,
  name: 'is_imported',
  type: 'boolean'
});

export class ActivityContractCreationAddCreationTypeColumns1650971250807
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'activities.activity_contract_created',
      columnCreationType
    );

    await queryRunner.manager.query(`
    UPDATE 
      activities.activity_contract_created 
    SET 
      is_imported = TRUE 
    WHERE 
      activity_sid IN (
        SELECT 
          a.sid 
        FROM 
          contracts.contract c 
          LEFT JOIN activities.activity a ON c.sid = a.contract_sid 
        WHERE 
          is_imported = TRUE
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'activities.activity_contract_created',
      columnCreationType
    );
  }
}
