import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaContracts1610362681460 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('contracts');
    await queryRunner.query(
      `COMMENT ON SCHEMA "contracts" IS 'all contracts objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('contracts');
  }
}
