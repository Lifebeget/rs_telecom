import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractExtendRequestBankData1610362897368
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'contract_extend_request_sid',
            type: 'uuid'
          },
          {
            comment: 'payment method',
            default: null,
            isNullable: true,
            name: 'payment_method',
            type: 'character varying(255)'
          },
          {
            comment: 'iban',
            default: null,
            isNullable: true,
            name: 'iban',
            type: 'character varying(255)'
          },
          {
            comment: 'bic',
            default: null,
            isNullable: true,
            name: 'bic',
            type: 'character varying(255)'
          },
          {
            comment: 'account',
            default: null,
            isNullable: true,
            name: 'account',
            type: 'character varying(255)'
          },
          {
            comment: 'bank code',
            default: null,
            isNullable: true,
            name: 'bank_code',
            type: 'character varying(255)'
          },
          {
            comment: 'bank name',
            default: null,
            isNullable: true,
            name: 'bank_name',
            type: 'character varying(255)'
          },
          {
            comment: 'owner name',
            default: null,
            isNullable: true,
            name: 'owner_name',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_extend_request_bank_data'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_extend_request_bank_data',
      new TableForeignKey({
        columnNames: ['contract_extend_request_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_extend_request'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_extend_request_bank_data" IS 'contract extend request bank data';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_extend_request_bank_data');
  }
}
