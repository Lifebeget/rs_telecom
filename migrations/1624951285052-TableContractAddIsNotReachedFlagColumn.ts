import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const column = new TableColumn({
  comment: 'is not reached contract flag',
  default: false,
  isNullable: false,
  name: 'is_not_reached_flg',
  type: 'boolean'
});

export class TableContractAddIsNotReachedFlagColumn1624951285052
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('contracts.contract', column);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', column);
  }
}
