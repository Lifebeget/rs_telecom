import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAddDistributeInfo1623927829439
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'contracts.contract',
      'project_id',
      'vo_project_id'
    );

    await queryRunner.addColumns('contracts.contract', [
      new TableColumn({
        comment: 'vodafone user id',
        default: null,
        isNullable: true,
        name: 'vo_user_id',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'vodafone user region',
        default: null,
        isNullable: true,
        name: 'vo_region',
        type: 'character varying(255)'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'contracts.contract',
      'vo_project_id',
      'project_id'
    );

    await queryRunner.dropColumn('contracts.contract', 'vo_user_id');
    await queryRunner.dropColumn('contracts.contract', 'vo_region');
  }
}
