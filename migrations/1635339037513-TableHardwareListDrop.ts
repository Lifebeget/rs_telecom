import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class TableHardwareListDrop1635339037513 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'hardware.hardware_list_item',
      'hardware_list_sid'
    );

    await queryRunner.dropForeignKey(
      'contracts.contract_extend_request',
      'FK_863bf17323d0aa4a98e09cdac3b'
    );

    await queryRunner.dropTable('hardware.hardware_list');

    // hardware list is deprecated because of removing import logic
    // hardware list now stores all hardware in one tabel
    await queryRunner.manager.query(
      `ALTER TABLE "hardware"."hardware_list_item" RENAME TO "hardware";`
    );

    await queryRunner.renameColumn(
      'contracts.contract_extend_request',
      'hardware_list_item_sid',
      'hardware_sid'
    );

    await queryRunner.createForeignKey(
      'contracts.contract_extend_request',
      new TableForeignKey({
        columnNames: ['hardware_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'hardware.hardware'
      })
    );

    await queryRunner.addColumns('hardware.hardware', [
      new TableColumn({
        isNullable: true,
        name: 'member_sid',
        type: 'uuid'
      })
    ]);

    await queryRunner.createForeignKey(
      'hardware.hardware',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    // need to add member sid to all devices, current logic is supervisor has created it
    await queryRunner.manager.query(
      `UPDATE 
       hardware.hardware
       SET member_sid = 
        (SELECT 
         member.sid FROM teams.member
        LEFT JOIN users."user" ON "user".sid = member.user_sid 
        WHERE "user".is_supervisor = TRUE LIMIT 1)`
    );

    await queryRunner.changeColumn(
      'hardware.hardware',
      'member_sid',
      new TableColumn({
        isNullable: false,
        name: 'member_sid',
        type: 'uuid'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration cannot be reverted');
  }
}
