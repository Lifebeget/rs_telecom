import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedIndexes1635430461573 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `create index if not exists dict_contract_status_name_index on contracts.dict_contract_status (name)`
    );
    await queryRunner.query(
      `create index if not exists contract_is_archived_flg_index on contracts.contract (is_archived_flg)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_status_status_sid_index on contracts.contract_x_status (status_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_status_contract_sid_index on contracts.contract_x_status (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_isDeleted_index on contracts.contract_x_status (is_deleted)`
    );
    await queryRunner.query(
      `create index if not exists contract_extension_variant_sid_index on contracts.contract (extension_variant_sid)`
    );
    await queryRunner.query(
      `create index if not exists extent_request_contract_extend_request_sid_index on contracts.contract_extend_request (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_member_member_sid_index on contracts.contract_x_member (member_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_member_contract_sid_index on contracts.contract_x_member (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_client_contract_sid_index on contracts.contract_client (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_team_team_sid_index on contracts.contract_x_team (team_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_team_contract_sid_index on contracts.contract_x_team (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists client_x_contract_client_sid_index on clients.client_x_contract (client_sid)`
    );
    await queryRunner.query(
      `create index if not exists client_x_contract_contract_sid_index on clients.client_x_contract (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_extend_request_address_extend_request_sid_index on contracts.contract_extend_request_address (contract_extend_request_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_member_log_contract_sid_index on contracts.contract_member_log (contract_sid)`
    );
    await queryRunner.query(
      `create index if not exists contract_member_log_created_at_index on contracts.contract_member_log (created_at)`
    );
    await queryRunner.query(
      `create index if not exists contract_x_status_type_index on contracts.contract_x_status (type)`
    );

    await queryRunner.query(`
    create index if not exists team_member_index_user on teams.member (user_sid);
    create index if not exists contract_x_member_index_sid on contracts.contract_x_member (contract_sid, member_sid);
    create index if not exists contract_x_team_index_all on contracts.contract_x_team (contract_sid, team_sid);
    create index if not exists client_x_contract_client_all_index on clients.client_x_contract (client_sid,contract_sid);
    create index if not exists extent_request_contract_sid_index on contracts.contract_extend_request (sid);
    create index if not exists teams_member_index_team_sid on teams.member (team_sid);
    create index if not exists teams_member_index_sid on teams.member (sid);

    create index if not exists dict_contract_status_index_sid on contracts.dict_contract_status (sid);
    create index if not exists teams_team_index_sid on teams.team (sid);
    create index if not exists teams_member_index_team_sid on teams.member (team_sid);
    create index if not exists teams_team_index_type on teams.team (type);
    create index if not exists contracts_dict_contract_status_index_visibility_id on contracts.dict_contract_status (visibility_id);
    create index if not exists contracts_contract_status_log_index_member on contracts.contract_status_log (member_sid);
    create index if not exists contracts_contract_status_log_index_contract_sid on contracts.contract_status_log (contract_sid);
    create index if not exists contracts_contract_status_log_index_contract_created_at on contracts.contract_status_log (created_at);
    create index if not exists contracts_contract_status_log_index_contract_status_sid on contracts.contract_status_log (status_sid);
    create index if not exists contracts_contract_index_sid on contracts.contract (sid);

    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`drop index dict_contract_status_name_index `);
    await queryRunner.query(`drop index contract_is_archived_flg_index `);
    await queryRunner.query(`drop index contract_x_status_status_sid_index`);
    await queryRunner.query(`drop index contract_x_status_contract_sid_index`);
    await queryRunner.query(`drop index contract_x_isDeleted_index`);
    await queryRunner.query(`drop index contract_x_isDeleted_index`);
    await queryRunner.query(`drop index contract_extension_variant_sid_index`);
    await queryRunner.query(
      `drop index extent_request_contract_extend_request_sid_index`
    );
    await queryRunner.query(`drop index contract_x_member_member_sid_index`);
    await queryRunner.query(`drop index contract_x_member_contract_sid_index`);
    await queryRunner.query(`drop index contract_client_contract_sid_index`);
    await queryRunner.query(`drop index contract_x_team_team_sid_index`);
    await queryRunner.query(`drop index contract_x_team_contract_sid_index`);
    await queryRunner.query(`drop index client_x_contract_client_sid_index`);
    await queryRunner.query(`drop index client_x_contract_contract_sid_index`);
    await queryRunner.query(
      `drop index contract_extend_request_address_extend_request_sid_index`
    );
    await queryRunner.query(
      `drop index contract_member_log_contract_sid_index`
    );
    await queryRunner.query(`drop index contract_member_log_created_at_index`);
    await queryRunner.query(`drop index contract_x_status_type_index`);
  }
}
