import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableShopScheduleAddWorkedAt1626849106512
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('shops.shop_schedule', 'week_range');

    await queryRunner.addColumn(
      'shops.shop_schedule',
      new TableColumn({
        comment: 'work date',
        default: null,
        isNullable: true,
        name: 'worked_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'shops.shop_schedule',
      new TableColumn({
        comment: 'week range',
        isNullable: false,
        name: 'week_range',
        type: 'jsonb'
      })
    );

    await queryRunner.dropColumn('shops.shop_schedule', 'worked_at');
  }
}
