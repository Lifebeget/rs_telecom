import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

const column = new TableColumn({
  default: null,
  isNullable: true,
  name: 'tariff_group_sid',
  type: 'uuid'
});

export class TableTeamAddTariffGroupColumn1624977800308
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('teams.team', column);

    await queryRunner.createForeignKey(
      'teams.team',
      new TableForeignKey({
        columnNames: ['tariff_group_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff_group'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'teams.team',
      'FK_fa79488658929081c98f3ecc6d2'
    );

    await queryRunner.dropColumn('teams.team', column);
  }
}
