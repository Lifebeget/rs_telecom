import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsEsignRequiredField1654844574534
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'required esign option for user',
        default: false,
        isNullable: false,
        name: 'is_esign_required',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
