import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractAddressMail1610362736362
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'client appeal (miss, mister, madam)',
            default: null,
            isNullable: true,
            name: 'appeal',
            type: 'character varying(255)'
          },
          {
            comment: 'client name',
            default: null,
            isNullable: true,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'client surname',
            default: null,
            isNullable: true,
            name: 'surname',
            type: 'character varying(255)'
          },
          {
            comment: 'address street',
            default: null,
            isNullable: true,
            name: 'street',
            type: 'character varying(255)'
          },
          {
            comment: 'address house',
            default: null,
            isNullable: true,
            name: 'house',
            type: 'character varying(255)'
          },
          {
            comment: 'address postcode',
            default: null,
            isNullable: true,
            name: 'postcode',
            type: 'character varying(255)'
          },
          {
            comment: 'address location',
            default: null,
            isNullable: true,
            name: 'location',
            type: 'character varying(255)'
          },
          {
            comment: 'address land',
            default: null,
            isNullable: true,
            name: 'land',
            type: 'character varying(255)'
          },
          {
            comment: 'email address',
            default: null,
            isNullable: true,
            name: 'email_address',
            type: 'character varying(255)'
          },
          {
            comment: 'email type',
            default: null,
            isNullable: true,
            name: 'email_type',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_address_mail'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_address_mail',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_address_mail" IS 'contract address mail';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_address_mail');
  }
}
