import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveContractStatusAndXStatusKey1654619090997
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'contracts.contract_x_status',
      'FK_72437533749c366110c484f5e93'
    );
    await queryRunner.dropForeignKey(
      'contracts.contract_x_status',
      'FK_c1f1f770cb1f616897f6f14f424'
    );
    await queryRunner.dropForeignKey(
      'contracts.contract_status_log',
      'FK_5034ee4201f23edaf005f09acc3'
    );
    await queryRunner.dropForeignKey(
      'contracts.contract_status_log',
      'FK_e185788e5e6027c127de56f3227'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
