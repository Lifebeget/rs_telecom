import { MigrationInterface, QueryRunner } from 'typeorm';

export class TableTariffXTeamDrop1625230542912 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff_x_team');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('cannot be reverted');
  }
}
