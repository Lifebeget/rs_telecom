import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnArchived = new TableColumn({
  comment: 'hardware archived state',
  default: false,
  isNullable: false,
  name: 'archived',
  type: 'boolean'
});

const columnArchivedAt = new TableColumn({
  comment: 'archived date',
  default: null,
  isNullable: true,
  name: 'archived_at',
  type: 'timestamp with time zone'
});

export class TableHardwareAddArchivedAtColumn1634534455372
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('hardware.hardware_list_item', columnArchived);
    await queryRunner.addColumn(
      'hardware.hardware_list_item',
      columnArchivedAt
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('hardware.hardware_list_item', columnArchived);
    await queryRunner.dropColumn(
      'hardware.hardware_list_item',
      columnArchivedAt
    );
  }
}
