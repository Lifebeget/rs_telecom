import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractFingerprintEmailAddress1631107280691
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'fingerprint email address',
        default: null,
        isNullable: true,
        name: 'fingerprint_email_address',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'fingerprint email address',
        default: null,
        isNullable: true,
        name: 'fingerprint_email_address',
        type: 'character varying(255)'
      })
    );
  }
}
