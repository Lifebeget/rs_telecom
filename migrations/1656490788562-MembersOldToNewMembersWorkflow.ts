import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

import { ContractStatus } from 'Constants';

export class MembersOldToNewMembersWorkflow1656490788562
  implements MigrationInterface {
  // eslint-disable-next-line sonarjs/cognitive-complexity -- is's migration
  async workflowAction({
    memberSid,
    contractSid,
    manager,
    teamType,
    createdAt,
    teamSid,
    statusTypeSid,
    statusSid
  }: {
    memberSid: string;
    contractSid: string;
    manager: EntityManager;
    teamType: string;
    createdAt: string;
    teamSid: string;
    statusTypeSid: string;
    statusSid: string;
  }): Promise<boolean> {
    let getActualFlowContract = null;
    if (teamType !== 'CC') {
      let flowSid = null;
      let isArchived = 'false';
      getActualFlowContract = await manager.query(
        `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' ORDER BY wcf.is_archived ASC, wcf.created_at DESC LIMIT 1`
      );

      const getContract = await manager.query(
        `SELECT * FROM contracts.contract as wcf WHERE sid = '${contractSid}'LIMIT 1`
      );

      if (!getContract || !getContract[0]) {
        return true;
      }

      if (!getActualFlowContract || !getActualFlowContract[0]) {
        const sqlAddFlow = `INSERT INTO workflows.contract_flow (sid, contract_sid, created_at, updated_at, is_archived, deleted_at) VALUES (DEFAULT, '${contractSid}'::uuid, '${createdAt}', DEFAULT, 'true'::boolean, null::timestamp with time zone );`;
        await manager.query(sqlAddFlow);
        const getCreatedFlow = await manager.query(
          `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' ORDER BY created_at DESC LIMIT 1`
        );
        flowSid = getCreatedFlow[0]['sid'] as string;
        isArchived = 'true';
        const sqlAddStatus = `INSERT INTO workflows.contract_flow_status (sid, flow_sid, contract_sid, team_sid, member_sid, status_sid, is_deleted, created_at, deleted_at, type_sid) VALUES (DEFAULT, '${flowSid}'::uuid, '${contractSid}'::uuid, '${teamSid}'::uuid, '${memberSid}'::uuid, '${statusSid}'::uuid, '${isArchived}', '${createdAt}'::timestamp, null::timestamp with time zone, '${statusTypeSid}'::uuid)`;
        await manager.query(sqlAddStatus);
      } else {
        isArchived = 'false';
        flowSid = getActualFlowContract[0]['sid'] as string;
      }

      if (!flowSid) {
        throw new Error('Что то пошло не так добавлении флоу');
      }

      const getMemberInActualFlow = await manager.query(
        `SELECT * FROM workflows.contract_flow_member as wcfm WHERE wcfm.flow_sid = '${flowSid}' and wcfm.member_sid = '${memberSid}' LIMIT 1`
      );

      if (!getMemberInActualFlow || !getMemberInActualFlow[0]) {
        const sqlAddTeam = `INSERT INTO workflows.contract_flow_member (sid, flow_sid, member_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${memberSid}'::uuid, 'false'::boolean, '${isArchived}'::boolean, '${createdAt}', null::timestamp with time zone)`;
        await manager.query(sqlAddTeam);
      }
    } else {
      getActualFlowContract = await manager.query(
        `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' AND is_archived = false ORDER BY created_at DESC LIMIT 1`
      );
      const isArchived = 'true';

      if (!getActualFlowContract || !getActualFlowContract[0]) {
        const sqlAddFlow = `INSERT INTO workflows.contract_flow (sid, contract_sid, created_at, updated_at, is_archived, deleted_at) VALUES (DEFAULT, '${contractSid}'::uuid, '${createdAt}', DEFAULT, 'true'::boolean, null::timestamp with time zone );`;
        await manager.query(sqlAddFlow);

        const lastFlow = await manager.query(
          `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' ORDER BY wcf.created_at DESC LIMIT 1`
        );

        const flowSid = lastFlow[0]['sid'] as string;

        const getMemberInActualFlow = await manager.query(
          `SELECT * FROM workflows.contract_flow_member as wcfm WHERE wcfm.flow_sid = '${flowSid}' and wcfm.member_sid = '${memberSid}' LIMIT 1`
        );

        if (!getMemberInActualFlow || !getMemberInActualFlow[0]) {
          const sqlAddTeam = `INSERT INTO workflows.contract_flow_member (sid, flow_sid, member_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${memberSid}'::uuid, 'false'::boolean, '${isArchived}'::boolean, '${createdAt}', null::timestamp with time zone)`;
          await manager.query(sqlAddTeam);
        }

        const getStatus = await manager.query(
          `SELECT * FROM workflows.contract_flow_status as ws WHERE ws.flow_sid = '${flowSid}' AND status_sid = '${statusSid}' AND type_sid = '${statusTypeSid}'`
        );

        if (!getStatus || !getStatus[0]) {
          const sqlAddStatus = `INSERT INTO workflows.contract_flow_status (sid, flow_sid, contract_sid, team_sid, member_sid, status_sid, is_deleted, created_at, deleted_at, type_sid) VALUES (DEFAULT, '${flowSid}'::uuid, '${contractSid}'::uuid, '${teamSid}'::uuid, '${memberSid}'::uuid, '${statusSid}'::uuid, '${isArchived}', '${createdAt}'::timestamp, null::timestamp with time zone, '${statusTypeSid}'::uuid)`;
          await manager.query(sqlAddStatus);
        }
      } else {
        const flowSid = getActualFlowContract[0]['sid'] as string;
        const sqlAddStatus = `INSERT INTO workflows.contract_flow_status (sid, flow_sid, contract_sid, team_sid, member_sid, status_sid, is_deleted, created_at, deleted_at, type_sid) VALUES (DEFAULT, '${flowSid}'::uuid, '${contractSid}'::uuid, '${teamSid}'::uuid, '${memberSid}'::uuid, '${statusSid}'::uuid, '${isArchived}', '${createdAt}'::timestamp, null::timestamp with time zone, '${statusTypeSid}'::uuid)`;
        await manager.query(sqlAddStatus);
        const getMemberInActualFlow = await manager.query(
          `SELECT * FROM workflows.contract_flow_member as wcfm WHERE wcfm.flow_sid = '${flowSid}' and wcfm.member_sid = '${memberSid}' LIMIT 1`
        );

        if (!getMemberInActualFlow || !getMemberInActualFlow[0]) {
          const sqlAddTeam = `INSERT INTO workflows.contract_flow_member (sid, flow_sid, member_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${memberSid}'::uuid, 'false'::boolean, '${isArchived}'::boolean, '${createdAt}', null::timestamp with time zone)`;
          await manager.query(sqlAddTeam);
        }
      }
    }

    return true;
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const assignmentRows = await queryRunner.manager
      .query(`SELECT ccxm.contract_sid as contract_sid, 
    ccxm.member_sid as member_sid,
    tt.type as team_type,
    tt.sid as team_sid,
    ccxm.created_at as created_at
    FROM contracts.contract_x_member as ccxm
    LEFT JOIN teams.member as tm ON tm.sid = ccxm.member_sid
    LEFT JOIN teams.team as tt ON tt.sid = tm.team_sid
    ORDER BY ccxm.created_at ASC`);

    const defaultStatus = await queryRunner.manager.query(
      `SELECT * FROM workflows.contract_status as status WHERE status.name = '${ContractStatus.Idle}' LIMIT 1`
    );
    if (!defaultStatus || !defaultStatus[0]) {
      throw new Error('Iddle not found');
    }
    const idleStatus = defaultStatus[0];

    for await (const data of assignmentRows) {
      const memberSid = data['member_sid'];
      const teamSid = data['team_sid'];
      const contractSid = data['contract_sid'];
      const teamType = data['team_type'];
      const createdAt = new Date(data['created_at']).toISOString();

      await this.workflowAction({
        contractSid,
        createdAt,
        manager: queryRunner.manager,
        memberSid,
        statusSid: idleStatus['sid'],
        statusTypeSid: idleStatus['type_sid'],
        teamSid,
        teamType: teamType
      });
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
