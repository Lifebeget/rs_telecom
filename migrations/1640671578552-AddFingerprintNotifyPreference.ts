import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from 'Server/modules/properties/entities';

export class AddFingerprintNotifyPreference1640671578552
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values([
        {
          // 12 * 60 * 60 - 12:00 a.m.
          description:
            'Fingerprint notification time in seconds. If set 0 - notification will be disabled',

          key: 'fingerprintNotifyTime',
          value: '43200'
        },
        {
          description:
            'Fingerprint notification repeat count. If set 0 - notification will be disabled',
          key: 'fingerprintNotifyRepeat',
          value: '2'
        }
      ])
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
