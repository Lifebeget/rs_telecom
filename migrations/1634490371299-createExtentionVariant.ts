import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { ExtensionVariantEntity } from 'Server/modules/contract/entities/extension-variant.entity';
import { ExtensionVariant } from 'Constants';

export class createExtentionVariant1634490371299 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'status name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'status description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.extension_variant'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ExtensionVariantEntity, ['name', 'description'])
      .values([
        {
          description: 'Contract can be normally extended',
          name: ExtensionVariant.normalExtension
        },
        {
          description: 'Contract can be early extended',
          name: ExtensionVariant.earlyExtension
        },
        {
          description: 'Contract cannot be extended',
          name: ExtensionVariant.cannotBeExtended
        },
        {
          description: 'Contract expired',
          name: 'expired' as ExtensionVariant
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."extension_variant" IS 'contract extension variant';`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'extension_variant_sid',
            type: 'uuid'
          },
          {
            comment: 'assigner',
            default: null,
            isNullable: true,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.extension_variant_log'
      })
    );

    await queryRunner.createForeignKeys('contracts.extension_variant_log', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),

      new TableForeignKey({
        columnNames: ['extension_variant_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.extension_variant'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."extension_variant_log" IS 'list of contracts and extension variants';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('contracts.extension_variant_log', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),

      new TableForeignKey({
        columnNames: ['extension_variant_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.extension_variant'
      })
    ]);
    await queryRunner.dropTable('contracts.extension_variant');
  }
}
