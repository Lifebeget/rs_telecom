import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddUpdatedAtColumnOnTableContract1649649781480
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'update date',
        default: 'now()',
        isNullable: false,
        name: 'updated_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('cannot be reverted');
  }
}
