import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaHardware1609833710818 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('hardware');
    await queryRunner.query(
      `COMMENT ON SCHEMA "hardware" IS 'all hardware objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('hardware');
  }
}
