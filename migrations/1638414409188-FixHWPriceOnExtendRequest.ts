import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixHWPriceOnExtendRequest1638414409188
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(`
      UPDATE contracts.contract_extend_request
      SET hw_price = NULL
      WHERE 1 = 1;

      WITH hw AS (
        SELECT hardware.*
        FROM contracts.contract_extend_request
          LEFT JOIN hardware.hardware ON contract_extend_request.hardware_sid = hardware.sid
        WHERE contract_extend_request.hardware_sid IS NOT NULL
      )
      UPDATE contracts.contract_extend_request
      SET hw_price = hw.price
      FROM hw
      WHERE contract_extend_request.hardware_sid = hw.sid;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
