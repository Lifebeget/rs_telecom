import { EnumValues } from 'enum-values';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { BookingType } from 'Constants';

export class AlterTableShopTimeBlock1625796010174
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            comment: 'start block time in millis',
            isNullable: false,
            name: 'start_time',
            type: 'integer'
          },
          {
            comment: 'end block time in millis',
            isNullable: false,
            name: 'end_time',
            type: 'integer'
          },
          {
            comment: 'time block type',
            enum: EnumValues.getValues(BookingType),
            isNullable: false,
            name: 'block_type',
            type: 'enum'
          },
          {
            comment: 'is archived time block',
            default: false,
            isNullable: false,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'archived date',
            default: null,
            isNullable: true,
            name: 'is_archived_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_time_block'
      })
    );

    await queryRunner.createForeignKey(
      'shops.shop_time_block',
      new TableForeignKey({
        columnNames: ['shop_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'shops.shop'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "shops"."shop_time_block" IS 'shop time block';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop_time_block');
  }
}
