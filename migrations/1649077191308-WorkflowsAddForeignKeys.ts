import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class WorkflowsAddForeignKeys1649077191308
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createForeignKeys('workflows.contract_flow', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    ]);

    await queryRunner.createForeignKeys('workflows.contract_flow_team', [
      new TableForeignKey({
        columnNames: ['flow_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.contract_flow'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.createForeignKeys('workflows.contract_flow_member', [
      new TableForeignKey({
        columnNames: ['flow_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.contract_flow'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.createForeignKeys('workflows.contract_flow_status', [
      new TableForeignKey({
        columnNames: ['flow_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.contract_flow'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.contract_status'
      }),
      new TableForeignKey({
        columnNames: ['type_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.status_type'
      })
    ]);

    await queryRunner.createForeignKeys('workflows.contract_status', [
      new TableForeignKey({
        columnNames: ['type_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.status_type'
      })
    ]);
    await queryRunner.createForeignKeys('workflows.status_visibility_team', [
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'workflows.contract_status'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
