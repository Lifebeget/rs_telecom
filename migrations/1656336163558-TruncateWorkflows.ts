import { MigrationInterface, QueryRunner } from 'typeorm';

export class TruncateWorkflows1656336163558 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM workflows.contract_flow_status');
    await queryRunner.query('DELETE FROM workflows.contract_flow_member');
    await queryRunner.query(
      'DELETE FROM workflows.contract_flow_properties_temporary'
    );
    await queryRunner.query('DELETE FROM workflows.contract_flow_team');
    await queryRunner.query('DELETE FROM workflows.contract_flow');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
