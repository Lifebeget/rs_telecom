import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaUsers1607942911150 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('users');
    await queryRunner.query(`COMMENT ON SCHEMA "users" IS 'all user objects';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('users');
  }
}
