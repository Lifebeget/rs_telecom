import { MigrationInterface, QueryRunner } from 'typeorm';

export class ExtractAppealFromContractRequestHistoryToContractClient1658136602517
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      WITH data AS (
        SELECT 
          CASE (
            REGEXP_MATCH(
              request_content, '"salutation":(\\d)'
            )
          ) [1] WHEN '1' THEN 'Herr' WHEN '2' THEN 'Frau' WHEN '3' THEN 'Firma' END appeal, 
          contract_sid 
        FROM 
          contracts.contract_request_history 
        WHERE 
          request_type = 'Xml' 
          AND request_status = 'Successful' 
          AND request_content like '%salutation%'
      )
      UPDATE 
        contracts.contract_client 
      SET 
        appeal = data.appeal 
      FROM 
        data 
      WHERE 
        data.contract_sid = contract_client.contract_sid`);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
