import { MigrationInterface, QueryRunner } from 'typeorm';

export class addedSuggestedRole1628063765082 implements MigrationInterface {
  public async up(_queryRunner: QueryRunner): Promise<void> {
    // old role inserts were removed from here
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    // old role inserts were removed from here
  }
}
