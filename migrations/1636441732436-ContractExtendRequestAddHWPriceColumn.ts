import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnHWFixedPrice = new TableColumn({
  comment: 'fixed price of hw at the moment of choosing',
  default: 0,
  isNullable: true,
  name: 'hw_price',
  type: 'real'
});

export class ContractExtendRequestAddHWPriceColumn1636441732436
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_extend_request',
      columnHWFixedPrice
    );

    // setting fixed price based on existing one in hardware table
    await queryRunner.manager.query(
      `UPDATE contracts.contract_extend_request 
        SET hw_price = 
        (SELECT price FROM hardware.hardware h 
         LEFT JOIN contracts.contract_extend_request ON hardware_sid = h.sid 
         WHERE hardware_sid IS NOT NULL LIMIT 1)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract_extend_request',
      columnHWFixedPrice
    );
  }
}
