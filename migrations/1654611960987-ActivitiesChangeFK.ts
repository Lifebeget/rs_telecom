import { MigrationInterface, QueryRunner } from 'typeorm';

export class ActivitiesChangeFK1654611960987 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'activities.activity_contract_status_changed',
      'FK_9047c055933e08094bff3947fdc'
    );

    await queryRunner.dropForeignKey(
      'activities.activity_contract_status_changed',
      'FK_f77f4dc31335e742051cce83249'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
