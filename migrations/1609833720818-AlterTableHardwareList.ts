import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableHardwareList1609833720818 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'creator',
            default: null,
            isNullable: true,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'hardware.hardware_list'
      })
    );

    await queryRunner.createForeignKey(
      'hardware.hardware_list',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "hardware"."hardware_list" IS 'hardware list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('hardware.hardware_list');
  }
}
