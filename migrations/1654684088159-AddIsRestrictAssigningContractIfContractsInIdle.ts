import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsRestrictAssigningContractIfContractsInIdle1654684088159
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'restict taking contract if there are contracts in idle',
        default: false,
        isNullable: false,
        name: 'limited_take_from_queue_for_idle',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
