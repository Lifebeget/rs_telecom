import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const avatarColumn: TableColumn = new TableColumn({
  comment: 'S3 public link',
  default: null,
  isNullable: true,
  name: 'avatar',
  type: 'character varying(255)'
});

export class UserAvatar1623737161391 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('users.user', avatarColumn);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', avatarColumn);
  }
}
