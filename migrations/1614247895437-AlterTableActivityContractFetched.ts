import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractFetched1614247895437
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'fetch method',
            isNullable: false,
            name: 'fetch_method',
            type: 'character varying(255)'
          }
        ],
        name: 'activities.activity_contract_fetched'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_fetched',
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_fetched" IS 'activity contract fetched';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_fetched');
  }
}
