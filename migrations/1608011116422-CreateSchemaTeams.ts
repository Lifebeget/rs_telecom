import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaGroups1608011116422 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('teams');
    await queryRunner.query(
      `COMMENT ON SCHEMA "teams" IS 'all teams objects (teams, members)';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('teams');
  }
}
