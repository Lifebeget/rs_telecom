import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class MigratePasswordLimitToSeparateSchema1662972151248
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('request_limit');
    await queryRunner.query(
      `COMMENT ON SCHEMA "request_limit" IS 'request limit objects';`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Data',
            default: "'{}'",
            isNullable: false,
            name: 'data',
            type: 'jsonb'
          },
          {
            comment: 'User snapshot',
            isNullable: false,
            name: 'snapshot',
            type: 'text'
          },
          {
            comment: 'Entry attempt',
            isNullable: false,
            name: 'attempt',
            type: 'int'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Expired date',
            isNullable: false,
            name: 'expired_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'request_limit.request_limit'
      })
    );

    queryRunner.query(`INSERT INTO request_limit.request_limit (sid, snapshot, attempt, created_at, expired_at)
                              SELECT sid, snapshot, attempt, created_at, expired_at
                                      FROM contracts.password_request_limit;
    `);

    queryRunner.query(`UPDATE request_limit.request_limit as rn
                              SET data = json_build_object('contractSid', ro.contract_sid)
                                    FROM contracts.password_request_limit as ro
                                          WHERE rn.sid = ro.sid;`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration is not revertable');
  }
}
