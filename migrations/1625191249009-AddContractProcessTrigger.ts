import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { ContractProcessTrigger } from 'Constants';

export class AddContractProcessTrigger1625191249009
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'process trigger method',
        default: null,
        enum: EnumValues.getValues(ContractProcessTrigger),
        isNullable: true,
        name: 'trigger_method',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'trigger_method');
  }
}
