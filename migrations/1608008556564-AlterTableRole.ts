import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { RoleEntity } from 'Server/modules/role/entities';

export class AlterTableRole1608008556564 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'role name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'role description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'security.role'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(RoleEntity, ['name'])
      .values([
        {
          name: 'supervisor root'
        },
        {
          name: 'supervisor regular'
        },
        {
          name: 'callcenter sales agent'
        },
        {
          name: 'callcenter presales agent'
        },
        {
          name: 'callcenter presales&sales agent'
        },
        {
          name: 'callcenter teamlead'
        },
        {
          name: 'callcenter admin'
        },
        {
          name: 'backoffice admin'
        },
        {
          name: 'qc agent'
        },
        {
          name: 'qc teamlead'
        }
      ])
      .execute();

    await queryRunner.query(`COMMENT ON TABLE "security"."role" IS 'roles';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('security.role');
  }
}
