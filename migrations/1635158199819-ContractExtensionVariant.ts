import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class ContractExtensionVariant1635158199819
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        isNullable: true,
        name: 'extension_variant_sid',
        type: 'uuid'
      })
    );

    //Added extension_variant_sid
    await queryRunner.query(`
      WITH extensionVariants AS (
        SELECT csl.contract_sid, ev.sid as status_sid, MAX(csl.created_at) as created_at
        FROM contracts.contract_status_log as csl
        LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csl.status_sid
        LEFT JOIN contracts.extension_variant as ev ON ev.name = dcs.name
        WHERE ev.name = 'normalExtension'
        OR ev.name = 'earlyExtension'
        OR ev.name = 'cannotBeExtended'
        GROUP BY csl.contract_sid, ev.sid
        ORDER BY MAX(csl.created_at) DESC
       )
     UPDATE contracts.contract
     SET extension_variant_sid = extensionVariants.status_sid
     FROM extensionVariants
     WHERE contracts.contract.sid = extensionVariants.contract_sid;`);

    //Added extension variant log
    await queryRunner.query(`
       WITH extensionVariants AS (
        SELECT csl.contract_sid, ev.sid as status_sid, csl.created_at, csl.member_sid
        FROM contracts.contract_status_log as csl
        LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csl.status_sid
        LEFT JOIN contracts.extension_variant as ev ON ev.name = dcs.name
        WHERE ev.name = 'normalExtension'
        OR ev.name = 'earlyExtension'
        OR ev.name = 'cannotBeExtended'
      )
      INSERT INTO contracts.extension_variant_log (extension_variant_sid, contract_sid, member_sid)
      SELECT status_sid, contract_sid, member_sid
      FROM extensionVariants`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM contracts.extension_variant_log');

    await queryRunner.dropColumn(
      'contracts.contract',
      new TableColumn({
        isNullable: false,
        name: 'extension_variant_sid',
        type: 'uuid'
      })
    );
  }
}
