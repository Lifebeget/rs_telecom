import { MigrationInterface, QueryRunner } from 'typeorm';

export class WorkflowsCreateIndexTables1655718208665
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    create index contract_flow_status_flow_sid_index
    on workflows.contract_flow_status (flow_sid);

    create index contract_flow_status_contract_sid_index
        on workflows.contract_flow_status (contract_sid);

    create index contract_flow_status_team_sid_index
        on workflows.contract_flow_status (team_sid);

    create index contract_flow_status_member_sid_index
        on workflows.contract_flow_status (member_sid);

    create index contract_flow_status_status_sid_index
        on workflows.contract_flow_status (status_sid);

    create index contract_flow_status_type_sid_index
        on workflows.contract_flow_status (type_sid);

    create index contract_flow_status_deleted_at_index
        on workflows.contract_flow_status (deleted_at);

    create index contract_flow_status_created_at_index
        on workflows.contract_flow_status (created_at);

    create index contract_flow_status_is_deleted_index
        on workflows.contract_flow_status (is_deleted);

    create index contract_flow_status_contract_sid_status_sid_index
        on workflows.contract_flow_status (contract_sid, status_sid);

    create index contract_flow_status_contract_sid_type_sid_index
        on workflows.contract_flow_status (contract_sid, type_sid);


      create index contract_flow_contract_sid_index
      on workflows.contract_flow (contract_sid);

      create index contract_flow_is_archived_index
      on workflows.contract_flow (is_archived);

      create index contract_flow_created_at_index
      on workflows.contract_flow (created_at);

      
      create index contract_flow_team_flow_sid_index
      on workflows.contract_flow_team (flow_sid);

      create index contract_flow_team_team_sid_index
        on workflows.contract_flow_team (team_sid);

        create index contract_flow_member_flow_sid_index
    on workflows.contract_flow_member (flow_sid);

    create index contract_flow_member_member_sid_index
        on workflows.contract_flow_member (member_sid);


    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
