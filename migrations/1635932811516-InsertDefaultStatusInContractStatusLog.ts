import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertDefaultStatusInContractStatusLog1635932811516
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      WITH contracts AS (
        SELECT contract.sid AS contract_sid,
               (SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle') AS status_sid,
               contract.created_at AS created_at
        FROM contracts.contract
        WHERE NOT EXISTS(
          SELECT sid FROM contracts.contract_status_log WHERE contract_status_log.contract_sid = contract.sid
        )
      )
      INSERT INTO contracts.contract_status_log (contract_sid, status_sid, created_at)
      SELECT contract_sid, status_sid, created_at
      FROM contracts;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('not revertable');
  }
}
