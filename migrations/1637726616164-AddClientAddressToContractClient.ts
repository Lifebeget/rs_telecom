import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientAddressToContractClient1637726616164
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract_client', [
      new TableColumn({
        comment: 'client appeal (miss, mister, madam)',
        default: null,
        isNullable: true,
        name: 'client_address_appeal',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'client name',
        default: null,
        isNullable: true,
        name: 'client_address_first_name',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'client surname',
        default: null,
        isNullable: true,
        name: 'client_address_surname',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'address street',
        default: null,
        isNullable: true,
        name: 'client_address_street',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'address house',
        default: null,
        isNullable: true,
        name: 'client_address_house',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'address postcode',
        default: null,
        isNullable: true,
        name: 'client_address_postcode',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'address location',
        default: null,
        isNullable: true,
        name: 'client_address_location',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'address land',
        default: null,
        isNullable: true,
        name: 'client_address_land',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'email address',
        default: null,
        isNullable: true,
        name: 'client_address_email_address',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'email type',
        default: null,
        isNullable: true,
        name: 'client_address_email_type',
        type: 'character varying(255)'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
