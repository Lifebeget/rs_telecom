import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class addTeamsidInFingerprint1633478403559
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_fingerprint',
      new TableColumn({
        //потому что фингерпринты уже есть, но лочим в entity
        comment: 'team sid',

        isNullable: true,

        name: 'team_sid',
        type: 'uuid'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_fingerprint', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('contracts.contract_fingerprint', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.dropColumn('contracts.contract_fingerprint', 'team_sid');
  }
}
