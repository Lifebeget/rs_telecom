import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { ContractRequestStatus } from 'Constants';

export class TableContractRequestHistoryAddStatus1625733044816
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_request_history',
      new TableColumn({
        comment: 'request status',
        default: `'${ContractRequestStatus.Failure}'`,
        enum: EnumValues.getValues(ContractRequestStatus),
        isNullable: false,
        name: 'request_status',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract_request_history',
      'request_status'
    );
  }
}
