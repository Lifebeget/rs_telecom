import { EnumValues } from 'enum-values';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { ContractStatusType } from 'Constants/ContractStatusType';

export class ContractXStatus1635138129755 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'type status',
            enum: EnumValues.getValues(ContractStatusType),
            isNullable: false,
            name: 'type',
            type: 'enum'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          }
        ],
        name: 'contracts.contract_x_status'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_x_status', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_status'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createForeignKeys('contracts.contract_x_status', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_status'
      })
    ]);
    await queryRunner.dropTable('contracts.contract_x_status');
  }
}
