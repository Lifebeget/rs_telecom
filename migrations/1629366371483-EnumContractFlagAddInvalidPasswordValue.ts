import { MigrationInterface, QueryRunner } from 'typeorm';

const enumTypeName = 'contracts.contract_flag_enum';

export class EnumContractFlagAddInvalidPasswordValue1629366371483
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TYPE ${enumTypeName} ADD VALUE 'InvalidPassword'`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Not revertable');
  }
}
