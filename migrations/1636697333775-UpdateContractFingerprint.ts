import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateContractFingerprint1636697333775
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      UPDATE contracts.contract_fingerprint
      SET team_sid = (SELECT member.team_sid FROM teams.member WHERE contract_fingerprint.member_sid = member.sid)
      WHERE team_sid ISNULL;
    `);

    await queryRunner.query(`
      ALTER TABLE contracts.contract_fingerprint
        ALTER COLUMN team_sid SET NOT NULL;
    `);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    void _queryRunner;
  }
}
