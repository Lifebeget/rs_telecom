import { MigrationInterface, QueryRunner } from 'typeorm';

import { ActivityType, ActivityTypeMap } from 'Constants';
import {
  ActivityEntity,
  ActivityTypeEntity
} from 'Server/modules/activity/entities';

type ActivityTypes =
  | ActivityTypeMap['ContractAssigned']
  | ActivityTypeMap['ContractCreated']
  | ActivityTypeMap['ContractFlagAdded']
  | ActivityTypeMap['ContractFlagRemoved']
  | ActivityTypeMap['ContractNoteAdded']
  | ActivityTypeMap['ContractPasswordVerified']
  | ActivityTypeMap['ContractReturnedForClarification']
  | ActivityTypeMap['ContractStatusChanged']
  | ActivityTypeMap['ContractMoved']
  | ActivityTypeMap['ContractUpdatedXML']
  | ActivityTypeMap['LeadCreated']
  | ActivityTypeMap['ClientCreated'];

const activitiesNewDescriptions: Record<ActivityTypes, string> = {
  ClientCreated: 'client created',
  ContractAssigned: 'contract assigned',
  ContractCreated: 'contract created',
  ContractFlagAdded: 'contract flag added',
  ContractFlagRemoved: 'contract flag removed',
  ContractMoved: 'contract moved',
  ContractNoteAdded: 'contract note added',
  ContractPasswordVerified: 'contract password verified',
  ContractReturnedForClarification: 'contrat returned for clarification',
  ContractStatusChanged: 'contract status changed',
  ContractUpdatedXML: 'contract updated xml',
  LeadCreated: 'lead created'
};

export class ResetActivity1621414215016 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // delete all recorded activities
    await queryRunner.manager.delete(ActivityEntity, {});

    // delete previous types - will be replaced with the new one
    await queryRunner.manager.delete(ActivityTypeEntity, {});

    // drop payload tables linked to old activity types
    await queryRunner.dropTable('activities.activity_contract_fetched');

    // add new activity types
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ActivityTypeEntity, ['name', 'description'])
      .values(
        Object.entries(activitiesNewDescriptions).map(
          ([name, description]) => ({
            description,
            name: name as ActivityType
          })
        )
      )
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration cannot be reverted');
  }
}
