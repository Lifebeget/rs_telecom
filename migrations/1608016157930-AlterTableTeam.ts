import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique
} from 'typeorm';

import { TeamEntity } from 'Server/modules/team/entities';
import { insertTeam } from 'Server/utils';

export class AlterTableTeam1608016157930 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'team name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'team description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'team owner',
            isNullable: true,
            name: 'parent_team_sid',
            type: 'uuid'
          },
          {
            comment: 'can have contract flag',
            default: false,
            isNullable: false,
            name: 'can_have_contract_flg',
            type: 'boolean'
          },
          {
            comment: 'can have lead flag',
            default: false,
            isNullable: false,
            name: 'can_have_lead_flg',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.team'
      })
    );

    await queryRunner.createUniqueConstraint(
      'teams.team',
      new TableUnique({
        columnNames: ['name', 'parent_team_sid']
      })
    );

    await queryRunner.createForeignKey(
      'teams.team',
      new TableForeignKey({
        columnNames: ['parent_team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(TeamEntity, ['name'])
      .values([
        {
          name: 'Organization'
        }
      ])
      .returning('')
      .execute();

    await insertTeam(queryRunner.manager, 'Organization', [
      {
        name: 'Back office Alfa'
      },
      {
        name: 'Call center Alfa'
      }
    ]);

    await insertTeam(queryRunner.manager, 'Back office Alfa', [
      { name: 'Qc alfa' }
      // { name: 'Qc beta', canHaveContractFlg: true, canHaveLeadFlg: false }
    ]);

    // await insertTeam(queryRunner.manager, 'Call center Alfa', [
    //   { name: 'Presales', canHaveContractFlg: true, canHaveLeadFlg: true },
    //   { name: 'Sales', canHaveContractFlg: true, canHaveLeadFlg: true },
    //   { name: 'Presales/Sales', canHaveContractFlg: true, canHaveLeadFlg: true }
    // ]);

    await queryRunner.query(
      `COMMENT ON TABLE "teams"."team" IS 'team of members';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.team');
  }
}
