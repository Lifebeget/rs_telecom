import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddFingerprintNotifyType1647579898595
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE contracts.fingerprint_notify_type_enum AS ENUM ('Manual', 'Auto')`
    );

    await queryRunner.dropColumn(
      'contracts.fingerprint_notify_log',
      'notified_at'
    );

    await queryRunner.addColumn(
      'contracts.fingerprint_notify_log',
      new TableColumn({
        comment: 'notify type',
        isNullable: true,
        name: 'type',
        type: 'contracts.fingerprint_notify_type_enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('cannot be reverted');
  }
}
