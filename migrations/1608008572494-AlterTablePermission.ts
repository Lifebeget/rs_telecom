import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['viewClientCard']
  | PermissionMap['addTeamClient']
  | PermissionMap['addClient']
  | PermissionMap['addClientNote']
  | PermissionMap['removeClientNote']
  | PermissionMap['viewMyContracts']
  | PermissionMap['viewTeamContracts']
  | PermissionMap['updateContract']
  | PermissionMap['rejectContract']
  | PermissionMap['addMemberContract']
  | PermissionMap['viewContractChecklist']
  | PermissionMap['updateContractChecklist']
  | PermissionMap['viewContractIssues']
  | PermissionMap['updateContractIssues']
  | PermissionMap['editContractExtendRequest']
  | PermissionMap['viewMyLeads']
  | PermissionMap['viewTeamLeads']
  | PermissionMap['addLead']
  | PermissionMap['updateLead']
  | PermissionMap['addTeamLead']
  | PermissionMap['canValidateLeadBySMS']
  | PermissionMap['canValidateLeadByPassword']
  | PermissionMap['leadsImportWithPassword']
  | PermissionMap['leadsImport']
  | PermissionMap['viewTeamInfo']
  | PermissionMap['addTeamMember']
  | PermissionMap['removeTeamMember']
  | PermissionMap['addTeam']
  | PermissionMap['updateTeam']
  | PermissionMap['addTeamContract']
  | PermissionMap['viewRoleInfo']
  | PermissionMap['addRole']
  | PermissionMap['addRolePermission']
  | PermissionMap['removeRolePermission']
  | PermissionMap['addMemberRole']
  | PermissionMap['removeMemberRole']
  | PermissionMap['addTeamRole']
  | PermissionMap['removeTeamRole']
  | PermissionMap['viewUserList']
  | PermissionMap['addUser']
  | PermissionMap['updateUser']
  | PermissionMap['blockUser']
  | PermissionMap['updateUserEmail']
  | PermissionMap['editSupervisors']
  | PermissionMap['viewHardwareInfo']
  | PermissionMap['addHardware']
  | PermissionMap['viewTariffInfo']
  | PermissionMap['updateTariffCommission']
  | PermissionMap['addTariff']
  | PermissionMap['viewDashboard']
>;

export class AlterTablePermission1608008572494 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'permission name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'permission description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'security.permission'
      })
    );

    const permissions: Record<PermissionKeys, string> = {
      // clients
      [Permission.viewClientCard]: 'view client card',
      [Permission.addTeamClient]: "add a client to the current member's team",
      [Permission.addClient]: 'add a client to a team',
      [Permission.addClientNote]: 'add client note',
      [Permission.removeClientNote]: 'remove client note',

      // contracts
      [Permission.viewTeamContracts]: "view team's contract info",
      [Permission.viewMyContracts]: 'view my contract info',
      [Permission.updateContract]: 'can update a contract (status, member)',
      [Permission.rejectContract]: 'can reject a contract (notInterested)',
      [Permission.addMemberContract]: 'add a contract to member',
      [Permission.viewContractChecklist]: 'can view contract checklist',
      [Permission.updateContractChecklist]: 'can update contract checklist',
      [Permission.viewContractIssues]: 'can view contract issues',
      [Permission.updateContractIssues]: 'can update contract issues',
      [Permission.editContractExtendRequest]:
        'can edit contract extend request',

      // leads
      [Permission.viewTeamLeads]: "can view team's lead info",
      [Permission.viewMyLeads]: 'can view my lead info',
      [Permission.addLead]: 'can add a lead',
      [Permission.updateLead]: 'can update a lead (status, member)',
      [Permission.leadsImport]: 'can upload files without validation queue',
      [Permission.leadsImportWithPassword]:
        'can upload files with validation queue',
      [Permission.canValidateLeadBySMS]: 'can request lead sms validation',
      [Permission.canValidateLeadByPassword]:
        'can request lead password validation',

      // teams
      [Permission.addTeamLead]: 'can add a team to lead',
      [Permission.viewTeamInfo]: "view all available teams' names and members",
      [Permission.addTeamMember]: 'add members to all available teams',
      [Permission.removeTeamMember]: 'can remove user from team',
      [Permission.addTeam]: 'add new team as sub team of any available team',
      [Permission.updateTeam]: 'can edit team info',
      [Permission.addTeamContract]: 'can add a contract to team',

      // roles
      [Permission.viewRoleInfo]: 'view all roles',
      [Permission.addRole]: 'add new role',
      [Permission.addRolePermission]: 'add role permission',
      [Permission.removeRolePermission]: 'remove role permission',
      [Permission.addMemberRole]: 'can add roles than the member currently has',
      [Permission.removeMemberRole]:
        'can remove roles than the member currently has',
      [Permission.addTeamRole]: 'can add roles than the member currently has',
      [Permission.removeTeamRole]:
        'can remove roles than the member currently has',

      // users
      [Permission.viewUserList]: 'view all users list',
      [Permission.addUser]:
        'add new user as a member of any available team (addUser is useless without addTeamMember because now we must perform creating member when adding a new user)',
      [Permission.updateUser]: 'update user',
      [Permission.blockUser]: 'block all actions for the user',
      [Permission.updateUserEmail]: 'can reset',
      [Permission.editSupervisors]: 'can edit supervisors',

      // hardware
      [Permission.viewHardwareInfo]: 'view hardware info',
      [Permission.addHardware]: 'can add hardware',

      // tariff
      [Permission.viewTariffInfo]: 'view tariff info',
      [Permission.updateTariffCommission]: 'can edit tariff commission',
      [Permission.addTariff]: 'can add tariff',

      // dashboard
      [Permission.viewDashboard]: 'view dashboard'
    };

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "security"."permission" IS 'permissions';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('security.permission');
  }
}
