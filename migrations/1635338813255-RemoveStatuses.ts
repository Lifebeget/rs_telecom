import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveStatuses1635338813255 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'normalExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'earlyExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'cannotBeExtended'`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration cannot be reverted');
  }
}
