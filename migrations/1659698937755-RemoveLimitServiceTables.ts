import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveLimitServiceTables1659698937755
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('limits.global_limit');
    await queryRunner.dropTable('limits.member_limit');
    await queryRunner.dropTable('limits.team_limit');
    await queryRunner.dropSchema('limits');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration is not revertable!');
  }
}
