import { MigrationInterface, QueryRunner } from 'typeorm';

const enumTypeName = 'contracts.contract_flag_enum';

export class EnumContractFlagAddAppointmentInProgress1633299701910
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TYPE ${enumTypeName} ADD VALUE 'AppointmentInProgress'`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Not revertible');
  }
}
