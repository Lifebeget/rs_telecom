import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableMemberOrigin1608016520826 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'user_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          }
        ],
        name: 'teams.member_origin'
      })
    );

    await queryRunner.createPrimaryKey('teams.member_origin', [
      'user_sid',
      'team_sid'
    ]);

    await queryRunner.createForeignKeys('teams.member_origin', [
      new TableForeignKey({
        columnNames: ['user_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'users.user'
      }),

      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "teams"."member_origin" IS 'list of team member origin';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.member_origin');
  }
}
