import { MigrationInterface, QueryRunner, Table } from 'typeorm';

const tableName = 'contracts.contract_extend_request_x_bank_data';

export class AlterTableContractExtendRequestXContractBankData1626957589052
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'contract_extend_request_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_bank_data_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        foreignKeys: [
          {
            columnNames: ['contract_extend_request_sid'],
            referencedColumnNames: ['sid'],
            referencedTableName: 'contracts.contract_extend_request'
          },
          {
            columnNames: ['contract_bank_data_sid'],
            referencedColumnNames: ['sid'],
            referencedTableName: 'contracts.contract_bank_data'
          }
        ],
        name: tableName,
        uniques: [
          {
            columnNames: [
              'contract_extend_request_sid',
              'contract_bank_data_sid'
            ]
          }
        ]
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(tableName);
  }
}
