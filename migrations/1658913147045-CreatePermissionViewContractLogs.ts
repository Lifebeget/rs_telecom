import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<Permission, PermissionMap['viewContractLog']>;

const permission: Record<PermissionKeys, string> = {
  [Permission.viewContractLog]: 'view contract log'
};

export class CreatePermissionViewContractLogs1658913147045
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.viewContractLog
    ]);

    await insertPermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewContractLog
    ]);

    await insertPermission(queryRunner.manager, 'supervisor system', [
      Permission.viewContractLog
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.viewContractLog
    ]);

    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewContractLog
    ]);

    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.viewContractLog
    ]);
  }
}
