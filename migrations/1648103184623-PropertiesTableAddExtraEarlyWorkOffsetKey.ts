import { Duration } from 'luxon';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from 'Server/modules/properties/entities';
import { PropertyKeys } from 'Server/modules/properties/property-keys.const';

export class PropertiesTableAddExtraEarlyWorkOffsetKey1648103184623
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values({
        description:
          'Time before early or normal extension dates when special agent can work with the contract',
        key: PropertyKeys.extraEarlyWorkOffset,
        value: Duration.fromObject({ days: 3 }).toMillis().toString()
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PropertiesEntity)
      .where({ key: PropertyKeys.extraEarlyWorkOffset })
      .execute();
  }
}
