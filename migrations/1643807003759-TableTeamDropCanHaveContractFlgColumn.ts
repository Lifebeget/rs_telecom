import { MigrationInterface, QueryRunner } from 'typeorm';

export class TableTeamDropCanHaveContractFlgColumn1643807003759
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'can_have_contract_flg');
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
