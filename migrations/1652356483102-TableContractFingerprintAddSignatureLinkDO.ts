import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractFingerprintAddsignatureLink1652356483102
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_fingerprint',
      new TableColumn({
        comment: 'fingerprint signature path for digital ocean space',
        isNullable: true,
        name: 'signature_link',
        type: 'character varying(255)'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
