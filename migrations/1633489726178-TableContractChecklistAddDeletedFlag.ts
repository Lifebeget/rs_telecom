import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractChecklistAddDeletedFlag1633489726178
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract_checklist', [
      new TableColumn({
        comment: 'is deleted',
        default: false,
        isNullable: false,
        name: 'is_deleted',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'deleted date',
        isNullable: true,
        name: 'is_deleted_at',
        type: 'timestamp with time zone'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract_checklist', 'is_deleted');
    await queryRunner.dropColumn(
      'contracts.contract_checklist',
      'is_deleted_at'
    );
  }
}
