import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientStatus1625191213582 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'client status',
        default: null,
        isNullable: true,
        name: 'client_status',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'client_status');
  }
}
