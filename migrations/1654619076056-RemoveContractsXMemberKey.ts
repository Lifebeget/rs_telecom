import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveContractsXMemberKey1654619076056
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'contracts.contract_x_member',
      'FK_892101e7484cd38d26dbea331f1'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
