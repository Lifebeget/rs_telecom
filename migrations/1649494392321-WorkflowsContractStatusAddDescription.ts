import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class WorkflowsContractStatusAddDescription1649494392321
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'workflows.contract_status',
      new TableColumn({
        comment: 'Description status',
        default: null,
        isNullable: true,
        name: 'description',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('workflows.contract_status', 'description');
  }
}
