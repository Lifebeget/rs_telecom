import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivity1614247669671 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'activity_type_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'note_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'lead_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'activities.activity'
      })
    );

    await queryRunner.createForeignKeys('activities.activity', [
      new TableForeignKey({
        columnNames: ['activity_type_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.dict_activity_type'
      }),
      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      }),
      new TableForeignKey({
        columnNames: ['note_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client_note'
      }),
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['lead_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.lead'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity" IS 'activity';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity');
  }
}
