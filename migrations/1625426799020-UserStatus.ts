import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class UserStatus1625426799020 implements MigrationInterface {
  private tableName = 'users.user_status';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'user identifier',
            isNullable: false,
            name: 'user_sid',
            type: 'uuid'
          },
          {
            comment: 'Latest used client socket identifier',
            isNullable: true,
            length: '255',
            name: 'last_socket_id',
            type: 'varchar'
          },
          {
            comment: 'User status - identifies that he is currently online',
            default: false,
            isNullable: false,
            name: 'online',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: this.tableName
      })
    );

    await queryRunner.createForeignKey(
      this.tableName,
      new TableForeignKey({
        columnNames: ['user_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'users.user'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      this.tableName,
      new TableForeignKey({
        columnNames: ['user_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'users.user'
      })
    );
    await queryRunner.dropTable(this.tableName);
  }
}
