import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const column = new TableColumn({
  comment: 'can user approve or reject confirmation request',
  default: false,
  isNullable: false,
  name: 'can_have_confirmation_request',
  type: 'boolean'
});

export class TableClientRemoveCanHaveConfirmationFlagColumn1651740353115
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', column);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('users.user', column);
  }
}
