import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientMkek1625191225101 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'mkek',
        default: null,
        isNullable: true,
        name: 'mkek',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'mkek');
  }
}
