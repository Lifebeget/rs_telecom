import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { LeadStatus } from 'Constants';
import { LeadStatusEntity } from 'Server/modules/lead/entities/lead-status.entity';

export class AlterTableDictLeadStatus1611999482281
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'status name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'status description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'leads.dict_lead_status'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(LeadStatusEntity, ['name', 'description'])
      .values([
        {
          description: 'Draft lead',
          name: LeadStatus.draft
        },
        {
          description: 'Lead confirmed',
          name: LeadStatus.confirmed
        },
        {
          description: 'Invalid operator',
          name: LeadStatus.invalidOperator
        },
        {
          description: 'Invalid phone number',
          name: 'invalidPhoneNumber' as LeadStatus
        },
        {
          description: 'Client is not interested',
          name: LeadStatus.notInterested
        },
        {
          description: 'Phone password is wrong',
          name: LeadStatus.phonePasswordWrong
        },
        {
          description: 'Sms password is missing',
          name: LeadStatus.smsPasswordMissing
        },
        {
          description: 'Sms password is wrong',
          name: LeadStatus.smsPasswordWrong
        },
        {
          description: 'Client is unavailable',
          name: LeadStatus.notReached
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "leads"."dict_lead_status" IS 'lead status';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.dict_lead_status');
  }
}
