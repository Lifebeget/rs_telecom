import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

const tariffColumn = new TableColumn({
  comment: 'system tariff',
  default: null,
  isNullable: true,
  name: 'tariff_sid',
  type: 'uuid'
});

const tariffForeignKey = new TableForeignKey({
  columnNames: ['tariff_sid'],
  referencedColumnNames: ['sid'],
  referencedTableName: 'tariffs.tariff'
});

export class TableContractAddTariffColumn1623936056299
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('contracts.contract', tariffColumn);
    await queryRunner.createForeignKey('contracts.contract', tariffForeignKey);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('contracts.contract', tariffForeignKey);
    await queryRunner.dropColumn('contracts.contract', tariffColumn);
  }
}
