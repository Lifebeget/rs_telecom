import { MigrationInterface, QueryRunner } from 'typeorm';

export class ContractXStatusValues1635152524047 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    //System statuses
    await queryRunner.query(`
      WITH contractStatuses AS (
        SELECT contract.sid as contract_sid , 
        contract.status_sid as status_sid,      
        MAX(csl.created_at) as created_at
        FROM contracts.contract as contract
        LEFT JOIN contracts.contract_status_log as csl ON contract.sid = csl.contract_sid
        LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csl.status_sid
        WHERE dcs.name = 'QCConfirmation'
        OR dcs.name = 'CCClarification'
        OR dcs.name = 'QCConfirmed'
        GROUP BY contract.sid, contract.status_sid 
      )
    INSERT INTO contracts.contract_x_status (contract_sid, status_sid, type)
    SELECT contract_sid, status_sid, 'System' as type
    FROM contractStatuses`);

    //Behavioral statuses
    await queryRunner.query(`
     WITH contractFlags AS (
       SELECT cxf.contract_sid, cxf.flag, dcs.sid as status_sid
       FROM contracts.contract_x_flag as cxf
       LEFT JOIN contracts.dict_contract_status as dcs ON dcs.name = cxf.flag::varchar
       WHERE cxf.flag::varchar = 'NotInterested'
       OR cxf.flag::varchar = 'NotReached'
     )
   INSERT INTO contracts.contract_x_status (contract_sid, status_sid, type)
   SELECT contract_sid, status_sid, 'Behavioral' as type
   FROM contractFlags`);

    //Default migration
    await queryRunner.query(`WITH contractXSatus AS (
      SELECT contract.sid as contract_sid, dcs.sid as system_status_sid  FROM contracts.contract as contract
      LEFT JOIN contracts.contract_x_status as cxs  ON cxs.contract_sid = contract.sid and cxs.type::varchar = 'System'
      LEFT JOIN contracts.dict_contract_status as dcs ON dcs.name = 'Idle'
      WHERE cxs.status_sid IS NULL
      GROUP BY contract.sid, system_status_sid
    )
    INSERT INTO contracts.contract_x_status (contract_sid, status_sid, type)
    SELECT contract_sid, system_status_sid, 'System' as type
    FROM contractXSatus
    `);

    await queryRunner.query(`WITH contractXSatus AS (
      SELECT contract.sid as contract_sid, dcs.sid as system_status_sid, contract.created_at as contract_created_at
      FROM contracts.contract as contract
        LEFT JOIN contracts.contract_x_status as cxs ON cxs.contract_sid = contract.sid and cxs.type::varchar = 'System'
        LEFT JOIN contracts.dict_contract_status as dcs ON dcs.name = 'Idle'
      WHERE cxs.status_sid IS NULL
      GROUP BY contract.sid, system_status_sid, contract.created_at
    )
    INSERT INTO contracts.contract_status_log (contract_sid, status_sid, created_at)
    SELECT contract_sid, system_status_sid, contract_created_at
    FROM contractXSatus`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM contracts.contract_x_status');
  }
}
