import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { DistributionRequestStatus } from 'Constants';

export class TableDistributionRequestHistoryAddStatus1651149448921
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE contracts.distribution_request_history_status_enum AS ENUM ('Failure', 'Successful', 'Fetching')`
    );

    await queryRunner.addColumn(
      'contracts.distribution_request_history',
      new TableColumn({
        comment: 'request status',
        default: `'${DistributionRequestStatus.Successful}'`,
        isNullable: false,
        name: 'request_status',
        type: 'contracts.distribution_request_history_status_enum '
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.distribution_request_history',
      'request_status'
    );

    await queryRunner.query(
      `DROP TYPE contracts.distribution_request_history_status_enum`
    );
  }
}
