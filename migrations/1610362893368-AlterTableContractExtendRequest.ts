import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractExtendRequest1610362893368
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'tariff name',
            default: null,
            isNullable: true,
            name: 'tariff_name',
            type: 'character varying(255)'
          },
          {
            comment: 'tariff option',
            default: null,
            isNullable: true,
            name: 'tariff_option',
            type: 'character varying(255)'
          },
          {
            comment: 'email',
            default: null,
            isNullable: true,
            name: 'email',
            type: 'character varying(255)'
          },
          {
            isNullable: false,
            name: 'source_team_sid',
            type: 'uuid'
          },
          {
            isNullable: true,
            name: 'hardware_list_item_sid',
            type: 'uuid'
          },
          {
            isNullable: true,
            name: 'tariff_sid',
            type: 'uuid'
          },
          {
            comment: 'commission gs amount',
            default: null,
            isNullable: true,
            name: 'commission_gs_amount',
            type: 'real'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_extend_request'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_extend_request', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['source_team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['hardware_list_item_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'hardware.hardware_list_item'
      }),
      new TableForeignKey({
        columnNames: ['tariff_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_extend_request" IS 'contract extend request';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_extend_request');
  }
}
