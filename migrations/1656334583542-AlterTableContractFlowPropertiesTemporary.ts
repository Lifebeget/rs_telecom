import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractFlowPropertiesTemporary1656334583542
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          new TableColumn({
            comment: 'Flow sid',
            isNullable: false,
            isPrimary: true,
            name: 'flow_sid',
            type: 'uuid'
          }),
          new TableColumn({
            comment:
              'Marks if the flow had active pending status before completion',
            isNullable: false,
            name: 'has_not_deleted_pending_status',
            type: 'boolean'
          }),
          new TableColumn({
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }),
          new TableColumn({
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          })
        ],
        foreignKeys: [
          new TableForeignKey({
            columnNames: ['flow_sid'],
            referencedColumnNames: ['sid'],
            referencedTableName: 'workflows.contract_flow'
          })
        ],
        name: 'workflows.contract_flow_properties_temporary'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('workflows.contract_flow_properties_temporary');
  }
}
