import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTariffMakeCommissionNullable1625138147909
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'call_center_commission',
      new TableColumn({
        comment: 'tariff call center commission',
        default: null,
        isNullable: true,
        name: 'call_center_commission',
        type: 'real'
      })
    );
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'client_commission',
      new TableColumn({
        comment: 'tariff client commission',
        default: null,
        isNullable: true,
        name: 'client_commission',
        type: 'real'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'call_center_commission',
      new TableColumn({
        comment: 'tariff call center commission',
        isNullable: false,
        name: 'call_center_commission',
        type: 'real'
      })
    );
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'client_commission',
      new TableColumn({
        comment: 'tariff client commission',
        isNullable: false,
        name: 'client_commission',
        type: 'real'
      })
    );
  }
}
