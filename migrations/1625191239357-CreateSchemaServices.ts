import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaServices1625191239357 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('services');
    await queryRunner.query(
      `COMMENT ON SCHEMA "services" IS 'all service objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('services');
  }
}
