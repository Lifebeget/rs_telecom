import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableHardwareListItem1609833730818
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'hardware list',
            default: null,
            isNullable: true,
            name: 'hardware_list_sid',
            type: 'uuid'
          },
          {
            comment: 'hardware name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'hardware price',
            isNullable: false,
            name: 'price',
            type: 'real'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'hardware.hardware_list_item'
      })
    );

    await queryRunner.createForeignKey(
      'hardware.hardware_list_item',
      new TableForeignKey({
        columnNames: ['hardware_list_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'hardware.hardware_list'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "hardware"."hardware_list_item" IS 'hardware list item';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('hardware.hardware_list_item');
  }
}
