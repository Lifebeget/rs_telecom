import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAddPartnerCardFetchedInfoColumns1636026043087
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        default: false,
        isNullable: false,
        name: 'is_partner_cards_fetched',
        type: 'boolean'
      })
    );

    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        default: null,
        isNullable: true,
        name: 'partner_cards_fetched_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
