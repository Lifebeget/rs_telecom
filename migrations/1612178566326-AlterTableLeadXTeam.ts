import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableLeadXTeam1612178566326 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'lead_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'leads.lead_x_team'
      })
    );

    await queryRunner.createPrimaryKey('leads.lead_x_team', [
      'lead_sid',
      'team_sid'
    ]);

    await queryRunner.createForeignKeys('leads.lead_x_team', [
      new TableForeignKey({
        columnNames: ['lead_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.lead'
      }),

      new TableForeignKey({
        columnNames: ['team_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "leads"."lead_x_team" IS 'list lead and team';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.lead_x_team');
  }
}
