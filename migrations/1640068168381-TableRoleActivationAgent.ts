import { MigrationInterface, QueryRunner } from 'typeorm';

import { RoleEntity } from 'Server/modules/role/entities';

export class TableRoleActivationAgent1640068168381
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(RoleEntity, ['name', 'description'])
      .values([
        {
          description: 'activation agent role',
          name: 'activation agent'
        }
      ])
      .execute();

    await queryRunner.query(
      `ALTER TYPE teams.team_type_enum ADD VALUE 'Activation' AFTER 'Promotion'`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(RoleEntity)
      .where({ name: 'activation agent' })
      .execute();

    await queryRunner.query(
      `ALTER TYPE teams.team_type_enum DROP VALUE 'Activation'`
    );
  }
}
