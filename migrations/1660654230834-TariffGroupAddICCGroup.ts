import { MigrationInterface, QueryRunner } from 'typeorm';

import { TariffGroupName } from 'Constants';
import { TariffGroupEntity } from 'Server/modules/tariff/entities';

export class TariffGroupAddICCGroup1660654230834 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      new TariffGroupEntity({ name: TariffGroupName.ICC })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
