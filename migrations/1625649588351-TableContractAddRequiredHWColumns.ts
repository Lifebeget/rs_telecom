import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columns: TableColumn[] = [
  new TableColumn({
    default: false,
    name: 'is_hw_omitted',
    type: 'boolean'
  }),
  new TableColumn({
    default: null,
    isNullable: true,
    name: 'hw_required_cost',
    type: 'real'
  })
];

export class TableContractAddRequiredHWColumns1625649588351
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract', columns);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('contracts.contract', columns);
  }
}
