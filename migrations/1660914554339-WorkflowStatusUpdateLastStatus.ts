import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

export class WorkflowStatusUpdateLastStatus1660914554339
  implements MigrationInterface {
  async getLastStatusesByFlow(flowSid: string, manager: EntityManager) {
    const sql = `SELECT DISTINCT last_value(wcfs.sid) over (
      PARTITION BY (wcfs.type_sid, wcfs.flow_sid)
      order by wcfs.created_at ASC
      RANGE BETWEEN
          UNBOUNDED PRECEDING
          AND UNBOUNDED FOLLOWING
      ) as sid
  FROM workflows.contract_flow_status as wcfs
  LEFT JOIN LATERAL (SELECT wcf.sid, wcf.contract_sid, wcf.is_archived, wcf.created_at
    FROM workflows.contract_flow as wcf
    WHERE wcf.sid = '${flowSid}'
    LIMIT 1
    ) as wcf ON TRUE
  LEFT JOIN workflows.contract_status as wcs ON wcs.sid = wcfs.status_sid
  LEFT JOIN workflows.status_type as wst ON wst.sid = wcs.type_sid
  WHERE wcfs.flow_sid = wcf.sid AND wcf.sid = '${flowSid}'
  AND (CASE WHEN wst.name = 'Additional' THEN wcfs.is_deleted = false ELSE wcfs.is_deleted = true OR wcfs.is_deleted = false END)
AND 
( CASE WHEN wcf.is_archived = true THEN (
  CASE WHEN (
    SELECT 
      DISTINCT last_value(wcfs_in.status_sid) over (
        PARTITION BY wcfs_in.flow_sid 
        order by 
          wcfs_in.created_at ASC RANGE BETWEEN UNBOUNDED PRECEDING 
          AND UNBOUNDED FOLLOWING
      ) 
    FROM 
      workflows.contract_flow_status as wcfs_in 
    WHERE 
      wcfs_in.flow_sid = wcfs.flow_sid 
      AND wcfs_in.type_sid = 'ad347db9-882f-4b52-8889-1ca18ac34b78' -- System TypeSID
  ) = 'f0048478-fedc-4a90-afea-c87fcfeed48a'  -- IdleStatus SID
  THEN wcfs.status_sid != 'e092c260-e7e7-4cf8-908b-2f8f326277bf' -- NotReached sid 
  ELSE wcfs.status_sid != 'e092c260-e7e7-4cf8-908b-2f8f326277bf' -- NotReached sid 
  AND wcfs.status_sid != '085889e7-9db7-401f-a5db-ad5e0832b1de'  -- Pending status sid
  END ) 
  ELSE
  wcfs.is_deleted = false
  END
)`;

    return manager.query(sql);
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .query(`drop index workflows.contract_flow_status_contract_sid_status_sid_index;
    drop index workflows.contract_flow_status_contract_sid_type_sid_index;
    drop index workflows.contract_flow_status_flow_sid_type_sid_index;`);

    await queryRunner.manager.query(
      `UPDATE workflows.contract_flow SET is_archived = true::boolean WHERE sid = '7a493e9f-7944-4e1a-ad8a-824fdd8da656'::uuid`
    ); // 1 контракт такой
    const sql = `SELECT DISTINCT wcf.sid FROM workflows.contract_flow as wcf 
    LEFT JOIN workflows.contract_flow_status as wcfs ON wcfs.flow_sid = wcf.sid
    WHERE wcfs.sid IS NOT NULL AND wcf.is_archived = true`;
    const flows = await queryRunner.manager.query(sql);

    for await (const data of flows) {
      const flowSid = data['sid'] as string;
      const statuses = await this.getLastStatusesByFlow(
        flowSid,
        queryRunner.manager
      );
      if (statuses && statuses[0] && statuses[0]['sid']) {
        const statusesSid = (statuses.map(
          (el: { sid: string; status_sid: string }) => el.sid
        ) as unknown) as Array<string>;
        //Сначала все статусы архивируем
        await queryRunner.manager.query(
          `UPDATE workflows.contract_flow_status SET is_deleted = true::boolean WHERE flow_sid = '${flowSid}'::uuid`
        );

        const updatedSql = `UPDATE workflows.contract_flow_status SET is_deleted = false::boolean WHERE sid  = ANY ('{ ${statusesSid.join(
          ','
        )}}')`;
        await queryRunner.manager.query(updatedSql);
      } else {
        throw new Error('last statuses error');
      }
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
