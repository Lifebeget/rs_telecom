import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractFingerprintAddNoteColumn1654517024165
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_fingerprint',
      new TableColumn({
        comment: 'displayed note for the fingerprint',
        default: null,
        isNullable: true,
        name: 'note',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract_fingerprint', 'note');
  }
}
