import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractAssigned1621417428975
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'member to',
            isNullable: false,
            name: 'member_to_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_contract_assigned'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_assigned',
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_assigned',
      new TableForeignKey({
        columnNames: ['member_to_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_assigned" IS 'activity contract assigned';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_assigned');
  }
}
