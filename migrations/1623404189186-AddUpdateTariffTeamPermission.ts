import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

type PermissionKeys = Extract<Permission, PermissionMap['updateTariffTeam']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.updateTariffTeam]: 'update tariff for a team'
};

export class AddUpdateTariffTeamPermission1623404189186
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permissions) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permissions) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permissions) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permissions) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permissions)
      })
      .execute();
  }
}
