import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, Role } from 'Constants';

import { insertPermission, removePermission } from '../utils';

const roles: Role[] = [
  Role['supervisor root'],
  Role['supervisor system'],
  Role['supervisor regular'],
  Role['system'],
  Role['callcenter teamlead'],
  Role['qc teamlead'],
  Role['callcenter admin']
];

export class RemoveViewMyContractsPermissionFromLeadingRoles1650574175727
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, [
          Permission.viewMyContracts
        ]);
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, [
          Permission.viewMyContracts
        ]);
      })
    );
  }
}
