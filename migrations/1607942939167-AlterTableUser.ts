import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableUser1607942939167 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'user email',
            isNullable: false,
            isUnique: true,
            name: 'email',
            type: 'character varying(255)'
          },
          {
            comment: 'user password',
            default: null,
            isNullable: true,
            name: 'password',
            type: 'character varying(255)'
          },
          {
            comment: 'user first name',
            isNullable: false,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'user last name',
            isNullable: false,
            name: 'last_name',
            type: 'character varying(255)'
          },
          {
            comment: 'user middle name',
            default: null,
            isNullable: true,
            name: 'middle_name',
            type: 'character varying(255)'
          },
          {
            comment: 'user supervisor flag',
            default: false,
            isNullable: false,
            name: 'is_supervisor',
            type: 'boolean'
          },
          {
            comment: 'user block flag',
            default: false,
            isNullable: false,
            name: 'is_blocked_flg',
            type: 'boolean'
          },
          {
            comment: 'user invalidate flag',
            default: false,
            isNullable: false,
            name: 'is_invalidated_flg',
            type: 'boolean'
          },
          {
            comment: 'user verified email flag',
            default: false,
            isNullable: false,
            name: 'is_verified_email_flg',
            type: 'boolean'
          },
          {
            comment: 'last login date',
            default: null,
            isNullable: true,
            name: 'last_login_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'blocked date',
            default: null,
            isNullable: true,
            name: 'blocked_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'validated user date',
            default: null,
            isNullable: true,
            name: 'validated_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'verified email date',
            default: null,
            isNullable: true,
            name: 'verified_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'users.user'
      })
    );

    await queryRunner.query(`COMMENT ON TABLE "users"."user" IS 'users';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('users.user');
  }
}
