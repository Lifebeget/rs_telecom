import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientAdv1625191217868 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'adv',
        default: null,
        isNullable: true,
        name: 'adv',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'adv');
  }
}
