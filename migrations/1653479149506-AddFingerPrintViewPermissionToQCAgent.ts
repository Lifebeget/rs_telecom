import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission } from 'Constants';

export class AddFingerPrintViewPermissionToQCAgent1653479149506
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintView
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintView
    ]);
  }
}
