import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class IsVerifiedPhoneColumn1625231343421 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'User phone has been verified',
        default: false,
        isNullable: false,
        name: 'is_verified_phone_flg',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', 'is_verified_phone_flg');
  }
}
