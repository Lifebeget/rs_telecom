import { MigrationInterface, QueryRunner, TableUnique } from 'typeorm';

export class TableMemberXRoleAddUnique1647934077940
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createUniqueConstraint(
      'teams.member_x_role',
      new TableUnique({
        columnNames: ['member_sid']
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'teams.member_x_role',
      new TableUnique({
        columnNames: ['member_sid']
      })
    );
  }
}
