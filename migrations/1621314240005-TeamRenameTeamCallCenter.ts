import { MigrationInterface, QueryRunner } from 'typeorm';

import { TeamEntity } from 'Server/modules/team/entities';

export class TeamRenameTeamCallCenter1621314240005
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update(TeamEntity, { name: 'DC Callcenter' })
      .where('name = :name', { name: 'Call center Alfa' })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update(TeamEntity, { name: 'Call center Alfa' })
      .where('name = :name', { name: 'DC Callcenter' })
      .execute();
  }
}
