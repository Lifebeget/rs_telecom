import { MigrationInterface, QueryRunner } from 'typeorm';

import { TeamEntity } from 'Server/modules/team/entities';
import {
  ContractStatusEntity,
  StatusVisibilityTeamEntity
} from 'Server/modules/workflow/entities';

export class WorkflowsAddedVisibilitiesInTeam1653934046341
  implements MigrationInterface {
  async insertTeamVisibilityStatus(
    teams: TeamEntity[],
    statusSid: string,
    queryRunner: QueryRunner
  ) {
    await Promise.all(
      teams.map(async el => {
        await queryRunner.manager.insert(StatusVisibilityTeamEntity, {
          statusSid,
          teamSid: el.sid
        });
      })
    );
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const statuses = await queryRunner.manager
      .createQueryBuilder(ContractStatusEntity, 'status')
      .getMany();
    const teams = await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .getMany();
    await Promise.all(
      statuses.map(async status => {
        await this.insertTeamVisibilityStatus(teams, status.sid, queryRunner);
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
