import { MigrationInterface, QueryRunner, TableUnique } from 'typeorm';

export class TableHardwareChangeNameColumn1635318848903
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createUniqueConstraint(
      'hardware.hardware_list_item',
      new TableUnique({
        columnNames: ['name']
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'hardware.hardware_list_item',
      new TableUnique({
        columnNames: ['name']
      })
    );
  }
}
