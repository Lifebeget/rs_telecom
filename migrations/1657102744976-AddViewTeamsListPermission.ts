import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission } from 'Server/utils';

type PermissionKeys = Extract<Permission, PermissionMap['viewTeamsList']>;

const permission: Record<PermissionKeys, string> = {
  [Permission.viewTeamsList]: 'view teams list'
};

export class AddViewTeamsListPermission1657102744976
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );

    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );

    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration is not revertable');
  }
}
