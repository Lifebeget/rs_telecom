import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientStars1625191215109 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'client stars',
        default: null,
        isNullable: true,
        name: 'client_stars',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'client_stars');
  }
}
