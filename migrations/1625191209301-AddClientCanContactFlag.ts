import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientCanContactFlag1625191209301
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('clients.client', [
      new TableColumn({
        comment: 'can contact with client by phone',
        default: null,
        isNullable: true,
        name: 'can_contact_by_phone',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'can contact with client by other type',
        default: null,
        isNullable: true,
        name: 'can_contact_by_other_type',
        type: 'boolean'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'can_contact_by_phone');
    await queryRunner.dropColumn('clients.client', 'can_contact_by_other_type');
  }
}
