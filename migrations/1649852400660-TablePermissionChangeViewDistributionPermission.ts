import { keys } from 'ramda';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission, PermissionMap, Role } from 'Constants';

type PermissionKeys = Extract<Permission, PermissionMap['viewDistributor']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.viewDistributor]: 'view distributor'
};

const roles: Role[] = [
  'supervisor system',
  'supervisor root',
  'supervisor regular',
  'activation agent'
];

export class TablePermissionChangeViewDistributionPermission1649852400660
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // remove old permission bindings
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, keys(permissions));
      })
    );

    // add new bindings
    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
