import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveStatusLog1635338807739 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    WITH contractStatusLog AS (
     SELECT csl.sid
     FROM contracts.contract_status_log as csl
     LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csl.status_sid
     WHERE 
     (
      dcs.name = 'normalExtension'
      OR dcs.name = 'earlyExtension'
      OR dcs.name = 'cannotBeExtended'
     )
   )
    DELETE FROM contracts.contract_status_log as ccsl
    WHERE ccsl.sid = (SELECt sid FROM contractStatusLog WHERE contractStatusLog.sid = ccsl.sid)
 `);

    //Эти уже данные есть в extension_variant
    await queryRunner.query(`
    WITH contractXStatus AS (
      SELECT cxs.status_sid
      FROM contracts.contract_x_status as cxs
      LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = cxs.status_sid
      WHERE
      (
      dcs.name = 'normalExtension'
      OR dcs.name = 'earlyExtension'
      OR dcs.name = 'cannotBeExtended'
      )
    )
    DELETE FROM contracts.contract_x_status as ccxs
    WHERE ccxs.status_sid = (SELECt status_sid FROM contractXStatus WHERE contractXStatus.status_sid = ccxs.status_sid)
  `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'normalExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'earlyExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'cannotBeExtended'`
    );
  }
}
