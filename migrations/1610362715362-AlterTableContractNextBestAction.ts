import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique
} from 'typeorm';

export class AlterTableContractNextBestAction1610362715362
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'vodofone id',
            default: null,
            isNullable: true,
            name: 'ban_id',
            type: 'character varying(255)'
          },
          {
            comment: 'phone number',
            default: null,
            isNullable: true,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            isNullable: false,
            name: 'nba_tip_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_next_best_action'
      })
    );

    await queryRunner.createUniqueConstraint(
      'contracts.contract_next_best_action',
      new TableUnique({
        columnNames: ['contract_sid', 'nba_tip_sid']
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_next_best_action', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['nba_tip_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_next_best_action_tip'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_next_best_action" IS 'contract next best action';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_next_best_action');
  }
}
