import { MigrationInterface, QueryRunner } from 'typeorm';

export class NewStatusDraft1661771821539 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .query(`INSERT INTO workflows.contract_status (sid, name, type_sid, is_deleted, created_at, deleted_at, description) 
      VALUES ('27f0c885-6154-4a73-968b-068b98aab212'::uuid, 'Draft'::varchar(255), 'ad347db9-882f-4b52-8889-1ca18ac34b78'::uuid, false::boolean, DEFAULT, null::timestamp with time zone, 'Status Draft: Default status for contracts that have never been in progress'::text)`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
