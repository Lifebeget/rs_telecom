import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class DictSortAddSpecificSortColumns1655357684898
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.dict_sort',
      new TableColumn({
        comment: 'name of specific column',
        isNullable: true,
        name: 'specific_column_value',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('cannot be reverted');
  }
}
