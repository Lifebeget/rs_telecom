import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemovingFlags1635256633003 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM contracts.contract_x_flag as cxf WHERE cxf.flag::varchar = 'NotInterested' OR cxf.flag::varchar = 'NotReached' `
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration cannot be reverted');
  }
}
