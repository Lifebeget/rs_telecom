import { MigrationInterface, QueryRunner } from 'typeorm';

export class ArchiveAppointmentsForContractsNotInProgress1661867634515
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // archive all appointments for contracts with no assignments
    await queryRunner.query(`
        with contract_with_no_assignments as (
          select contract.sid from contracts.contract
          left join contracts.assignment on assignment.contract_sid = contract.sid
          group by contract.sid
          having count(assignment.sid) filter (where assignment.is_deleted = false) = 0
        ),
        contract_with_appointments as (
          select distinct(contract.sid) from contracts.contract
          left join contracts.contract_appointment appointment on appointment.contract_sid = contract.sid
          where appointment.sid is not null and appointment.is_archived_flg = false
        )
        update contracts.contract_appointment app
        set is_archived_flg = true
        from contracts.contract_appointment appointment
        left join contract_with_no_assignments on contract_with_no_assignments.sid = appointment.contract_sid 
        left join contract_with_appointments on contract_with_appointments.sid = appointment.contract_sid 
        where app.sid = appointment.sid
        and contract_with_no_assignments.sid is not null
        and contract_with_appointments.sid is not null 
        and appointment.is_archived_flg = false
      `);

    // remove all appointment flags for contracts with no active assignments
    // copy from migrations RemoveInvalidContractAppointmetFlags1660818032832
    await queryRunner.query(
      `
        with contract_with_invalid_appointment_flag as (
          select 
            contract.sid as contract_sid 
          from 
            contracts.contract 
            left join contracts.contract_x_flag ON contract_x_flag.contract_sid = contract.sid 
            left join contracts.contract_appointment ON contract_appointment.contract_sid = contract.sid 
            AND contract_appointment.is_archived_flg = false 
          where 
            contract_x_flag.flag :: text like 'Appoint%' 
          group by 
            contract.sid, 
            contract_x_flag.flag 
          having 
            count(contract_appointment.sid) = 0
        ) 
        delete from 
          contracts.contract_x_flag 
        where 
          contract_x_flag.contract_sid in (
            select 
              contract_sid 
            from 
              contract_with_invalid_appointment_flag
          ) 
          and (
            contract_x_flag.flag = 'Appointed' 
            or contract_x_flag.flag = 'AppointmentInProgress'
          )
        `
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
