import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientCallEvaluation1625191221009
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'call evaluation',
        default: null,
        isNullable: true,
        name: 'call_evaluation',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'call_evaluation');
  }
}
