import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';
import { EnumValues } from 'enum-values';

import { LeadRequestStatus } from '../../Constants';

export class AlterTableLeadRequestHistory1661107710993
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'method sid',
            isNullable: false,
            name: 'method_sid',
            type: 'uuid'
          },
          {
            comment: 'phone number',
            isNullable: false,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'password',
            default: null,
            isNullable: true,
            name: 'password',
            type: 'character varying(255)'
          },
          {
            comment: 'member sid',
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'lead sid',
            isNullable: false,
            name: 'lead_sid',
            type: 'uuid'
          },
          {
            comment: 'request content',
            isNullable: false,
            name: 'request_content',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'request status',
            enum: EnumValues.getValues(LeadRequestStatus),
            isNullable: false,
            name: 'request_status',
            type: 'enum'
          }
        ],
        name: 'leads.lead_request_history'
      })
    );

    await queryRunner.createForeignKeys('leads.lead_request_history', [
      new TableForeignKey({
        columnNames: ['method_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.dict_request_method'
      }),

      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),
      new TableForeignKey({
        columnNames: ['lead_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.lead'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "leads"."lead_request_history" IS 'lead request history';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.lead_request_history');
  }
}
