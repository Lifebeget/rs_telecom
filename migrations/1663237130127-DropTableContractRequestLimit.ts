import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropTableContractRequestLimit1663237130127
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.password_request_limit');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration in not revertable');
  }
}
