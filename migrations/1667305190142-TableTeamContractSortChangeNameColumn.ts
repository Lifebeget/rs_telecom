import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTeamContractSortChangeNameColumn1667305190142
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM teams.team_contract_sort');

    await queryRunner.changeColumn(
      'teams.team_contract_sort',
      'name',
      new TableColumn({
        comment: 'sort name',
        isNullable: false,
        isPrimary: true,
        name: 'name',
        type: 'contracts.contract_sort_name_type_enum'
      })
    );

    await queryRunner.changeColumn(
      'teams.team_contract_sort',
      'team_sid',
      new TableColumn({
        isNullable: false,
        isPrimary: true,
        name: 'team_sid',
        type: 'uuid'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration is not revertable');
  }
}
