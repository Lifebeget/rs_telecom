import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class TableContractPasswordLimit1651822177162
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Contract sid',
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'User snapshot',
            isNullable: false,
            name: 'snapshot',
            type: 'text'
          },
          {
            comment: 'Entry attempt',
            isNullable: false,
            name: 'attempt',
            type: 'int'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },

          {
            comment: 'Expired date',
            isNullable: false,
            name: 'expired_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.password_request_limit'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.password_request_limit',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.password_request_limit');
  }
}
