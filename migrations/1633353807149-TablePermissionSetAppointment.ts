import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { PermissionEntity } from 'Server/modules/role/entities';
import { Permission, PermissionMap } from 'Constants';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['viewMyAppointments']
  | PermissionMap['viewTeamAppointments']
  | PermissionMap['viewContractAppointments']
  | PermissionMap['addAppointment']
  | PermissionMap['updateAppointment']
  | PermissionMap['deleteAppointment']
>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.viewMyAppointments]: 'view my appointments',
  [Permission.viewTeamAppointments]: 'view team appointments',
  [Permission.viewContractAppointments]: 'view contract appointments',
  [Permission.addAppointment]: 'add appointment',
  [Permission.updateAppointment]: 'update appointment',
  [Permission.deleteAppointment]: 'delete appointment'
};

const allRoles = [
  'supervisor system',
  'supervisor root',
  'supervisor regular',
  'callcenter sales agent',
  'callcenter presales agent',
  'callcenter presales&sales agent',
  'callcenter teamlead',
  'callcenter admin',
  'qc agent',
  'qc teamlead'
];

const viewTeamAppointmentsRoles = [
  'supervisor system',
  'supervisor root',
  'supervisor regular',
  'callcenter teamlead',
  'callcenter admin',
  'qc teamlead'
];

const viewContractAppointmentsRoles = [
  'supervisor system',
  'supervisor root',
  'supervisor regular'
];

export class TablePermissionSetAppointment1633353807149
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await Promise.all(
      allRoles.map(async role => {
        await insertPermission(queryRunner.manager, role, [
          Permission.viewMyAppointments,
          Permission.addAppointment,
          Permission.updateAppointment,
          Permission.deleteAppointment
        ]);
      })
    );

    await Promise.all(
      viewTeamAppointmentsRoles.map(async role => {
        await insertPermission(queryRunner.manager, role, [
          Permission.viewTeamAppointments
        ]);
      })
    );

    await Promise.all(
      viewContractAppointmentsRoles.map(async role => {
        await insertPermission(queryRunner.manager, role, [
          Permission.viewContractAppointments
        ]);
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      allRoles.map(async role => {
        await removePermission(queryRunner.manager, role, [
          Permission.viewMyAppointments
        ]);
      })
    );

    await Promise.all(
      viewTeamAppointmentsRoles.map(async role => {
        await removePermission(queryRunner.manager, role, [
          Permission.viewTeamAppointments
        ]);
      })
    );

    await Promise.all(
      viewContractAppointmentsRoles.map(async role => {
        await removePermission(queryRunner.manager, role, [
          Permission.viewContractAppointments
        ]);
      })
    );
  }
}
