import { MigrationInterface, QueryRunner } from 'typeorm';

const enumName = 'contracts.contract_request_history_request_status_enum';

export class TableContractRequestHistoryExtendRequestStatusColumn1633346055475
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TYPE ${enumName} ADD VALUE 'Fetching'
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('not revertable');
  }
}
