import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class RemoveViewLeadsPermissionFromQCTeamleads1658996822552
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.viewTeamLeads
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.viewTeamLeads
    ]);
  }
}
