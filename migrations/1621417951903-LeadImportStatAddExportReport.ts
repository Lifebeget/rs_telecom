import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class LeadImportStatAddExportReport1621417951903
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'leads.lead_import_stat',
      'report_name',
      new TableColumn({
        comment: 'leads import report file name',
        default: null,
        isNullable: true,
        name: 'report_file_id',
        type: 'character varying(255)'
      })
    );

    await queryRunner.addColumn(
      'leads.lead_import_stat',
      new TableColumn({
        comment: 'leads import export file name',
        default: null,
        isNullable: true,
        name: 'export_file_id',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameColumn(
      'leads.lead_import_stat',
      'report_file_id',
      new TableColumn({
        comment: 'leads import report name',
        default: null,
        isNullable: true,
        name: 'report_name',
        type: 'character varying(255)'
      })
    );

    await queryRunner.dropColumn('leads.lead_import_stat', 'export_file_id');
  }
}
