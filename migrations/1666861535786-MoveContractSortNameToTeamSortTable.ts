import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableUnique
} from 'typeorm';

export class MoveContractSortNameToTeamSortTable1666861535786
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE contracts.contract_sort_name_type_enum AS ENUM ('Easy', 'Import', 'Pending')`
    );

    await queryRunner.addColumn(
      'teams.team_x_contract_sort',
      new TableColumn({
        comment: 'sort name',
        isNullable: true,
        name: 'name',
        type: 'contracts.contract_sort_name_type_enum'
      })
    );

    await queryRunner.changeColumn(
      'teams.team_x_contract_sort',
      'team_sid',
      new TableColumn({
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    );

    await queryRunner.dropForeignKey(
      'teams.team_x_contract_sort',
      'FK_b102e69cbca265ecda247a9cbcf'
    );

    await queryRunner.dropColumn(
      'teams.team_x_contract_sort',
      new TableColumn({
        isNullable: false,
        name: 'sort_sid',
        type: 'uuid'
      })
    );

    await queryRunner.manager.query(
      `ALTER TABLE "teams"."team_x_contract_sort" RENAME TO "team_contract_sort";`
    );

    await queryRunner.createUniqueConstraint(
      'teams.team_contract_sort',
      new TableUnique({
        columnNames: ['team_sid', 'name']
      })
    );

    await queryRunner.dropTable('contracts.dict_sort');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration is irrevertable');
  }
}
