import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractExtendRequestColumnHwPriceChangeDefault1638366734833
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'contracts.contract_extend_request',
      // old column presented to highlight change difference
      new TableColumn({
        comment: 'fixed price of hw at the moment of choosing',
        default: 0,
        isNullable: true,
        name: 'hw_price',
        type: 'real'
      }),
      new TableColumn({
        // changed field
        comment: 'fixed price of hw at the moment of choosing',

        default: null,

        isNullable: true,

        name: 'hw_price',
        type: 'real'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
