import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTariff1609833733818 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'tariff list',
            default: null,
            isNullable: true,
            name: 'tariff_list_sid',
            type: 'uuid'
          },
          {
            comment: 'tariff name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'tariff price',
            isNullable: false,
            name: 'price',
            type: 'real'
          },
          {
            comment: 'tariff call center commission',
            isNullable: false,
            name: 'call_center_commission',
            type: 'real'
          },
          {
            comment: 'tariff client commission',
            isNullable: false,
            name: 'client_commission',
            type: 'real'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'tariffs.tariff'
      })
    );

    await queryRunner.createForeignKey(
      'tariffs.tariff',
      new TableForeignKey({
        columnNames: ['tariff_list_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff_list'
      })
    );

    await queryRunner.query(`COMMENT ON TABLE "tariffs"."tariff" IS 'tariff';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff');
  }
}
