import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission } from 'Server/utils';

type PermissionKeys = Extract<Permission, PermissionMap['changeContractAgent']>;

const permission: Record<PermissionKeys, string> = {
  [Permission.changeContractAgent]: 'view client info'
};

export class AddNewPermissionChangeAgentOnContract1666263452161
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'callcenter teamlead',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('This migration is irrevertable');
  }
}
