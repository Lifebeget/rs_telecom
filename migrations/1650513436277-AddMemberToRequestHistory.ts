import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddMemberToRequestHistory1650513436277
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_request_history',
      new TableColumn({
        isNullable: true,
        name: 'member_sid',
        type: 'uuid'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_request_history',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(`
      UPDATE contracts.contract_request_history
      SET member_sid = (SELECT member.sid
                        FROM teams.member
                                 LEFT JOIN users."user" ON member.user_sid = "user".sid
                        WHERE "user".is_system = TRUE
                        LIMIT 1);
    `);

    await queryRunner.changeColumn(
      'contracts.contract_request_history',
      'member_sid',
      new TableColumn({
        isNullable: false,
        name: 'member_sid',
        type: 'uuid'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
