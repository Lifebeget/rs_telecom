import { Duration } from 'luxon';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from 'Server/modules/properties/entities';
import { PropertyKeys } from 'Server/modules/properties/property-keys.const';

export class TablePropertiesAddPendingafterNotReachedDelay1656916182103
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values({
        description:
          'Set pending status since time in milliseconds passed after not reached',
        key: PropertyKeys.pendingAfterNotReachedDelay,
        value: Duration.fromObject({ hours: 2 }).toMillis().toString()
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PropertiesEntity)
      .where({ key: PropertyKeys.pendingAfterNotReachedDelay })
      .execute();
  }
}
