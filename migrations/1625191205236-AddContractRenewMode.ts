import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { CanBeRenewed } from 'Constants';

export class AddContractRenewMode1625191205236 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'contract renew mode',
        default: null,
        enum: EnumValues.getValues(CanBeRenewed),
        isNullable: true,
        name: 'renew_mode',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'renew_mode');
  }
}
