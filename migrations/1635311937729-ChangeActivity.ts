import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class ChangeActivity1635311937729 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'extension variant from',
            isNullable: false,
            name: 'exchange_variant_from_sid',
            type: 'uuid'
          },
          {
            comment: 'extension variant to',
            isNullable: false,
            name: 'exchange_variant_to_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_extension_variant_changed'
      })
    );

    await queryRunner.createForeignKeys(
      'activities.activity_extension_variant_changed',
      [
        new TableForeignKey({
          columnNames: ['activity_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'activities.activity'
        }),
        new TableForeignKey({
          columnNames: ['exchange_variant_from_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.extension_variant'
        }),
        new TableForeignKey({
          columnNames: ['exchange_variant_to_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.extension_variant'
        })
      ]
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_extension_variant_changed" IS 'activity contract extension variant changed';`
    );

    //Move Statuses From and to
    await queryRunner.query(`
      WITH contractActivity AS (
        SELECT csc.activity_sid, ev.sid as status_from_sid, ev2.sid as status_to_sid
        FROM activities.activity_contract_status_changed as csc
          LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csc.status_from_sid
          LEFT JOIN contracts.dict_contract_status as dcs2 ON dcs2.sid = csc.status_to_sid
          LEFT JOIN contracts.extension_variant as ev ON ev.name = dcs.name
          LEFT JOIN contracts.extension_variant as ev2 ON ev2.name = dcs2.name
        WHERE (
          dcs.name = 'normalExtension'
          OR dcs.name = 'earlyExtension'
          OR dcs.name = 'cannotBeExtended'
        ) 
        AND (
          dcs2.name = 'normalExtension'
          OR dcs2.name = 'earlyExtension'
          OR dcs2.name = 'cannotBeExtended'
        )
      )
      INSERT INTO activities.activity_extension_variant_changed (activity_sid, exchange_variant_from_sid, exchange_variant_to_sid)
      SELECT activity_sid, status_from_sid, status_to_sid
      FROM contractActivity;
    `);

    //Delete from and to
    await queryRunner.query(`
      WITH contractActivity AS (
        SELECT csc.activity_sid, csc.status_from_sid, csc.status_to_sid
        FROM activities.activity_contract_status_changed as csc
          LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csc.status_from_sid
          LEFT JOIN contracts.dict_contract_status as dcs2 ON dcs2.sid = csc.status_to_sid
        WHERE (
          dcs.name = 'normalExtension'
          OR dcs.name = 'earlyExtension'
          OR dcs.name = 'cannotBeExtended'
        ) AND (
          dcs2.name = 'normalExtension'
          OR dcs2.name = 'earlyExtension'
          OR dcs2.name = 'cannotBeExtended'
        )
      )
      DELETE FROM activities.activity_contract_status_changed as acsc  
      WHERE acsc.activity_sid  = (SELECT activity_sid FROM contractActivity WHERE acsc.activity_sid = contractActivity.activity_sid);
    `);

    //Move Statuses only From
    await queryRunner.query(`
      WITH contractActivity AS (
        SELECT csc.activity_sid, csc.status_from_sid, csc.status_to_sid, dcs3.sid as default_status
        FROM activities.activity_contract_status_changed as csc
          LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csc.status_from_sid
          LEFT JOIN contracts.dict_contract_status as dcs2 ON dcs2.sid = csc.status_to_sid
          LEFT JOIN contracts.dict_contract_status as dcs3 ON dcs3.name = 'Idle'
        WHERE (
         dcs.name = 'normalExtension'
         OR dcs.name = 'earlyExtension'
         OR dcs.name = 'cannotBeExtended' 
        ) AND (
         dcs2.name = 'QCConfirmation'
         OR dcs2.name = 'CCClarification'
         OR dcs2.name = 'QCConfirmed'
        )
      )
      UPDATE activities.activity_contract_status_changed 
      SET activity_sid = contractActivity.activity_sid,
          status_from_sid = contractActivity.default_status,
          status_to_sid = contractActivity.status_to_sid
      FROM contractActivity
      WHERE activities.activity_contract_status_changed.activity_sid = contractActivity.activity_sid;
    `);

    //Move Statuses only TO
    await queryRunner.query(`
      WITH contractActivity AS (
        SELECT csc.activity_sid, csc.status_from_sid, csc.status_to_sid, dcs3.sid as default_status
        FROM activities.activity_contract_status_changed as csc
          LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = csc.status_from_sid
          LEFT JOIN contracts.dict_contract_status as dcs2 ON dcs2.sid = csc.status_to_sid
          LEFT JOIN contracts.dict_contract_status as dcs3 ON dcs3.name = 'Idle'
        WHERE (
          dcs.name = 'QCConfirmation'
          OR dcs.name = 'CCClarification'
          OR dcs.name = 'QCConfirmed'
        ) AND (
          dcs2.name = 'normalExtension'
          OR dcs2.name = 'earlyExtension'
          OR dcs2.name = 'cannotBeExtended'
        ) 
      )
      UPDATE activities.activity_contract_status_changed
      SET activity_sid = contractActivity.activity_sid,
          status_from_sid = contractActivity.status_from_sid,
          status_to_sid = contractActivity.default_status
      FROM contractActivity
      WHERE activities.activity_contract_status_changed.activity_sid = contractActivity.activity_sid;
    `);

    //Updste activity_contract_created
    await queryRunner.query(`
      WITH contractActivity AS (
        SELECT acc.activity_sid, acc.status_sid, dcs2.sid as default_status
        FROM activities.activity_contract_created as acc
          LEFT JOIN contracts.dict_contract_status as dcs ON dcs.sid = acc.status_sid
          LEFT JOIN contracts.dict_contract_status as dcs2 ON dcs2.name = 'Idle'
        WHERE (
          dcs.name = 'normalExtension'
          OR dcs.name = 'earlyExtension'
          OR dcs.name = 'cannotBeExtended'
        )
      )
      UPDATE activities.activity_contract_created
      SET status_sid = contractActivity.default_status
      FROM contractActivity
      WHERE activities.activity_contract_created.activity_sid = contractActivity.activity_sid;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // throw new Error(
    //   "Oops, unable to execute 'down'.  All subsequent reverse operations are not possible"
    // );

    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'normalExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'earlyExtension'`
    );
    await queryRunner.query(
      `DELETE FROM contracts.dict_contract_status WHERE name = 'cannotBeExtended'`
    );
  }
}
