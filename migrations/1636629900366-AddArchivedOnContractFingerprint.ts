import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddArchivedOnContractFingerprint1636629900366
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract_fingerprint', [
      new TableColumn({
        comment: 'is fingerprint archived',
        default: false,
        isNullable: false,
        name: 'is_archived',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'date fingerprint archived',
        default: null,
        isNullable: true,
        name: 'is_archived_at',
        type: 'timestamp with time zone'
      })
    ]);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    void _queryRunner;
  }
}
