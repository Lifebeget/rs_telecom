import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractStatusEntity } from '../modules/workflow/entities';
import {
  ContractStatusesStatsDefault,
  contractStatusesStatsDefault
} from '../modules/contract/constants';

export class AddColumnWorkflowContractStatus1662986430171
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'workflows.contract_status',
      new TableColumn({
        comment: 'has statistic',
        default: false,
        name: 'has_statistic',
        type: 'boolean'
      })
    );

    const contractStatus = await queryRunner.manager.query(`
        select * from workflows.contract_status`);

    await Promise.all(
      contractStatus.map(
        (contractStatus: {
          name: ContractStatusesStatsDefault;
          sid: string;
        }) => {
          const name: ContractStatusesStatsDefault = contractStatus.name;
          if (contractStatusesStatsDefault.includes(name)) {
            return queryRunner.manager.update(
              ContractStatusEntity,
              contractStatus.sid,
              { hasStatistic: true }
            );
          }
        }
      )
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
