import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableClientRank1625191227756 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            comment: 'current rank',
            default: null,
            isNullable: true,
            name: 'current_rank',
            type: 'character varying(255)'
          },
          {
            comment: 'current rank points',
            default: null,
            isNullable: true,
            name: 'points',
            type: 'character varying(255)'
          },
          {
            comment: 'next rank',
            default: null,
            isNullable: true,
            name: 'next_rank',
            type: 'character varying(255)'
          },
          {
            comment: 'start rank date',
            default: null,
            isNullable: true,
            name: 'started_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'end rank date',
            default: null,
            isNullable: true,
            name: 'ended_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'clients.client_rank'
      })
    );

    await queryRunner.createForeignKey(
      'clients.client_rank',
      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "clients"."client_rank" IS 'client rank';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clients.client_rank');
  }
}
