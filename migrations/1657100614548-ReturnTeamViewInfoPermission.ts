import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class ReturnTeamViewInfoPermission1657100614548
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.viewTeamInfo
    ]);

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.viewTeamInfo
    ]);

    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.viewTeamInfo
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.viewTeamInfo
    ]);

    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.viewTeamInfo
    ]);

    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.viewTeamInfo
    ]);
  }
}
