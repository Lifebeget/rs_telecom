import { keys } from 'ramda';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission, PermissionMap, Role } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

type PermissionKeys = Extract<Permission, PermissionMap['fetchDistributor']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.fetchDistributor]: 'fetch distributor'
};

const roles: Role[] = [
  'supervisor system',
  'supervisor root',
  'supervisor regular'
];

export class TablePermissionAddFetchDistributorPermission1649849639118
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }
}
