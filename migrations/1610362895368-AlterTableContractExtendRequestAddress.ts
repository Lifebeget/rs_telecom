import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractExtendRequestAddress1610362895368
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'contract_extend_request_sid',
            type: 'uuid'
          },
          {
            comment: 'client appeal (miss, mister, madam)',
            default: null,
            isNullable: true,
            name: 'appeal',
            type: 'character varying(255)'
          },
          {
            comment: 'client name',
            default: null,
            isNullable: true,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'client surname',
            default: null,
            isNullable: true,
            name: 'surname',
            type: 'character varying(255)'
          },
          {
            comment: 'address street',
            default: null,
            isNullable: true,
            name: 'street',
            type: 'character varying(255)'
          },
          {
            comment: 'address house',
            default: null,
            isNullable: true,
            name: 'house',
            type: 'character varying(255)'
          },
          {
            comment: 'address postcode',
            default: null,
            isNullable: true,
            name: 'postcode',
            type: 'character varying(255)'
          },
          {
            comment: 'address location',
            default: null,
            isNullable: true,
            name: 'location',
            type: 'character varying(255)'
          },
          {
            comment: 'address land',
            default: null,
            isNullable: true,
            name: 'land',
            type: 'character varying(255)'
          },
          {
            comment: 'email address',
            default: null,
            isNullable: true,
            name: 'email_address',
            type: 'character varying(255)'
          },
          {
            comment: 'email type',
            default: null,
            isNullable: true,
            name: 'email_type',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_extend_request_address'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_extend_request_address',
      new TableForeignKey({
        columnNames: ['contract_extend_request_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_extend_request'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_extend_request_address" IS 'contract extend request address';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_extend_request_address');
  }
}
