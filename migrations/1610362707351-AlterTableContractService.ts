import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractService1610362707351
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'name',
            default: null,
            isNullable: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'code',
            default: null,
            isNullable: true,
            name: 'code',
            type: 'character varying(255)'
          },
          {
            comment: 'removable',
            default: null,
            isNullable: true,
            name: 'removable',
            type: 'boolean'
          },
          {
            comment: 'unremovable message',
            default: null,
            isNullable: true,
            name: 'unremovable_message',
            type: 'character varying(255)'
          },
          {
            comment: 'link mod',
            default: null,
            isNullable: true,
            name: 'link_mod',
            type: 'character varying(255)'
          },
          {
            comment: 'link',
            default: null,
            isNullable: true,
            name: 'link',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_service'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_service',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_service" IS 'contract service list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_service');
  }
}
