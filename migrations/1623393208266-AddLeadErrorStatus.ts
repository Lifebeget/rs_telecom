import { MigrationInterface, QueryRunner } from 'typeorm';

import { LeadStatus } from 'Constants';
import { LeadStatusEntity } from 'Server/modules/lead/entities';

export class AddLeadErrorStatus1623393208266 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(LeadStatusEntity, ['name', 'description'])
      .values({
        description: 'Lead validation error',
        name: LeadStatus.error
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(LeadStatusEntity)
      .where('name = :name', { name: LeadStatus.error })
      .execute();
  }
}
