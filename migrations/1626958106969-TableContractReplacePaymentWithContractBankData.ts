import { times } from 'remeda';
import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

import {
  ContractBankDataEntity,
  ContractEntity
} from 'Server/modules/contract/entities';

const tableName = 'contracts.contract';
const bankDataColumnName = 'contract_bank_data_sid';

export class TableContractReplacePaymentWithContractBankData1626958106969
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      tableName,
      new TableColumn({
        isNullable: true,
        isUnique: true,
        name: bankDataColumnName,
        type: 'uuid' // nullable is temporary, before payment copying
      })
    );

    await queryRunner.createForeignKey(
      tableName,
      new TableForeignKey({
        columnNames: ['contract_bank_data_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_bank_data'
      })
    );

    type PaymentKey =
      | 'contract_sid'
      | 'payment_method'
      | 'iban'
      | 'bic'
      | 'account_number'
      | 'bank_code'
      | 'bank_name'
      | 'owner_name'
      | 'created_at';

    const contractPayments: Record<
      PaymentKey,
      /* eslint-disable @typescript-eslint/no-explicit-any -- constructing an utility type */
      any
    >[] = await queryRunner.manager
      .createQueryBuilder()
      .select('*')
      .from('contracts.contract_payment', 'payment')
      .execute();

    const contractBankDataSids = await Promise.all(
      contractPayments.map(
        ({
          account_number,
          bank_code,
          bank_name,
          bic,
          iban,
          owner_name,
          payment_method,
          created_at
        }) =>
          queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(ContractBankDataEntity)
            .values({
              accountNumber: account_number,
              bankCode: bank_code,
              bankName: bank_name,
              bic,
              createdAt: created_at,
              iban,
              ownerName: owner_name,
              paymentMethod: payment_method
            })
            .returning('sid')
            .execute()
      )
    );

    await Promise.all(
      times(contractPayments.length, i => i).map(i => {
        const { contract_sid } = contractPayments[i];
        const [{ sid }] = contractBankDataSids[i].raw;

        return queryRunner.manager.update(
          ContractEntity,
          { sid: contract_sid },
          { bankData: sid }
        );
      })
    );

    await queryRunner.changeColumn(
      tableName,
      bankDataColumnName,
      new TableColumn({
        isNullable: false,
        isUnique: true,
        name: bankDataColumnName,
        type: 'uuid'
      })
    );

    await queryRunner.dropTable('contracts.contract_payment');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Not revertable'); // it could be but not realized
  }
}
