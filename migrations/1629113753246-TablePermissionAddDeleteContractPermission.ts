import { keys } from 'ramda';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<Permission, PermissionMap['deleteContract']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.deleteContract]: 'delete a contract'
};

export class TablePermissionAddDeleteContractPermission1629113753246
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      keys(permissions)
    );

    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      keys(permissions)
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      keys(permissions)
    );
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      keys(permissions)
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permissions)
      })
      .execute();
  }
}
