import { MigrationInterface, QueryRunner } from 'typeorm';

export class WorkflowsCreateIndes1657571550368 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(
      `create index contract_status_name_index on workflows.contract_status (name)`
    );
    await queryRunner.manager.query(
      `create index contract_status_type_sid_index on workflows.contract_status (type_sid)`
    );
    await queryRunner.manager.query(
      `create index contract_flow_team_is_deleted_sid_index on workflows.contract_flow_team (is_deleted)`
    );
    await queryRunner.manager.query(
      `create index request_contract_extend_request_index_is_deleted on contracts.contract_extend_request (is_deleted);`
    );

    await queryRunner.manager.query(
      `create index contract_contract_phone_number_search_index on contracts.contract (lower(replace(phone_number, ' ', '')) varchar_pattern_ops);`
    );

    await queryRunner.manager.query(
      `create index clients_client_first_name_index_search on clients.client (lower(first_name) varchar_pattern_ops);`
    );

    await queryRunner.manager.query(
      `create index clients_client_surname_index_search on clients.client (lower(surname) varchar_pattern_ops);`
    );
    await queryRunner.manager
      .query(`create index clients_client_surname_first_name_concat_index on clients.client ((surname || ' ' || first_name));
      create index clients_client_first_name_surname_concat_index on clients.client ((first_name || ' ' || surname ));
      create index clients_client_surname_first_name_concat_lower_index on clients.client ((lower(surname) || ' ' || lower(first_name)));
      create index clients_client_first_name_surname_concat_lower_index on clients.client ((lower(first_name) || ' ' || lower(surname) ));`);

    await queryRunner.manager
      .query(`create index contract_flow_status_flow_sid_type_sid_index
      on workflows.contract_flow_status (flow_sid, type_sid);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
