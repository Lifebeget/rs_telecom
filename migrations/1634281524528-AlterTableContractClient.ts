import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableContractClient1634281524528
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'contract identification',
            isNullable: false,
            isPrimary: true,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'client appeal (miss, mister, madam)',
            default: null,
            isNullable: true,
            name: 'appeal',
            type: 'character varying(255)'
          },
          {
            comment: 'client name',
            isNullable: false,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'client surname',
            isNullable: false,
            name: 'surname',
            type: 'character varying(255)'
          },
          {
            comment: 'client birthday',
            default: null,
            isNullable: true,
            name: 'birthday',
            type: 'timestamp with time zone'
          },
          {
            comment: 'phone number',
            default: null,
            isNullable: true,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'vodofone id',
            default: null,
            isNullable: true,
            name: 'ban_id',
            type: 'character varying(255)'
          },
          {
            comment: 'ban password',
            default: null,
            isNullable: true,
            name: 'ban_password',
            type: 'character varying(255)'
          },
          {
            comment: 'vo field',
            default: null,
            isNullable: true,
            name: 'vo_field',
            type: 'character varying(255)'
          },
          {
            comment: 'seller number (VOID)',
            default: null,
            isNullable: true,
            name: 'current_seller_id',
            type: 'character varying(255)'
          },
          {
            comment: 'ban of contract renewal',
            default: null,
            isNullable: true,
            name: 'black_listed_flg',
            type: 'boolean'
          },
          {
            comment: 'date ban of contract renewal',
            default: null,
            isNullable: true,
            name: 'black_listed_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'id from third-party system',
            default: null,
            isNullable: true,
            name: 'ancor_id',
            type: 'character varying(255)'
          },
          {
            comment: 'phone number ndc code',
            default: null,
            isNullable: true,
            name: 'ndc',
            type: 'character varying(255)'
          },
          {
            comment: 'phone number msisdn code',
            default: null,
            isNullable: true,
            name: 'msisdn',
            type: 'character varying(255)'
          },
          {
            comment: 'can contact with client by phone',
            default: null,
            isNullable: true,
            name: 'can_contact_by_phone',
            type: 'boolean'
          },
          {
            comment: 'can contact with client by other type',
            default: null,
            isNullable: true,
            name: 'can_contact_by_other_type',
            type: 'boolean'
          },
          {
            comment: 'client status',
            default: null,
            isNullable: true,
            name: 'client_status',
            type: 'character varying(255)'
          },
          {
            comment: 'client stars',
            default: null,
            isNullable: true,
            name: 'client_stars',
            type: 'character varying(255)'
          },
          {
            comment: 'adv',
            default: null,
            isNullable: true,
            name: 'adv',
            type: 'character varying(255)'
          },
          {
            comment: 'person evaluation',
            default: null,
            isNullable: true,
            name: 'person_evaluation',
            type: 'character varying(255)'
          },
          {
            comment: 'call evaluation',
            default: null,
            isNullable: true,
            name: 'call_evaluation',
            type: 'character varying(255)'
          },
          {
            comment: 'account type',
            default: null,
            isNullable: true,
            name: 'account_type',
            type: 'character varying(255)'
          },
          {
            comment: 'mkek',
            default: null,
            isNullable: true,
            name: 'mkek',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_client'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_client');
  }
}
