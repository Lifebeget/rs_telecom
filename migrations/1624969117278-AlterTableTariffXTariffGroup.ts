import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTariffXTariffGroup1624969117278
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'tariff_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'tariff_group_sid',
            type: 'uuid'
          },
          {
            comment: 'override tariff call center commission',
            default: null,
            isNullable: true,
            name: 'call_center_commission',
            type: 'real'
          },
          {
            comment: 'override tariff client commission',
            default: null,
            isNullable: true,
            name: 'client_commission',
            type: 'real'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'tariffs.tariff_x_tariff_group'
      })
    );

    await queryRunner.createForeignKeys('tariffs.tariff_x_tariff_group', [
      new TableForeignKey({
        columnNames: ['tariff_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff'
      }),
      new TableForeignKey({
        columnNames: ['tariff_group_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff_group'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "tariffs"."tariff_x_tariff_group" IS 'list tariff and tariff group';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff_x_tariff_group');
  }
}
