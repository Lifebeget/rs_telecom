import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdateShopSchedule1629138949565 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`TRUNCATE shops.shop_schedule`);

    await queryRunner.addColumns('shops.shop_schedule', [
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'promoter_schedule_sid',
        type: 'uuid'
      }),
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'shop_schedule_state_sid',
        type: 'uuid'
      }),
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'day_sid',
        type: 'uuid'
      })
    ]);

    await queryRunner.dropColumn('shops.shop_schedule', 'member_sid');
    await queryRunner.dropColumn('shops.shop_schedule', 'time_block_sid');
    await queryRunner.dropColumn('shops.shop_schedule', 'is_canceled_flg');
    await queryRunner.dropColumn('shops.shop_schedule', 'is_canceled_at');
    await queryRunner.dropColumn('shops.shop_schedule', 'worked_at');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('shops.shop_schedule', [
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'promoter_schedule_sid',
        type: 'uuid'
      }),
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'shop_schedule_state_sid',
        type: 'uuid'
      }),
      new TableColumn({
        default: null,
        isNullable: false,
        name: 'day_sid',
        type: 'uuid'
      })
    ]);

    await queryRunner.addColumns('shop.shop_schedule', [
      new TableColumn({
        default: null,
        isNullable: true,
        name: 'time_block_sid',
        type: 'uuid'
      }),
      new TableColumn({
        comment: 'is canceled schedule',
        default: false,
        isNullable: false,
        name: 'is_canceled_flg',
        type: 'boolean'
      }),

      new TableColumn({
        comment: 'canceled date',
        default: null,
        isNullable: true,
        name: 'is_canceled_at',
        type: 'timestamp with time zone'
      }),
      new TableColumn({
        comment: 'work date',
        default: null,
        isNullable: true,
        name: 'worked_at',
        type: 'timestamp with time zone'
      }),
      new TableColumn({
        isNullable: false,
        name: 'member_sid',
        type: 'uuid'
      })
    ]);
  }
}
