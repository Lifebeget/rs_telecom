import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission } from 'Server/utils';

export class AddCreateUserPermissionToCCAdmin1638945298208
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.addTeamMember,
      Permission.viewUserList,
      Permission.getUserOnline
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
