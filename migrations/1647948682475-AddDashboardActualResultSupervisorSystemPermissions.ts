import { MigrationInterface, QueryRunner } from 'typeorm';

import { insertPermission, removePermission } from 'Server/utils';
import { Permission } from 'Constants';

export class AddDashboardActualResultSupervisorSystemPermissions1647948682475
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'supervisor system', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);
  }
}
