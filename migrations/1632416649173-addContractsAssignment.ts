import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class addContractsAssignment1632416649173 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'contracts.contract identifier',
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'Member identifier',
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'is deleted flag',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: null,
            isNullable: true,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.assignment'
      })
    );

    await queryRunner.createForeignKeys('contracts.assignment', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('contracts.assignment', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);
    await queryRunner.dropTable('contracts.assignment');
  }
}
