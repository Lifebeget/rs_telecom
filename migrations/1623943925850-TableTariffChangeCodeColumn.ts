import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTariffChangeCodeColumn1623943925850
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'code',
      new TableColumn({
        comment: 'tariff code',
        default: null,
        isNullable: true,
        isUnique: true,
        name: 'code',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'tariffs.tariff',
      'code',
      new TableColumn({
        comment: 'tariff code',
        default: "''",
        isNullable: false,
        name: 'code',
        type: 'character varying(255)'
      })
    );
  }
}
