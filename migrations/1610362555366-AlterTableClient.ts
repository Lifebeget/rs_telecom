import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableClient1610362555366 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'client appeal (miss, mister, madam)',
            default: null,
            isNullable: true,
            name: 'appeal',
            type: 'character varying(255)'
          },
          {
            comment: 'client name',
            isNullable: false,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'client surname',
            isNullable: false,
            name: 'surname',
            type: 'character varying(255)'
          },
          {
            comment: 'client birthday',
            default: null,
            isNullable: true,
            name: 'birthday',
            type: 'timestamp with time zone'
          },
          {
            comment: 'phone number',
            default: null,
            isNullable: true,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'vodofone id',
            default: null,
            isNullable: true,
            name: 'ban_id',
            type: 'character varying(255)'
          },
          {
            comment: 'vo field',
            default: null,
            isNullable: true,
            name: 'vo_field',
            type: 'character varying(255)'
          },
          {
            comment: 'seller number (VOID)',
            default: null,
            isNullable: true,
            name: 'current_seller_id',
            type: 'character varying(255)'
          },
          {
            comment: 'ban of contract renewal',
            default: null,
            isNullable: true,
            name: 'black_listed_flg',
            type: 'boolean'
          },
          {
            comment: 'date ban of contract renewal',
            default: null,
            isNullable: true,
            name: 'black_listed_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'id a third-party system',
            default: null,
            isNullable: true,
            name: 'ancor_id',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'clients.client'
      })
    );

    await queryRunner.query(`COMMENT ON TABLE "clients"."client" IS 'client';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clients.client');
  }
}
