import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class TableActivityAddMemberColumn1620977116959
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'activities.activity',
      new TableColumn({
        isNullable: true,
        name: 'member_sid',
        type: 'uuid'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'activities.activity',
      new TableColumn({
        isNullable: true,
        name: 'member_sid',
        type: 'uuid'
      })
    );

    await queryRunner.dropForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );
  }
}
