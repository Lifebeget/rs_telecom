import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTariffAddUpdatedAtColumn1657792820974
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'tariffs.tariff',
      new TableColumn({
        comment: 'update date',
        default: 'now()',
        isNullable: false,
        name: 'updated_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('tariffs.tariff', 'updated_at');
  }
}
