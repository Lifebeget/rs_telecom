import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { ChecklistItemState } from 'Constants';
import { ContractChecklistStatusEntity } from 'Server/modules/contract/entities';

export class AlterTableDictContractCheckListStatus1611999446281
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'type name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'type description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_contract_checklist_status'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ContractChecklistStatusEntity, ['name', 'description'])
      .values([
        {
          description: 'undefined item of check list',
          name: ChecklistItemState.Undefined
        },
        {
          description: 'fulfilled item of check list',
          name: ChecklistItemState.Fulfilled
        },
        {
          description: 'unfulfilled item of check list',
          name: ChecklistItemState.Unfulfilled
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_contract_checklist_status" IS 'list of contract check status';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.dict_contract_checklist_status');
  }
}
