import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddColumnCreatingContractsWithAPassword1660419882927
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'Creating contracts with a password',
        default: false,
        name: 'creating_contracts_with_a_password',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
