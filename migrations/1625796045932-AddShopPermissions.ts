import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity, RoleEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['addShop']
  | PermissionMap['updateShop']
  | PermissionMap['removeShop']
  | PermissionMap['addMemberToShop']
  | PermissionMap['removeMemberToShop']
  | PermissionMap['viewTeamShop']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.addShop]: 'can create shop',
  [Permission.updateShop]: 'can update shop info',
  [Permission.removeShop]: 'can remove shop',
  [Permission.addMemberToShop]: 'can add member to shop',
  [Permission.removeMemberToShop]: 'can remove member to shop',
  [Permission.viewTeamShop]: 'can view only team shop'
};

export class AddShopPermissions1625716421585 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(RoleEntity, ['name'])
      .values([
        {
          name: 'booking team leader'
        },
        {
          name: 'booking manager'
        },
        {
          name: 'booking vf manager'
        },
        {
          name: 'booking promouter'
        }
      ])
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking team leader',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking manager',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking vf manager',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking promouter',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking team leader',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking manager',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking vf manager',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking promouter',
      Object.keys(permission) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(RoleEntity)
      .where('name IN (:...names)', {
        names: [
          'booking team leader',
          'booking manager',
          'booking vf manager',
          'booking promouter'
        ]
      })
      .execute();
  }
}
