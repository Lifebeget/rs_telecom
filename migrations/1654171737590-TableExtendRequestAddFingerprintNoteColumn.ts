import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableExtendRequestAddFingerprintNoteColumn1654171737590
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_extend_request',
      new TableColumn({
        comment: 'note that will be added to the next sent fingerprint',
        default: null,
        isNullable: true,
        name: 'fingerprint_note',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract_extend_request',
      'fingerprint_note'
    );
  }
}
