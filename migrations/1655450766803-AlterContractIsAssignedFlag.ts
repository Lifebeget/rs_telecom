import { MigrationInterface, QueryRunner } from 'typeorm';

export class AlterContractIsAssignedFlag1655450766803
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE contracts.contract DROP COLUMN IF EXISTS is_assigned'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
