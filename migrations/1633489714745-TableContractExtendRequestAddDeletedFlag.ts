import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractExtendRequestAddDeletedFlag1633489714745
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract_extend_request', [
      new TableColumn({
        comment: 'is deleted',
        default: false,
        isNullable: false,
        name: 'is_deleted',
        type: 'boolean'
      }),
      new TableColumn({
        comment: 'deleted date',
        isNullable: true,
        name: 'is_deleted_at',
        type: 'timestamp with time zone'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract_extend_request',
      'is_deleted'
    );
    await queryRunner.dropColumn(
      'contracts.contract_extend_request',
      'is_deleted_at'
    );
  }
}
