import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class AddPermissioToSendFpToCCTeamleadsAdmins1661753209779
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintEdit
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintEdit
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintEdit
    ]);

    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintEdit
    ]);
  }
}
