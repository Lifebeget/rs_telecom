import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAddProjectId1622797963011
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'project id',
        default: null,
        isNullable: true,
        name: 'project_id',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'project_id');
  }
}
