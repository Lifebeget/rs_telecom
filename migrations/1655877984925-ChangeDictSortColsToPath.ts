import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class ChangeDictSortColsToPath1655877984925
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.dict_sort',
      new TableColumn({
        comment: 'path to table column',
        isNullable: true,
        name: 'column_path',
        type: 'text'
      })
    );

    await queryRunner.query(`
      UPDATE contracts.dict_sort as cds
      SET column_path = CONCAT('contract.' , cds.column_name)
      `);

    await queryRunner.dropColumn(
      'contracts.dict_sort',
      new TableColumn({
        comment: 'name of column in contracts contract',
        default: `''`,
        isNullable: false,
        name: 'column_name',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is not revertable');
  }
}
