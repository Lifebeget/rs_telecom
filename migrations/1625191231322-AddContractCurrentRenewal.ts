import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractCurrentRenewal1625191231322
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'contract current renewal',
        default: null,
        isNullable: true,
        name: 'current_renewal',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'current_renewal');
  }
}
