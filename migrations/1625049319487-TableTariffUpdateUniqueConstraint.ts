import { MigrationInterface, QueryRunner, TableUnique } from 'typeorm';

export class TableTariffUpdateUniqueConstraint1625049319487
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'tariffs.tariff',
      'UQ_3adebae7745541f68e6d11a7518'
    );

    await queryRunner.createUniqueConstraint(
      'tariffs.tariff',
      new TableUnique({
        columnNames: ['code', 'tariff_list_sid']
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'tariffs.tariff',
      'UQ_360e4acd9f9fab830d0289803ea'
    );

    await queryRunner.createUniqueConstraint(
      'tariffs.tariff',
      new TableUnique({
        columnNames: ['code']
      })
    );
  }
}
