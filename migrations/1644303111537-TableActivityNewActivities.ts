import { MigrationInterface, QueryRunner } from 'typeorm';

import { ActivityType } from 'Constants';
import { ActivityTypeEntity } from 'Server/modules/activity/entities';

export class TableActivityNewActivities1644303111537
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ActivityTypeEntity, ['name', 'description'])
      .values([
        {
          description: 'The password of the contract is wrong',
          name: ActivityType.ContractWrongPassword
        },
        {
          description: 'The password of the contract has been updated',
          name: ActivityType.ContractUpdatedPassword
        }
      ])
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
