import { MigrationInterface, QueryRunner } from 'typeorm';

export class TruncateContractPhoneNumber1634284713210
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('truncate contracts.contract_phone_number');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
