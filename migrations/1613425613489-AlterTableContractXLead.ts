import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractXLead1613425613489
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'lead_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_x_lead'
      })
    );

    await queryRunner.createPrimaryKey('contracts.contract_x_lead', [
      'contract_sid',
      'lead_sid'
    ]);

    await queryRunner.createForeignKeys('contracts.contract_x_lead', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),

      new TableForeignKey({
        columnNames: ['lead_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.lead'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_x_lead" IS 'list contract and lead';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_x_lead');
  }
}
