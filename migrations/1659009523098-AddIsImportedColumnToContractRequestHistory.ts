import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsImportedColumnToContractRequestHistory1659009523098
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_request_history',
      new TableColumn({
        comment: 'is contract import request',
        default: false,
        isNullable: false,
        name: 'is_imported',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is not revertable');
  }
}
