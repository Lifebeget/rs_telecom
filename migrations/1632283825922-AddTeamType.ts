import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { TeamType } from 'Constants';
import { TeamEntity } from 'Server/modules/team/entities';

export class AddTeamType1632283825922 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE teams.team_type_enum AS ENUM ('Organization', 'BackOffice', 'CC', 'QC', 'Promotion')`
    );

    await queryRunner.addColumn(
      'teams.team',
      new TableColumn({
        comment: 'team type',
        isNullable: true,
        name: 'type',
        type: 'teams.team_type_enum'
      })
    );

    // by default type is call center
    await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .update(TeamEntity)
      .set({
        type: TeamType.CC
      })
      .execute();

    // next let's set the organization
    await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .update(TeamEntity)
      .set({
        type: TeamType.Organization
      })
      .where(`team.name = 'Organization'`)
      .execute();

    // now backoffice!
    await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .update(TeamEntity)
      .set({
        type: TeamType.BackOffice
      })
      .where(`team.name = 'Back office Alfa'`)
      .execute();

    // after that we are taking the QC
    await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .update(TeamEntity)
      .set({
        type: TeamType.QC
      })
      .where(`team.name = 'Qc alfa'`)
      .execute();

    // promotion group..
    await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .update(TeamEntity)
      .set({
        type: TeamType.Promotion
      })
      .where(`team.name = 'Via Sales'`)
      .execute();

    // and callcenters are callcenters - nothing to change
    // DC Callcenter and Kiev

    await queryRunner.changeColumn(
      'teams.team',
      'type',
      new TableColumn({
        comment: 'team type',
        isNullable: false,
        name: 'type',
        type: 'teams.team_type_enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TYPE teams.team_type_enum`);
  }
}
