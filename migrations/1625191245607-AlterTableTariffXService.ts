import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTariffXService1625191245607
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'tariff_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'service_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'tariffs.tariff_x_service'
      })
    );

    await queryRunner.createPrimaryKey('tariffs.tariff_x_service', [
      'tariff_sid',
      'service_sid'
    ]);

    await queryRunner.createForeignKeys('tariffs.tariff_x_service', [
      new TableForeignKey({
        columnNames: ['tariff_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff'
      }),
      new TableForeignKey({
        columnNames: ['service_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'services.service'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "tariffs"."tariff_x_service" IS 'list tariff and service';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff_x_service');
  }
}
