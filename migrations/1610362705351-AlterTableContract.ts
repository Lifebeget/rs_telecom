import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContract1610362705351 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'phone number',
            isNullable: false,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'password',
            default: null,
            isNullable: true,
            name: 'password',
            type: 'character varying(255)'
          },
          {
            comment: 'seller number (VOID)',
            default: null,
            isNullable: true,
            name: 'seller_id',
            type: 'character varying(255)'
          },
          {
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'tariff name',
            default: null,
            isNullable: true,
            name: 'tariff_name',
            type: 'character varying(255)'
          },
          {
            comment: 'tariff option',
            default: null,
            isNullable: true,
            name: 'tariff_option',
            type: 'character varying(255)'
          },
          {
            comment: 'if true, can not renewal contract',
            default: null,
            isNullable: true,
            name: 'tariff_sim_only',
            type: 'boolean'
          },
          {
            comment: 'monthly invoice date',
            default: null,
            isNullable: true,
            name: 'invoice_month_day',
            type: 'integer'
          },
          {
            comment: 'payment method',
            default: null,
            isNullable: true,
            name: 'payment_method',
            type: 'character varying(255)'
          },
          {
            comment: 'serial key sim card',
            default: null,
            isNullable: true,
            name: 'serial_card_number',
            type: 'character varying(255)'
          },
          {
            comment: 'count of linked sim card',
            default: null,
            isNullable: true,
            name: 'serial_cards_count',
            type: 'integer'
          },
          {
            comment: 'product type',
            default: null,
            isNullable: true,
            name: 'product_type',
            type: 'character varying(255)'
          },
          {
            comment: 'contract activation date',
            default: null,
            isNullable: true,
            name: 'activation_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'last renewal date of contract',
            default: null,
            isNullable: true,
            name: 'last_renewal_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'next renewal date of contract',
            default: null,
            isNullable: true,
            name: 'next_renewal_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'expiration date of contract',
            default: null,
            isNullable: true,
            name: 'expiration_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'archived contract flag',
            default: false,
            isNullable: false,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'is not interested contract flag',
            default: false,
            isNullable: false,
            name: 'is_not_interested_flg',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract',
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_status'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract" IS 'contract';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract');
  }
}
