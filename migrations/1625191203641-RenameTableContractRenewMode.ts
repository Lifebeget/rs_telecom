import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';
import { EnumValues } from 'enum-values';

import { CanBeRenewed } from 'Constants';

export class RenameTableContractRenewMode1625191203641
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_renew_mode');
    await queryRunner.dropTable('contracts.dict_contract_can_be_renewed');

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'type',
            default: null,
            isNullable: true,
            name: 'type',
            type: 'character varying(255)'
          },
          {
            comment: 'name',
            default: null,
            isNullable: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'eligibility index',
            default: null,
            isNullable: true,
            name: 'eligibility_index',
            type: 'integer'
          },
          {
            comment: 'can be renewed',
            default: null,
            enum: EnumValues.getValues(CanBeRenewed),
            isNullable: true,
            name: 'can_be_renewed',
            type: 'enum'
          },
          {
            comment: 'renew start date',
            default: null,
            isNullable: true,
            name: 'next_renewal_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'soc date',
            default: null,
            isNullable: true,
            name: 'expiration_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_subsidy_eligibility'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_subsidy_eligibility',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_subsidy_eligibility" IS 'contract subsidy eligibility';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_subsidy_eligibility');

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'type name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'type description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_contract_can_be_renewed'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('contracts.dict_contract_can_be_renewed', ['name', 'description'])
      .values([
        {
          description: 'cannot be renewed',
          name: CanBeRenewed.Red
        },
        {
          description: 'can be renewed without grant',
          name: CanBeRenewed.Yellow
        },
        {
          description: 'can be renewed with grant',
          name: CanBeRenewed.Green
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_contract_can_be_renewed" IS 'contract can be renewed types';`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'type',
            default: null,
            isNullable: true,
            name: 'type',
            type: 'character varying(255)'
          },
          {
            comment: 'name',
            default: null,
            isNullable: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'eligibility index',
            default: null,
            isNullable: true,
            name: 'eligibility_index',
            type: 'integer'
          },
          {
            comment: 'can be renewed',
            isNullable: false,
            name: 'can_be_renewed',
            type: 'uuid'
          },
          {
            comment: 'renew start date',
            default: null,
            isNullable: true,
            name: 'renew_start_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'soc date',
            default: null,
            isNullable: true,
            name: 'soc_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_renew_mode'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_renew_mode', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['can_be_renewed'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_can_be_renewed'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_renew_mode" IS 'contract service list';`
    );
  }
}
