import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

const tableName = 'contracts.contract';

const column = new TableColumn({
  default: null,
  isNullable: true,
  name: 'main_contract_sid',
  type: 'uuid'
});

export class TableContractAddMainContractColumn1633073140994
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(tableName, column);
    await queryRunner.createForeignKey(
      tableName,
      new TableForeignKey({
        columnNames: [column.name],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('not revertable because of foreign key');
  }
}
