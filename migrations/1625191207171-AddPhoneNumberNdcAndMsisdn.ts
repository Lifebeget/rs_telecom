import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddPhoneNumberNdcAndMsisdn1625191207171
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract', [
      new TableColumn({
        comment: 'phone number ndc code',
        default: null,
        isNullable: true,
        name: 'ndc',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'phone number msisdn code',
        default: null,
        isNullable: true,
        name: 'msisdn',
        type: 'character varying(255)'
      })
    ]);

    await queryRunner.addColumns('contracts.contract_phone_number', [
      new TableColumn({
        comment: 'phone number ndc code',
        default: null,
        isNullable: true,
        name: 'ndc',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'phone number msisdn code',
        default: null,
        isNullable: true,
        name: 'msisdn',
        type: 'character varying(255)'
      })
    ]);

    await queryRunner.addColumns('clients.client', [
      new TableColumn({
        comment: 'phone number ndc code',
        default: null,
        isNullable: true,
        name: 'ndc',
        type: 'character varying(255)'
      }),
      new TableColumn({
        comment: 'phone number msisdn code',
        default: null,
        isNullable: true,
        name: 'msisdn',
        type: 'character varying(255)'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'ndc');
    await queryRunner.dropColumn('contracts.contract', 'msisdn');
    await queryRunner.dropColumn('contracts.contract_phone_number', 'ndc');
    await queryRunner.dropColumn('contracts.contract_phone_number', 'msisdn');
    await queryRunner.dropColumn('clients.client', 'ndc');
    await queryRunner.dropColumn('clients.client', 'msisdn');
  }
}
