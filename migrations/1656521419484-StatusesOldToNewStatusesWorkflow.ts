import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

export class StatusesOldToNewStatusesWorkflow1656521419484
  implements MigrationInterface {
  async workflowAction({
    memberSid,
    contractSid,
    manager,
    statusSid,
    teamSid,
    createdAt,
    typeSid
  }: {
    memberSid: string;
    contractSid: string;
    manager: EntityManager;
    statusSid: string;
    teamSid: string;
    createdAt: string;
    typeSid: string;
  }): Promise<boolean> {
    let getActualFlowContract = null;

    getActualFlowContract = await manager.query(
      `SELECT * FROM workflows.contract_flow as wcf WHERE wcf.contract_sid = '${contractSid}' ORDER BY wcf.is_archived ASC, wcf.created_at DESC LIMIT 1`
    );

    let flowSid = null;

    if (getActualFlowContract && getActualFlowContract[0]) {
      flowSid = getActualFlowContract[0]['sid'] as string;
    } else {
      return true;
    }

    const getStatus = await manager.query(
      `SELECT * FROM workflows.contract_flow_status as ws WHERE ws.flow_sid = '${flowSid}'::uuid AND ws.status_sid = '${statusSid}' AND ws.type_sid = '${typeSid}'::uuid AND ws.is_deleted = 'false'::boolean`
    );
    if (getStatus && getStatus[0]) {
      const updateOldStatuses = `UPDATE workflows.contract_flow_status SET is_deleted = 'true'::boolean, deleted_at = '${createdAt}'::timestamp WHERE flow_sid = '${flowSid}'::uuid AND type_sid = '${typeSid}'::uuid`;
      await manager.query(updateOldStatuses);
    }

    //Remove not riched
    if (typeSid === 'ad347db9-882f-4b52-8889-1ca18ac34b78') {
      //if typeSid === system
      const removeBehaverStatus = `UPDATE workflows.contract_flow_status SET is_deleted = 'true'::boolean, deleted_at = '${createdAt}'::timestamp WHERE flow_sid = '${flowSid}'::uuid AND type_sid = '38d9ba4d-3490-472b-a160-dcbc49a0609e'::uuid`;
      await manager.query(removeBehaverStatus);

      const removeAdditionalStatus = `UPDATE workflows.contract_flow_status SET is_deleted = 'true'::boolean, deleted_at = '${createdAt}'::timestamp WHERE flow_sid = '${flowSid}'::uuid AND type_sid = '76978ab9-670a-4164-83aa-de1261a036d7'::uuid`;
      await manager.query(removeAdditionalStatus);
    }

    const sqlAddStatus = `INSERT INTO workflows.contract_flow_status (sid, flow_sid, contract_sid, team_sid, member_sid, status_sid, is_deleted, created_at, deleted_at, type_sid) VALUES (DEFAULT, '${flowSid}'::uuid, '${contractSid}'::uuid, '${teamSid}'::uuid, '${memberSid}'::uuid, '${statusSid}'::uuid, DEFAULT, '${createdAt}'::timestamp, null::timestamp with time zone, '${typeSid}'::uuid)`;
    await manager.query(sqlAddStatus);

    return true;
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const assignmentRows = await queryRunner.manager
      .query(`SELECT status.member_sid as member_sid, 
    status.contract_sid as contract_sid, 
    status.status_sid as status_sid, 
    status.team_sid as team_sid, 
    status.created_at as created_at, 
    wcs.type_sid as type_sid
    FROM contracts.contract_status_log as status
    LEFT JOIN workflows.contract_status as wcs ON wcs.sid = status.status_sid
    ORDER BY status.created_at ASC`);

    for await (const data of assignmentRows) {
      const memberSid = data['member_sid'];
      const contractSid = data['contract_sid'];
      const statusSid = data['status_sid'];
      const teamSid = data['team_sid'];
      const createdAt = new Date(data['created_at']).toISOString();
      const typeSid = data['type_sid'];

      await this.workflowAction({
        contractSid,
        createdAt,
        manager: queryRunner.manager,
        memberSid,
        statusSid,
        teamSid,
        typeSid
      });
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
