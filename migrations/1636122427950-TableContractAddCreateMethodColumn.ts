import { values } from 'remeda';
import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractRequestType } from 'Constants';

export class TableContractAddCreateMethodColumn1636122427950
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        default: null,
        enum: values(ContractRequestType),
        isNullable: true,
        name: 'create_method',
        type: 'enum'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('Not revertable');
  }
}
