import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsFingerprintRequiredColumn1643599472792
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'teams.team',
      new TableColumn({
        comment: 'required fingerprint option for team',
        default: null,
        isNullable: true,
        name: 'is_fingerprint_required',
        type: 'boolean'
      })
    );

    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'required fingerprint option for member',
        default: true,
        isNullable: false,
        name: 'is_fingerprint_required',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
