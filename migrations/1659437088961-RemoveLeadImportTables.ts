import { MigrationInterface, QueryRunner } from 'typeorm';

import { removePermission } from 'Server/utils';
import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['leadsImport']
  | PermissionMap['leadsImportWithCarrier']
  | PermissionMap['leadsImportWithPassword']
>;

export class RemoveLeadImportTables1659437088961 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.lead_import_stat');
    await queryRunner.dropTable('leads.dict_lead_import_stat_status');

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.leadsImportWithCarrier
    ]);
    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.leadsImportWithCarrier
    ]);
    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.leadsImportWithCarrier
    ]);
    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.leadsImportWithCarrier
    ]);

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.leadsImportWithPassword
    ]);
    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.leadsImportWithPassword
    ]);
    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.leadsImportWithPassword
    ]);

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.leadsImport
    ]);
    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.leadsImport
    ]);
    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.leadsImport
    ]);
    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.leadsImport
    ]);
    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.leadsImport
    ]);
    await removePermission(queryRunner.manager, 'booking manager', [
      Permission.leadsImport
    ]);

    const permissionsToDelete: Record<PermissionKeys, string> = {
      [Permission.leadsImport]: 'can upload files without validation queue',
      [Permission.leadsImportWithCarrier]:
        'can upload files with leads for carrier validation',
      [Permission.leadsImportWithPassword]:
        'can upload files with validation queue'
    };

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permissionsToDelete)
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is irrevertable');
  }
}
