import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class StatusesAddNewTables1649076622144 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('workflows');

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Contract sid',
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          },

          {
            comment: 'Is contrat flow archived',
            default: false,
            isNullable: false,
            name: 'is_archived',
            type: 'boolean'
          },
          {
            comment: 'update date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.contract_flow'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Flow sid',
            isNullable: false,
            name: 'flow_sid',
            type: 'uuid'
          },
          {
            comment: 'Team sid',
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'Is this member start the flow',
            default: false,
            isNullable: false,
            name: 'is_started',
            type: 'boolean'
          },
          {
            comment: 'Is team deleted',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },

          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'deleted date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.contract_flow_team'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Flow sid',
            name: 'flow_sid',
            type: 'uuid'
          },
          {
            comment: 'Member sid',
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },

          {
            comment: 'Is this member start the flow',
            default: false,
            isNullable: false,
            name: 'is_started',
            type: 'boolean'
          },

          {
            comment: 'Is member deleted',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },

          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'deleted date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.contract_flow_member'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Flow sid',
            name: 'flow_sid',
            type: 'uuid'
          },
          {
            comment: 'Contract sid',
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'Team sid',
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'Member sid',
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'Status sid',
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'Is deleted status',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'contract status type',
            default: null,
            isNullable: true,
            name: 'type_sid',
            type: 'uuid'
          },
          {
            comment: 'update date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.contract_flow_status'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Status Name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'Status type sid',
            isNullable: false,
            name: 'type_sid',
            type: 'uuid'
          },
          {
            comment: 'Is team deleted',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.contract_status'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'team name',
            isNullable: false,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'Is team deleted',
            default: false,
            isNullable: false,
            name: 'is_deleted',
            type: 'boolean'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'NULL',
            isNullable: true,
            name: 'deleted_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'workflows.status_type'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'Status sid',
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'Flow sid',
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          }
        ],
        name: 'workflows.status_visibility_team'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
