import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnOld = new TableColumn({
  comment: 'status from',
  isNullable: false,
  name: 'status_from_sid',
  type: 'uuid'
});
const columnNew = new TableColumn({
  comment: 'status from',
  isNullable: true,
  name: 'status_from_sid',
  type: 'uuid'
});

export class TableActivityContractStatusChangedChangeStatusFromSidToNullable1657554646630
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'activities.activity_contract_status_changed',
      columnOld,
      columnNew
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'activities.activity_contract_status_changed',
      columnNew,
      columnOld
    );
  }
}
