import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from 'Server/modules/properties/entities';
import { PropertyKeys } from 'Server/modules/properties/property-keys.const';

export class AddValuesForContractPasswordRequestLimit1652103642569
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values({
        description: 'Number of seconds to block the request',
        key: PropertyKeys.contractPasswordLimitIterationSec,
        value: '30'
      })
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values({
        description: 'Number of hour to remove obsolete entries',
        key: PropertyKeys.contractPasswordLimitClearHour,
        value: '24'
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PropertiesEntity)
      .where({ key: PropertyKeys.contractPasswordLimitIterationSec })
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PropertiesEntity)
      .where({ key: PropertyKeys.contractPasswordLimitClearHour })
      .execute();
  }
}
