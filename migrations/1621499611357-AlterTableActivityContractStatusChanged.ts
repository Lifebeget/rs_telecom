import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractStatusChanged1621499611357
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'status from',
            isNullable: false,
            name: 'status_from_sid',
            type: 'uuid'
          },
          {
            comment: 'status to',
            isNullable: false,
            name: 'status_to_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_contract_status_changed'
      })
    );

    await queryRunner.createForeignKeys(
      'activities.activity_contract_status_changed',
      [
        new TableForeignKey({
          columnNames: ['activity_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'activities.activity'
        }),
        new TableForeignKey({
          columnNames: ['status_from_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.dict_contract_status'
        }),
        new TableForeignKey({
          columnNames: ['status_to_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.dict_contract_status'
        })
      ]
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_status_changed" IS 'activity contract status changed';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_status_changed');
  }
}
