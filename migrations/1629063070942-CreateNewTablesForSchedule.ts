import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateNewTablesForSchedule1629063070942
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE shops.shop_schedule_state_status AS ENUM ('Draft', 'Confirm', 'Done')`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Shop SID',
            isNullable: false,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            comment: 'from date',
            isNullable: false,
            name: 'from',
            type: 'timestamp with time zone'
          },
          {
            comment: 'to date',
            isNullable: false,
            name: 'to',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Shop Schedule State status',
            isNullable: false,
            name: 'status',
            type: 'shops.shop_schedule_state_status'
          },
          {
            comment: 'Datetime created',
            default: 'now()',
            isNullable: true,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_schedule_state'
      })
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'Shop SID',
            isNullable: false,
            name: 'shop_sid',
            type: 'uuid'
          },
          {
            comment: 'Date of day',
            isNullable: false,
            name: 'date',
            type: 'date'
          },
          {
            comment: 'Is the day canceled',
            default: false,
            isNullable: true,
            name: 'is_canceled',
            type: 'boolean'
          },
          {
            comment: 'Date of the day canceled',
            default: null,
            isNullable: true,
            name: 'is_canceled_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'Datetime created',
            default: 'now()',
            isNullable: true,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'shops.shop_day'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops.shop_schedule_state');
    await queryRunner.dropTable('shops.shop_day');
  }
}
