import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

// import { TeamEntity } from 'Server/modules/team/entities';

export class TableTeamAddIsPromotion1625815087239
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'teams.team',
      new TableColumn({
        comment: 'team promotion flag',
        default: false,
        isNullable: false,
        name: 'is_promotion_flg',
        type: 'boolean'
      })
    );

    // isPromitionFlg is removed now
    // await queryRunner.manager
    //   .createQueryBuilder()
    //   .update(TeamEntity, { isPromitionFlg: true })
    //   .where('name = :name', { name: 'Via Sales' })
    //   .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'is_promotion_flg');
  }
}
