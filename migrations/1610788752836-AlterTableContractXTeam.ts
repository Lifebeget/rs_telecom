import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractXTeam1610788752836
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_x_team'
      })
    );

    await queryRunner.createPrimaryKey('contracts.contract_x_team', [
      'contract_sid',
      'team_sid'
    ]);

    await queryRunner.createForeignKeys('contracts.contract_x_team', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),

      new TableForeignKey({
        columnNames: ['team_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_x_team" IS 'list contract and team';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_x_team');
  }
}
