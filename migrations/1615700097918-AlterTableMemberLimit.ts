import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableMemberLimit1615700097918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'vodafone day limit',
            isNullable: false,
            name: 'vodafone_limit',
            type: 'integer'
          },
          {
            comment: 'twilio day limit',
            isNullable: false,
            name: 'twilio_limit',
            type: 'integer'
          },
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'limits.member_limit'
      })
    );

    await queryRunner.createForeignKey(
      'limits.member_limit',
      new TableForeignKey({
        columnNames: ['member_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "limits"."member_limit" IS 'member request limit';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('limits.member_limit');
  }
}
