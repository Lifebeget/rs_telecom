import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableContractNestBestActionTip1610362713351
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'row id',
            default: null,
            isNullable: true,
            name: 'row_id',
            type: 'character varying(255)'
          },
          {
            comment: 'company code',
            default: null,
            isNullable: true,
            name: 'company_code',
            type: 'character varying(255)'
          },
          {
            comment: 'cell code',
            default: null,
            isNullable: true,
            name: 'cell_code',
            type: 'character varying(255)'
          },
          {
            comment: 'extraction date',
            default: null,
            isNullable: true,
            name: 'extraction_date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'flow chart id',
            default: null,
            isNullable: true,
            name: 'flow_chart_id',
            type: 'character varying(255)'
          },
          {
            comment: 'tip text',
            default: null,
            isNullable: true,
            name: 'tip_text',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_next_best_action_tip'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_next_best_action_tip" IS 'contract next best action tip';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_next_best_action_tip');
  }
}
