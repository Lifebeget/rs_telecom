import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsDeletedOnContractExtendRequestBankData1637231667874
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns(
      'contracts.contract_extend_request_x_bank_data',
      [
        new TableColumn({
          comment: 'is deleted',
          default: false,
          isNullable: false,
          name: 'is_deleted',
          type: 'boolean'
        }),
        new TableColumn({
          comment: 'deleted date',
          isNullable: true,
          name: 'is_deleted_at',
          type: 'timestamp with time zone'
        })
      ]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
