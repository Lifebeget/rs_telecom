import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaTariffs1609833731818 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('tariffs');
    await queryRunner.query(
      `COMMENT ON SCHEMA "tariffs" IS 'all tariffs objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('tariffs');
  }
}
