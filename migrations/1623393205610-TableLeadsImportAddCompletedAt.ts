import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableLeadsImportAddCompletedAt1623393205610
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'leads.lead_import_stat',
      new TableColumn({
        comment: 'import completed date',
        default: null,
        isNullable: true,
        name: 'completed_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('leads.lead_import_stat', 'completed_at');
  }
}
