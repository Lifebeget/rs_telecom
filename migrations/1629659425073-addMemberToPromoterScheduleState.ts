import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class addMemberToPromoterScheduleState1629659425073
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('shops.promoter_schedule_state', [
      new TableColumn({
        isNullable: false,
        name: 'member_sid',
        type: 'uuid'
      })
    ]);

    await queryRunner.createForeignKeys('shops.promoter_schedule_state', [
      new TableForeignKey({
        columnNames: ['member_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('shops.promoter_schedule_state', [
      new TableForeignKey({
        columnNames: ['member_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.dropColumns('shops.promoter_schedule_state', [
      new TableColumn({
        isNullable: false,
        name: 'member_sid',
        type: 'uuid'
      })
    ]);
  }
}
