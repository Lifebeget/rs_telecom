import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { RoleEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

const allPermissions = [
  Permission.viewClientCard,
  Permission.addTeamClient,
  Permission.addClient,
  Permission.addClientNote,
  Permission.removeClientNote,
  Permission.viewMyContracts,
  Permission.viewTeamContracts,
  Permission.updateContract,
  Permission.rejectContract,
  Permission.addMemberContract,
  Permission.viewContractChecklist,
  Permission.updateContractChecklist,
  Permission.viewContractIssues,
  Permission.updateContractIssues,
  Permission.editContractExtendRequest,
  Permission.viewMyLeads,
  Permission.viewTeamLeads,
  Permission.addLead,
  Permission.updateLead,
  Permission.addTeamLead,
  Permission.canValidateLeadBySMS,
  Permission.canValidateLeadByPassword,
  Permission.leadsImportWithPassword,
  Permission.leadsImport,
  Permission.viewTeamInfo,
  Permission.addTeamMember,
  Permission.removeTeamMember,
  Permission.addTeam,
  Permission.updateTeam,
  Permission.addTeamContract,
  Permission.viewRoleInfo,
  Permission.addRole,
  Permission.addRolePermission,
  Permission.removeRolePermission,
  Permission.addMemberRole,
  Permission.removeMemberRole,
  Permission.addTeamRole,
  Permission.removeTeamRole,
  Permission.viewUserList,
  Permission.addUser,
  Permission.updateUser,
  Permission.blockUser,
  Permission.updateUserEmail,
  Permission.editSupervisors,
  Permission.viewHardwareInfo,
  Permission.addHardware,
  Permission.viewTariffInfo,
  Permission.updateTariffCommission,
  Permission.addTariff,
  Permission.viewDashboard
];

export class AddRoleSupervisorSystem1620902186628
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(RoleEntity, ['name'])
      .values([
        {
          name: 'supervisor system'
        }
      ])
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      allPermissions
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      allPermissions
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(RoleEntity)
      .where('name = :name', {
        name: 'supervisor system'
      })
      .execute();
  }
}
