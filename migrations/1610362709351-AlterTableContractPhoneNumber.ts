import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractPhoneNumber1610362709351
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'phone number',
            default: null,
            isNullable: true,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_phone_number'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_phone_number',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_phone_number" IS 'contract phone number list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_phone_number');
  }
}
