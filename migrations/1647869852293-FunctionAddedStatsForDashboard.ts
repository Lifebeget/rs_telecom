import { MigrationInterface, QueryRunner } from 'typeorm';

export class FunctionAddedStatsForDashboard1647869852293
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    CREATE OR REPLACE function getcontracts_stat_by_team(teamSids text)
    RETURNS table
            (
                contract_sid_res uuid,
                status_sid       uuid,
                status_type      text
            )
as
$$
DECLARE
    DEFAULT_STATUS_SID uuid;
    team_sids_arr      uuid[];
BEGIN
    team_sids_arr = string_to_array(teamSids, ',');

    SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
    return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                                                 (CASE
                                                                      WHEN ccsl.status_sid IS NULL
                                                                          THEN DEFAULT_STATUS_SID
                                                                      ELSE ccsl.status_sid END)  as status_sid,
                                                                 (CASE
                                                                      WHEN ccsl.status_type IS NULL THEN 'System'
                                                                      ELSE ccsl.status_type END) as status_type
                 FROM contracts.contract as ccontract
                          LEFT JOIN contracts.contract_x_team as ccxt ON ccxt.contract_sid = ccontract.sid
                          LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                                                         first_value(ccsl.status_sid)
                                                         over (
                                                             PARTITION BY (CASE
                                                                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                                   THEN 'Behavioral'
                                                                               WHEN cdcs.visibility_id = 8
                                                                                   THEN 'Additional'
                                                                               ELSE 'System' END)
                                                             order by ccsl.created_at DESC
                                                             RANGE BETWEEN
                                                                 UNBOUNDED PRECEDING AND
                                                                 UNBOUNDED FOLLOWING
                                                             )                   as status_sid,
                                                         (CASE
                                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                  THEN 'Behavioral'
                                                              WHEN cdcs.visibility_id = 8
                                                                  THEN 'Additional'
                                                              ELSE 'System' END) as status_type,
                                                         ccsl.contract_sid
                                             FROM contracts.contract_status_log as ccsl
                                                      LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
                                             WHERE (cdcs.visibility_id = 0
                                                 OR cdcs.visibility_id = 1
                                                 OR cdcs.visibility_id = 2
                                                 OR cdcs.visibility_id = 3
                                                 OR cdcs.visibility_id = 4
                                                 OR cdcs.visibility_id = 5
                                                 OR cdcs.visibility_id = 6
                                                 OR cdcs.visibility_id = 7
                                                 OR cdcs.visibility_id = 8
                                                 OR cdcs.visibility_id = 9
                                                 OR cdcs.visibility_id = 10
                                                 OR cdcs.visibility_id = 11
                                                 OR cdcs.visibility_id = 12)
                                               AND ccsl.contract_sid = ccontract.sid
                                               AND ccsl.contract_sid IS NOT NULL
                                               AND ccsl.team_sid = ANY (team_sids_arr)

                     ) as ccsl
                                    ON ccsl.contract_sid = ccontract.sid AND
                                       ccsl.status_type IS NOT NULL AND ccsl.contract_sid = ccxt.contract_sid
                 WHERE ccxt.team_sid = ANY (team_sids_arr);
END;
$$
    LANGUAGE plpgsql;




CREATE OR REPLACE function getcontracts_stat_by_team_without_member(teamSids text)
    RETURNS table
            (
                contract_sid_res uuid,
                status_sid       uuid,
                status_type      text
            )
as
$$
DECLARE
    DEFAULT_STATUS_SID uuid;
    team_sids_arr      uuid[];
BEGIN
    team_sids_arr = string_to_array(teamSids, ',');

    SELECT sid FROM contracts.dict_contract_status WHERE name = 'Idle' LIMIT 1 into DEFAULT_STATUS_SID;
    return query SELECT DISTINCT ON (ccontract.sid, status_type) ccontract.sid                   as contract_sid_res,
                                                                 (CASE
                                                                      WHEN ccsl.status_sid IS NULL
                                                                          THEN DEFAULT_STATUS_SID
                                                                      ELSE ccsl.status_sid END)  as status_sid,
                                                                 (CASE
                                                                      WHEN ccsl.status_type IS NULL THEN 'System'
                                                                      ELSE ccsl.status_type END) as status_type
                 FROM contracts.contract as ccontract
                          LEFT JOIN contracts.contract_x_team as ccxt ON ccxt.contract_sid = ccontract.sid
                          LEFT JOIN LATERAL (SELECT DISTINCT ON ( status_type, ccsl.contract_sid)
                                                         first_value(ccsl.status_sid)
                                                         over (
                                                             PARTITION BY (CASE
                                                                               WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                                   THEN 'Behavioral'
                                                                               WHEN cdcs.visibility_id = 8
                                                                                   THEN 'Additional'
                                                                               ELSE 'System' END)
                                                             order by ccsl.created_at DESC
                                                             RANGE BETWEEN
                                                                 UNBOUNDED PRECEDING AND
                                                                 UNBOUNDED FOLLOWING
                                                             )                   as status_sid,
                                                         (CASE
                                                              WHEN cdcs.visibility_id = 6 OR cdcs.visibility_id = 7
                                                                  THEN 'Behavioral'
                                                              WHEN cdcs.visibility_id = 8
                                                                  THEN 'Additional'
                                                              ELSE 'System' END) as status_type,
                                                         ccsl.contract_sid
                                             FROM contracts.contract_status_log as ccsl
                                                      LEFT JOIN contracts.dict_contract_status as cdcs ON cdcs.sid = ccsl.status_sid
                                                      LEFT JOIN contracts.contract_x_status as cxs
                                                                ON cxs.status_sid = ccsl.status_sid AND
                                                                   cxs.contract_sid = ccontract.sid AND
                                                                   cxs.is_deleted = false
                                             WHERE (cdcs.visibility_id = 0
                                                 OR cdcs.visibility_id = 1
                                                 OR cdcs.visibility_id = 2
                                                 OR cdcs.visibility_id = 3
                                                 OR cdcs.visibility_id = 4
                                                 OR cdcs.visibility_id = 5
                                                 OR cdcs.visibility_id = 6
                                                 OR cdcs.visibility_id = 7
                                                 OR cdcs.visibility_id = 8
                                                 OR cdcs.visibility_id = 9
                                                 OR cdcs.visibility_id = 10
                                                 OR cdcs.visibility_id = 11
                                                 OR cdcs.visibility_id = 12)
                                               AND ccsl.contract_sid = ccontract.sid
                                               AND ccsl.contract_sid IS NOT NULL
                     ) as ccsl
                                    ON ccsl.contract_sid = ccontract.sid AND
                                       ccsl.status_type IS NOT NULL
                 WHERE ccxt.team_sid = ANY (team_sids_arr)
                   AND ccxt.team_sid IS NOT NULL;

END;
$$
    LANGUAGE plpgsql;


    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP FUNCTION IF EXISTS getcontracts_stat_by_team(teamSids text);`
    );
    await queryRunner.query(
      `DROP FUNCTION IF EXISTS getcontracts_stat_by_team_without_member(teamSids text);`
    );
  }
}
