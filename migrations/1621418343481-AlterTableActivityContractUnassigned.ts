import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractUnassigned1621418343481
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'member from',
            isNullable: false,
            name: 'member_from_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_contract_unassigned'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_unassigned',
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_unassigned',
      new TableForeignKey({
        columnNames: ['member_from_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_unassigned" IS 'activity contract unassigned';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_unassigned');
  }
}
