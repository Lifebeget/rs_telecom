import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class AddFingerprintEsignPermissionToCCAgents1655203811444
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignDownload,
      Permission.fingerprintESignEdit
    ]);

    await insertPermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [
        Permission.fingerprintESignView,
        Permission.fingerprintESignDownload,
        Permission.fingerprintESignEdit
      ]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignDownload,
      Permission.fingerprintESignEdit
    ]);

    await removePermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [
        Permission.fingerprintESignView,
        Permission.fingerprintESignDownload,
        Permission.fingerprintESignEdit
      ]
    );
  }
}
