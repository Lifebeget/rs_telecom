import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveInvalidContractAppointmetFlags1660818032832
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `
        with contract_with_invalid_appointment_flag as (
          select 
            contract.sid as contract_sid 
          from 
            contracts.contract 
            left join contracts.contract_x_flag ON contract_x_flag.contract_sid = contract.sid 
            left join contracts.contract_appointment ON contract_appointment.contract_sid = contract.sid 
            AND contract_appointment.is_archived_flg = false 
          where 
            contract_x_flag.flag :: text like 'Appoint%' 
          group by 
            contract.sid, 
            contract_x_flag.flag 
          having 
            count(contract_appointment.sid) = 0
        ) 
        delete from 
          contracts.contract_x_flag 
        where 
          contract_x_flag.contract_sid in (
            select 
              contract_sid 
            from 
              contract_with_invalid_appointment_flag
          ) 
          and (
            contract_x_flag.flag = 'Appointed' 
            or contract_x_flag.flag = 'AppointmentInProgress'
          )
        `
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
