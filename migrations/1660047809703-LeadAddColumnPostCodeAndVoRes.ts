import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class LeadAddColumnPostCodeAndVoRes1660047809703
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'leads.lead',
      new TableColumn({
        comment: 'address postcode',
        default: null,
        isNullable: true,
        name: 'post_code',
        type: 'character varying(255)'
      })
    );
    await queryRunner.addColumn(
      'leads.lead',
      new TableColumn({
        comment: 'request content',
        isNullable: true,
        name: 'request_content',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
