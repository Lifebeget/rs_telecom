import { values } from 'remeda';
import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractRequestStatus } from 'Constants';

const tableName = 'contracts.contract_request_history';
const columnName = 'request_status';

export class TableContractRequestHistoryChangeRequestStatusColumn1633346464620
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      tableName,
      columnName,
      new TableColumn({
        comment: 'request status',
        default: `'${ContractRequestStatus.Fetching}'`,
        enum: values(ContractRequestStatus),
        isNullable: false,
        name: 'request_status',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('not revertable');
  }
}
