import { keys } from 'ramda';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['getWorkingDay']
  | PermissionMap['startWorkingDay']
  | PermissionMap['stopWorkingDay']
  | PermissionMap['getUserOnline']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.getWorkingDay]: 'get working day',
  [Permission.startWorkingDay]: 'start working day',
  [Permission.stopWorkingDay]: 'stop working day',
  [Permission.getUserOnline]: 'get user online'
};
export class TablePermissionAddWorkingDayPermission1627273006945
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'booking team leader',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'booking manager',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'booking vf manager',
      keys(permission)
    );
    await insertPermission(
      queryRunner.manager,
      'booking promouter',
      keys(permission)
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'booking team leader',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'booking manager',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'booking vf manager',
      keys(permission)
    );
    await removePermission(
      queryRunner.manager,
      'booking promouter',
      keys(permission)
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();
  }
}
