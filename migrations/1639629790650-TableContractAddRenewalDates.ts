import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAddRenewalDates1639629790650
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract', [
      new TableColumn({
        comment: 'early extension date',
        default: null,
        isNullable: true,
        name: 'early_extension_date',
        type: 'timestamp with time zone'
      }),
      new TableColumn({
        comment: 'normal extension date',
        default: null,
        isNullable: true,
        name: 'normal_extension_date',
        type: 'timestamp with time zone'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
