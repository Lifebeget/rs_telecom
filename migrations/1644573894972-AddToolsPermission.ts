import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

export class AddToolsPermission1644573894972 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values({
        description: 'can see tools tab',
        name: Permission.viewTools
      })
      .execute();

    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.viewTools
    ]);

    await insertPermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewTools
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name = :name', { name: Permission.viewTools })
      .execute();

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.viewTools
    ]);

    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewTools
    ]);
  }
}
