import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class AddFingerPrintEsignViewPermissionToActivation1653372300274
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'activation agent', [
      Permission.fingerprintView,
      Permission.fingerprintESignView
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'activation agent', [
      Permission.fingerprintView,
      Permission.fingerprintESignView
    ]);
  }
}
