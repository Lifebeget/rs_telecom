import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

export class addPublicPreference1639128221544 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          new TableColumn({
            comment: 'key for value',
            isNullable: false,
            isPrimary: true,
            name: 'key',
            type: 'character varying(255)'
          }),

          new TableColumn({
            comment: 'value that is used by the key',
            isNullable: false,
            name: 'value',
            type: 'character varying(255)'
          }),

          new TableColumn({
            comment: 'Description key value',
            isNullable: false,
            name: 'description',
            type: 'text'
          })
        ],
        name: 'properties'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.properties');
  }
}
