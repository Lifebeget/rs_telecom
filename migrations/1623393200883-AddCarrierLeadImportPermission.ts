import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

export class AddCarrierLeadImportPermission1623393200883
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values({
        description: 'can upload files with leads for carrier validation',
        name: Permission.leadsImportWithCarrier
      })
      .execute();

    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.leadsImportWithCarrier
    ]);

    await insertPermission(queryRunner.manager, 'supervisor system', [
      Permission.leadsImportWithCarrier
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.leadsImportWithCarrier
    ]);

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.leadsImportWithCarrier
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name = :name', { name: Permission.leadsImportWithCarrier })
      .execute();

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.leadsImportWithCarrier
    ]);

    await removePermission(queryRunner.manager, 'supervisor system', [
      Permission.leadsImportWithCarrier
    ]);

    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.leadsImportWithCarrier
    ]);

    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.leadsImportWithCarrier
    ]);
  }
}
