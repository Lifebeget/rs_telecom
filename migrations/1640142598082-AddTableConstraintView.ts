import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTableConstraintView1640142598082 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE OR REPLACE VIEW public."constraints"
        AS
        SELECT con.conname,
               nsp.nspname,
               rel.relname
        FROM pg_constraint con
                 JOIN pg_class rel ON rel.oid = con.conrelid
                 JOIN pg_namespace nsp ON nsp.oid = con.connamespace;
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
