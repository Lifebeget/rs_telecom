import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const languageColumn: TableColumn = new TableColumn({
  comment: 'User selected language',
  isNullable: true,
  name: 'language',
  type: 'character varying(255)'
});

export class UserLanguage1623910934131 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('users.user', languageColumn);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', languageColumn);
  }
}
