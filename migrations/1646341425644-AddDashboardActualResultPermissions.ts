import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

export class AddDashboardActualResultPermissions1646341425644
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values([
        {
          description: 'can view dashboard actual statistics',
          name: Permission.viewActualStatistics
        },
        {
          description: 'can view dashboard actual statistics for CC teams',
          name: Permission.viewCCActualStatistics
        }
      ])
      .execute();

    await insertPermission(queryRunner.manager, 'supervisor root', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);

    await insertPermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN(:...names)', {
        names: [
          Permission.viewActualStatistics,
          Permission.viewCCActualStatistics
        ]
      })
      .execute();

    await removePermission(queryRunner.manager, 'supervisor root', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);

    await removePermission(queryRunner.manager, 'supervisor regular', [
      Permission.viewActualStatistics,
      Permission.viewCCActualStatistics
    ]);
  }
}
