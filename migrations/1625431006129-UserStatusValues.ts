import { MigrationInterface, QueryRunner, Repository } from 'typeorm';

import { UserEntity, UserStatusEntity } from '../modules/user/entities';

export class UserStatusValues1625430464591 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const users: UserEntity[] = await queryRunner.manager
      .createQueryBuilder(UserEntity, 'u')
      .select(['sid', 'created_at'])
      .getMany();

    const userStatusRepository: Repository<UserStatusEntity> = queryRunner.manager.getRepository(
      UserStatusEntity
    );

    await userStatusRepository.save(
      users.map(({ sid: user, createdAt }) =>
        userStatusRepository.create({
          createdAt,
          online: false,
          user
        })
      )
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .getRepository(UserStatusEntity)
      .createQueryBuilder()
      .delete()
      .execute();
  }
}
