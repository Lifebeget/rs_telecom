import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractRecommendation1625191229323
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'contract recommendation',
        default: null,
        isNullable: true,
        name: 'recommendation',
        type: 'text'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'recommendation');
  }
}
