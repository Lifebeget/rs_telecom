import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddClientAccountType1625191223139 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'clients.client',
      new TableColumn({
        comment: 'account type',
        default: null,
        isNullable: true,
        name: 'account_type',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('clients.client', 'account_type');
  }
}
