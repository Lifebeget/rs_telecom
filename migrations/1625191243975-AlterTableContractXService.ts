import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractXService1625191243975
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'service_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_x_service'
      })
    );

    await queryRunner.createPrimaryKey('contracts.contract_x_service', [
      'contract_sid',
      'service_sid'
    ]);

    await queryRunner.createForeignKeys('contracts.contract_x_service', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['service_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'services.service'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_x_service" IS 'list contract and service';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_x_service');
  }
}
