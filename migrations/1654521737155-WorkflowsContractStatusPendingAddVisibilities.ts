import { MigrationInterface, QueryRunner } from 'typeorm';

import { ContractStatus } from 'Constants';
import { TeamEntity } from 'Server/modules/team/entities';
import {
  ContractStatusEntity,
  StatusVisibilityTeamEntity
} from 'Server/modules/workflow/entities';

export class WorkflowsContractStatusPendingAddVisibilities1654521737155
  implements MigrationInterface {
  async insertTeamVisibilityStatus(
    teams: TeamEntity[],
    statusSid: string,
    queryRunner: QueryRunner
  ) {
    await Promise.all(
      teams.map(async el => {
        await queryRunner.manager.insert(StatusVisibilityTeamEntity, {
          statusSid,
          teamSid: el.sid
        });
      })
    );
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const status = await queryRunner.manager
      .createQueryBuilder(ContractStatusEntity, 'status')
      .where('name = :name', { name: ContractStatus.Pending })
      .getOne();

    if (!status) {
      throw new Error('Pending status not found');
    }
    const teams = await queryRunner.manager
      .createQueryBuilder(TeamEntity, 'team')
      .getMany();

    await this.insertTeamVisibilityStatus(teams, status.sid, queryRunner);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
