import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class CreateTableFingerprintNotifyLog1640589350335
  implements MigrationInterface {
  public async up(queryRunner: Required<QueryRunner>): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'fingerprint identifier',
            isNullable: false,
            name: 'fingerprint_sid',
            type: 'uuid'
          },
          {
            comment: 'notified date',
            isNullable: false,
            name: 'notified_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.fingerprint_notify_log'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.fingerprint_notify_log',
      new TableForeignKey({
        columnNames: ['fingerprint_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_fingerprint'
      })
    );

    await queryRunner.query(`
      with contract_fingerprint as (
        select contract_fingerprint.*
        from contracts.contract_fingerprint
        where contract_fingerprint.type = 'ESign'
          and contract_fingerprint.completed_at isnull
          and contract_fingerprint.failed_at isnull
          and contract_fingerprint.sent_at is not null
          and extract(day from now() - contract_fingerprint.sent_at) < 2
      )
      insert into contracts.fingerprint_notify_log (fingerprint_sid, notified_at)
      select contract_fingerprint.sid, contract_fingerprint.sent_at
      from contract_fingerprint;
    `);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."fingerprint_notify_log" IS 'fingerprint log';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
