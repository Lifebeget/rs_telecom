import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractRequestType } from 'Constants';
import { values } from 'Utils';

export class TableContractAddUpdateMethodLastColumn1636123531144
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        default: null,
        enum: values(ContractRequestType),
        isNullable: true,
        name: 'update_method_last',
        type: 'enum'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('Not revertable');
  }
}
