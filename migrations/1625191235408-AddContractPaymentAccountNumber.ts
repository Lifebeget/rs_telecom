import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractPaymentAccountNumber1625191235408
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_payment',
      new TableColumn({
        comment: 'account number',
        default: null,
        isNullable: true,
        name: 'account_number',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract_payment',
      'account_number'
    );
  }
}
