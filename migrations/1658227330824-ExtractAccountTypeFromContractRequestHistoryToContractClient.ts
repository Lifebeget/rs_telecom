import { MigrationInterface, QueryRunner } from 'typeorm';

export class ExtractAccountTypeFromContractRequestHistoryToContractClient1658227330824
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      WITH data AS (
        SELECT 
          (
            REGEXP_MATCH(
              request_content, '"customerType":"(\\w)"'
            )
          )[1] account_type,
          contract_sid 
        FROM 
          contracts.contract_request_history 
        WHERE 
          request_type = 'Xml' 
          AND request_status = 'Successful' 
          AND request_content like '%customerType%'
      )
      UPDATE 
        contracts.contract_client 
      SET 
        account_type = data.account_type  
      FROM 
        data 
      WHERE 
        data.contract_sid = contract_client.contract_sid`);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
