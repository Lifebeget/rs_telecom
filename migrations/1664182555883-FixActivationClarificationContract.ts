import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

export class FixActivationClarificationContract1664182555883
  implements MigrationInterface {
  async getLastStatusesByFlow(flowSid: string, manager: EntityManager) {
    const sql = `SELECT DISTINCT last_value(wcfs.sid) over (
      PARTITION BY (wcfs.type_sid, wcfs.flow_sid)
      order by wcfs.created_at ASC
      RANGE BETWEEN
          UNBOUNDED PRECEDING
          AND UNBOUNDED FOLLOWING
      ) as sid
  FROM workflows.contract_flow_status as wcfs
  LEFT JOIN LATERAL (SELECT wcf.sid, wcf.contract_sid, wcf.is_archived, wcf.created_at
    FROM workflows.contract_flow as wcf
    WHERE wcf.sid = '${flowSid}'
    LIMIT 1
    ) as wcf ON TRUE
  LEFT JOIN workflows.contract_status as wcs ON wcs.sid = wcfs.status_sid
  LEFT JOIN workflows.status_type as wst ON wst.sid = wcs.type_sid
  WHERE wcfs.flow_sid = wcf.sid AND wcf.sid = '${flowSid}'
  AND (CASE WHEN wst.name = 'Additional' THEN wcfs.is_deleted = false ELSE wcfs.is_deleted = true OR wcfs.is_deleted = false END)
AND 
( CASE WHEN wcf.is_archived = true THEN (
  CASE WHEN (
    SELECT 
      DISTINCT last_value(wcfs_in.status_sid) over (
        PARTITION BY wcfs_in.flow_sid 
        order by 
          wcfs_in.created_at ASC RANGE BETWEEN UNBOUNDED PRECEDING 
          AND UNBOUNDED FOLLOWING
      ) 
    FROM 
      workflows.contract_flow_status as wcfs_in 
    WHERE 
      wcfs_in.flow_sid = wcfs.flow_sid 
      AND wcfs_in.type_sid = 'ad347db9-882f-4b52-8889-1ca18ac34b78' -- System TypeSID
  ) = 'f0048478-fedc-4a90-afea-c87fcfeed48a'  -- IdleStatus SID
  THEN wcfs.status_sid != 'e092c260-e7e7-4cf8-908b-2f8f326277bf' -- NotReached sid 
  ELSE wcfs.status_sid != 'e092c260-e7e7-4cf8-908b-2f8f326277bf' -- NotReached sid 
  AND wcfs.status_sid != '085889e7-9db7-401f-a5db-ad5e0832b1de'  -- Pending status sid
  END ) 
  ELSE
  wcfs.is_deleted = false
  END
)`;

    return manager.query(sql);
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const flows = await queryRunner.query(`SELECT DISTINCT flow_status.flow_sid FROM workflows.contract_flow_status as flow_status
      LEFT JOIN workflows.contract_flow as flow on flow.sid = flow_status.flow_sid
      LEFT JOIN workflows.contract_status as status ON status.sid = flow_status.status_sid
      WHERE flow.is_archived = false AND flow_status.is_deleted = false AND status.type_sid = 'ad347db9-882f-4b52-8889-1ca18ac34b78' -- SYSTEM
      GROUP BY flow_status.flow_sid
      HAVING COUNT(flow_status.sid) > 1
      `);

    for await (const flow of flows) {
      if (flow && flow['flow_sid']) {
        const flowSid = flow['flow_sid'] as string;

        const statuses = await this.getLastStatusesByFlow(
          flowSid,
          queryRunner.manager
        );
        if (statuses && statuses[0] && statuses[0]['sid']) {
          const statusesSid = (statuses.map(
            (el: { sid: string; status_sid: string }) => el.sid
          ) as unknown) as Array<string>;
          //Сначала все статусы архивируем
          await queryRunner.manager.query(
            `UPDATE workflows.contract_flow_status SET is_deleted = true::boolean WHERE flow_sid = '${flowSid}'::uuid`
          );

          const updatedSql = `UPDATE workflows.contract_flow_status SET is_deleted = false::boolean WHERE sid  = ANY ('{ ${statusesSid.join(
            ','
          )}}')`;
          await queryRunner.manager.query(updatedSql);
        } else {
          throw new Error('last statuses error');
        }
      }
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
