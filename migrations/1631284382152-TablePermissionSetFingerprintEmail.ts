import { MigrationInterface, QueryRunner } from 'typeorm';
import { keys } from 'ramda';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<Permission, PermissionMap['setFingerprintEmail']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.setFingerprintEmail]: 'set fingerprint email'
};

const roles: string[] = [
  'supervisor system',
  'supervisor root',
  'supervisor regular',
  'callcenter sales agent',
  'callcenter presales agent',
  'callcenter presales&sales agent',
  'callcenter teamlead',
  'callcenter admin',
  'qc agent',
  'qc teamlead'
];

export class TablePermissionSetFingerprintEmail1631284382152
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, keys(permissions));
      })
    );
  }
}
