import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { CanBeRenewed } from 'Constants';

export class AlterTableDictContractCanBeRenewed1610362710351
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'type name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'type description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_contract_can_be_renewed'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('contracts.dict_contract_can_be_renewed', ['name', 'description'])
      .values([
        {
          description: 'cannot be renewed',
          name: CanBeRenewed.Red
        },
        {
          description: 'can be renewed without grant',
          name: CanBeRenewed.Yellow
        },
        {
          description: 'can be renewed with grant',
          name: CanBeRenewed.Green
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_contract_can_be_renewed" IS 'contract can be renewed types';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.dict_contract_can_be_renewed');
  }
}
