import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const tableName = 'contracts.contract';

const column = new TableColumn({
  default: false,
  isNullable: false,
  name: 'is_partner',
  type: 'boolean'
});

export class TableContractAddIsPartnerColumn1632986421420
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(tableName, column);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(tableName, column);
  }
}
