import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableLeadImportStat1613425612489
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'file identifier',
            isNullable: false,
            isUnique: true,
            name: 'file_id',
            type: 'character varying(255)'
          },
          {
            comment: 'leads import report name',
            default: null,
            isNullable: true,
            name: 'report_name',
            type: 'character varying(255)'
          },
          {
            comment: 'lead import stat status identifier',
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          },
          {
            comment: 'team identifier',
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'file name',
            isNullable: false,
            name: 'file_name',
            type: 'character varying(255)'
          },
          {
            comment: 'processed count',
            default: 0,
            isNullable: false,
            name: 'processed',
            type: 'integer'
          },
          {
            comment: 'total count',
            default: 0,
            isNullable: false,
            name: 'total',
            type: 'integer'
          },
          {
            comment: 'valid count',
            default: 0,
            isNullable: false,
            name: 'valid',
            type: 'integer'
          },
          {
            comment: 'could be extended count',
            default: 0,
            isNullable: false,
            name: 'could_be_extended',
            type: 'integer'
          },
          {
            comment: 'invalid count',
            default: 0,
            isNullable: false,
            name: 'invalid',
            type: 'integer'
          },
          {
            comment: 'invalid wrong number count',
            default: 0,
            isNullable: false,
            name: 'invalid_wrong_number',
            type: 'integer'
          },
          {
            comment: 'invalid already exists count',
            default: 0,
            isNullable: false,
            name: 'invalid_already_exists',
            type: 'integer'
          },
          {
            comment: 'invalid wrong carrier count',
            default: 0,
            isNullable: false,
            name: 'invalid_wrong_carrier',
            type: 'integer'
          },
          {
            comment: 'valid passwords count',
            default: 0,
            isNullable: false,
            name: 'valid_passwords',
            type: 'integer'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'leads.lead_import_stat'
      })
    );

    await queryRunner.createForeignKeys('leads.lead_import_stat', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'leads.dict_lead_import_stat_status'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "leads"."lead_import_stat" IS 'imported lead statistic';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.lead_import_stat');
  }
}
