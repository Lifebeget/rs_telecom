import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableUserAddCanTakeFromQueueExtraEarlyColumn1646227712336
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment:
          'can take contracts from queue with before extension variant allows',
        default: false,
        isNullable: false,
        name: 'can_take_from_queue_extra_early',
        type: 'boolean'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
