import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from '../modules/properties/entities';

export class AddContractImportWhiteList1646181285416
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values([
        {
          description: 'White list of IP address for contract import ',
          key: 'contractImportWhiteList',
          value: '[]'
        }
      ])
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
