import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission } from 'Server/utils';

export class AddFingerprintPermissionToCCAdminAndCCTeamlead1638939946194
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintView,
      Permission.fingerprintESignView
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintView,
      Permission.fingerprintESignView
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
