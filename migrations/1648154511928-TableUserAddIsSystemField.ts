import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { UserEntity } from '../modules/user/entities';

export class TableUserAddIsSystemField1648154511928
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'system user',
        default: false,
        isNullable: false,
        name: 'is_system',
        type: 'boolean'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .update(UserEntity, { isSystem: true })
      .where('first_name = :firstName')
      .andWhere('last_name = :lastName')
      .setParameters({
        firstName: 'Admin',
        lastName: 'System'
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', 'is_system');
  }
}
