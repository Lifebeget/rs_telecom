import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const tableName = 'contracts.contract';
const column = new TableColumn({
  default: false,
  isNullable: false,
  name: 'is_take_from_queue_locked',
  type: 'boolean'
});

export class TableContractAddIsTakeFromQueueLocked1630929384139
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(tableName, column);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(tableName, column);
  }
}
