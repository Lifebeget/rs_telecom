import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnValidationSuccess = new TableColumn({
  default: true,
  name: 'validation_success',
  type: 'boolean'
});

const columnLastValidatedAt = new TableColumn({
  default: 'now()',
  name: 'last_validated_at',
  type: 'timestamp with time zone'
});

export class TableContractAddValidationColumns1625240400303
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('contracts.contract', [
      columnValidationSuccess,
      columnLastValidatedAt
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('contracts.contract', [
      columnValidationSuccess,
      columnLastValidatedAt
    ]);
  }
}
