import { EnumValues } from 'enum-values';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

import { ContractAppointmentStatus } from 'Constants';

export class AlterTableContractAppointment1632958097598
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'appointment date',
            isNullable: false,
            name: 'date',
            type: 'timestamp with time zone'
          },
          {
            comment: 'appointment status',
            enum: EnumValues.getValues(ContractAppointmentStatus),
            isNullable: false,
            name: 'status',
            type: 'enum'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'user_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            comment: 'archived contract appointment flag',
            default: false,
            isNullable: false,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_appointment'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_appointment', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['user_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'users.user'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),
      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      })
    ]);

    await queryRunner.createIndices('contracts.contract_appointment', [
      new TableIndex({
        columnNames: ['date', 'contract_sid'],
        isUnique: true,
        name: 'UQ_contract_date',
        where: 'is_archived_flg is false'
      }),
      new TableIndex({
        columnNames: ['date', 'client_sid'],
        isUnique: true,
        name: 'UQ_client_date',
        where: 'is_archived_flg is false'
      }),
      new TableIndex({
        columnNames: ['date', 'user_sid'],
        isUnique: true,
        name: 'UQ_user_date',
        where: 'is_archived_flg is false'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_appointment" IS 'contract appointment';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_appointment');
  }
}
