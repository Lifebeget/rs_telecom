import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  PermissionMap['updateContractInfoInCC']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.updateContractInfoInCC]: 'update contract info in cc stage'
};

export class CreatePermissionUpdateForCC1657264121752
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(queryRunner.manager, 'callcenter presales agent', [
      Permission.updateContractInfoInCC
    ]);

    await insertPermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [Permission.updateContractInfoInCC]
    );

    await insertPermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.updateContractInfoInCC
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();

    await removePermission(queryRunner.manager, 'callcenter presales agent', [
      Permission.updateContractInfoInCC
    ]);

    await removePermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [Permission.updateContractInfoInCC]
    );

    await removePermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.updateContractInfoInCC
    ]);
  }
}
