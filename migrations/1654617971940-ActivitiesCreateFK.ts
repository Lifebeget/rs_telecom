import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class ActivitiesCreateFK1654617971940 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createForeignKeys(
      'activities.activity_contract_status_changed',
      [
        new TableForeignKey({
          columnNames: ['status_from_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'workflows.contract_status'
        }),
        new TableForeignKey({
          columnNames: ['status_to_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'workflows.contract_status'
        })
      ]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
