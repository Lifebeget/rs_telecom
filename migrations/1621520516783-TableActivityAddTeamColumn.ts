import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class TableActivityAddTeamColumn1621520516783
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'activities.activity',
      new TableColumn({
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'activities.activity',
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    );

    await queryRunner.dropColumn(
      'activities.activity',
      new TableColumn({
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    );
  }
}
