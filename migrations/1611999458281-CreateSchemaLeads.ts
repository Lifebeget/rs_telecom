import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaLeads1611999458281 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('leads');
    await queryRunner.query(
      `COMMENT ON SCHEMA "leads" IS 'all leads objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('leads');
  }
}
