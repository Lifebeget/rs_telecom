import { MigrationInterface, QueryRunner } from 'typeorm';

import { LeadStatus } from 'Constants';
import { LeadStatusEntity } from 'Server/modules/lead/entities';

export class DictLeadStatusRemoveInvalidPhoneNumber1620906959784
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(LeadStatusEntity)
      .where('name = :name', { name: 'invalidPhoneNumber' })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(LeadStatusEntity, ['name', 'description'])
      .values({
        description: 'Invalid phone number',
        name: 'invalidPhoneNumber' as LeadStatus
      })
      .execute();
  }
}
