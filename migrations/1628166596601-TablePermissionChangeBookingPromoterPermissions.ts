import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

const permissionsToRemove = [
  Permission.getShopPlanning,
  Permission.getShopSchedule,
  Permission.getUserOnline,
  Permission.viewTeamShop,
  Permission.removeMemberToShop,
  Permission.addMemberToShop,
  Permission.removeShop,
  Permission.updateShop,
  Permission.addShop,
  Permission.assignPromoters
];

export class TablePermissionChangeBookingPromoterPermissions1628166596601
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'booking promouter',
      permissionsToRemove
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(
      queryRunner.manager,
      'booking promouter',
      permissionsToRemove
    );
  }
}
