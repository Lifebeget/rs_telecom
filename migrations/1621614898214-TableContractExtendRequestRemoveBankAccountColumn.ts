import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractExtendRequestRemoveBankAccountColumn1621614898214
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // copy current "account" values into "iban"
    await queryRunner.manager
      .createQueryBuilder()
      .update('contracts.contract_extend_request_bank_data')
      .set({ iban: () => 'account' })
      .execute();

    await queryRunner.dropColumn(
      'contracts.contract_extend_request_bank_data',
      new TableColumn({
        comment: 'account',
        default: null,
        isNullable: true,
        name: 'account',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update('contracts.contract_extend_request_bank_data')
      .set({ account: () => 'iban' })
      .execute();

    await queryRunner.addColumn(
      'contracts.contract_extend_request_bank_data',
      new TableColumn({
        comment: 'account',
        default: null,
        isNullable: true,
        name: 'account',
        type: 'character varying(255)'
      })
    );
  }
}
