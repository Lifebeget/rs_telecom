import { MigrationInterface, QueryRunner } from 'typeorm';

import { ActivityTypeEntity } from '../modules/activity/entities';
import { ActivityType } from '../../Constants';

export class AddNewActivityType1655801643072 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ActivityTypeEntity, ['name', 'description'])
      .values({
        description: 'Contract was unassigned from',
        name: ActivityType.ContractFlowFinished
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
