import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableService1625191241007 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'name',
            default: null,
            isNullable: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'code',
            isNullable: false,
            isUnique: true,
            name: 'code',
            type: 'character varying(255)'
          },
          {
            comment: 'removable',
            default: null,
            isNullable: true,
            name: 'removable',
            type: 'boolean'
          },
          {
            comment: 'unremovable message',
            default: null,
            isNullable: true,
            name: 'unremovable_message',
            type: 'character varying(255)'
          },
          {
            comment: 'link mod',
            default: null,
            isNullable: true,
            name: 'link_mod',
            type: 'character varying(255)'
          },
          {
            comment: 'link',
            default: null,
            isNullable: true,
            name: 'link',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'services.service'
      })
    );

    await queryRunner.dropForeignKey(
      'contracts.contract_next_best_action_tip_service',
      'FK_391430be506b35917d362db625f'
    );

    await queryRunner.createForeignKey(
      'contracts.contract_next_best_action_tip_service',
      new TableForeignKey({
        columnNames: ['service_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'services.service'
      })
    );

    await queryRunner.dropTable('contracts.contract_service');

    await queryRunner.query(
      `COMMENT ON TABLE "services"."service" IS 'service list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('services.service');
  }
}
