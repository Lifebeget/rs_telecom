import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

const tableName = 'contracts.contract_extend_request';
const bankDataColumnName = 'contract_bank_data_sid';

export class TableExtendRequestReplaceBankDataWithContractBankData1626958106970
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      tableName,
      new TableColumn({
        isNullable: true,
        isUnique: true,
        name: bankDataColumnName,
        type: 'uuid' // nullable is temporary, before payment copying
      })
    );

    await queryRunner.createForeignKey(
      tableName,
      new TableForeignKey({
        columnNames: ['contract_bank_data_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_bank_data'
      })
    );

    await queryRunner.query(`
      UPDATE contracts.contract_extend_request 
      SET contract_bank_data_sid = contracts.contract.contract_bank_data_sid
      FROM contracts.contract
      WHERE contracts.contract.sid = contract_sid
    `);

    await queryRunner.changeColumn(
      tableName,
      bankDataColumnName,
      new TableColumn({
        isNullable: false,
        isUnique: true,
        name: bankDataColumnName,
        type: 'uuid'
      })
    );

    await queryRunner.dropTable('contracts.contract_extend_request_bank_data');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('Not revertable'); // it could be but not realized
  }
}
