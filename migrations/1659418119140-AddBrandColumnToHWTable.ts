import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddBrandColumnToHWTable1659418119140
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'hardware.hardware',
      new TableColumn({
        comment: 'hardware brand',
        default: null,
        isNullable: true,
        name: 'brand',
        type: 'character varying(255)'
      })
    );

    await queryRunner.query(`
    UPDATE hardware.hardware 
    SET brand = 
    CASE 
        WHEN name LIKE 'Apple%' THEN 'Apple'
        WHEN name LIKE 'Huawei%' THEN 'Huawei'
        WHEN name LIKE 'Blackview%' THEN 'Blackview' 
        WHEN name LIKE 'Nokia%' THEN 'Nokia'
        WHEN name LIKE 'Oppo%' THEN 'Oppo'
        WHEN name LIKE 'Samsung%' THEN 'Samsung'
        WHEN name LIKE 'Sony%' THEN 'Sony'
        WHEN name LIKE 'Xiaomi%' THEN 'Xiaomi'
        END`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;

    throw new Error('this migration is irrevertable!');
  }
}
