import { MigrationInterface, QueryRunner } from 'typeorm';

const tableName = 'contracts.contract_x_flag';

export class TableContractXFlagChangePrimaryKeyConstraint1630481338420
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropPrimaryKey(tableName);

    await queryRunner.createPrimaryKey(tableName, ['contract_sid', 'flag']);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropPrimaryKey(tableName);

    await queryRunner.createPrimaryKey(tableName, ['contract_sid']);
  }
}
