import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableLeadsImportAddValidateFileId1623393204018
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'leads.lead_import_stat',
      new TableColumn({
        comment: 'leads import validate file name',
        default: null,
        isNullable: true,
        name: 'validate_file_id',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('leads.lead_import_stat', 'validate_file_id');
  }
}
