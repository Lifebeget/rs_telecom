import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTariffXTeam1609833735805 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'tariff_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'override tariff call center commission',
            default: null,
            isNullable: true,
            name: 'call_center_commission',
            type: 'real'
          },
          {
            comment: 'override tariff client commission',
            default: null,
            isNullable: true,
            name: 'client_commission',
            type: 'real'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'tariffs.tariff_x_team'
      })
    );

    await queryRunner.createForeignKeys('tariffs.tariff_x_team', [
      new TableForeignKey({
        columnNames: ['tariff_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'tariffs.tariff'
      }),
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "tariffs"."tariff_x_team" IS 'list tariff and team';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tariffs.tariff_x_team');
  }
}
