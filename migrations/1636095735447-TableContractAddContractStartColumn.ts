import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractAddContractStartColumn1636095735447
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'Contract start date (somehow it could be useful)',
        default: null,
        isNullable: true,
        name: 'contract_start_date',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
