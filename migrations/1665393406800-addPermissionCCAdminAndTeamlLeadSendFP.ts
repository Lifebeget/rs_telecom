import { MigrationInterface, QueryRunner } from 'typeorm';

import { PermissionEntity } from '../modules/role/entities';
import { Permission } from '../../Constants';
import { insertPermission, removePermission } from '../utils';

export class addPermissionCCAdminAndTeamlLeadSendFP1665393406800
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values({
        description: 'permission to send fingerprint',
        name: Permission.fingerprintSend
      })
      .execute();

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintSend
    ]);

    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintSend
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.fingerprintSend
    ]);
    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.fingerprintSend
    ]);
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name = :name', {
        name: Permission.fingerprintSend
      })
      .execute();
  }
}
