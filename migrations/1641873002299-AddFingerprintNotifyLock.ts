import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddFingerprintNotifyLock1641873002299
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.fingerprint_notify_lock'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."fingerprint_notify_lock" IS 'fingerprint notify lock';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
