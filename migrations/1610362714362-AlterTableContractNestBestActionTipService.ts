import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableContractNestBestActionTipService1610362714362
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'next_best_action_tip_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'service_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_next_best_action_tip_service'
      })
    );

    await queryRunner.createPrimaryKey(
      'contracts.contract_next_best_action_tip_service',
      ['next_best_action_tip_sid', 'service_sid']
    );

    await queryRunner.createForeignKeys(
      'contracts.contract_next_best_action_tip_service',
      [
        new TableForeignKey({
          columnNames: ['next_best_action_tip_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.contract_next_best_action_tip'
        }),
        new TableForeignKey({
          columnNames: ['service_sid'],
          referencedColumnNames: ['sid'],
          referencedTableName: 'contracts.contract_service'
        })
      ]
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_next_best_action_tip_service" IS 'contract next best action tip service list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(
      'contracts.contract_next_best_action_tip_service'
    );
  }
}
