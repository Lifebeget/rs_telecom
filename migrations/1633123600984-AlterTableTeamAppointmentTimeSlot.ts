import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTeamAppointmentTimeSlot1633123600984
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'start offset from daystart in millis',
            isNullable: false,
            name: 'start',
            type: 'integer'
          },
          {
            comment: 'end offset from daystart in millis',
            isNullable: false,
            name: 'end',
            type: 'integer'
          },
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            comment: 'archived appointment time slot flag',
            default: false,
            isNullable: false,
            name: 'is_archived_flg',
            type: 'boolean'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.team_appointment_time_slot'
      })
    );

    await queryRunner.createForeignKey(
      'teams.team_appointment_time_slot',
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "teams"."team_appointment_time_slot" IS 'team appointment time slot';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.team_appointment_time_slot');
  }
}
