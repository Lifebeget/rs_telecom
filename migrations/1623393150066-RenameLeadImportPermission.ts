import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

export class RenameLeadImportPermission1623393150066
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update(PermissionEntity, { name: Permission.leadsImport })
      .where('name = :name', { name: 'importCommonLeads' })
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .update(PermissionEntity, { name: Permission.leadsImportWithPassword })
      .where('name = :name', { name: 'importAdvancedLeads' })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update(PermissionEntity, { name: 'importCommonLeads' as Permission })
      .where('name = :name', { name: Permission.leadsImport })
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .update(PermissionEntity, { name: 'importAdvancedLeads' as Permission })
      .where('name = :name', { name: Permission.leadsImportWithPassword })
      .execute();
  }
}
