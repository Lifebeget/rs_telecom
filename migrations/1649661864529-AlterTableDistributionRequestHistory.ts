import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableDistributionRequestHistory1649661864529
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'postcode',
            isNullable: false,
            name: 'postcode',
            type: 'character varying(255)'
          },
          {
            comment: 'phone number',
            isNullable: false,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'national destination code',
            isNullable: false,
            name: 'ndc',
            type: 'character varying(255)'
          },
          {
            comment: 'mobile subscriber integrated services digital number',
            isNullable: false,
            name: 'msisdn',
            type: 'character varying(255)'
          },
          {
            comment: 'request content',
            isNullable: false,
            name: 'request_content',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.distribution_request_history'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.distribution_request_history',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.distribution_request_history',
      new TableForeignKey({
        columnNames: ['member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."distribution_request_history" IS 'distribution request history';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.distribution_request_history');
  }
}
