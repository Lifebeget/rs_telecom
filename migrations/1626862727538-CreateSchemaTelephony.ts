import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaTelephony1626862727538 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('telephony');
    await queryRunner.query(
      `COMMENT ON SCHEMA "telephony" IS 'all related to telephony';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('telephony');
  }
}
