import { MigrationInterface, QueryRunner, Table } from 'typeorm';

const tableName = 'contracts.contract_bank_data';

export class AlterTableContractBankData1626947195560
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'payment method',
            default: null,
            isNullable: true,
            name: 'payment_method',
            type: 'character varying(255)'
          },
          {
            comment: 'iban',
            default: null,
            isNullable: true,
            name: 'iban',
            type: 'character varying(255)'
          },
          {
            comment: 'account number',
            default: null,
            isNullable: true,
            name: 'account_number',
            type: 'character varying(255)'
          },
          {
            comment: 'bic',
            default: null,
            isNullable: true,
            name: 'bic',
            type: 'character varying(255)'
          },
          {
            comment: 'bank code',
            default: null,
            isNullable: true,
            name: 'bank_code',
            type: 'character varying(255)'
          },
          {
            comment: 'bank name',
            default: null,
            isNullable: true,
            name: 'bank_name',
            type: 'character varying(255)'
          },
          {
            comment: 'owner name',
            default: null,
            isNullable: true,
            name: 'owner_name',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: tableName
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(tableName);
  }
}
