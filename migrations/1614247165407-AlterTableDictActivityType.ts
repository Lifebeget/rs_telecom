import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AlterTableDictActivityType1614247165407
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'activity type name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'activity type description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'activities.dict_activity_type'
      })
    );

    // await queryRunner.manager
    //   .createQueryBuilder()
    //   .insert()
    //   .into(ActivityTypeEntity, ['name', 'description'])
    //   .values([
    //     {
    //       name: "ClientAdded",
    //       description: 'client added'
    //     },
    //     {
    //       name: "NoteCreated",
    //       description: 'contract note not created'
    //     },
    //     {
    //       name: "ContractAdded",
    //       description: 'contract added'
    //     },
    //     {
    //       name: "LeadCarrierFetched",
    //       description: 'lead carrier fetched'
    //     },
    //     {
    //       name: "ContractFetched",
    //       description: 'contract fetched'
    //     },
    //     {
    //       name: "ContractMoved",
    //       description: 'contract moved to another team'
    //     },
    //     {
    //       name: "ContractConfirmedByQC",
    //       description: 'contract confirmed by QC team'
    //     },
    //     {
    //       name: "ContractNotInterested",
    //       description: 'contract not interested'
    //     }
    //   ])
    //   .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."dict_activity_type" IS 'list of activity type';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.dict_activity_type');
  }
}
