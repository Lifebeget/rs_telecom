import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableTeamSuggestedRole1608016365312
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'team_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'role_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.team_suggested_role'
      })
    );

    await queryRunner.createPrimaryKey('teams.team_suggested_role', [
      'team_sid',
      'role_sid'
    ]);

    await queryRunner.createForeignKeys('teams.team_suggested_role', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      }),

      new TableForeignKey({
        columnNames: ['role_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.role'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE teams.team_suggested_role IS 'list of teams and suggested roles';`
    );

    // old role inserts were removed from here
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.team_suggested_role');
  }
}
