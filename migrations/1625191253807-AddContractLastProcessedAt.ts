import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddContractLastProcessedAt1625191253807
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract',
      new TableColumn({
        comment: 'last processed date',
        default: null,
        isNullable: true,
        name: 'last_processed_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract', 'last_processed_at');
  }
}
