import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddCronJobLock1643709922378 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        DROP TABLE contracts.fingerprint_notify_lock;
      `);

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'job name',
            isNullable: false,
            name: 'job_name',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'public.cron_job_lock'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "public"."cron_job_lock" IS 'cron job lock';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
