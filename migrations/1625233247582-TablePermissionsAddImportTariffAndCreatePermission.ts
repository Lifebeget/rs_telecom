import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import {
  PermissionEntity,
  RoleXPermissionEntity
} from 'Server/modules/role/entities';

import { findOnePermission, findOneRole } from '../utils';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['importTariffAndCreate']
  | PermissionMap['importTariffAndUpdate']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.importTariffAndCreate]: 'import tariffs and create',
  [Permission.importTariffAndUpdate]: 'import tariffs and update'
};

export class TablePermissionsAddImportTariffAndCreatePermission1625233247582
  implements MigrationInterface {
  private async insertInto(
    manager: EntityManager,
    role: string,
    permissions: Permission[]
  ): Promise<void> {
    const { sid: roleSid } = await findOneRole(manager, role);
    const permissionEntities = await Promise.all(
      permissions.map(permission => findOnePermission(manager, permission))
    );

    await manager
      .createQueryBuilder()
      .insert()
      .into(RoleXPermissionEntity, ['role', 'permission'])
      .values(
        permissionEntities.map(({ sid: permissionSid }) => ({
          permission: permissionSid,
          role: roleSid
        }))
      )
      .execute();
  }

  private async removeInto(
    manager: EntityManager,
    role: string,
    permissions: Permission[]
  ): Promise<void> {
    const { sid: roleSid } = await findOneRole(manager, role);
    const permissionEntities = await Promise.all(
      permissions.map(permission => findOnePermission(manager, permission))
    );

    await Promise.all(
      permissionEntities.map(({ sid: permissionSid }) =>
        manager
          .createQueryBuilder()
          .delete()
          .from(RoleXPermissionEntity)
          .where('role_sid := roleSid', { roleSid })
          .andWhere('permission_sid := permissionSid', { permissionSid })
          .execute()
      )
    );
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await this.insertInto(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await this.removeInto(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();
  }
}
