import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { LeadRequestMethod } from 'Constants';
import { LeadDictRequestMethodEntity } from 'Server/modules/lead/entities';

export class AlterTableDictRequestMethod1661093965776
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'method name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'method description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'leads.dict_request_method'
      })
    );

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(LeadDictRequestMethodEntity, ['name', 'description'])
      .values([
        {
          description: 'Request method Easy',
          name: LeadRequestMethod.Easy
        },
        {
          description: 'Request method vo',
          name: LeadRequestMethod.VO
        }
      ])
      .execute();

    await queryRunner.query(
      `COMMENT ON TABLE "leads"."dict_request_method" IS 'list of  request methods';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('leads.dict_request_method');
  }
}
