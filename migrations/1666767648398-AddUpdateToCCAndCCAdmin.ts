import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class AddUpdateToCCAndCCAdmin1666767648398
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.updateContractInfo
    ]);

    await insertPermission(queryRunner.manager, 'callcenter admin', [
      Permission.updateContractInfo
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'callcenter teamlead', [
      Permission.updateContractInfo
    ]);

    await removePermission(queryRunner.manager, 'callcenter admin', [
      Permission.updateContractInfo
    ]);
  }
}
