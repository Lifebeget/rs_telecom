import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

export class WorkflowsAddAnotherTeamInActualFlow1657557051711
  implements MigrationInterface {
  // eslint-disable-next-line sonarjs/cognitive-complexity -- is's migration
  async workflowAction({
    teamSid,
    manager,
    contractSid,
    createdAt
  }: {
    teamSid: string;
    manager: EntityManager;
    contractSid: string;
    createdAt: string;
  }): Promise<boolean> {
    const getLastFlow = await manager.query(
      `SELECT * FROM workflows.contract_flow  WHERE contract_sid = '${contractSid}' ORDER BY created_at DESC LIMIT 1`
    );

    if (!getLastFlow || !getLastFlow[0]) {
      return true;
    }

    const flowSid = getLastFlow[0]['sid'] as string;

    const checkTeam = await manager.query(
      `SELECT * FROM workflows.contract_flow_team  WHERE team_sid = '${teamSid}' AND flow_sid = '${flowSid}' ORDER BY created_at DESC LIMIT 1`
    );
    if (checkTeam && checkTeam[0] && checkTeam[0]['sid']) {
      return true;
    }

    const isArchived = getLastFlow[0]['is_archived'] as string;
    const sqlAddTeam = `INSERT INTO workflows.contract_flow_team (sid, flow_sid, team_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${teamSid}'::uuid, 'false'::boolean, '${isArchived}'::boolean, '${createdAt}'::timestamp with time zone, null::timestamp with time zone)`;
    await manager.query(sqlAddTeam);

    return true;
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const teams = await queryRunner.manager.query(
      `SELECT ccxt.contract_sid, ccxt.team_sid, ccxt.created_at, team.type as type
      FROM contracts.contract_x_team as ccxt
      LEFT JOIN teams.team as team ON team.sid = ccxt.team_sid
      WHERE team.type != 'CC' and team.type != 'Organization' `
    );
    if (!teams || !teams[0]) {
      throw new Error('Members not found');
    }

    for await (const data of teams) {
      const teamSid = data['team_sid'];
      const contractSid = data['contract_sid'];
      const createdAt = new Date(data['created_at']).toISOString();
      await this.workflowAction({
        contractSid,
        createdAt,
        manager: queryRunner.manager,
        teamSid
      });
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
