import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';
import { EnumValues } from 'enum-values';

import { ContractRequestType } from 'Constants';

export class AlterTableContractRequestHistory1625191201906
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'request type',
            enum: EnumValues.getValues(ContractRequestType),
            isNullable: false,
            name: 'request_type',
            type: 'enum'
          },
          {
            comment: 'phone number',
            isNullable: false,
            name: 'phone_number',
            type: 'character varying(255)'
          },
          {
            comment: 'password',
            default: null,
            isNullable: true,
            name: 'password',
            type: 'character varying(255)'
          },
          {
            default: null,
            isNullable: true,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'request content',
            isNullable: false,
            name: 'request_content',
            type: 'text'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_request_history'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_request_history',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_request_history" IS 'contract request history';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_request_history');
  }
}
