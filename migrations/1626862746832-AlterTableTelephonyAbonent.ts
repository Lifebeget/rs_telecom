import { MigrationInterface, QueryRunner, Table } from 'typeorm';

const tableName = 'telephony.abonent';

export class AlterTableTelephonyAbonent1626862746832
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            name: 'name',
            type: 'character varying(255)'
          },
          {
            isUnique: true,
            name: 'external_id',
            type: 'character varying(255)' // it's a unique id from voximplant api
          },
          {
            name: 'password',
            type: 'character varying(255)'
          },
          {
            isUnique: true,
            name: 'member_sid',
            type: 'uuid' // only one abonent per one member
          },
          {
            comment: 'create date',
            default: 'now()',
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        foreignKeys: [
          {
            columnNames: ['member_sid'],
            referencedColumnNames: ['sid'],
            referencedTableName: 'teams.member'
          }
        ],
        name: tableName
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(tableName);
  }
}
