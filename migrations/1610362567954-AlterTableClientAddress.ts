import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableClientAddress1610362567954
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'client_sid',
            type: 'uuid'
          },
          {
            comment: 'client appeal (miss, mister, madam)',
            default: null,
            isNullable: true,
            name: 'appeal',
            type: 'character varying(255)'
          },
          {
            comment: 'client name',
            default: null,
            isNullable: true,
            name: 'first_name',
            type: 'character varying(255)'
          },
          {
            comment: 'client surname',
            default: null,
            isNullable: true,
            name: 'surname',
            type: 'character varying(255)'
          },
          {
            comment: 'address street',
            default: null,
            isNullable: true,
            name: 'street',
            type: 'character varying(255)'
          },
          {
            comment: 'address house',
            default: null,
            isNullable: true,
            name: 'house',
            type: 'character varying(255)'
          },
          {
            comment: 'address postcode',
            default: null,
            isNullable: true,
            name: 'postcode',
            type: 'character varying(255)'
          },
          {
            comment: 'address location',
            default: null,
            isNullable: true,
            name: 'location',
            type: 'character varying(255)'
          },
          {
            comment: 'address land',
            default: null,
            isNullable: true,
            name: 'land',
            type: 'character varying(255)'
          },
          {
            comment: 'email address',
            default: null,
            isNullable: true,
            name: 'email_address',
            type: 'character varying(255)'
          },
          {
            comment: 'email type',
            default: null,
            isNullable: true,
            name: 'email_type',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'clients.client_address'
      })
    );

    await queryRunner.createForeignKey(
      'clients.client_address',
      new TableForeignKey({
        columnNames: ['client_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'clients.client'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "clients"."client_address" IS 'client address';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clients.client_address');
  }
}
