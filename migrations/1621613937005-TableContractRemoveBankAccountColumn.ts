import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractRemoveBankAccountColumn1621613937005
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // copy current "account" values into "iban"
    await queryRunner.manager
      .createQueryBuilder()
      .update('contracts.contract_payment')
      .set({ iban: () => 'account' })
      .execute();

    await queryRunner.dropColumn(
      'contracts.contract_payment',
      new TableColumn({
        comment: 'account',
        default: null,
        isNullable: true,
        name: 'account',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .update('contracts.contract_payment')
      .set({ account: () => 'iban' })
      .execute();

    await queryRunner.addColumn(
      'contracts.contract_payment',
      new TableColumn({
        comment: 'account',
        default: null,
        isNullable: true,
        name: 'account',
        type: 'character varying(255)'
      })
    );
  }
}
