import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission, removePermission } from 'Server/utils';

export class AddFingerPrintEsignDownloadPermissionToQCAndActivation1653371372602
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(queryRunner.manager, 'qc agent', [
      Permission.fingerprintESignDownload
    ]);
    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintESignDownload
    ]);
    await insertPermission(queryRunner.manager, 'activation agent', [
      Permission.fingerprintESignDownload
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(queryRunner.manager, 'qc agent', [
      Permission.fingerprintESignDownload
    ]);
    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintESignDownload
    ]);
    await removePermission(queryRunner.manager, 'activation agent', [
      Permission.fingerprintESignDownload
    ]);
  }
}
