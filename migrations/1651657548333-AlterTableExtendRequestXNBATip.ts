import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableExtendRequestXNBATip1651657548333
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'contract extend request identifier',
            isNullable: false,
            isPrimary: true,
            name: 'extend_request_sid',
            type: 'uuid'
          },
          {
            comment: 'nba tip identifier',
            isNullable: false,
            isPrimary: true,
            name: 'nba_tip_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.extend_request_x_nba_tip'
      })
    );

    await queryRunner.createForeignKeys('contracts.extend_request_x_nba_tip', [
      new TableForeignKey({
        columnNames: ['extend_request_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_extend_request'
      }),
      new TableForeignKey({
        columnNames: ['nba_tip_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_next_best_action_tip'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.extend_request_x_nba_tip');
  }
}
