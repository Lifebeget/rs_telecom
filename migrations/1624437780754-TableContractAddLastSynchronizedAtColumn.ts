import { pick } from 'remeda';
import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

import { ContractEntity } from 'Server/modules/contract/entities';

const columnLastSynchronizedAt = new TableColumn({
  comment: 'contract last synchronization date',
  isNullable: false,
  name: 'last_synchronized_at',
  type: 'timestamp with time zone'
});

export class TableContractAddLastSynchronizedAtColumn1624437780754
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const columnLastSynchronizedAtNullable = new TableColumn({
      ...pick(columnLastSynchronizedAt, [
        'name',
        'type',
        'isNullable',
        'comment'
      ]),
      default: null,
      isNullable: true
    });

    await queryRunner.addColumn(
      'contracts.contract',
      columnLastSynchronizedAtNullable
    );

    await queryRunner.manager
      .createQueryBuilder(ContractEntity, 'c')
      .update()
      .set({
        lastSynchronizedAt: () => 'created_at'
      })
      .execute();

    await queryRunner.changeColumn(
      'contracts.contract',
      columnLastSynchronizedAtNullable,
      columnLastSynchronizedAt
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'contracts.contract',
      columnLastSynchronizedAt
    );
  }
}
