import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaLimits1615699401198 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('limits');
    await queryRunner.query(
      `COMMENT ON SCHEMA "limits" IS 'all limits objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('limits');
  }
}
