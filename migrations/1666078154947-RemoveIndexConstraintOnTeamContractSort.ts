import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveIndexConstraintOnTeamContractSort1666078154947
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropUniqueConstraint(
      'teams.team_x_contract_sort',
      'UQ_e63cf3435e16ed05607be47d0a4'
    );
    await queryRunner.dropUniqueConstraint(
      'teams.team_x_contract_sort',
      'UQ_9562e76ece059dae921eae37e0e'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('This migration is irrevertable');
  }
}
