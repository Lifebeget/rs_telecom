import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['fingerprintView']
  | PermissionMap['fingerprintESignView']
  | PermissionMap['fingerprintEdit']
  | PermissionMap['fingerprintESignEdit']
  | PermissionMap['fingerprintDownload']
  | PermissionMap['fingerprintESignDownload']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.fingerprintView]: 'view fingerprint',
  [Permission.fingerprintESignView]: 'view esing fingerprint',
  [Permission.fingerprintEdit]: 'sent fingerprint',
  [Permission.fingerprintESignEdit]: 'sent esign fingerprint',
  [Permission.fingerprintDownload]: 'download fingerprint',
  [Permission.fingerprintESignDownload]: 'download esing fingerprint'
};

export class AddFingerprintPermission1622020862476
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permission).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );

    await insertPermission(queryRunner.manager, 'callcenter presales agent', [
      Permission.fingerprintView,
      Permission.fingerprintEdit
    ]);
    await insertPermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.fingerprintView,
      Permission.fingerprintEdit
    ]);
    await insertPermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [Permission.fingerprintView, Permission.fingerprintEdit]
    );

    await insertPermission(queryRunner.manager, 'qc agent', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignEdit
    ]);
    await insertPermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignEdit
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor root',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor regular',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permission) as Permission[]
    );

    await removePermission(queryRunner.manager, 'callcenter presales agent', [
      Permission.fingerprintView,
      Permission.fingerprintEdit
    ]);
    await removePermission(queryRunner.manager, 'callcenter sales agent', [
      Permission.fingerprintView,
      Permission.fingerprintEdit
    ]);
    await removePermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      [Permission.fingerprintView, Permission.fingerprintEdit]
    );

    await removePermission(queryRunner.manager, 'qc agent', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignEdit
    ]);
    await removePermission(queryRunner.manager, 'qc teamlead', [
      Permission.fingerprintESignView,
      Permission.fingerprintESignEdit
    ]);

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permission)
      })
      .execute();
  }
}
