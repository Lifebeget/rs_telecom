import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSchemaActivities1614245414042 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createSchema('activities');
    await queryRunner.query(
      `COMMENT ON SCHEMA "activities" IS 'all activities objects';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('activities');
  }
}
