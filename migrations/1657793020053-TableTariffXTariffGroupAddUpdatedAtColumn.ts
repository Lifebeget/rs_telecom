import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTariffXTariffGroupAddUpdatedAtColumn1657793020053
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'tariffs.tariff_x_tariff_group',
      new TableColumn({
        comment: 'update date',
        default: 'now()',
        isNullable: false,
        name: 'updated_at',
        type: 'timestamp with time zone'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('tariffs.tariff_x_tariff_group', 'updated_at');
  }
}
