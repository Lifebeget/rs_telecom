import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveQCTimeout1654156298788 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(`DELETE
    FROM contracts.contract_status_log
    WHERE sid IN (
        SELECT sid
        FROM contracts.contract_status_log as ccsl
        WHERE status_sid = 'b0047ed0-82e7-4fb9-ac3b-0d6c49a9ca72'
          AND created_at >= '2022-04-26 00:00:00 +00:00'
          AND created_at <= '2022-04-26 23:59:59 +00:00'
          AND sid NOT IN (
            SELECT DISTINCT first_value(ccsl.sid) over (
                partition by ccsl.contract_sid
                order by ccsl.created_at asc
                RANGE BETWEEN
                    UNBOUNDED PRECEDING AND
                    UNBOUNDED FOLLOWING
                )
            FROM contracts.contract_status_log as ccsl
            WHERE status_sid = 'b0047ed0-82e7-4fb9-ac3b-0d6c49a9ca72'
              AND created_at >= '2022-04-26 00:00:00 +00:00'
              AND created_at <= '2022-04-26 23:59:59 +00:00'
        )
    );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
