import { MigrationInterface, QueryRunner } from 'typeorm';

import { ActivityType } from 'Constants';
import { ActivityTypeEntity } from 'Server/modules/activity/entities';

export class ActivityTypeAddContractAssigned1620991208895
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ActivityTypeEntity, ['name', 'description'])
      .values([
        {
          description: 'contract assigned',
          name: ActivityType.ContractAssigned
        }
      ])
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(ActivityTypeEntity)
      .where('name = :activityName', {
        activityName: ActivityType.ContractAssigned
      })
      .execute();
  }
}
