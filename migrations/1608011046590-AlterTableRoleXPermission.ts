import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { Permission } from 'Constants';
import { insertPermission } from 'Server/utils';
import { mergePermissions, permissionsWithout } from 'Helpers';

const allPermissions = [
  Permission.viewClientCard,
  Permission.addTeamClient,
  Permission.addClient,
  Permission.addClientNote,
  Permission.removeClientNote,
  Permission.viewMyContracts,
  Permission.viewTeamContracts,
  Permission.updateContract,
  Permission.rejectContract,
  Permission.addMemberContract,
  Permission.viewContractChecklist,
  Permission.updateContractChecklist,
  Permission.viewContractIssues,
  Permission.updateContractIssues,
  Permission.editContractExtendRequest,
  Permission.viewMyLeads,
  Permission.viewTeamLeads,
  Permission.addLead,
  Permission.updateLead,
  Permission.addTeamLead,
  Permission.canValidateLeadBySMS,
  Permission.canValidateLeadByPassword,
  Permission.leadsImportWithPassword,
  Permission.leadsImport,
  Permission.viewTeamInfo,
  Permission.addTeamMember,
  Permission.removeTeamMember,
  Permission.addTeam,
  Permission.updateTeam,
  Permission.addTeamContract,
  Permission.viewRoleInfo,
  Permission.addRole,
  Permission.addRolePermission,
  Permission.removeRolePermission,
  Permission.addMemberRole,
  Permission.removeMemberRole,
  Permission.addTeamRole,
  Permission.removeTeamRole,
  Permission.viewUserList,
  Permission.addUser,
  Permission.updateUser,
  Permission.blockUser,
  Permission.updateUserEmail,
  Permission.editSupervisors,
  Permission.viewHardwareInfo,
  Permission.addHardware,
  Permission.viewTariffInfo,
  Permission.updateTariffCommission,
  Permission.addTariff,
  Permission.viewDashboard
];

export class AlterTableUserXPermission1608011046590
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'role_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'permission_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'security.role_x_permission'
      })
    );

    await queryRunner.createPrimaryKey('security.role_x_permission', [
      'role_sid',
      'permission_sid'
    ]);

    await queryRunner.createForeignKeys('security.role_x_permission', [
      new TableForeignKey({
        columnNames: ['role_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.role'
      }),

      new TableForeignKey({
        columnNames: ['permission_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.permission'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "security"."role_x_permission" IS 'list of roles and permissions';`
    );

    await insertPermission(
      queryRunner.manager,
      'supervisor root',
      permissionsWithout(allPermissions, [Permission.viewDashboard])
    );

    await insertPermission(
      queryRunner.manager,
      'supervisor regular',
      permissionsWithout(allPermissions, [
        Permission.viewDashboard,
        Permission.editSupervisors
      ])
    );

    const permissionsCallCenterAgentPresales = [
      Permission.viewDashboard,
      Permission.addClient,
      Permission.addLead,
      Permission.viewMyLeads,
      Permission.canValidateLeadByPassword,
      Permission.canValidateLeadBySMS
    ];

    await insertPermission(
      queryRunner.manager,
      'callcenter presales agent',
      permissionsCallCenterAgentPresales
    );

    const permissionsCallCenterAgentSales = [
      Permission.updateContractIssues,
      Permission.viewClientCard,
      Permission.viewDashboard,
      Permission.addClientNote,
      Permission.removeClientNote,
      Permission.viewMyContracts,
      Permission.viewContractIssues,
      Permission.editContractExtendRequest,
      Permission.updateContract,
      Permission.rejectContract,
      Permission.viewHardwareInfo,
      Permission.viewTariffInfo
    ];
    await insertPermission(
      queryRunner.manager,
      'callcenter sales agent',
      permissionsCallCenterAgentSales
    );

    const permissionsCallCenterAgentPresalesSales = mergePermissions(
      permissionsCallCenterAgentPresales,
      permissionsCallCenterAgentSales,
      [Permission.canValidateLeadByPassword, Permission.canValidateLeadBySMS]
    );
    await insertPermission(
      queryRunner.manager,
      'callcenter presales&sales agent',
      permissionsCallCenterAgentPresalesSales
    );

    const permissionsCallCenterTeamLead = permissionsWithout(
      mergePermissions(permissionsCallCenterAgentPresalesSales, [
        Permission.viewTeamContracts,
        Permission.viewTeamLeads,
        Permission.viewTeamInfo,
        Permission.leadsImport,
        Permission.addTeamLead,
        Permission.addTeamClient
      ]),
      [Permission.viewDashboard]
    );
    await insertPermission(
      queryRunner.manager,
      'callcenter teamlead',
      permissionsCallCenterTeamLead
    );

    const permissionsCallCenterAdmin = mergePermissions(
      permissionsCallCenterTeamLead,
      [
        Permission.addUser,
        Permission.updateUser,
        Permission.updateUserEmail,
        Permission.blockUser
      ]
    );
    await insertPermission(
      queryRunner.manager,
      'callcenter admin',
      permissionsCallCenterAdmin
    );

    const permissionsQCAgent = [
      Permission.viewDashboard,
      Permission.viewClientCard,
      Permission.addClientNote,
      Permission.removeClientNote,
      Permission.viewMyContracts,
      Permission.viewContractChecklist,
      Permission.updateContractChecklist,
      Permission.editContractExtendRequest,
      Permission.updateContract,
      Permission.rejectContract,
      Permission.viewHardwareInfo,
      Permission.viewTariffInfo
    ];
    await insertPermission(queryRunner.manager, 'qc agent', permissionsQCAgent);

    const permissionsQCTeamlead = permissionsWithout(
      mergePermissions(permissionsQCAgent, [
        Permission.viewTeamContracts,
        Permission.viewTeamLeads,
        Permission.viewTeamInfo
      ]),
      [Permission.viewDashboard]
    );
    await insertPermission(
      queryRunner.manager,
      'qc teamlead',
      permissionsQCTeamlead
    );

    const permissionsBackofficeAdmin = mergePermissions(permissionsQCTeamlead, [
      Permission.addUser,
      Permission.updateUser,
      Permission.updateUserEmail,
      Permission.blockUser
    ]);
    await insertPermission(
      queryRunner.manager,
      'backoffice admin',
      permissionsBackofficeAdmin
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('security.role_x_permission');
  }
}
