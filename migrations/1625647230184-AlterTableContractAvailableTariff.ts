import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique
} from 'typeorm';

export class AlterTableContractAvailableTariff1625647230184
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'code',
            isNullable: false,
            name: 'code',
            type: 'character varying(255)'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_available_tariff'
      })
    );

    await queryRunner.createForeignKey(
      'contracts.contract_available_tariff',
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      })
    );

    await queryRunner.createUniqueConstraint(
      'contracts.contract_available_tariff',
      new TableUnique({
        columnNames: ['contract_sid', 'code']
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_available_tariff" IS 'contract available tariff list';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_available_tariff');
  }
}
