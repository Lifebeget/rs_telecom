import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddIdleOnContractXMember1637223126891
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      WITH contracts AS (
        SELECT contract_x_member.contract_sid, contract_x_member.member_sid, created_at
        FROM contracts.contract_x_member
        EXCEPT ALL
        SELECT contract_status_log.contract_sid, contract_status_log.member_sid, created_at
        FROM contracts.contract_status_log
        WHERE status_sid = (SELECT dict_contract_status.sid FROM contracts.dict_contract_status WHERE dict_contract_status.name = 'Idle')
      )
      INSERT INTO contracts.contract_status_log (contract_sid, status_sid, member_sid, created_at)
      SELECT contracts.contract_sid, (SELECT dict_contract_status.sid FROM contracts.dict_contract_status WHERE dict_contract_status.name = 'Idle'), contracts.member_sid, contracts.created_at
      FROM contracts;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
