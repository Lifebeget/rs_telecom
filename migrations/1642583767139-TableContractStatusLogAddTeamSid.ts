import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableContractStatusLogAddTeamSid1642583767139
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_status_log',
      new TableColumn({
        comment: 'the team of the member who sets the status',
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    );

    await queryRunner.manager.query(`
    WITH subquery AS (
      SELECT  csl.sid as sid, csl.member_sid as member, member.team_sid as team_sid 
      FROM contracts.contract_status_log as csl
      LEFT JOIN teams.member as member ON member.sid = csl.member_sid
    )
    UPDATE contracts.contract_status_log
    SET team_sid = subquery.team_sid
    FROM subquery AS subquery
    WHERE contract_status_log.sid = subquery.sid;
    `);

    await queryRunner.query(
      `create index if not exists index_contract_status_log_team_sid on contracts.contract_status_log (team_sid)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('contracts.contract_status_log', 'team_sid');
  }
}
