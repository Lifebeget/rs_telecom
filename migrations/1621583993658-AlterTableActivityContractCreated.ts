import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableActivityContractCreated1621583993658
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'activity_sid',
            type: 'uuid'
          },
          {
            comment: 'status from',
            isNullable: false,
            name: 'status_sid',
            type: 'uuid'
          }
        ],
        name: 'activities.activity_contract_created'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_created',
      new TableForeignKey({
        columnNames: ['activity_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'activities.activity'
      })
    );

    await queryRunner.createForeignKey(
      'activities.activity_contract_created',
      new TableForeignKey({
        columnNames: ['status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_status'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "activities"."activity_contract_created" IS 'activity contract created';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('activities.activity_contract_created');
  }
}
