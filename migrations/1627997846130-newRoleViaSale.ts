import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { RoleEntity } from 'Server/modules/role/entities';
import { insertPermission, removePermission } from 'Server/utils';

type PermissionKeys = Extract<
  Permission,
  | PermissionMap['addShop']
  | PermissionMap['updateShop']
  | PermissionMap['removeShop']
  | PermissionMap['addMemberToShop']
  | PermissionMap['removeMemberToShop']
  | PermissionMap['viewTeamShop']
  | PermissionMap['assignPromoters']
  | PermissionMap['getShopSchedule']
  | PermissionMap['getShopPlanning']
>;

const permission: Record<PermissionKeys, string> = {
  [Permission.addShop]: 'can create shop',
  [Permission.updateShop]: 'can update shop info',
  [Permission.removeShop]: 'can remove shop',
  [Permission.addMemberToShop]: 'can add member to shop',
  [Permission.removeMemberToShop]: 'can remove member to shop',
  [Permission.viewTeamShop]: 'can view only team shop',
  [Permission.assignPromoters]: 'assign promoters',
  [Permission.getShopSchedule]: 'get shop schedule',
  [Permission.getShopPlanning]: 'get shop planning'
};

export class newRoleViaSale1627997846130 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(RoleEntity, ['name'])
      .values([
        {
          name: 'booking area manager'
        },
        {
          name: 'booking regional manager'
        },
        {
          name: 'booking store manager'
        },
        {
          name: 'booking group manager'
        }
      ])
      .execute();

    await insertPermission(
      queryRunner.manager,
      'booking area manager',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking regional manager',
      Object.keys(permission) as Permission[]
    );
    await insertPermission(
      queryRunner.manager,
      'booking store manager',
      Object.keys(permission) as Permission[]
    );

    await insertPermission(
      queryRunner.manager,
      'booking group manager',
      Object.keys(permission) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'booking area manager',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking regional manager',
      Object.keys(permission) as Permission[]
    );
    await removePermission(
      queryRunner.manager,
      'booking store manager',
      Object.keys(permission) as Permission[]
    );

    await removePermission(
      queryRunner.manager,
      'booking group manager',
      Object.keys(permission) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(RoleEntity)
      .where('name IN (:...names)', {
        names: [
          'booking area manager',
          'booking regional manager',
          'booking store manager',
          'booking group manager'
        ]
      })
      .execute();
  }
}
