import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class AlterTableMemberXRole1608016521110 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            name: 'member_sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'role_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.member_x_role'
      })
    );

    await queryRunner.createPrimaryKey('teams.member_x_role', [
      'member_sid',
      'role_sid'
    ]);

    await queryRunner.createForeignKeys('teams.member_x_role', [
      new TableForeignKey({
        columnNames: ['member_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      }),

      new TableForeignKey({
        columnNames: ['role_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.role'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "teams"."member_x_role" IS 'list of members and roles';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.member_x_role');
  }
}
