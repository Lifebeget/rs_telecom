import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class ContractNextBestActionAddCascadeRemoving1625191237504
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'contracts.contract_next_best_action',
      'FK_809c87cff0f002bdc0f80a4411b'
    );

    await queryRunner.createForeignKey(
      'contracts.contract_next_best_action',
      new TableForeignKey({
        columnNames: ['nba_tip_sid'],
        onDelete: 'CASCADE',
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_next_best_action_tip'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'contracts.contract_next_best_action',
      'FK_809c87cff0f002bdc0f80a4411b'
    );

    await queryRunner.createForeignKey(
      'contracts.contract_next_best_action',
      new TableForeignKey({
        columnNames: ['nba_tip_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_next_best_action_tip'
      })
    );
  }
}
