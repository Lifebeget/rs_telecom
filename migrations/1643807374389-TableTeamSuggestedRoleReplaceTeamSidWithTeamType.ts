import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

import { Role, TeamType } from 'Constants';
import { addSuggestedRoleForTeamType } from 'Server/utils';

export class TableTeamSuggestedRoleReplaceTeamSidWithTeamType1643807374389
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('teams.team_suggested_role');

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'team type',
            isNullable: false,
            name: 'team_type',
            type: 'teams.team_type_enum'
          },
          {
            isNullable: false,
            name: 'role_sid',
            type: 'uuid'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'teams.team_suggested_role'
      })
    );

    await queryRunner.createPrimaryKey('teams.team_suggested_role', [
      'team_type',
      'role_sid'
    ]);

    await queryRunner.createForeignKey(
      'teams.team_suggested_role',
      new TableForeignKey({
        columnNames: ['role_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'security.role'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE teams.team_suggested_role IS 'list of team types and suggested roles';`
    );

    // insert data
    const teamToRoles: Record<TeamType, Role[]> = {
      [TeamType.Organization]: [
        Role['supervisor regular'],
        Role['supervisor root']
      ],
      [TeamType.CC]: [
        Role['callcenter admin'],
        Role['callcenter presales agent'],
        Role['callcenter presales&sales agent'],
        Role['callcenter sales agent'],
        Role['callcenter teamlead']
      ],
      [TeamType.QC]: [Role['qc agent'], Role['qc teamlead']],
      [TeamType.Activation]: [Role['activation agent']],
      [TeamType.BackOffice]: [Role['backoffice admin']],
      [TeamType.Promotion]: [
        Role['booking area manager'],
        Role['booking group manager'],
        Role['booking manager'],
        Role['booking promouter'],
        Role['booking regional manager'],
        Role['booking store manager'],
        Role['booking team leader'],
        Role['booking vf manager']
      ]
    };

    await Promise.all(
      Object.entries(teamToRoles).map(([teamType, roleNames]) =>
        addSuggestedRoleForTeamType(
          queryRunner.manager,
          teamType as TeamType,
          roleNames
        )
      )
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {
    throw new Error('not revertable');
  }
}
