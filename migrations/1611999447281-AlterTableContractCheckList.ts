import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique
} from 'typeorm';

export class AlterTableContractCheckList1611999447281
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identifier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            isNullable: false,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'sort index',
            isNullable: false,
            name: 'index',
            type: 'integer'
          },
          {
            comment: 'question text',
            isNullable: false,
            name: 'question',
            type: 'text'
          },
          {
            comment: 'comment text',
            default: null,
            isNullable: true,
            name: 'comment',
            type: 'text'
          },
          {
            isNullable: false,
            name: 'check_list_status_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'possibly_fulfilled',
            type: 'boolean'
          },
          {
            default: null,
            isNullable: true,
            name: 'possibly_fulfilled_member_sid',
            type: 'uuid'
          },
          {
            default: null,
            isNullable: true,
            name: 'possibly_fulfilled_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update date',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.contract_checklist'
      })
    );

    await queryRunner.createUniqueConstraint(
      'contracts.contract_checklist',
      new TableUnique({
        columnNames: ['contract_sid', 'index']
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_checklist', [
      new TableForeignKey({
        columnNames: ['contract_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract'
      }),
      new TableForeignKey({
        columnNames: ['check_list_status_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.dict_contract_checklist_status'
      }),
      new TableForeignKey({
        columnNames: ['possibly_fulfilled_member_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.member'
      })
    ]);

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."contract_checklist" IS 'list of contract check';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.contract_checklist');
  }
}
