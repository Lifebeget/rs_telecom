import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { OrderBy } from 'Constants';

export class AlterTableDictSort1652685522028 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE contracts.order_by_sort_enum AS ENUM ('ASC', 'DESC')`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            comment: 'identefier',
            default: 'uuid_generate_v4()',
            isNullable: false,
            isPrimary: true,
            name: 'sid',
            type: 'uuid'
          },
          {
            comment: 'sort name',
            isNullable: false,
            isUnique: true,
            name: 'name',
            type: 'character varying(255)'
          },
          {
            comment: 'sort description',
            default: null,
            isNullable: true,
            name: 'description',
            type: 'text'
          },
          {
            comment: 'name of column in contracts contract',
            default: `''`,
            isNullable: false,
            name: 'column_name',
            type: 'text'
          },
          {
            comment: 'order by sort order',
            default: `'${OrderBy.ASC}'`,
            isNullable: false,
            name: 'order',
            type: 'contracts.order_by_sort_enum'
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          },
          {
            comment: 'update time',
            default: 'now()',
            isNullable: false,
            name: 'updated_at',
            type: 'timestamp with time zone'
          }
        ],
        name: 'contracts.dict_sort'
      })
    );

    await queryRunner.query(
      `COMMENT ON TABLE "contracts"."dict_sort" IS 'list of sort variants';`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contracts.dict_sort');
  }
}
