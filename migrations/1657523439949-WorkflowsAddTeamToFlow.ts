import { EntityManager, MigrationInterface, QueryRunner } from 'typeorm';

export class WorkflowsAddTeamToFlow1657523439949 implements MigrationInterface {
  // eslint-disable-next-line sonarjs/cognitive-complexity -- is's migration
  async workflowAction({
    manager,
    createdAt,
    teamSid,
    flowSid,
    isDeleted,
    isStarted
  }: {
    manager: EntityManager;
    createdAt: string;
    teamSid: string;
    flowSid: string;
    isDeleted: string;
    isStarted: string;
  }): Promise<boolean> {
    const getActualTeam = await manager.query(
      `SELECT * FROM workflows.contract_flow_team as wcf WHERE flow_sid = '${flowSid}' AND team_sid = '${teamSid}' and is_deleted = '${isDeleted}'`
    );
    if (!getActualTeam || !getActualTeam[0]) {
      const sqlAddTeam = `INSERT INTO workflows.contract_flow_team (sid, flow_sid, team_sid, is_started, is_deleted, created_at, deleted_at) VALUES (DEFAULT, '${flowSid}'::uuid, '${teamSid}'::uuid, '${isStarted}'::boolean, '${isDeleted}'::boolean, '${createdAt}'::timestamp with time zone, null::timestamp with time zone)`;
      await manager.query(sqlAddTeam);
    }

    return true;
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    const members = await queryRunner.manager.query(
      `SELECT wcfm.member_sid as member_sid,
      member.team_sid as team_sid,
      wcfm.flow_sid as flow_sid,
      wcfm.is_deleted as is_deleted,
      wcfm.is_started as is_started,
      wcfm.created_at as created_at
      FROM workflows.contract_flow_member as wcfm
      LEFT JOIN teams.member as member ON member.sid = wcfm.member_sid`
    );
    if (!members || !members[0]) {
      throw new Error('Members not found');
    }

    for await (const data of members) {
      const teamSid = data['team_sid'];
      const flowSid = data['flow_sid'];
      const isDeleted = data['is_deleted'];
      const isStarted = data['is_started'];
      const createdAt = new Date(data['created_at']).toISOString();

      await this.workflowAction({
        createdAt,
        flowSid,
        isDeleted,
        isStarted,
        manager: queryRunner.manager,
        teamSid
      });
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
