import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columnArchived = new TableColumn({
  comment: 'tariff archived state',
  default: false,
  isNullable: false,
  name: 'archived',
  type: 'boolean'
});

export class TableTariffAddArchivedColumn1624271281894
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('tariffs.tariff', columnArchived);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('tariffs.tariff', columnArchived);
  }
}
