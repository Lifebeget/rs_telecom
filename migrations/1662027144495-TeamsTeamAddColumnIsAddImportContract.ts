import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TeamsTeamAddColumnIsAddImportContract1662027144495
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'teams.team',
      new TableColumn({
        comment: 'Adds a contract after import to the team',
        default: false,
        name: 'is_add_import_contract',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'is_add_import_contract');
  }
}
