import { MigrationInterface, QueryRunner } from 'typeorm';

import { ContractStatus, ContractStatusType } from 'Constants';
import {
  ContractStatusEntity,
  ContractStatusTypeEntity
} from 'Server/modules/workflow/entities';

export class WorkflowsContractStatusPending1654519787999
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const behavioralType = await queryRunner.manager
      .createQueryBuilder(ContractStatusTypeEntity, 'type')
      .where('name = :name', { name: ContractStatusType.Behavioral })
      .getOne();
    if (!behavioralType) {
      throw new Error('Behavioral type not found');
    }

    await queryRunner.manager.save(
      queryRunner.manager.create(ContractStatusEntity, {
        description:
          'Status Pending: set after the status "Not reached" when time has passed.',
        name: ContractStatus.Pending,
        typeSid: behavioralType.sid
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
