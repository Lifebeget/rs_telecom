import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { LeadCreationMethod } from 'Constants';

export class TableLeadAddMethod1623393206460 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'leads.lead',
      new TableColumn({
        comment: 'lead creation method',
        default: `'${LeadCreationMethod.Import}'`,
        enum: EnumValues.getValues(LeadCreationMethod),
        isNullable: false,
        name: 'method',
        type: 'enum'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('leads.lead', 'method');
  }
}
