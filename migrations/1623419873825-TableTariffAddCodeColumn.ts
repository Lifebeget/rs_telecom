import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TableTariffAddCodeColumn1623419873825
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'tariffs.tariff',
      new TableColumn({
        comment: 'tariff code',
        default: "''",
        isNullable: false,
        name: 'code',
        type: 'character varying(255)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'tariffs.tariff',
      new TableColumn({
        comment: 'tariff code',
        default: "''",
        isNullable: false,
        name: 'code',
        type: 'character varying(255)'
      })
    );
  }
}
