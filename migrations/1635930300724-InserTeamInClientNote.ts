import { MigrationInterface, QueryRunner } from 'typeorm';

export class InserTeamInClientNote1635930300724 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      WITH teams AS (
        SELECT client_note.sid AS client_note_sid, member.team_sid AS team_sid
        FROM clients.client_note
        LEFT JOIN teams.member ON client_note.member_sid = member.sid
      )
      UPDATE clients.client_note
      SET team_sid = teams.team_sid
      FROM teams
      WHERE teams.client_note_sid = client_note.sid;

      ALTER TABLE clients.client_note
        ALTER COLUMN team_sid SET NOT NULL;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('Not revertable');
  }
}
