import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, Role } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

const roles: Role[] = [
  Role['qc agent'],
  Role['callcenter presales&sales agent'],
  Role['activation agent'],
  Role['callcenter sales agent'],
  Role['callcenter presales agent']
];

export class AddPermissionForViewMyContractsTab1652868859451
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values({
        description: 'can view my contracts tab in navigation',
        name: Permission.viewMyContractsPage
      })
      .execute();

    await Promise.all(
      roles.map(async role => {
        await insertPermission(queryRunner.manager, role, [
          Permission.viewMyContractsPage
        ]);
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      roles.map(async role => {
        await removePermission(queryRunner.manager, role, [
          Permission.viewMyContractsPage
        ]);
      })
    );
  }
}
