import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, PermissionMap } from 'Constants';
import { PermissionEntity } from 'Server/modules/role/entities';

import { insertPermission, removePermission } from '../utils';

type PermissionKeys = Extract<Permission, PermissionMap['addTariffList']>;

const permissions: Record<PermissionKeys, string> = {
  [Permission.addTariffList]: 'add tariff list'
};

export class AddTariffListUploadingPermission1623135477979
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PermissionEntity, ['name', 'description'])
      .values(
        Object.entries(permissions).map(([name, description]) => ({
          description,
          name: name as Permission
        }))
      )
      .execute();

    await insertPermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await removePermission(
      queryRunner.manager,
      'supervisor system',
      Object.keys(permissions) as Permission[]
    );

    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PermissionEntity)
      .where('name IN (:...names)', {
        names: Object.keys(permissions)
      })
      .execute();
  }
}
