import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class PhoneNumberColumn1624628290894 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users.user',
      new TableColumn({
        comment: 'user phone number',
        isNullable: true,
        name: 'phone_number',
        type: 'varchar(20)'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users.user', 'phone_number');
  }
}
