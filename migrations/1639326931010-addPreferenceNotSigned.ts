import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from 'Server/modules/properties/entities/properties.entity';

export class addPreferenceNotSigned1639326931010 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values({
        // 3 * 24 * 60
        description: 'Status notSigned set time in minutes',

        key: 'notSignedSchedule',
        value: '4320'
      })
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(PropertiesEntity)
      .where({ key: 'notSignedSchedule' })
      .execute();
  }
}
