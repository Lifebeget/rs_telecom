import { MigrationInterface, QueryRunner, Table } from 'typeorm';

const tableName = 'contracts.contract_x_flag';
const enumTypeName = 'contracts.contract_flag_enum';

export class AlterTableContractXFlag1629202990939
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE ${enumTypeName} AS ENUM ('HWRequired', 'NotInterested', 'NotReached')`
    );

    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isNullable: false,
            isPrimary: true,
            name: 'contract_sid',
            type: 'uuid'
          },
          {
            comment: 'contract flag enum value',
            isNullable: false,
            name: 'flag',
            type: enumTypeName
          },
          {
            comment: 'create date',
            default: 'now()',
            isNullable: false,
            name: 'created_at',
            type: 'timestamp with time zone'
          }
        ],
        foreignKeys: [
          {
            columnNames: ['contract_sid'],
            referencedColumnNames: ['sid'],
            referencedTableName: 'contracts.contract'
          }
        ],
        name: tableName,
        uniques: [
          {
            columnNames: ['contract_sid', 'flag']
          }
        ]
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(tableName);

    await queryRunner.query(`DROP TYPE ${enumTypeName}`);
  }
}
