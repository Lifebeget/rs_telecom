import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class addTeamSidClientNote1633530884835 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('clients.client_note', [
      new TableColumn({
        comment: 'Team identifier',
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    ]);
    await queryRunner.createForeignKeys('clients.client_note', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('clients.client_note', [
      new TableForeignKey({
        columnNames: ['team_sid'],
        referencedColumnNames: ['sid'],
        referencedTableName: 'teams.team'
      })
    ]);
    await queryRunner.dropColumns('clients.client_note', [
      new TableColumn({
        comment: 'Team identifier',
        isNullable: true,
        name: 'team_sid',
        type: 'uuid'
      })
    ]);
  }
}
