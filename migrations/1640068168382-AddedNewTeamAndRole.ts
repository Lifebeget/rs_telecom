import { MigrationInterface, QueryRunner } from 'typeorm';

import { Permission, TeamType } from 'Constants';
import { insertPermission, insertTeamAndType } from 'Server/utils';

const permissionsActivationAgent = [
  Permission.viewDashboard,
  Permission.viewClientCard,
  Permission.addClientNote,
  Permission.removeClientNote,
  Permission.viewMyContracts,
  Permission.viewContractChecklist,
  Permission.updateContractChecklist,
  Permission.editContractExtendRequest,
  Permission.updateContract,
  Permission.rejectContract,
  Permission.viewHardwareInfo,
  Permission.viewTariffInfo,
  Permission.setFingerprintEmail
];
export class AddedNewTeamAndRole1640068168382 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await insertPermission(
      queryRunner.manager,
      'activation agent',
      permissionsActivationAgent
    );

    await insertTeamAndType(queryRunner.manager, 'Organization', [
      {
        name: 'Activation',
        type: TeamType.Activation
      }
    ]);

    // old role inserts were removed from here
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
  }
}
