import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class TeamsTeamAddColumnIsHideActivitiesInfo1664363717706
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'teams.team',
      new TableColumn({
        comment: 'Hide activity info like names and avatar',
        default: false,
        name: 'is_hide_activity_info',
        type: 'boolean'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('teams.team', 'is_hide_activity_info');
  }
}
