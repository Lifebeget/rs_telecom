import { MigrationInterface, QueryRunner } from 'typeorm';

import { PropertiesEntity } from '../modules/properties/entities';

export class AddDisableOfESignReminderProperties1647958693097
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(PropertiesEntity, ['key', 'value', 'description'])
      .values([
        {
          // 0 - off, 1 - on
          description:
            'Fingerprint notification enable option. If set 0 - notification will be disabled',

          key: 'fingerprintNotifyEnable',
          value: '0'
        }
      ])
      .execute();

    await queryRunner.manager.query(`
      UPDATE public.properties
      SET value = '0'
      WHERE properties.key = 'fingerprintNotifyRepeat';

      UPDATE public.properties
      SET value = '0'
      WHERE properties.key = 'fingerprintNotifyTime';
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    void queryRunner;
    throw new Error('cannot be reverted');
  }
}
