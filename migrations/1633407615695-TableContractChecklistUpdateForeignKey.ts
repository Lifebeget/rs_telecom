import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class TableContractChecklistUpdateForeignKey1633407615695
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'contracts.contract_checklist',
      new TableColumn({
        comment: 'contract extend request identifier',
        isNullable: true,
        name: 'contract_extend_request_sid',
        type: 'uuid'
      })
    );

    await queryRunner.createForeignKeys('contracts.contract_checklist', [
      new TableForeignKey({
        columnNames: ['contract_extend_request_sid'],
        name: 'FK_90d6bfba9ff10fe0adbd1e14a33',
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_extend_request'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'contracts.contract_checklist',
      new TableForeignKey({
        columnNames: ['contract_extend_request_sid'],
        name: 'FK_90d6bfba9ff10fe0adbd1e14a33',
        referencedColumnNames: ['sid'],
        referencedTableName: 'contracts.contract_extend_request'
      })
    );

    await queryRunner.dropColumn(
      'contracts.contract_checklist',
      'contract_extend_request_sid'
    );
  }
}
