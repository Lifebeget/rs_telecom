import fs from 'fs';

import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { WsAdapter } from '@nestjs/platform-ws';
import bodyParser from 'body-parser';
import { Request } from 'express';
import { Settings } from 'luxon';
import compression from 'compression';
import helmet from 'helmet';
import morgan from 'morgan';

import { tooBusy } from './middlewares';
import { AppModule } from './modules';
import { LoggerService } from './providers';
import { morganToJson } from './utils/morgan-custom.log';

Settings.defaultLocale = 'en';
Settings.defaultZoneName = 'utc';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix('/api');
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

  const configService = app.get<ConfigService>(ConfigService);
  const loggerService = app.get<LoggerService>(LoggerService);
  const port = configService.get<number>('SERVER_PORT', 5000);
  const environment = configService.get<string>('NODE_ENV', 'production');

  if (environment === 'production') {
    app.enableShutdownHooks();
  }

  if (environment === 'development') {
    const options = new DocumentBuilder()
      .addBearerAuth()
      .setTitle('Telecom')
      .build();
    const document = SwaggerModule.createDocument(app, options);

    fs.writeFileSync('./swagger.json', JSON.stringify(document));
    SwaggerModule.setup('swagger', app, document);
  }

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  morgan.format('json', morganToJson);

  app.use(
    morgan(environment !== 'development' ? 'json' : 'dev', {
      skip: (req: Request) => {
        const blackList = [
          '/api/health',
          '/api/leads-import/is-paused',
          '/api/leads-import/stats-from'
        ];

        return blackList.some(url => req.originalUrl.includes(url));
      }
    })
  );

  app.use(helmet());
  app.use(compression());

  if (environment === 'production') {
    app.use(tooBusy);
  }

  app.useWebSocketAdapter(new WsAdapter(app));

  await app.listen(Number(port));
  loggerService.info(
    `Service successful started on host http://127.0.0.1:${port}`
  );

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}

bootstrap();
