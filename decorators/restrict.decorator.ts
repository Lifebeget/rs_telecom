import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

import { JwtAuthGuard } from 'Server/guards';

export const Restrict = (): MethodDecorator =>
  applyDecorators(ApiBearerAuth(), UseGuards(JwtAuthGuard));
