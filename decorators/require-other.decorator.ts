import { ClassConstructor } from 'class-transformer';
import {
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  registerDecorator
} from 'class-validator';
import { isNil } from 'remeda';

@ValidatorConstraint({ name: 'RequireOther' })
export class RequireOtherConstraint implements ValidatorConstraintInterface {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- value can be any value u know value is just a value (any value literally) */
  validate(value: any, args: ValidationArguments) {
    const [fn] = args.constraints;
    const relatedValue = fn(args.object);

    if (!isNil(value) && isNil(relatedValue)) {
      return false;
    }

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    const [relatedPropertyName] = args.constraints;

    // TODO: more typings? @TripleHeaven
    return `${
      (relatedPropertyName as string | undefined) || 'property unknown'
    } or ${args.property} doesn't exist`;
  }
}

export const RequireOther = <T>(
  type: ClassConstructor<T>,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- required field can be any type */
  property: (o: T) => any,
  validationOptions?: ValidationOptions
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- pure object needed for constructor property */
) => (object: any, propertyName: string) => {
  registerDecorator({
    constraints: [property],
    options: validationOptions,
    propertyName,
    target: object.constructor,
    validator: RequireOtherConstraint
  });
};
