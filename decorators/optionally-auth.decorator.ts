import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

import { OptionalTokenGuard } from '../guards';

export const OptionallyAuth = (): MethodDecorator =>
  applyDecorators(ApiBearerAuth(), UseGuards(OptionalTokenGuard));
