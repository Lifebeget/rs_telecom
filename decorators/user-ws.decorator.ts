import { Request } from 'express';
import { ExecutionContext, createParamDecorator } from '@nestjs/common';

import { JwtPayloadModel } from '../modules/auth/models';

export const UserWs = createParamDecorator<
  string,
  ExecutionContext,
  JwtPayloadModel
>((data: string, context: ExecutionContext) => {
  const req = context.switchToWs().getData<Request>();

  return req.user as JwtPayloadModel;
});
