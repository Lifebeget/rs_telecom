import { Request } from 'express';
import { ApiExtraModels, ApiQuery, getSchemaPath } from '@nestjs/swagger';
import {
  ExecutionContext,
  applyDecorators,
  createParamDecorator
} from '@nestjs/common';

import { PaginationModel } from 'Server/models';

export const Pagination = createParamDecorator<
  string,
  ExecutionContext,
  PaginationModel
>((data, context) => {
  const req = context.switchToHttp().getRequest<Request>();

  return {
    /* eslint-disable @typescript-eslint/no-unnecessary-condition -- query may be undefined */
    skip: Number(req.query?.skip) || 0,
    take: Number(req.query?.take) || 10
    /* eslint-disable @typescript-eslint/no-unnecessary-condition -- query may be undefined */
  };
});

export const ApiPagination = () =>
  applyDecorators(
    ApiExtraModels(PaginationModel),
    ApiQuery({
      schema: {
        $ref: getSchemaPath(PaginationModel)
      }
    })
  );
