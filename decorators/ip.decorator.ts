import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { Request } from 'express';

export const Ip = createParamDecorator<unknown, ExecutionContext, string>(
  (data, context) => {
    const req = context.switchToHttp().getRequest<Request>();

    return (
      req.header('x-forwarded-for')?.split(',')[0] ||
      req.socket.remoteAddress ||
      ''
    );
  }
);
