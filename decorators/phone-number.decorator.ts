import { Injectable } from '@nestjs/common';
import { Transform } from 'class-transformer';
import {
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  registerDecorator
} from 'class-validator';

import { parsePhoneNumberToString, validatePhoneNumber } from 'Utils';

@ValidatorConstraint({ async: true, name: 'PhoneNumberValidate' })
@Injectable()
class PhoneNumberValidateClass implements ValidatorConstraintInterface {
  async validate(value: string) {
    try {
      return validatePhoneNumber(value);
    } catch (e) {
      return false;
    }
  }

  defaultMessage() {
    return `Phone number is not valid`;
  }
}

export function PhoneNumberValidate(validationOptions?: ValidationOptions) {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- any object can be decorated */
  return function (object: any, propertyName: string) {
    registerDecorator({
      constraints: [],
      name: 'PhoneNumberValidate',
      options: validationOptions,
      propertyName: propertyName,
      target: object.constructor,
      validator: PhoneNumberValidateClass
    });
  };
}

export const TransformNumber = (): PropertyDecorator =>
  Transform(({ value }) => parsePhoneNumberToString(value));
