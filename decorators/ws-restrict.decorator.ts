import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

import { JwtWsAuthGuard } from 'Server/guards/jwt-ws.guard';

export const WsRestrict = (): MethodDecorator =>
  applyDecorators(ApiBearerAuth(), UseGuards(JwtWsAuthGuard));
