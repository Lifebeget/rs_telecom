import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard, IAuthGuard } from '@nestjs/passport';

import { Errors } from 'Constants';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') implements IAuthGuard {
  handleRequest<TUser>(err: unknown, user: TUser | null): TUser {
    if (err || !user) {
      throw err || new UnauthorizedException(Errors.WRONG_AUTH_DATA);
    }

    return user;
  }
}
