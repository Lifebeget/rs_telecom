import {
  ExecutionContext,
  Injectable,
  UnauthorizedException
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { Errors } from 'Constants';

@Injectable()
export class JwtWsAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest<TUser>(err: unknown, user: TUser | null): TUser {
    if (err || !user) {
      throw err || new UnauthorizedException(Errors.WRONG_AUTH_DATA);
    }

    return user;
  }
}
