import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class OptionalTokenGuard extends AuthGuard('jwt') {
  handleRequest<User>(err: unknown, user: User): User | null {
    return user || null;
  }
}
