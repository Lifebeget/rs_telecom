import { ApiProperty } from '@nestjs/swagger';

export class PaginationModel {
  @ApiProperty({
    default: 10,
    description: 'Set limit of sample.',
    example: 10,
    required: false,
    type: Number
  })
  skip?: number | null;

  @ApiProperty({
    description: 'Set offset of sample.',
    example: 0,
    required: false,
    type: Number
  })
  take?: number | null;
}
