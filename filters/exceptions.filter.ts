import { Request, Response } from 'express';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Errors } from 'Constants';
import { LoggerService } from 'Server/providers';

type TException = {
  message: string;
  error: string;
};

@Catch()
export class ExceptionsFilter implements ExceptionFilter {
  constructor(
    private readonly configService: ConfigService,
    private readonly loggerService: LoggerService
  ) {}

  catch(err: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<Response>();
    const req = ctx.getRequest<Request>();

    this.loggerService.error(err.stack || '', {
      req
    });

    const environment = this.configService.get<string>(
      'NODE_ENV',
      'production'
    );

    if (err instanceof HttpException) {
      const status = err.getStatus();
      const exception = err.getResponse() as TException;

      return res.status(status).json({
        message: environment === 'development' ? exception.error : undefined,
        statusCode: exception.message
      });
    }

    res.status(500).json({
      statusCode: Errors.SOMETHING_WENT_WRONG
    });
  }
}
