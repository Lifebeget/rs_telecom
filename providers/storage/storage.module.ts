import { DynamicModule, Global, Module } from '@nestjs/common';

import { STORAGE_MODULE_OPTIONS } from './constants';
import { StorageModuleOptions } from './types';
import { StorageService } from './storage.service';

@Global()
@Module({})
export class StorageModule {
  public static forRootAsync(options: StorageModuleOptions): DynamicModule {
    return {
      exports: [StorageService],
      module: StorageModule,
      providers: [
        {
          provide: STORAGE_MODULE_OPTIONS,
          ...options
        },
        StorageService
      ]
    };
  }
}
