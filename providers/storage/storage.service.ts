import { Inject, Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';

import { StorageOptions } from './types';
import { STORAGE_MODULE_OPTIONS } from './constants';

@Injectable()
export class StorageService {
  public readonly client: S3;

  constructor(
    @Inject(STORAGE_MODULE_OPTIONS)
    private readonly options: StorageOptions
  ) {
    this.client = new S3(options);
  }

  public async putObject(
    params: S3.Types.PutObjectRequest
  ): Promise<S3.Types.PutObjectOutput> {
    return this.client.putObject(params).promise();
  }

  public async getObject(
    params: S3.Types.GetObjectAclRequest
  ): Promise<S3.Types.GetObjectAclOutput> {
    return this.client.getObject(params).promise();
  }

  public async removeObject(
    params: S3.Types.DeleteObjectRequest
  ): Promise<S3.Types.DeleteObjectOutput> {
    return this.client.deleteObject(params).promise();
  }
}
