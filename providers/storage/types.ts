import { FactoryProvider } from '@nestjs/common';
import { S3 } from 'aws-sdk';

export type StorageOptions = S3.Types.ClientConfiguration;

export type StorageModuleOptions = Pick<
  FactoryProvider<S3.Types.ClientConfiguration>,
  'useFactory' | 'inject'
>;
