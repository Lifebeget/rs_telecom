import { Inject, Injectable } from '@nestjs/common';
import Bottleneck from 'bottleneck';

import { DEBOUNCE_MODULE_OPTIONS } from './constants';
import { DebounceOption } from './types';

@Injectable()
export class DebounceService extends Bottleneck {
  constructor(
    @Inject(DEBOUNCE_MODULE_OPTIONS) private readonly options: DebounceOption
  ) {
    super(options);
  }
}
