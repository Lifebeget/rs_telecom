import Bottleneck from 'bottleneck';
import { FactoryProvider } from '@nestjs/common';

export type DebounceOption = Bottleneck.ConstructorOptions;

export type DebounceModuleOption = Pick<
  FactoryProvider<DebounceOption>,
  'inject' | 'useFactory'
>;
