import { DynamicModule, Module } from '@nestjs/common';

import { DEBOUNCE_MODULE_OPTIONS } from './constants';
import { DebounceModuleOption } from './types';
import { DebounceService } from './debounce.service';

@Module({})
export class DebounceModule {
  public static forRootAsync(options: DebounceModuleOption): DynamicModule {
    return {
      exports: [DebounceService],
      module: DebounceModule,
      providers: [
        {
          provide: DEBOUNCE_MODULE_OPTIONS,
          ...options
        },
        DebounceService
      ]
    };
  }
}
