export class GetCarrierDto {
  public readonly phoneNumber!: string;
  public readonly member?: string | null;
  public readonly team?: string | null;
}
