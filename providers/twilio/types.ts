import { FactoryProvider } from '@nestjs/common';

export type TwilioOptions = {
  accountSid?: string;
  authToken?: string;
};

export type TwilioModuleOptions = Pick<
  FactoryProvider<TwilioOptions>,
  'useFactory' | 'inject'
>;
