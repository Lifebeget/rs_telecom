import {
  BadRequestException,
  CACHE_MANAGER,
  Inject,
  Injectable,
  OnModuleInit,
  ServiceUnavailableException
} from '@nestjs/common';
import yesno from 'yesno';
import { Cache } from 'cache-manager';
import twilio, { Twilio } from 'twilio';
import { ConfigService } from '@nestjs/config';

import { Errors } from 'Constants';
import { parsePhoneNumber } from 'Utils';

import { LoggerService } from '../logger';

import { GetCarrierDto } from './dto';
import { TWILIO_MODULE_OPTIONS } from './constants';
import { TwilioOptions } from './types';

interface Carrier {
  name: string;
}

function isCarrier(obj: Carrier | Record<string, unknown>): obj is Carrier {
  return typeof obj['name'] === 'string';
}

@Injectable()
export class TwilioService implements OnModuleInit {
  public readonly client: Twilio | null = null;

  constructor(
    @Inject(TWILIO_MODULE_OPTIONS)
    private readonly options: TwilioOptions,
    @Inject(CACHE_MANAGER)
    private readonly cacheManager: Cache,
    private readonly configService: ConfigService,
    private readonly loggerService: LoggerService
  ) {
    const { accountSid, authToken } = this.options;

    if (accountSid && authToken) {
      this.client = twilio(accountSid, authToken);
    }
  }

  async onModuleInit() {
    if (
      this.configService.get<string>('NODE_ENV', 'production') === 'development'
    ) {
      if (
        this.configService.get<string>('TWILIO_ACCOUNT_SID') ||
        this.configService.get<string>('TWILIO_AUTH_TOKEN')
      ) {
        const ok = await yesno({
          question: 'Start the app with an access to the paid Twillio API?'
        });
        if (!ok) {
          this.loggerService.error(
            `Can't run without permission to use Twillio API (u can clear your .env variables TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN)`
          );
          process.exit(1);
        } else {
          this.loggerService.warning('Be careful');
        }
      }
    }
  }

  async getCarrier(getCarrierDto: GetCarrierDto): Promise<string> {
    // await this.limitService.checkGlobalLimit('twilio');

    // if (getCarrierDto.team) {
    //   await this.limitService.checkTeamLimit('twilio', getCarrierDto.team);
    // }

    // if (getCarrierDto.member) {
    //   await this.limitService.checkMemberLimit('twilio', getCarrierDto.member);
    // }

    if (
      this.configService.get<string>('TWILLIO_TEST_MODE', 'false') === 'true'
    ) {
      this.loggerService.warning('Twillio service is running in test mode');
      this.loggerService.warning('Get carrier', {
        payload: getCarrierDto
      });

      return this.configService.get<string>('VODAFONE_CARRIER', 'unknown');
    }

    if (!this.client) {
      if (this.configService.get<string>('NODE_ENV') === 'development') {
        return this.configService.get<string>('VODAFONE_CARRIER', 'unknown');
      } else {
        throw new ServiceUnavailableException(
          Errors.TWILIO_SERVICE_IS_UNAVAILABLE
        );
      }
    }

    try {
      const cacheKey = `carrier:${getCarrierDto.phoneNumber}`;
      const cachedCarrierName = await this.cacheManager.get<string>(cacheKey);

      if (cachedCarrierName) {
        return cachedCarrierName;
      }

      const phoneNumber = parsePhoneNumber(
        getCarrierDto.phoneNumber,
        'DE'
      ).formatInternational();

      const { carrier } = await this.client.lookups
        .phoneNumbers(phoneNumber)
        .fetch({ type: ['carrier'] });

      if (!isCarrier(carrier)) {
        throw new Error('Invalid carrier object');
      }

      this.loggerService.info(`Carrier for ${phoneNumber}: ${carrier.name}`);

      await this.cacheManager.set(cacheKey, carrier.name, { ttl: Infinity });

      return carrier.name;
    } catch (err) {
      this.loggerService.warning(
        `Twillio: cannot get carrier for number ${getCarrierDto.phoneNumber}`
      );
      this.loggerService.error(err);
      throw new BadRequestException(Errors.TWILIO_ERROR);
    }
  }
}
