import { CacheModule, DynamicModule, Global, Module } from '@nestjs/common';

import { TWILIO_MODULE_OPTIONS } from './constants';
import { TwilioService } from './twilio.service';
import { TwilioModuleOptions } from './types';

@Global()
@Module({})
export class TwilioModule {
  static forRootAsync(options: TwilioModuleOptions): DynamicModule {
    return {
      exports: [TwilioService],
      imports: [CacheModule.register()],
      module: TwilioModule,
      providers: [
        {
          provide: TWILIO_MODULE_OPTIONS,
          ...options
        },
        TwilioService
      ]
    };
  }
}
