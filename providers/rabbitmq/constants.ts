/* eslint-disable no-restricted-syntax -- enums here will be removed */

export const enum Exchanges {
  NOTIFY = 'crm.e.notify',
  QUERY = 'crm.e.query',
  RELEASE = 'crm.e.release'
}

export const enum Queues {
  NOTIFY_REQUEST = 'crm.e.notify.request',
  NOTIFY_RESPONSE = 'crm.e.notify.response',
  QUERY_REQUEST = 'crm.e.query.request',
  QUERY_RESPONSE = 'crm.e.query.response',
  RELEASE_REQUEST = 'crm.e.release.request',
  RELEASE_RESPONSE = 'crm.e.release.response',
  SYNC_REQUEST = 'crm.e.sync.request',
  SYNC_RESPONSE = 'crm.e.sync.response'
}

export const enum RoutingKeys {
  REQUEST = 'request',
  RESPONSE = 'response'
}
