import { Injectable, Optional } from '@nestjs/common';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { ConsumeMessage } from 'amqplib';
import { plainToClass } from 'class-transformer';
import Timeout from 'await-timeout';

import { LoggerService } from '../logger';

import { RoutingKeys } from './constants';
import { ClassConstructor, PublishConfig } from './types';

@Injectable()
export class RabbitmqService {
  constructor(
    private readonly loggerService: LoggerService,
    @Optional() private readonly amqpConnection?: AmqpConnection
  ) {
    if (!this.amqpConnection) {
      this.loggerService.info('RMQ Not connected');
    }
  }

  public publish<T>(
    keyId: string,
    payload: Record<string, unknown>,
    Model: ClassConstructor<T>,
    { queue, exchange }: PublishConfig,
    contractSid: string
  ): Promise<T> {
    this.loggerService.info(
      `Initialize request by keyId '${keyId}' and contractSid: '${contractSid}'`
    );

    return Timeout.wrap(
      new Promise<T>(async (resolve, reject) => {
        try {
          if (!this.amqpConnection) {
            return false;
          }
          const channel = this.amqpConnection.channel;
          await channel.prefetch(1);

          const { consumerTag } = await channel.consume(
            queue,
            async (msg: ConsumeMessage | null) => {
              try {
                if (!msg || !msg.properties.expiration) {
                  throw new Error('Receive empty message');
                }

                const response = plainToClass(
                  Model,
                  JSON.parse(msg.content.toString())
                );

                if (!response.keyId || !response.payload) {
                  throw new Error(
                    `Incorrect message: ${JSON.stringify(
                      msg.content.toString(),
                      null,
                      2
                    )}`
                  );
                }

                if (response.keyId !== keyId) {
                  this.loggerService.warning(
                    `Reject response by keyId '${response.keyId}'`
                  );

                  return channel.reject(msg, true);
                }
                this.loggerService.debug(
                  `Request keyId: '${keyId}' and contractSid: '${contractSid}' payload `,
                  {
                    payload: response.payload
                  }
                );

                channel.ack(msg);
                resolve(response.payload);
                await channel.cancel(consumerTag);

                this.loggerService.info(
                  `Request keyId:  '${keyId}' and contractSid: '${contractSid}' completed`
                );
              } catch (err) {
                this.loggerService.error(err);
                reject(err);

                if (msg) {
                  channel.reject(msg, false);
                }
                await channel.cancel(consumerTag);
              }
            }
          );

          await channel.publish(
            exchange,
            RoutingKeys.REQUEST,
            Buffer.from(
              JSON.stringify({
                keyId,
                payload
              })
            ),
            {
              contentType: 'application/json',
              deliveryMode: 2,
              expiration: 1000 * 30
            }
          );
        } catch (err) {
          reject(err);
        }
      }),
      1000 * 30,
      `Aborted request keyId: '${keyId}' and contractSid: '${contractSid}' by timeout of 30s`
    );
  }
}
