import { Exchanges, Queues } from './constants';

export interface ClassConstructor<T> {
  new (...args: unknown[]): { keyId: string; payload: T };
}

export interface PublishConfig {
  queue: Queues; // response queue
  exchange: Exchanges; // request exchange
}
