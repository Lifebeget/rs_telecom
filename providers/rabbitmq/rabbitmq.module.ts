import { URL } from 'url';

import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RabbitMQModule as RabbitModule } from '@golevelup/nestjs-rabbitmq';

import { EmptyModule } from 'Server/utils/empty.module';
import { isRabbitMQDisabled } from 'Server/utils/env-params';

import { RabbitmqService } from './rabbitmq.service';

@Global()
@Module({
  exports: [RabbitModule, RabbitmqService],
  imports: [
    ConfigModule.forRoot(),
    isRabbitMQDisabled()
      ? EmptyModule
      : RabbitModule.forRootAsync(RabbitModule, {
          inject: [ConfigService],
          useFactory: (configService: ConfigService) => {
            const url = new URL('amqp://127.0.0.1');

            url.host = configService.get<string>('RABBITMQ_HOST', '127.0.0.1');
            url.port = configService.get<string>('RABBITMQ_PORT', '5672');
            url.username = configService.get<string>('RABBITMQ_USER', '');
            url.password = configService.get<string>('RABBITMQ_PASSWORD', '');

            return {
              connectionInitOptions: {
                wait: false
              },
              connectionManagerOptions: {
                heartbeatIntervalInSeconds: 20
              },
              prefetchCount: 1,
              uri: url.toString()
            };
          }
        })
  ],
  providers: [RabbitmqService]
})
export class RabbitMQModule {}
