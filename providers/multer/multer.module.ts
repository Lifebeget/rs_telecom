import crypto from 'crypto';
import path from 'path';

import mime from 'mime';
import { diskStorage } from 'multer';
import { BadRequestException, Global, Module } from '@nestjs/common';
import { MulterModule as ExpressMulterModule } from '@nestjs/platform-express';

import { getUploadFolder } from 'Server/utils';
import { Errors } from 'Constants';

@Global()
@Module({
  exports: [ExpressMulterModule],
  imports: [
    ExpressMulterModule.register({
      storage: diskStorage({
        destination: 'uploads',
        filename: (req, file, cb) => {
          const uploads = getUploadFolder().replace('uploads/', '');

          const hash = crypto.pseudoRandomBytes(16).toString('hex');
          const ext = mime.getExtension(file.mimetype);

          if (!ext) {
            throw new BadRequestException(Errors.SOMETHING_WENT_WRONG);
          }
          cb(null, path.join(uploads, `${hash}.${ext}`));
        }
      })
    })
  ]
})
export class MulterModule {}
