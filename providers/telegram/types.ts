import { FactoryProvider } from '@nestjs/common';

export type TelegramOptions = {
  apiKey?: string;
  chatId?: string;
};

export type TelegramModuleOptions = Pick<
  FactoryProvider<TelegramOptions>,
  'useFactory' | 'inject'
>;
