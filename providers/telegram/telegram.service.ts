import {
  BadRequestException,
  Inject,
  Injectable,
  ServiceUnavailableException
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import TelegramBot from 'node-telegram-bot-api';

import { Errors } from 'Constants';

import { LoggerService } from '../logger';

import { TELEGRAM_MODULE_OPTIONS } from './constants';
import { TelegramOptions } from './types';

@Injectable()
export class TelegramService {
  private readonly bot?: TelegramBot;

  constructor(
    @Inject(TELEGRAM_MODULE_OPTIONS)
    private readonly options: TelegramOptions,
    private readonly configService: ConfigService,
    private readonly loggerService: LoggerService
  ) {
    if (this.options.apiKey) {
      this.bot = new TelegramBot(this.options.apiKey);
    }
  }

  // chatId is taken from .env by default but it is possible to provide another chatId if necessary
  public sendMessage(text: string, chatId = this.options.chatId) {
    if (
      this.configService.get<string>('TELEGRAM_TEST_MODE', 'false') === 'true'
    ) {
      this.loggerService.warning('Telegram service is running in test mode');
      this.loggerService.warning('Send telegram message', {
        payload: {
          chatId,
          text
        }
      });

      return true;
    }

    if (!this.bot) {
      this.loggerService.error(
        'Telegram client is not instantiated. Perhaps API key is missing'
      );
      throw new ServiceUnavailableException(Errors.TELEGRAM);
    }

    if (chatId === undefined) {
      this.loggerService.error('Telegram chat id is not provided');
      throw new BadRequestException(Errors.TELEGRAM);
    }

    try {
      return this.bot.sendMessage(chatId, text);
    } catch (e) {
      this.loggerService.error(
        'Something went wrong when sending telegram message. Perhaps API key is invalid or bot does not have permissions to send to this chat'
      );
      throw new BadRequestException(Errors.TELEGRAM);
    }
  }
}
