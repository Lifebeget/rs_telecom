import { DynamicModule, Global, Module } from '@nestjs/common';

import { TELEGRAM_MODULE_OPTIONS } from './constants';
import { TelegramService } from './telegram.service';
import { TelegramModuleOptions } from './types';

@Global()
@Module({})
export class TelegramModule {
  public static forRootAsync(options: TelegramModuleOptions): DynamicModule {
    return {
      exports: [TelegramService],
      module: TelegramModule,
      providers: [
        {
          provide: TELEGRAM_MODULE_OPTIONS,
          ...options
        },
        TelegramService
      ]
    };
  }
}
