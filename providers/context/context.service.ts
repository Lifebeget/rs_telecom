import { AsyncLocalStorage } from 'async_hooks';

import { Injectable } from '@nestjs/common';

@Injectable()
export class ContextService extends AsyncLocalStorage<Map<string, string>> {}
