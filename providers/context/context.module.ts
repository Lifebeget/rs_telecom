import { DynamicModule, Global, Module } from '@nestjs/common';

import { ContextService } from './context.service';

@Global()
@Module({})
export class ContextModule {
  static forRoot(): DynamicModule {
    return {
      exports: [ContextService],
      module: ContextModule,
      providers: [ContextService]
    };
  }
}
