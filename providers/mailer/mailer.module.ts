import { DynamicModule, Global, Module } from '@nestjs/common';

import { MAILER_MODULE_OPTIONS } from './constants';
import { MailerModuleOptions } from './types';
import { MailerService } from './mailer.service';

@Global()
@Module({})
export class MailerModule {
  public static forRootAsync(options: MailerModuleOptions): DynamicModule {
    return {
      exports: [MailerService],
      module: MailerModule,
      providers: [
        {
          provide: MAILER_MODULE_OPTIONS,
          ...options
        },
        MailerService
      ]
    };
  }
}
