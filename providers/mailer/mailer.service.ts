import {
  Inject,
  Injectable,
  ServiceUnavailableException
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Transporter, createTransport } from 'nodemailer';

import { Errors } from 'Constants';

import { LoggerService } from '../logger';

import { MAILER_MODULE_OPTIONS } from './constants';
import { MailerOptions } from './types';

@Injectable()
export class MailerService {
  private readonly transporter: Transporter;

  constructor(
    @Inject(MAILER_MODULE_OPTIONS)
    private readonly options: MailerOptions,
    private readonly configService: ConfigService,
    private readonly loggerService: LoggerService
  ) {
    this.transporter = createTransport(this.options);
  }

  async send(to: string, subject: string, body: string) {
    if (
      this.configService.get<string>('MAILER_TEST_MODE', 'false') === 'true'
    ) {
      this.loggerService.warning('Mailer service is running in test mode');
      this.loggerService.info('Send mail', {
        payload: {
          from: this.configService.get<string>('MAILER_FROM'),
          subject: subject,
          text: body,
          to: to
        }
      });

      return true;
    }
    try {
      await this.transporter.sendMail({
        from: this.configService.get('MAILER_FROM'),
        subject: subject,
        text: body,
        to: to
      });
    } catch (err) {
      throw new ServiceUnavailableException(
        Errors.MAILER_SERVICE_IS_UNAVAILABLE,
        'Mailer service is unavailable'
      );
    }
  }

  async sendHtml(to: string, subject: string, html: string) {
    if (
      this.configService.get<string>('MAILER_TEST_MODE', 'false') === 'true'
    ) {
      this.loggerService.warning('Mailer service is running in test mode');
      this.loggerService.info('Send mail', {
        payload: {
          from: this.configService.get<string>('MAILER_FROM'),
          html,
          subject: subject,
          to: to
        }
      });

      return true;
    }
    try {
      await this.transporter.sendMail({
        from: this.configService.get('MAILER_FROM'),
        html,
        subject: subject,
        to: to
      });
    } catch (err) {
      throw new ServiceUnavailableException(
        Errors.MAILER_SERVICE_IS_UNAVAILABLE,
        'Mailer service is unavailable'
      );
    }
  }

  async sendWithFile(
    to: string,
    subject: string,
    body: string,
    file: { filename: string; content: Buffer }
  ) {
    if (
      this.configService.get<string>('MAILER_TEST_MODE', 'false') === 'true'
    ) {
      this.loggerService.warning('Mailer service is running in test mode');
      this.loggerService.info('Send mail', {
        payload: {
          attachments: [{ ...file }],
          from: this.configService.get<string>('MAILER_FROM'),
          subject: subject,
          text: body,
          to: to
        }
      });

      return true;
    }
    try {
      await this.transporter.sendMail({
        attachments: [{ ...file }],
        from: this.configService.get('MAILER_FROM'),
        subject: subject,
        text: body,
        to: to
      });
    } catch (err) {
      throw new ServiceUnavailableException(
        Errors.MAILER_SERVICE_IS_UNAVAILABLE,
        'Mailer service is unavailable'
      );
    }
  }
}
