import { FactoryProvider } from '@nestjs/common';
import SMTPConnection from 'nodemailer/lib/smtp-connection';

export type MailerOptions = SMTPConnection.Options;

export type MailerModuleOptions = Pick<
  FactoryProvider<SMTPConnection.Options>,
  'useFactory' | 'inject'
>;
