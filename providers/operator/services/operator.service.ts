import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef
} from '@nestjs/common';
import axios from 'axios';
import { DateTime } from 'luxon';
import { isNil } from 'remeda';
import { EntityManager, In } from 'typeorm';
import { validateOrReject } from 'class-validator';

import {
  ActivityType,
  ContractFlag,
  ContractRequestStatus,
  ContractRequestType,
  DistributionRequestStatus,
  Errors
} from 'Constants';
import { TEAM_TYPES_CAN_HAVE_CONTRACT } from 'Helpers';
import { CONTRACT_INFO_EXPIRED_DAYS_PERIOD } from 'Server/constants';
import { ContractService } from 'Server/modules/contract/contract.service';
import {
  ContractRepository,
  ContractRequestHistoryRepository,
  DistributionRequestHistoryRepository
} from 'Server/modules/contract/repositories';
import { ContractFlagService } from 'Server/modules/contract/services';
import { MemberRepository } from 'Server/modules/member/repositories';
import { TeamRepository } from 'Server/modules/team/repositories';
import {
  DebounceService,
  LoggerService,
  MailerService,
  TelegramService
} from 'Server/providers';

import {
  ActivateValidateDto,
  GetContractDistributorDto,
  GetContractInfoOptionsDto,
  ValidateByPhoneOptionsDto
} from '../dto';
import {
  CouldNotBeAuthenticateException,
  IncorrectPhoneOrPasswordException,
  PasswordValidationFailedException,
  VONotSupportedException,
  VoException
} from '../exceptions';
import {
  CreateContractEasyXml,
  GetContractInfoOptions,
  RequestModel,
  ValidateByPhoneOptions
} from '../interfaces';
import {
  ActivateModel,
  ClientInfoXMLRequestModel,
  DistributorModel,
  NotEasyInfoXMLRequestModel,
  PartnerCardsXMLResponseModel
} from '../models';
import { getRequestContent } from '../utils';
import {
  ContractEntity,
  ContractRequestHistoryEntity
} from '../../../modules/contract/entities';
import { LeadEntity } from '../../../modules/lead/entities';
import { ClientEntity } from '../../../modules/client/entities';

import { ActivityOperatorService } from './activity.operator.service';
import { SynchronizeService } from './synchronize.service';
import { VOHttpService } from './vo-http.service';

@Injectable()
export class OperatorService {
  constructor(
    private readonly activityOperatorService: ActivityOperatorService,
    private readonly contractHelpersService: ContractFlagService,
    private readonly contractRepository: ContractRepository,
    private readonly contractRequestHistoryRepository: ContractRequestHistoryRepository,
    private readonly contractService: ContractService,
    private readonly debounceService: DebounceService,
    private readonly distributionRequestHistoryRepository: DistributionRequestHistoryRepository,
    private readonly loggerService: LoggerService,
    private readonly mailerService: MailerService,
    private readonly memberRepository: MemberRepository,
    private readonly synchronizeService: SynchronizeService,
    private readonly teamRepository: TeamRepository,
    @Inject(forwardRef(() => TelegramService))
    private readonly telegramService: TelegramService,
    private readonly voHttpService: VOHttpService
  ) {}

  async activateValidate(
    activateValidateDto: ActivateValidateDto
  ): Promise<ActivateModel> {
    const { getResponse } = await this.voHttpService.activateJSONRequest(
      activateValidateDto.phoneNumber
    );

    return getResponse();
  }

  async getVo(
    phoneNumber: string,
    postCode: string
  ): Promise<DistributorModel> {
    const { getResponse } = await this.voHttpService.getDistributorRequest(
      phoneNumber,
      postCode
    );

    return getResponse();
  }

  async validateByPhone(
    leadEntity: LeadEntity,
    manager: EntityManager,
    validateDto: ValidateByPhoneOptions,
    validateOptions: ValidateByPhoneOptionsDto
  ): Promise<ContractEntity> {
    // await this.limitService.checkGlobalLimit('vodafone');

    // if (validateOptions.team) {
    //   await this.limitService.checkTeamLimit(
    //     'vodafone',
    //     validateOptions.team
    //   );
    // }

    // if (validateOptions.member) {
    //   await this.limitService.checkMemberLimit(
    //     'vodafone',
    //     validateOptions.member
    //   );
    // }

    // FIXME:
    if (validateOptions.team) {
      const teamEntity = await this.teamRepository.findOne({
        sid: validateOptions.team,
        type: In(TEAM_TYPES_CAN_HAVE_CONTRACT)
      });

      if (!teamEntity) {
        throw new BadRequestException(
          Errors.TEAM_NOT_FOUND,
          `Team "${validateOptions.team}" doesn't exist or can't have a contract`
        );
      }
    }

    this.loggerService.info('Initialize JSON request');

    const { debounce = true } = validateOptions;

    const getJSONRequest = debounce
      ? this.debounceService.wrap(this.voHttpService.getJSONRequest)
      : this.voHttpService.getJSONRequest;

    const contractRequestEntity = new ContractRequestHistoryEntity();
    contractRequestEntity.content = JSON.stringify({});
    contractRequestEntity.memberSid = validateOptions.member;
    contractRequestEntity.password = validateDto.password;
    contractRequestEntity.phoneNumber = validateDto.phoneNumber;
    contractRequestEntity.requestStatus = ContractRequestStatus.Fetching;
    contractRequestEntity.type = ContractRequestType.Json;

    await manager.save(ContractRequestHistoryEntity, contractRequestEntity);

    try {
      const { getResponse, getRaw } = await getJSONRequest.call(
        this.voHttpService,
        validateDto
      );

      await manager.update(
        ContractRequestHistoryEntity,
        { sid: contractRequestEntity.sid },
        {
          content: getRaw(),
          requestStatus: ContractRequestStatus.Successful
        }
      );

      const jsonRequestModel = getResponse();

      await validateOrReject(jsonRequestModel).catch(err => {
        throw err;
      });

      this.loggerService.info(
        'JSON request completed. Initialize synchronization...'
      );

      const contractEntity = await this.synchronizeService.synchronize({
        leadEntity,
        manager,
        method: ContractRequestType.Json,
        requestModel: jsonRequestModel,
        validOptions: { ...validateDto, ...validateOptions }
      });

      await manager.update(
        ContractRequestHistoryEntity,
        { sid: contractRequestEntity.sid },
        {
          contract: contractEntity.sid
        }
      );

      return contractEntity;
    } catch (err) {
      this.loggerService.error(err);

      if (axios.isAxiosError(err) && err.response) {
        await this.contractRequestHistoryRepository.update(
          { sid: contractRequestEntity.sid },
          {
            content: getRequestContent(err),
            requestStatus: ContractRequestStatus.Failure
          }
        );
      }

      throw err;
    }
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  async getContractInfo(
    leadEntity: LeadEntity,
    manager: EntityManager,
    getContractInfoDto: GetContractInfoOptions,
    getContractInfoOptionsDto: GetContractInfoOptionsDto,
    externalRequestModel: RequestModel<
      ClientInfoXMLRequestModel | NotEasyInfoXMLRequestModel
    >
  ): Promise<ContractEntity> {
    const contract = await this.contractRepository.getContract(
      getContractInfoDto.contractSid
    );

    const isExpired =
      contract.lastProcessedAt &&
      -1 *
        DateTime.fromJSDate(contract.lastProcessedAt)
          .diffNow('days')
          .as('days') <
        CONTRACT_INFO_EXPIRED_DAYS_PERIOD;

    if (isExpired) {
      throw new BadRequestException(Errors.CONTRACT_INFO_DOES_NOT_EXPIRED);
    }
    const currentDate = DateTime.now().toUTC().toJSDate();

    const contractRequestEntity = await this.contractRequestHistoryRepository.save(
      this.contractRequestHistoryRepository.create({
        content: JSON.stringify({}),
        contract: getContractInfoDto.contractSid,
        memberSid: getContractInfoDto.memberSid,
        password: getContractInfoDto.password,
        phoneNumber: getContractInfoDto.phoneNumber,
        requestStatus: ContractRequestStatus.Fetching,
        type: ContractRequestType.Xml
      })
    );

    try {
      const { getResponse, getRaw } = externalRequestModel;

      await this.contractRequestHistoryRepository.update(
        { sid: contractRequestEntity.sid },
        {
          content: getRaw(),
          requestStatus: ContractRequestStatus.Successful
        }
      );

      const xmlRequestModel = getResponse();

      await validateOrReject(xmlRequestModel).catch(err => {
        throw err;
      });

      this.loggerService.info(
        'XML request completed. Initialize synchronization...'
      );

      this.loggerService.debug('Synchronize contract entity');

      const contractEntity = await this.synchronizeService.synchronize({
        leadEntity,
        manager,
        method: ContractRequestType.Xml,
        requestModel: xmlRequestModel,
        validOptions: {
          member: getContractInfoDto.memberSid,
          password: getContractInfoDto.password,
          phoneNumber: getContractInfoDto.phoneNumber
        }
      });

      this.loggerService.debug('Update contract processing info');

      await this.contractHelpersService.removeContractFlagWithSynchronization({
        contractSid: getContractInfoDto.contractSid,
        flag: ContractFlag.InvalidPassword,
        manager
      });

      await manager.update(
        ContractEntity,
        { sid: getContractInfoDto.contractSid },
        {
          lastProcessedAt: currentDate,
          processStatus: ContractRequestStatus.Successful,
          processTrigger: getContractInfoOptionsDto.trigger
        }
      );

      this.loggerService.debug('Success. Exiting getContractInfo function');

      await this.contractService.emitContractInfoUpdate({
        contractSid: getContractInfoDto.contractSid,
        success: true
      });

      return contractEntity;
    } catch (err) {
      this.loggerService.debug('Error. Setting failure status');

      await this.contractRepository.update(
        { sid: getContractInfoDto.contractSid },
        {
          processStatus: ContractRequestStatus.Failure,
          processTrigger: getContractInfoOptionsDto.trigger
        }
      );

      await this.contractRequestHistoryRepository.update(
        { sid: contractRequestEntity.sid },
        {
          requestStatus: ContractRequestStatus.Failure
        }
      );

      if (
        (axios.isAxiosError(err) && err.response) ||
        err instanceof VoException
      ) {
        await this.contractRequestHistoryRepository.update(
          { sid: contractRequestEntity.sid },
          {
            content: getRequestContent(err)
          }
        );
      }

      if (err instanceof IncorrectPhoneOrPasswordException) {
        await this.contractHelpersService.addContractFlagWithSynchronization({
          contractSid: getContractInfoDto.contractSid,
          flag: ContractFlag.InvalidPassword
        });
        await this.activityOperatorService.createActivity(
          getContractInfoDto.contractSid,
          ActivityType.ContractWrongPassword
        );
      }

      if (err instanceof CouldNotBeAuthenticateException) {
        const matches = err.message.match(/\\"(.*?)\\"/);
        const voNumber =
          !!matches && matches.length > 1
            ? // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
              matches[1] ?? contract.userId
            : contract.userId;

        if (voNumber) {
          const message = `VO ${voNumber} has a wrong password, please, update the password. Thank you`;
          await this.telegramService.sendMessage(message);
        }
      }

      if (err instanceof VONotSupportedException) {
        await this.contractRepository.update(
          { sid: contract.sid },
          {
            projectId: null,
            region: null,
            userId: null
          }
        );

        if (getContractInfoOptionsDto.retry) {
          throw err;
        }

        return this.getContractInfo(
          leadEntity,
          manager,
          getContractInfoDto,
          {
            ...getContractInfoOptionsDto,
            retry: true
          },
          externalRequestModel
        );
      }

      this.loggerService.debug('Error. Exiting with exception throwing');

      throw err;
    }
  }

  async updateFailure({
    getContractInfoDto,
    getContractInfoOptionsDto,
    err
  }: {
    getContractInfoDto: GetContractInfoOptions;
    getContractInfoOptionsDto: GetContractInfoOptionsDto;
    err: unknown;
  }) {
    {
      const contract = await this.contractRepository.getContract(
        getContractInfoDto.contractSid
      );

      const contractRequestEntity = await this.contractRequestHistoryRepository.save(
        this.contractRequestHistoryRepository.create({
          content: JSON.stringify({}),
          contract: getContractInfoDto.contractSid,
          memberSid: getContractInfoDto.memberSid,
          password: getContractInfoDto.password,
          phoneNumber: getContractInfoDto.phoneNumber,
          requestStatus: ContractRequestStatus.Fetching,
          type: ContractRequestType.Xml
        })
      );

      this.loggerService.debug('Error. Setting failure status');

      await this.contractRepository.update(
        { sid: getContractInfoDto.contractSid },
        {
          processStatus: ContractRequestStatus.Failure,
          processTrigger: getContractInfoOptionsDto.trigger
        }
      );

      await this.contractRequestHistoryRepository.update(
        { sid: contractRequestEntity.sid },
        {
          requestStatus: ContractRequestStatus.Failure
        }
      );

      if (
        (axios.isAxiosError(err) && err.response) ||
        err instanceof VoException
      ) {
        await this.contractRequestHistoryRepository.update(
          { sid: contractRequestEntity.sid },
          {
            content: getRequestContent(err)
          }
        );
      }

      if (err instanceof IncorrectPhoneOrPasswordException) {
        await this.contractHelpersService.addContractFlagWithSynchronization({
          contractSid: getContractInfoDto.contractSid,
          flag: ContractFlag.InvalidPassword
        });
        await this.activityOperatorService.createActivity(
          getContractInfoDto.contractSid,
          ActivityType.ContractWrongPassword
        );
      }

      if (err instanceof CouldNotBeAuthenticateException) {
        const matches = err.message.match(/\\"(.*?)\\"/);
        const voNumber =
          !!matches && matches.length > 1
            ? // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
              matches[1] ?? contract.userId
            : contract.userId;

        if (voNumber) {
          const message = `VO ${voNumber} has a wrong password, please, update the password. Thank you`;
          await this.telegramService.sendMessage(message);
        }
      }

      await this.contractService.emitContractInfoUpdate({
        contractSid: getContractInfoDto.contractSid,
        error: err,
        success: false
      });

      this.loggerService.debug('Error. Exiting with exception throwing');

      throw err;
    }
  }

  async getContractDistributor(
    getContractDistributorDto: GetContractDistributorDto
  ): Promise<DistributorModel> {
    const contract = await this.contractRepository.getContract(
      getContractDistributorDto.contractSid
    );

    if (contract.userId) {
      return {
        projectId: contract.projectId ?? null,
        region: contract.region ?? null,
        userId: contract.userId
      };
    }

    const distributionRequestEntity = await this.distributionRequestHistoryRepository.save(
      this.distributionRequestHistoryRepository.create({
        content: JSON.stringify({}),
        contract: getContractDistributorDto.contractSid,
        member: getContractDistributorDto.memberSid,
        msisdn: '',
        ndc: '',
        phoneNumber: getContractDistributorDto.phoneNumber,
        postcode: getContractDistributorDto.postcode,
        requestStatus: DistributionRequestStatus.Fetching
      })
    );

    try {
      const {
        getResponse,
        getRaw,
        ndc,
        msisdn
      } = await this.voHttpService.getDistributorRequest(
        getContractDistributorDto.phoneNumber,
        getContractDistributorDto.postcode
      );

      await this.distributionRequestHistoryRepository.update(
        {
          sid: distributionRequestEntity.sid
        },
        {
          content: getRaw(),
          msisdn,
          ndc,
          requestStatus: DistributionRequestStatus.Successful
        }
      );

      const distributorModel = getResponse();

      await this.contractRepository.update(
        { sid: contract.sid },
        {
          projectId: distributorModel.projectId,
          region: distributorModel.region,
          userId: distributorModel.userId
        }
      );

      return distributorModel;
    } catch (err) {
      this.loggerService.debug('Error. Setting failure status');

      await this.distributionRequestHistoryRepository.update(
        { sid: distributionRequestEntity.sid },
        {
          requestStatus: DistributionRequestStatus.Failure
        }
      );

      if (
        (axios.isAxiosError(err) && err.response) ||
        err instanceof VoException
      ) {
        await this.distributionRequestHistoryRepository.update(
          { sid: distributionRequestEntity.sid },
          {
            content: getRequestContent(err)
          }
        );
      }

      throw err;
    }
  }

  async getDecodedIbanPart(ibanEncodedPart: string): Promise<string> {
    const { data } = await this.voHttpService.getDecodedIbanPart(
      ibanEncodedPart
    );

    const dataString = data.toString();

    if (dataString.trim() === '' || isNil(dataString)) {
      throw new Error("That's not an iban!");
    }

    return dataString;
  }

  async validateContractPassword(
    phoneNumber: string,
    password: string
  ): Promise<boolean> {
    try {
      await this.voHttpService.validateContractPassword(phoneNumber, password);

      return true;
    } catch (err) {
      if (err instanceof PasswordValidationFailedException) {
        return false;
      }

      throw err;
    }
  }

  async setMiddlewareContractPassword(
    phoneNumber: string,
    password: string
  ): Promise<void> {
    await this.voHttpService.setMiddlewareContractPassword(
      phoneNumber,
      password
    );
  }

  async fetchContractPartnerCards({
    phoneNumber,
    password,
    userId,
    banId
  }: {
    phoneNumber: string;
    password: string;
    userId: string;
    banId: string;
  }): Promise<PartnerCardsXMLResponseModel> {
    return this.voHttpService.fetchContractPartnerCards({
      banId,
      password,
      phoneNumber,
      userId
    });
  }

  async getXMLResponse({
    phoneNumber,
    password,
    userId,
    postCode
  }: {
    phoneNumber: string;
    postCode: string;
    password: string;
    userId: string;
  }) {
    const { getResponse } = await this.voHttpService.getXMLRequest(
      phoneNumber,
      password,
      userId,
      postCode
    );

    return getResponse();
  }

  async getXMLRequestModel({
    phoneNumber,
    password,
    userId,
    postCode
  }: {
    phoneNumber: string;
    postCode: string;
    password: string;
    userId: string;
  }) {
    return this.voHttpService.getXMLRequest(
      phoneNumber,
      password,
      userId,
      postCode
    );
  }

  async createContractEasyXml(
    lastProcessedAt: Date,
    distributor: DistributorModel,
    clientEntity: ClientEntity,
    leadEntity: LeadEntity,
    manager: EntityManager,
    {
      phoneNumber,
      member,
      password,
      team,
      assignToProvidedMember
    }: CreateContractEasyXml,
    xmlRequest: ClientInfoXMLRequestModel | NotEasyInfoXMLRequestModel
  ): Promise<ContractEntity> {
    const contractEntity = await this.contractRepository.getMemberActualContract(
      phoneNumber
    );
    if (contractEntity) {
      throw new Error('Contract exists');
    }

    return this.synchronizeService.synchronize({
      clientEntity,
      distributor,
      lastProcessedAt,
      leadEntity,
      manager,
      method: ContractRequestType.Xml,
      requestModel: xmlRequest,
      validOptions: {
        assignToProvidedMember,
        member,
        password,
        phoneNumber,
        team
      }
    });
  }
}
