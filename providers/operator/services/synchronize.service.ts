import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';

import { ContractRepository } from 'Server/modules/contract/repositories';

import { OmitValidOptionsTeam, VOResponse, ValidOptions } from '../interfaces';
import { MemberRepository } from '../../../modules/member/repositories';
import { ContractRequestType } from '../../../../Constants';
import { ContractEntity } from '../../../modules/contract/entities';
import { LeadEntity } from '../../../modules/lead/entities';
import { ClientEntity } from '../../../modules/client/entities';
import { DistributorModel } from '../models';

import { SynchronizeUpdateContractService } from './synchronize-update-contract.service';
import { SynchronizeCreateContractService } from './synchronize-create-contract.service';

@Injectable()
export class SynchronizeService {
  constructor(
    private readonly contractRepository: ContractRepository,
    private readonly synchronizeCreateContractService: SynchronizeCreateContractService,
    private readonly synchronizeUpdateContractService: SynchronizeUpdateContractService,
    private readonly memberRepository: MemberRepository
  ) {}

  private parseValidOptionsTeam(
    validOptions: ValidOptions
  ): OmitValidOptionsTeam {
    if (!validOptions.team) {
      throw new Error('Team is required!');
    }

    return {
      ...validOptions,
      team: validOptions.team
    };
  }

  async synchronize({
    lastProcessedAt,
    distributor,
    isImported,
    clientEntity,
    leadEntity,
    manager,
    requestModel,
    validOptions,
    method
  }: {
    lastProcessedAt?: Date;
    distributor?: DistributorModel;
    isImported?: boolean;
    clientEntity?: ClientEntity;
    leadEntity: LeadEntity;
    manager: EntityManager;
    requestModel: VOResponse;
    validOptions: ValidOptions;
    method: ContractRequestType;
  }): Promise<ContractEntity> {
    // @TODO https://devblogs.microsoft.com/typescript/announcing-typescript-4-6-rc/#cfa-destructured-discriminated-unions
    const contractEntity = await this.contractRepository.getMemberActualContract(
      requestModel.contract.phoneNumber,
      { expand: true }
    );
    const systemMember = await this.memberRepository.getSystemMember();
    const getMember = () => {
      if (validOptions.assignToProvidedMember) {
        return validOptions.member || systemMember.sid;
      }

      return systemMember.sid;
    };
    const member = getMember();
    if (contractEntity) {
      return this.synchronizeUpdateContractService.updateContract({
        manager,
        member,
        method,
        requestModel,
        validOptions
      });
    } else {
      return this.synchronizeCreateContractService.createContract({
        clientEntity,
        distributor,
        isImported,
        lastProcessedAt,
        leadEntity,
        manager,
        member,
        method,
        requestModel,
        validOptions: this.parseValidOptionsTeam(validOptions)
      });
    }
  }
}
