import { EntityManager } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { U } from 'ts-toolbelt';
import { DateTime } from 'luxon';
import { isNil, omitBy } from 'remeda';

import { getExtensionVariant } from 'Helpers';
import { TariffRepository } from 'Server/modules/tariff/repositories';

import {
  ContractAddressInvoiceEntity,
  ContractBankDataEntity,
  ContractClientEntity,
  ContractEntity,
  ContractExtendRequestAddressEntity,
  ContractExtendRequestEntity,
  ExtensionVariantLogEntity
} from '../../../modules/contract/entities';
import { ContractRequestType, ExtensionVariant } from '../../../../Constants';
import {
  Address,
  BankData,
  BankDataType,
  Client,
  PartnerCard,
  VOResponse,
  ValidOptions
} from '../interfaces';
import {
  ContractBankDataRepository,
  ContractClientRepository,
  ContractExtendRequestAddressRepository,
  ContractExtendRequestRepository,
  ContractRepository,
  ExtensionVariantRepository
} from '../../../modules/contract/repositories';
import { ContractService } from '../../../modules/contract/contract.service';
import { LoggerService } from '../../logger';
import {
  ClientAddressEntity,
  ClientEntity,
  ClientRankEntity
} from '../../../modules/client/entities';
import { TariffEntity } from '../../../modules/tariff/entities';
import { ContractPartnerProcessService } from '../../../modules/contract/services';
import { MemberRepository } from '../../../modules/member/repositories';

import { SynchronizeCreateContractService } from './synchronize-create-contract.service';

@Injectable()
export class SynchronizeUpdateContractService {
  constructor(
    private readonly contractRepository: ContractRepository,
    private readonly contractService: ContractService,
    private readonly loggerService: LoggerService,
    private readonly contractBankDataRepository: ContractBankDataRepository,
    private readonly memberRepository: MemberRepository,
    private readonly contractExtendRequestAddressRepository: ContractExtendRequestAddressRepository,
    private readonly contractPartnerProcessService: ContractPartnerProcessService,
    private readonly extensionVariantRepository: ExtensionVariantRepository,
    private readonly synchronizeCreateContract: SynchronizeCreateContractService,
    private readonly contractClientRepository: ContractClientRepository,
    private readonly contractExtendRequestRepository: ContractExtendRequestRepository,
    private readonly tariffRepository: TariffRepository
  ) {}

  private async updateClientAddressInvoice({
    manager,
    contractEntity,
    addressInvoice
  }: {
    manager: EntityManager;
    contractEntity: ContractEntity;
    addressInvoice: Address;
  }) {
    if (addressInvoice.firstName) {
      await manager.update(
        ContractAddressInvoiceEntity,
        { contract: contractEntity.sid },
        omitBy(addressInvoice, isNil)
      );

      this.loggerService.info(
        `Contract address "${contractEntity.sid}" successfully updated`
      );
    }
  }

  private async updateClient({
    manager,
    contractEntity,
    client,
    addressInvoice,
    password,
    customerAddress
  }: {
    manager: EntityManager;
    contractEntity: ContractEntity;
    client: Client;
    addressInvoice: Address;
    customerAddress: Address;
    password: U.Nullable<string>;
  }): Promise<ContractClientEntity> {
    const contractClientEntity = new ContractClientEntity({
      accountType: client.accountType,
      adv: client.adv,
      appeal: client.appeal,
      banId: client.banId,
      banPassword: password,
      birthday: client.birthday,
      clientAddressAppeal: client.appeal,
      clientAddressEmailAddress: addressInvoice.emailAddress,
      clientAddressEmailType: client.emailType,
      clientAddressFirstName: addressInvoice.firstName,
      clientAddressHouse: addressInvoice.house,
      clientAddressLand: addressInvoice.land,
      clientAddressLocation: addressInvoice.location,
      clientAddressPostcode: addressInvoice.postcode,
      clientAddressStreet: addressInvoice.street,
      clientAddressSurname: addressInvoice.surname,
      clientStars: client.clientStars,
      clientStatus: client.clientStatus,
      contract: contractEntity.sid,
      firstName: client.firstName,
      mkek: client.mkek,
      surname: client.surname
    });
    const contractClient = await this.contractClientRepository.findOne({
      contract: contractEntity.sid
    });

    if (!contractClient) {
      throw new Error('Client not found');
    }

    await manager.update(
      ContractClientEntity,
      {
        contract: contractEntity.sid
      },
      contractClientEntity
    );

    await manager.update(
      ClientEntity,
      { sid: contractEntity.client.sid },
      {
        accountType: client.accountType,
        adv: client.adv,
        banId: client.banId,
        banPassword: password,
        birthday: client.birthday,
        clientStars: client.clientStars,
        clientStatus: client.clientStatus,
        mkek: client.mkek
      }
    );
    if (!contractEntity.client.firstName && client.firstName) {
      await manager.update(
        ClientEntity,
        { sid: contractEntity.client.sid },
        {
          firstName: client.firstName
        }
      );
    }

    if (!contractEntity.client.surname && client.surname) {
      await manager.update(
        ClientEntity,
        { sid: contractEntity.client.sid },
        {
          surname: client.surname
        }
      );
    }
    this.loggerService.info(
      `Client "${contractEntity.client.sid}" successfully updated`
    );
    await manager.update(
      ClientAddressEntity,
      { client: contractEntity.client.sid },
      {
        ...omitBy(customerAddress, isNil),
        emailType: client.emailType
      }
    );

    this.loggerService.info(
      `Client address "${contractEntity.client.sid}" successfully updated`
    );
    if (client.rank) {
      await manager.update(
        ClientRankEntity,
        { client: contractEntity.client.sid },
        {
          current: client.rank.current,
          endedAt: client.rank.endDate,
          next: client.rank.next,
          points: client.rank.points,
          startedAt: client.rank.startDate
        }
      );

      this.loggerService.info(
        `Client rank "${contractEntity.client.sid}" successfully updated`
      );
    }
    await this.updateClientAddressInvoice({
      addressInvoice: addressInvoice,
      contractEntity,
      manager
    });

    return contractClientEntity;
  }

  private async updateContractAndExtension({
    earlyExtensionDate,
    normalExtensionDate,
    nextRenewalDate,
    contractStartDate,
    method,
    manager,
    contractEntity,
    extensionVariantSid,
    member,
    tariffName,
    tariffOption,
    isHWOmitted,
    requiredHWCost,
    tariffEntity,
    updateDate
  }: {
    earlyExtensionDate: U.Nullable<Date>;
    normalExtensionDate: U.Nullable<Date>;
    isHWOmitted: boolean;
    requiredHWCost: U.Nullable<number>;
    tariffEntity: TariffEntity | undefined;
    nextRenewalDate: U.Nullable<Date>;
    contractStartDate: U.Nullable<Date>;
    method: ContractRequestType;
    manager: EntityManager;
    contractEntity: ContractEntity;
    member: string;
    extensionVariantSid: string;
    tariffName: U.Nullable<string>;
    tariffOption: U.Nullable<string>;
    updateDate: Date;
  }) {
    await manager.update(
      ContractEntity,
      {
        sid: contractEntity.sid
      },
      {
        contractStartDate: contractStartDate,
        earlyExtensionDate,
        isHWOmitted,
        lastSynchronizedAt: updateDate,
        nextRenewalDate: nextRenewalDate,
        normalExtensionDate,
        requiredHWCost,
        tariff: tariffEntity ? tariffEntity.sid : null,
        tariffName: tariffName,
        tariffOption: tariffOption,
        updateMethodLast: method,
        validationSuccess: true
      }
    );
    this.loggerService.info(`Update contract "${contractEntity.sid}" info`);
    if (contractEntity.extension !== extensionVariantSid) {
      await manager.update(
        ContractEntity,
        {
          sid: contractEntity.sid
        },
        {
          extension: extensionVariantSid
        }
      );

      const extensionVariantLog = new ExtensionVariantLogEntity();

      extensionVariantLog.contract = contractEntity.sid;
      extensionVariantLog.extension = extensionVariantSid;
      extensionVariantLog.member = member;
      await manager.save(ExtensionVariantLogEntity, extensionVariantLog);

      this.loggerService.info(
        `Update contract "${contractEntity.sid}" status on "${extensionVariantSid}"`
      );
    }
  }

  private async updateContractBankData({
    contractEntity,
    manager,
    bankData
  }: {
    contractEntity: ContractEntity;
    manager: EntityManager;
    bankData: BankData;
  }): Promise<ContractBankDataEntity> {
    const contractBankData = await this.contractBankDataRepository.getBankDataBySid(
      contractEntity.bankData
    );
    for (const bankDataKey in bankData) {
      if (bankData[bankDataKey as BankDataType]) {
        contractBankData[bankDataKey as BankDataType] =
          bankData[bankDataKey as BankDataType];
      }
    }

    await manager.update(
      ContractBankDataEntity,
      { sid: contractEntity.bankData },
      contractBankData
    );
    this.loggerService.info(
      `Contract payment "${contractEntity.sid}" successfully updated`
    );

    return contractBankData;
  }

  private async savePartnerCards({
    contractBankDataEntity,
    contractExtendRequestAddressEntity,
    clientSid,
    contractExtendRequestEntity,
    manager,
    contractClientEntity,
    contractEntity,
    memberSid,
    partnerCards
  }: {
    contractBankDataEntity: ContractBankDataEntity;
    contractExtendRequestAddressEntity: ContractExtendRequestAddressEntity;
    clientSid: string;
    contractExtendRequestEntity: ContractExtendRequestEntity;
    manager: EntityManager;
    contractClientEntity: ContractClientEntity;
    contractEntity: ContractEntity;
    memberSid: string;
    partnerCards: PartnerCard[];
  }) {
    await this.contractPartnerProcessService.processFetchedPartnerCards({
      cards: partnerCards,
      clientSid,
      contractBankDataEntity,
      contractClient: contractClientEntity,
      contractExtendRequestAddressEntity,
      contractOrigin: contractEntity,
      contractOriginExtendRequest: contractExtendRequestEntity,
      manager,
      member: await this.memberRepository.getMember(memberSid, { expand: true })
    });
  }

  private async updateContractExtendRequest(
    contractExtendRequestEntity: ContractExtendRequestEntity,
    manager: EntityManager,
    tariffEntity?: TariffEntity
  ) {
    if (tariffEntity) {
      await manager.update(
        ContractExtendRequestEntity,
        { sid: contractExtendRequestEntity.sid },
        {
          tariff: tariffEntity.sid,
          tariffName: tariffEntity.name,
          tariffOption: tariffEntity.code
        }
      );
    }
  }

  public async updateContract({
    member,
    requestModel,
    validOptions,
    method,
    manager
  }: {
    member: string;
    requestModel: VOResponse;
    validOptions: ValidOptions;
    method: ContractRequestType;
    manager: EntityManager;
  }) {
    const contractEntity = await this.contractRepository.getMemberActualContract(
      validOptions.phoneNumber,
      { expand: true }
    );
    if (!contractEntity) {
      throw new Error("Contract doesn't exist");
    }
    const vvlNormal = requestModel.contract.subsidyEligibility.find(
      ({ extension }) => extension === ExtensionVariant.normalExtension
    );
    const vvlEarly = requestModel.contract.subsidyEligibility.find(
      ({ extension }) => extension === ExtensionVariant.earlyExtension
    );
    const extensionVariantEntity = await this.extensionVariantRepository.findOneOrFail(
      {
        name: getExtensionVariant({
          nextSubsidyDate: requestModel.contract.nextRenewalDate,
          vvlEarlyDate: vvlEarly?.nextRenewalDate,
          vvlNormalDate: vvlNormal?.nextRenewalDate
        })
      }
    );
    const {
      tariffName,
      tariffEntity
    } = await this.synchronizeCreateContract.getOneActualTariff(
      requestModel.contract.tariffOption,
      requestModel.contract.tariffName
    );

    const {
      isHWOmitted,
      requiredHWCost
    } = await this.synchronizeCreateContract.parseTariffNameAndOption(
      tariffName
    );

    await this.updateContractAndExtension({
      contractEntity,
      contractStartDate: requestModel.contract.contractStartDate,
      earlyExtensionDate: vvlEarly?.nextRenewalDate,
      extensionVariantSid: extensionVariantEntity.sid,
      isHWOmitted,
      manager,
      member,
      method,
      nextRenewalDate: requestModel.contract.nextRenewalDate,
      normalExtensionDate: vvlNormal?.nextRenewalDate,
      requiredHWCost,
      tariffEntity,
      tariffName,
      tariffOption: requestModel.contract.tariffOption,
      updateDate:
        requestModel.contract.updateDate || DateTime.now().toUTC().toJSDate()
    });
    const contractClientEntity = await this.updateClient({
      addressInvoice: requestModel.contract.addressInvoice,
      client: requestModel.client,
      contractEntity,
      customerAddress: requestModel.contract.customerAddress,
      manager,
      password: requestModel.contract.password
    });
    const contractExtendRequestEntity = await this.contractExtendRequestRepository.getContractExtendRequest(
      contractEntity.sid,
      { expand: true }
    );
    await this.updateContractExtendRequest(
      contractExtendRequestEntity,
      manager,
      tariffEntity
    );
    const contractBankDataEntity = await this.updateContractBankData({
      bankData: requestModel.contract.bankData,
      contractEntity,
      manager
    });

    await this.synchronizeCreateContract.saveAvailableTariffs({
      availableTariffs: requestModel.contract.availableTariffs,
      contractEntity,
      manager
    });
    if (requestModel.contract.services?.length) {
      await this.synchronizeCreateContract.saveContractService(
        requestModel.contract.services,
        manager,
        contractEntity,
        requestModel.contract.tariffOption
      );
    }
    if (requestModel.contract.partnerCards?.length) {
      await this.savePartnerCards({
        clientSid: contractEntity.client.sid,
        contractBankDataEntity,
        contractClientEntity,
        contractEntity,
        contractExtendRequestAddressEntity: contractExtendRequestEntity.address,
        contractExtendRequestEntity,
        manager,
        memberSid: member,
        partnerCards: requestModel.contract.partnerCards
      });
    }

    await this.synchronizeCreateContract.createNba(
      requestModel.contract.nba,
      manager,
      contractEntity
    );

    if (validOptions.password) {
      await this.synchronizeCreateContract.saveActivityPasswordVerified({
        contractEntity,
        manager,
        member,
        team: validOptions.team
      });
    }

    await this.synchronizeCreateContract.updateContractExtension({
      contractEntity,
      extensionVariantEntity
    });

    return contractEntity;
  }
}
