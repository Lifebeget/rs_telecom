import { HttpService } from '@nestjs/axios';
import { HttpStatus, Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import Timeout from 'await-timeout';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { lastValueFrom } from 'rxjs';

import { BERLIN_TIME_ZONE } from 'Constants';
import { DebounceService, LoggerService } from 'Server/providers';
import { getPhoneNumberCode } from 'Server/utils';

import { RETRY_KEY } from '../constants';
import { RequestModel, ValidateByPhoneOptions } from '../interfaces';
import { AbortedByTimeoutException } from '../exceptions';
import {
  ActivateModel,
  ClientInfoXMLRequestModel,
  DistributorModel,
  JSONRequestModel,
  NotEasyInfoXMLRequestModel,
  PartnerCardsXMLResponseModel
} from '../models';
import { getErrorCode, getException } from '../utils';

@Injectable()
export class VOHttpService implements OnModuleInit {
  constructor(
    private readonly configService: ConfigService,
    private readonly debounceService: DebounceService,
    private readonly httpService: HttpService,
    private readonly loggerService: LoggerService
  ) {}

  @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: BERLIN_TIME_ZONE })
  private nightly() {
    this.setDebounce(
      this.configService.get<number>(
        'JSON_VALIDATION_NIGHTLY_DEBOUNCE_TIMEOUT',
        20
      )
    );
  }

  @Cron(CronExpression.EVERY_DAY_AT_7AM, { timeZone: BERLIN_TIME_ZONE })
  private daily() {
    this.setDebounce(
      this.configService.get<number>(
        'JSON_VALIDATION_DAILY_DEBOUNCE_TIMEOUT',
        20
      )
    );
  }

  private setDebounce(debounce: number) {
    this.loggerService.info(`Json validation debounce set to ${debounce} sec`);

    const axios = this.httpService.axiosRef;
    axios.defaults.timeout = debounce * 1000;

    this.debounceService.updateSettings({
      minTime: debounce * 1000
    });
  }

  private transform<T>(
    data: unknown,
    Model: ClassConstructor<T>
  ): RequestModel<T> {
    return {
      getRaw() {
        return typeof data === 'string' ? data : JSON.stringify(data);
      },
      getResponse() {
        return plainToClass<T, unknown>(Model, data);
      }
    };
  }

  public onModuleInit() {
    const intercept = (response: AxiosResponse) => {
      this.loggerService.info('Operator response', {
        payload: {
          data: response.data,
          params: response.config.params,
          status: response.status,
          url: response.config.url
        }
      });

      if (typeof response.data === 'string') {
        const Exception = getException(response.data);

        if (Exception) {
          return Promise.reject(new Exception(response.data, response.status));
        }

        if (response.config.url === '/decrypt') {
          return;
        }

        return Promise.reject({
          isAxiosError: true,
          response: response.data
        });
      }

      if (typeof response.data === 'object') {
        const { data } = response;
        const message = getErrorCode(data);

        if (typeof message === 'string') {
          const Exception = getException(message);

          if (Exception) {
            return Promise.reject(
              new Exception(response.data, response.status)
            );
          }

          return Promise.reject({
            isAxiosError: true,
            response: response.data
          });
        }
      }
    };

    const reject = (err: unknown) => {
      if (axios.isAxiosError(err)) {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
        return Promise.reject(err?.response?.data || err.message);
      }

      return Promise.reject(err);
    };

    /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- refactor required */
    const retry = async (err: any) => {
      if (err.config[RETRY_KEY]) {
        this.loggerService.info('Operator response', {
          payload: {
            data: err.response.data,
            params: err.response.config.params,
            status: err.response.status,
            url: err.response.config.url
          }
        });

        return Promise.reject(
          new AbortedByTimeoutException(err.response.data, err.response.status)
        );
      }

      err.config[RETRY_KEY] = 1;

      this.loggerService.info('Retry request', {
        payload: {
          params: err.response.config.params,
          url: err.response.config.url
        }
      });

      await Timeout.set(err.config.timeout || 20 * 1000);

      return this.httpService.axiosRef.request(err.config);
    };

    this.httpService.axiosRef.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        this.loggerService.info('Operator request', {
          payload: {
            params: config.params,
            url: config.url
          }
        });

        return config;
      }
    );

    this.httpService.axiosRef.interceptors.response.use(
      (response: AxiosResponse) => intercept(response) ?? response,
      err => {
        if (err.code && err.code === 'ECONNABORTED') {
          return Promise.reject({
            isAxiosError: true,
            response: {
              message: 'Request aborted by timeout'
            }
          });
        }

        if (err.response) {
          if (err.response.config.url === '/reqmtanplz') {
            if (err.response.status === HttpStatus.INTERNAL_SERVER_ERROR) {
              return retry(err);
            }
          }

          return intercept(err.response) ?? reject(err);
        }

        return reject(err);
      }
    );
  }

  async activateJSONRequest(
    phoneNumber: string
  ): Promise<RequestModel<ActivateModel>> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    const baseUrlAlternativeJSON = this.configService.get<string>(
      'VO_HOST_ALTERNATIVE_JSON'
    );

    if (baseUrlAlternativeJSON) {
      const response = await lastValueFrom(
        this.httpService.request({
          baseURL: baseUrlAlternativeJSON,
          data: {
            phoneNumber: `${ndc}${msisdn}`
          },
          method: 'post',
          url: '/info'
        })
      );

      return this.transform(response.data, ActivateModel);
    } else if (this.configService.get<string>('VO_HOST_SMS')) {
      const response = await lastValueFrom(
        this.httpService.request({
          baseURL: this.configService.get<string>('VO_HOST_SMS'),
          method: 'get',
          params: {
            phone: `${ndc}${msisdn}`
          },
          url: '/send-sms-code'
        })
      );

      return this.transform(response.data, ActivateModel);
    } else {
      const response = await lastValueFrom(
        this.httpService.request({
          method: 'get',
          params: {
            msisdn,
            ndc
          },
          url: '/reqmtansendsms'
        })
      );

      return this.transform(response.data, ActivateModel);
    }
  }

  async getJSONRequest({
    phoneNumber,
    password,
    requestId,
    code
  }: ValidateByPhoneOptions): Promise<RequestModel<JSONRequestModel>> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    let url, params;

    if (password) {
      url = '/reqmtanplz';
      params = {
        msisdn,
        ndc,
        password
      };
    }

    if (requestId && code) {
      url = '/reqmtanchecksms';
      params = {
        msisdn,
        ndc,
        requestid: requestId,
        sms_code: code
      };
    }

    if (!url && !params) {
      throw new Error('Invalid parameters');
    }

    const baseUrlAlternativeJSON = this.configService.get<string>(
      'VO_HOST_ALTERNATIVE_JSON'
    );
    if (baseUrlAlternativeJSON && requestId && code) {
      url = '/sms-code';

      const response = await lastValueFrom(
        this.httpService.request({
          baseURL: baseUrlAlternativeJSON,
          data: {
            code,
            phoneNumber: `${ndc}${msisdn}`
          },
          method: 'post',
          url
        })
      );

      const d = response.data;

      const address = {
        address: {
          city: d.city,
          countryCode: 'D',
          email: '',
          firstName: d.firstName,
          houseNumber: d.houseNumber,
          lastName1: d.lastName,
          postCode: d.postCode,
          streetName: d.street
        }
      };

      const nextSubsidyDate =
        d.inlifeTariffChangeEligibility.eligiblity === 'Y'
          ? '14. Oktober 2021'
          : '10. Dezember 2077';
      const data = {
        myData: {
          ban: d.BAN,
          bankAccount: {},
          billingAddress: address,
          customerAddress: address,
          givenName: d.firstName,
          msisdn: (d.MSISDN as string).replace('GSM', ''),
          nextSubsidyDate: nextSubsidyDate,
          subscriberAddress: address,
          surName: d.lastName,
          tariffOption: d.currentTariffName
        },
        vfplan: {
          vfplan: d.currentTariff
        }
      };

      return this.transform(data, JSONRequestModel);
    } else if (this.configService.get<string>('VO_HOST_SMS')) {
      const response = await lastValueFrom(
        this.httpService.request({
          baseURL: this.configService.get<string>('VO_HOST_SMS'),
          method: 'get',
          params: {
            phone_nr_request_id: requestId,
            sms_code: code
          },
          url: '/check-sms-code'
        })
      );

      return this.transform(response.data, JSONRequestModel);
    } else {
      const response = await lastValueFrom(
        this.httpService.request({
          method: 'get',
          params,
          url
        })
      );

      return this.transform(response.data, JSONRequestModel);
    }
  }

  async getXMLRequest(
    phoneNumber: string,
    password: string,
    userId: string,
    postcode: string
  ): Promise<
    RequestModel<ClientInfoXMLRequestModel | NotEasyInfoXMLRequestModel>
  > {
    try {
      const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

      const response = await lastValueFrom(
        this.httpService.request({
          baseURL: this.configService.get<string>('ALTERNATIVE_VO_HOST'),
          method: 'get',
          params: {
            msisdn,
            ndc,
            password,
            postcode: Number(postcode),
            vouserid: userId
          },
          timeout: 6 * 60 * 1000,
          url: '/reqeposvopost' // 6 min
        })
      );

      if (response.data?.easyInfo) {
        return this.transform(
          response.data?.easyInfo?.M?.[0]?.A?.[0]?.data?.data?.data?.steps?.[0]
            ?.message?.model,
          NotEasyInfoXMLRequestModel
        );
      }

      return this.transform(
        response.data?.['Antworten']?.['SubHandyUndOderKundendaten'],
        ClientInfoXMLRequestModel
      );
    } catch (err) {
      throw err;
    }
  }

  async getDistributorRequest(
    phoneNumber: string,
    postcode: string
  ): Promise<RequestModel<DistributorModel> & { ndc: string; msisdn: string }> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    const response = await lastValueFrom(
      this.httpService.request({
        baseURL: this.configService.get<string>('ALTERNATIVE_VO_HOST'),
        method: 'get',
        params: {
          msisdn,
          ndc,
          postcode: Number(postcode)
        },
        url: '/setuservobypostcode'
      })
    );

    return {
      ...this.transform(response.data, DistributorModel),
      msisdn,
      ndc
    };
  }

  async getDecodedIbanPart(ibanEncodedPart: string): Promise<AxiosResponse> {
    return lastValueFrom(
      this.httpService.request({
        method: 'get',
        params: {
          str: ibanEncodedPart
        },
        url: '/decrypt'
      })
    );
  }

  async validateContractPassword(
    phoneNumber: string,
    password: string
  ): Promise<void> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    await lastValueFrom(
      this.httpService.request({
        method: 'get',
        params: {
          msisdn,
          ndc,
          password
        },
        url: '/reqmtancheckpassword'
      })
    );
  }

  async setMiddlewareContractPassword(
    phoneNumber: string,
    password: string
  ): Promise<void> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    await lastValueFrom(
      this.httpService.request({
        method: 'put',
        params: {
          msisdn,
          ndc,
          password
        },
        url: '/setuserpassword'
      })
    );
  }

  async fetchContractPartnerCards({
    phoneNumber,
    password,
    userId,
    banId
  }: {
    phoneNumber: string;
    password: string;
    userId: string;
    banId: string;
  }): Promise<PartnerCardsXMLResponseModel> {
    const { ndc, msisdn } = getPhoneNumberCode(phoneNumber);

    // reqeposban?ndc=XXX&msisdn=XXXXX&password=XXXX&vouserid=XXXX-XXX&ban=XXX
    const { data } = await lastValueFrom(
      this.httpService.request({
        method: 'get',
        params: {
          ban: banId,
          msisdn,
          ndc,
          password,
          vouserid: userId
        },
        url: '/reqeposban'
      })
    );

    return plainToClass(PartnerCardsXMLResponseModel, data);
  }
}
