import { BadRequestException, Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { isString, uniq } from 'remeda';
import { U } from 'ts-toolbelt';
import { validate } from 'class-validator';

import {
  ContractRepository,
  ContractXTeamRepository,
  ExtensionVariantRepository
} from 'Server/modules/contract/repositories';
import { ClientXContractRepository } from 'Server/modules/client/repositories';
import { MemberRepository } from 'Server/modules/member/repositories';
import { TeamEntity } from 'Server/modules/team/entities';

import {
  ContractAddressInvoiceEntity,
  ContractAddressMailEntity,
  ContractAvailableTariffEntity,
  ContractBankDataEntity,
  ContractClientEntity,
  ContractEntity,
  ContractExtendRequestAddressEntity,
  ContractExtendRequestEntity,
  ContractMemberLogEntity,
  ContractNextBestActionEntity,
  ContractNextBestActionTipEntity,
  ContractNextBestActionTipServiceEntity,
  ContractXLeadEntity,
  ContractXServiceEntity,
  ContractXTeamEntity,
  ExtensionVariantEntity
} from '../../../modules/contract/entities';
import {
  Address,
  AvailableTariffs,
  BankData,
  Client,
  Contract,
  Nba,
  OmitValidOptionsTeam,
  PartnerCard,
  Service,
  VOResponse
} from '../interfaces';
import { ClientService } from '../../../modules/client/client.service';
import { ActivityTypeRepository } from '../../../modules/activity/repositoties';
import { ContractService } from '../../../modules/contract/contract.service';
import { LoggerService } from '../../logger';
import { TariffRepository } from '../../../modules/tariff/repositories';
import {
  ContractMemberService,
  ContractPartnerProcessService
} from '../../../modules/contract/services';
import {
  LeadRepository,
  LeadStatusRepository
} from '../../../modules/lead/repositories';
import { WorkflowService } from '../../../modules/workflow/services';
import { getExtensionVariant } from '../../../../Helpers';
import { parsePhoneNumber, parseTariffHW } from '../../../../Utils';
import { ContractError } from '../../../../Constants/errorValues';
import {
  ActivityType,
  ContractRequestType,
  ContractStatus,
  ExtensionVariant,
  LeadStatus
} from '../../../../Constants';
import {
  ActivityContractAssignedEntity,
  ActivityContractCreatedEntity,
  ActivityEntity
} from '../../../modules/activity/entities';
import { LeadEntity } from '../../../modules/lead/entities';
import {
  ClientAddressEntity,
  ClientEntity,
  ClientNoteEntity,
  ClientXContractEntity
} from '../../../modules/client/entities';
import {
  TariffEntity,
  TariffXServiceEntity
} from '../../../modules/tariff/entities';
import { ContractFlowModel } from '../../../modules/workflow/models';
import { MailAndInvoiceAddressRequest } from '../interfaces/mail-and-invoice-address-request';
import { DistributorModel } from '../models';
import { ServiceEntity } from '../../../modules/service/entities';
import { ServiceRepository } from '../../../modules/service/repositories';

@Injectable()
export class SynchronizeCreateContractService {
  constructor(
    private readonly serviceRepository: ServiceRepository,
    private readonly clientService: ClientService,
    private readonly activityTypeRepository: ActivityTypeRepository,
    private readonly contractRepository: ContractRepository,
    private readonly contractService: ContractService,
    private readonly loggerService: LoggerService,
    private readonly contractPartnerProcessService: ContractPartnerProcessService,
    private readonly tariffRepository: TariffRepository,
    private readonly contractXTeamRepository: ContractXTeamRepository,
    private readonly memberRepository: MemberRepository,
    private readonly contractMemberService: ContractMemberService,
    private readonly leadRepository: LeadRepository,
    private readonly leadStatusRepository: LeadStatusRepository,
    private readonly clientXContractRepository: ClientXContractRepository,
    private readonly extensionVariantRepository: ExtensionVariantRepository,
    private readonly workflowService: WorkflowService
  ) {}

  private async saveContractBankData(
    bankData: BankData,
    manager: EntityManager
  ): Promise<ContractBankDataEntity> {
    try {
      const insertData = manager.create(ContractBankDataEntity, bankData);
      //@Todo: оставил на всякий. Удалить в слеюущей итерации
      const errors = await validate(insertData);
      if (errors.length > 0) {
        throw new Error('Error validate in saveContractBankData');
      }
      const result = await manager.save(insertData);

      if (result.sid) {
        return result;
      }
      throw new Error('Error in saveContractBankData method');
    } catch (err) {
      throw err;
    }
  }

  async saveAvailableTariffs({
    availableTariffs,
    manager,
    contractEntity
  }: {
    availableTariffs: U.Nullable<AvailableTariffs[]>;
    manager: EntityManager;
    contractEntity: ContractEntity;
  }) {
    if (availableTariffs?.length) {
      if (
        await manager.findOne(ContractAvailableTariffEntity, {
          contract: contractEntity.sid
        })
      ) {
        await manager.delete(ContractAvailableTariffEntity, {
          contract: contractEntity.sid
        });
      }

      await manager.save(
        ContractAvailableTariffEntity,
        availableTariffs.map(
          ({ code }) =>
            new ContractAvailableTariffEntity({
              code,
              contract: contractEntity.sid
            })
        )
      );
    }
  }

  private async tariffXService(
    manager: EntityManager,
    tariffOption: U.Nullable<string>,
    serviceEntities: ServiceEntity[]
  ) {
    const tariffEntity = await manager.findOne(TariffEntity, {
      code: tariffOption
    });

    if (tariffEntity) {
      if (
        await manager.findOne(TariffXServiceEntity, {
          tariff: tariffEntity.sid
        })
      ) {
        await manager.delete(TariffXServiceEntity, {
          tariff: tariffEntity.sid
        });
      }

      await manager.save(
        TariffXServiceEntity,
        serviceEntities.map(
          ({ sid: service }) =>
            new TariffXServiceEntity({
              service,
              tariff: tariffEntity.sid
            })
        )
      );
    }
  }

  async saveContractService(
    services: Service[],
    manager: EntityManager,
    contractEntity: ContractEntity,
    tariffOption: U.Nullable<string>
  ) {
    if (
      await manager.findOne(ContractXServiceEntity, {
        contract: contractEntity.sid
      })
    ) {
      await manager.delete(ContractXServiceEntity, {
        contract: contractEntity.sid
      });
    }
    const serviceList = uniq(
      services.map(({ code }) => {
        if (!code) {
          throw Error('Service code is required');
        }

        return code;
      })
    );
    const existingServicesEntities = await this.serviceRepository.getServiceByCodeList(
      serviceList
    );
    const serviceEntities = await Promise.all(
      services.map(
        async ({
          name,
          code: servicesCode,
          removable,
          unremovableMessage,
          linkMod,
          link
        }) => {
          if (!servicesCode) {
            throw Error('Service code is required');
          }
          const serviceEntity = existingServicesEntities.find(
            ({ code }) => code === servicesCode
          );
          if (serviceEntity) {
            return serviceEntity;
          }

          return manager.save(
            ServiceEntity,
            new ServiceEntity({
              code: servicesCode,
              link,
              linkMod,
              name,
              removable,
              unremovableMessage
            })
          );
        }
      )
    );
    await manager.save(
      ContractXServiceEntity,
      serviceEntities.map(
        ({ sid: service }) =>
          new ContractXServiceEntity({
            contract: contractEntity.sid,
            service
          })
      )
    );
    this.loggerService.info(
      `Contract services "${contractEntity.sid}" successfully saved`
    );
    await this.tariffXService(manager, tariffOption, serviceEntities);
  }

  private async saveContract({
    earlyExtensionDate,
    normalExtensionDate,
    tariffName,
    lastProcessedAt,
    contractBankDataEntitySid,
    contract,
    extensionVariantEntity,
    manager,
    password,
    isHWOmitted,
    requiredHWCost,
    tariffEntity,
    method,
    voUserId,
    projectId,
    region
  }: {
    tariffName: string | null;
    earlyExtensionDate: U.Nullable<Date>;
    normalExtensionDate: U.Nullable<Date>;
    lastProcessedAt?: Date;
    contractBankDataEntitySid: string;
    contract: Contract;
    extensionVariantEntity: ExtensionVariantEntity;
    manager: EntityManager;
    isHWOmitted: boolean;
    requiredHWCost: U.Nullable<number>;
    password?: string | null;
    tariffEntity: TariffEntity | undefined;
    method: ContractRequestType;
    voUserId?: string | null;
    projectId: string;
    region: string;
  }): Promise<ContractEntity> {
    //@Todo: оставил временно. Убрать try catch, он не нужен
    try {
      const contractEntity = await manager.save(
        manager.create(ContractEntity, {
          bankData: contractBankDataEntitySid,
          contractStartDate: contract.contractStartDate,
          createMethod: method,
          earlyExtensionDate,
          extension: extensionVariantEntity.sid,
          isHWOmitted,
          lastProcessedAt,
          lastSynchronizedAt: new Date(),
          nextRenewalDate: contract.nextRenewalDate,
          normalExtensionDate,
          password: password,
          phoneNumber: contract.phoneNumber,
          requiredHWCost,
          tariff: tariffEntity ? tariffEntity.sid : null,
          tariffName: tariffName,
          tariffOption: contract.tariffOption,
          updateMethodLast: method,
          validationSuccess: true
        })
      );

      if (contractEntity.sid && voUserId) {
        await manager.update(
          ContractEntity,
          { sid: contractEntity.sid },
          {
            projectId: projectId,
            region: region,
            userId: voUserId
          }
        );
      }

      return contractEntity;
    } catch (err) {
      throw err;
    }
  }

  async createNba(
    nba: U.Nullable<Nba>,
    manager: EntityManager,
    contractEntity: ContractEntity
  ) {
    if (nba) {
      if (
        await manager.findOne(ContractNextBestActionEntity, {
          contract: contractEntity.sid
        })
      ) {
        await manager.delete(ContractNextBestActionEntity, {
          contract: contractEntity.sid
        });
      }
      const contractNextBestActionTipEntities = await manager.save(
        ContractNextBestActionTipEntity,
        nba.tips.map(
          ({ rowId, cellCode, extractionDate, flowChartId, tipText }) =>
            new ContractNextBestActionTipEntity({
              cellCode,
              extractionDate,
              flowChartId,
              rowId,
              tipText
            })
        )
      );

      await manager.save(
        ContractNextBestActionEntity,
        contractNextBestActionTipEntities.map(
          ({ sid: tips }) =>
            new ContractNextBestActionEntity({
              banId: nba.banId,
              contract: contractEntity.sid,
              phoneNumber: nba.phoneNumber,
              tips
            })
        )
      );

      const services = uniq(
        nba.tips.reduce<string[]>((prev, { services }) => {
          services.forEach(service => {
            if (service) {
              prev.push(service);
            }
          });

          return prev;
        }, [])
      );
      const existingServicesEntities = await this.serviceRepository.getServiceByCodeList(
        services
      );
      const serviceEntities = await Promise.all(
        services.map(async code => {
          const serviceEntity = existingServicesEntities.find(
            (res: ServiceEntity) => res.code === code
          );
          if (serviceEntity) {
            return serviceEntity;
          }

          return manager.save(
            ServiceEntity,
            new ServiceEntity({
              code,
              name: code
            })
          );
        })
      );

      const contractNextBestActionTipServiceEntities = nba.tips.reduce<
        ContractNextBestActionTipServiceEntity[]
      >((prev, { services }, index) => {
        services.forEach(service => {
          const serviceEntity = serviceEntities.find(
            ({ code }) => code === service
          );

          if (serviceEntity) {
            prev.push(
              new ContractNextBestActionTipServiceEntity({
                service: serviceEntity.sid,
                tips: contractNextBestActionTipEntities[index].sid
              })
            );
          }
        });

        return prev;
      }, []);
      await manager.save(
        ContractNextBestActionTipServiceEntity,
        contractNextBestActionTipServiceEntities
      );
      this.loggerService.info(
        `Contract NBA "${contractEntity.sid}" successfully updated`
      );
    }
  }

  private async saveActivityContractCreate({
    isImported,
    contractEntity,
    team,
    manager,
    member
  }: {
    isImported?: boolean;
    contractEntity: ContractEntity;
    team: string;
    manager: EntityManager;
    member: string;
  }) {
    const activityType = await this.activityTypeRepository.getTypeByName(
      ActivityType.ContractCreated
    );

    const activityEntity = new ActivityEntity();
    activityEntity.contract = contractEntity.sid;
    activityEntity.type = activityType.sid;
    activityEntity.member = member;
    activityEntity.team = team;

    await manager.save(ActivityEntity, activityEntity);
    const statusIdle = await this.workflowService.getContractStatusByName(
      ContractStatus.Idle
    );

    const activityContractCreatedEntity = new ActivityContractCreatedEntity();
    activityContractCreatedEntity.activity = activityEntity.sid;
    activityContractCreatedEntity.status = statusIdle.sid;
    activityContractCreatedEntity.isImported = isImported || false;
    await manager.save(
      ActivityContractCreatedEntity,
      activityContractCreatedEntity
    );
  }

  private async saveMailAndInvoiceAddress({
    contractEntity,
    addressInvoice,
    addressMail,
    customerAddress,
    tariffName,
    tariffOption,
    manager,
    team,
    client
  }: {
    tariffName: U.Nullable<string>;
    tariffOption: U.Nullable<string>;
    contractEntity: ContractEntity;
    addressMail: Address;
    addressInvoice: Address;
    customerAddress: Address;
    manager: EntityManager;
    team: string;
    client: Client;
  }): Promise<MailAndInvoiceAddressRequest> {
    const contractAddressInvoiceEntity = new ContractAddressInvoiceEntity();
    contractAddressInvoiceEntity.contract = contractEntity.sid;
    contractAddressInvoiceEntity.firstName = addressInvoice.firstName;
    contractAddressInvoiceEntity.surname = addressInvoice.surname;
    contractAddressInvoiceEntity.street = addressInvoice.street;
    contractAddressInvoiceEntity.house = addressInvoice.house;
    contractAddressInvoiceEntity.land = addressInvoice.land;
    contractAddressInvoiceEntity.postcode = addressInvoice.postcode;
    contractAddressInvoiceEntity.location = addressInvoice.location;
    contractAddressInvoiceEntity.emailAddress = addressInvoice.emailAddress;

    await manager.save(
      ContractAddressInvoiceEntity,
      contractAddressInvoiceEntity
    );
    const contractAddressMailEntity = new ContractAddressMailEntity();
    contractAddressMailEntity.contract = contractEntity.sid;
    contractAddressMailEntity.firstName = addressMail.firstName;
    contractAddressMailEntity.surname = addressMail.surname;
    contractAddressMailEntity.street = addressMail.street;
    contractAddressMailEntity.house = addressMail.house;
    contractAddressMailEntity.land = addressMail.land;
    contractAddressMailEntity.postcode = addressMail.postcode;
    contractAddressMailEntity.location = addressMail.location;
    contractAddressMailEntity.emailAddress = addressMail.emailAddress;

    await manager.save(ContractAddressMailEntity, contractAddressMailEntity);

    return this.saveExtendRequestAddress({
      client,
      contractEntity,
      customerAddress,
      manager,
      tariffName,
      tariffOption,
      team: team
    });
  }

  private async saveExtendRequestAddress({
    contractEntity,
    tariffName,
    tariffOption,
    customerAddress,
    manager,
    team,
    client
  }: {
    tariffName: U.Nullable<string>;
    tariffOption: U.Nullable<string>;
    contractEntity: ContractEntity;
    customerAddress: Address;
    manager: EntityManager;
    team: string;
    client: Client;
  }): Promise<MailAndInvoiceAddressRequest> {
    const contractExtendRequestEntity = new ContractExtendRequestEntity({
      bankData: contractEntity.bankData,

      contract: contractEntity.sid,

      // team is required indeed?
      tariff: contractEntity.tariff,

      tariffName: tariffName,
      tariffOption: tariffOption,
      team: team
    });

    await manager.save(
      ContractExtendRequestEntity,
      contractExtendRequestEntity
    );
    const contractExtendRequestAddressEntity = new ContractExtendRequestAddressEntity(
      {
        emailAddress: customerAddress.emailAddress,
        extendRequest: contractExtendRequestEntity.sid,
        firstName: client.firstName,
        house: customerAddress.house,
        land: customerAddress.land,
        location: customerAddress.location,
        postcode: customerAddress.postcode,
        street: customerAddress.street,
        surname: client.surname
      }
    );

    await manager.save(
      ContractExtendRequestAddressEntity,
      contractExtendRequestAddressEntity
    );

    return { contractExtendRequestAddressEntity, contractExtendRequestEntity };
  }

  private async saveActivityContractAssigned({
    manager,
    contractEntity,
    member,
    team
  }: {
    manager: EntityManager;
    contractEntity: ContractEntity;
    member: string;
    team?: string;
  }) {
    const activityType = await this.activityTypeRepository.getTypeByName(
      ActivityType.ContractAssigned
    );

    const activityEntity = new ActivityEntity();
    activityEntity.contract = contractEntity.sid;
    activityEntity.type = activityType.sid;
    activityEntity.member = member;
    activityEntity.team = team;

    await manager.save(ActivityEntity, activityEntity);

    const activityContractAssignedEntity = new ActivityContractAssignedEntity();
    activityContractAssignedEntity.activity = activityEntity.sid;
    activityContractAssignedEntity.memberTo = member;

    await manager.save(
      ActivityContractAssignedEntity,
      activityContractAssignedEntity
    );

    const contractMemberLog = new ContractMemberLogEntity();

    contractMemberLog.contract = contractEntity.sid;
    contractMemberLog.member = member;

    await manager.save(ContractMemberLogEntity, contractMemberLog);

    this.loggerService.info(
      `Added contract "${contractEntity.sid}" to member "${member}"`
    );
  }

  private async updateClient({
    clientEntity,
    customerAddress,
    requestClient,
    requestPhoneNumber,
    manager,
    contractEntity,
    phoneNumber,
    leadEntity
  }: {
    clientEntity?: ClientEntity;
    requestPhoneNumber: string;
    requestClient: Client;
    customerAddress: Address;
    manager: EntityManager;
    contractEntity: ContractEntity;
    phoneNumber: string;
    leadEntity: LeadEntity;
  }): Promise<ContractClientEntity> {
    let client;

    let clientDuplicate;
    if (!clientEntity) {
      clientDuplicate = await this.clientService.getClientDuplicate(
        leadEntity.client
      );
      client = clientDuplicate?.sid ?? leadEntity.client;
    } else {
      client = clientEntity.sid;
    }

    let clientXContractEntity = await this.clientXContractRepository.findOne({
      client: client,
      contract: contractEntity.sid
    });

    if (!clientXContractEntity) {
      clientXContractEntity = new ClientXContractEntity();
      clientXContractEntity.client = client;
      clientXContractEntity.contract = contractEntity.sid;

      await manager.save(ClientXContractEntity, clientXContractEntity);
    }

    await manager.update(
      ClientEntity,
      { sid: client },
      {
        banId: requestClient.banId,
        firstName: requestClient.firstName,
        phoneNumber: requestPhoneNumber,
        surname: requestClient.surname
      }
    );
    const contractClientEntity = new ContractClientEntity({
      appeal: requestClient.appeal,
      banId: requestClient.banId,
      birthday: requestClient.birthday,
      clientAddressEmailAddress: customerAddress.emailAddress,
      clientAddressFirstName: customerAddress.firstName,
      clientAddressHouse: customerAddress.house,
      clientAddressLand: customerAddress.land,
      clientAddressLocation: customerAddress.location,
      clientAddressPostcode: customerAddress.postcode,
      clientAddressStreet: customerAddress.street,
      clientAddressSurname: customerAddress.surname,
      contract: contractEntity.sid,
      firstName: requestClient.firstName,
      phoneNumber: phoneNumber,
      surname: requestClient.surname
    });
    await manager.save(ContractClientEntity, contractClientEntity);

    await manager.update(
      ClientAddressEntity,
      { client: client },
      customerAddress
    );

    if (clientDuplicate) {
      await manager.update(
        LeadEntity,
        { sid: leadEntity.sid },
        {
          client: client
        }
      );

      await manager.delete(ActivityEntity, {
        client: leadEntity.client
      });

      await manager.delete(ClientNoteEntity, {
        client: leadEntity.client
      });

      await manager.delete(ClientAddressEntity, {
        client: leadEntity.client
      });

      await manager.delete(ClientXContractEntity, {
        client: leadEntity.client
      });

      await manager.delete(ClientEntity, {
        sid: leadEntity.client
      });
    }
    if (!contractEntity.password && leadEntity.password) {
      await manager.update(
        ContractEntity,
        {
          sid: contractEntity.sid
        },
        {
          password: leadEntity.password
        }
      );
    }

    return contractClientEntity;
  }

  public async saveActivityPasswordVerified({
    manager,
    contractEntity,
    member,
    team
  }: {
    manager: EntityManager;
    team?: string;
    member: string;
    contractEntity: ContractEntity;
  }) {
    const activityTypePasswordVerified = await this.activityTypeRepository.getTypeByName(
      ActivityType.ContractPasswordVerified
    );

    const activityEntityPasswordVerified = new ActivityEntity();
    activityEntityPasswordVerified.contract = contractEntity.sid;
    activityEntityPasswordVerified.type = activityTypePasswordVerified.sid;
    activityEntityPasswordVerified.member = member;
    activityEntityPasswordVerified.team = team;

    await manager.save(ActivityEntity, activityEntityPasswordVerified);
  }

  private async updateLead({
    leadEntity,
    client,
    manager,
    contractEntity
  }: {
    leadEntity: LeadEntity;
    client: Client;
    contractEntity: ContractEntity;
    manager: EntityManager;
  }) {
    const leadStatusEntity = await this.leadStatusRepository.getStatusByName(
      LeadStatus.confirmed
    );

    if (!client.firstName) {
      throw new Error('No firstName provided');
    }

    if (!client.surname) {
      throw new Error('No surname provided');
    }

    const name = [client.firstName, client.surname].filter(isString).join(' ');

    if (name.length) {
      await manager.update(
        LeadEntity,
        { sid: leadEntity.sid },
        {
          name,
          status: leadStatusEntity.sid
        }
      );
    }
    this.loggerService.info(`Confirm lead "${leadEntity.sid}"`);
    const contractXLeadEntity = new ContractXLeadEntity();

    contractXLeadEntity.contract = contractEntity.sid;
    contractXLeadEntity.lead = leadEntity.sid;

    await manager.save(ContractXLeadEntity, contractXLeadEntity);
  }

  async updateContractExtension({
    contractEntity,
    extensionVariantEntity
  }: {
    contractEntity: ContractEntity;
    extensionVariantEntity: ExtensionVariantEntity;
  }) {
    if (contractEntity.extension !== extensionVariantEntity.sid) {
      await this.contractService.emitContractExtensionVariantUpdate(
        contractEntity.sid,
        extensionVariantEntity.sid
      );
    }
  }

  private async actualFlow(
    contractEntity: ContractEntity,
    validOptions: OmitValidOptionsTeam,
    manager: EntityManager
  ) {
    const actualFlow = await this.workflowService.getActualFlowWithoutThrow(
      contractEntity.sid
    );
    if (!actualFlow && validOptions.assignToProvidedMember) {
      await this.workflowService.createFlowWithManager({
        contractSid: contractEntity.sid,
        manager: manager,
        memberSid: validOptions.member,
        teamSid: validOptions.team
      });
    }
  }

  private async addMemberInFlow({
    contractEntity,
    manager,
    member,
    actualFlow
  }: {
    manager: EntityManager;
    contractEntity: ContractEntity;
    member: string;
    actualFlow: ContractFlowModel;
  }) {
    await this.workflowService.addMemberInFlow({
      actualFlowSid: actualFlow.sid,
      contractSid: contractEntity.sid,
      manager: manager,
      memberSid: member
    });
  }

  private parsePhoneNumber(phoneNumber: string) {
    if (parsePhoneNumber(phoneNumber).formatInternational() !== phoneNumber) {
      throw new BadRequestException(
        ContractError.SYNC_PHONE_NUMBER_IS_DIFFERENT
      );
    }
  }

  async parseTariffNameAndOption(tariffName: string | null) {
    if (tariffName) {
      const tariffParseResult = parseTariffHW(tariffName);
      let isHWOmitted, requiredHWCost;
      if (tariffParseResult.isHWOmitted) {
        isHWOmitted = true;
      } else {
        isHWOmitted = false;
        requiredHWCost = tariffParseResult.requiredHWCost;
      }

      return { isHWOmitted, requiredHWCost };
    }

    return { isHWOmitted: false, requiredHWCost: null };
  }

  private async addTeamContractXTeam(
    contractSid: string,
    teamSid: string,
    manager: EntityManager
  ) {
    const contractXTeamEntity = await this.contractXTeamRepository.findOne({
      contract: contractSid,
      team: teamSid
    });

    if (!contractXTeamEntity && teamSid) {
      await manager.save(
        manager.create(ContractXTeamEntity, {
          contract: contractSid,
          team: teamSid
        })
      );
    }
  }

  private async addContractToTeamsBy(
    contractSid: string,
    manager: EntityManager
  ) {
    const teams = await manager
      .createQueryBuilder(TeamEntity, 't')
      .leftJoin(
        ContractXTeamEntity,
        'cxt',
        `cxt.team_sid = t.sid AND cxt.contract_sid = '${contractSid}'`
      )
      .where('t.is_add_import_contract = :isAddImportContract', {
        isAddImportContract: true
      })
      .andWhere('cxt.contract_sid IS NULL')
      .getMany();

    if (teams.length >= 1) {
      await Promise.all(
        teams.map(async team => {
          await manager.save(
            manager.create(ContractXTeamEntity, {
              contract: contractSid,
              team: team.sid
            })
          );
        })
      );
    }
  }

  private async savePartnerCards({
    contractBankDataEntity,
    contractExtendRequestAddressEntity,
    clientSid,
    contractExtendRequestEntity,
    manager,
    contractClientEntity,
    contractEntity,
    memberSid,
    partnerCards
  }: {
    contractBankDataEntity: ContractBankDataEntity;
    contractExtendRequestAddressEntity: ContractExtendRequestAddressEntity;
    clientSid: string;
    contractExtendRequestEntity: ContractExtendRequestEntity;
    manager: EntityManager;
    contractClientEntity: ContractClientEntity;
    contractEntity: ContractEntity;
    memberSid: string;
    partnerCards: PartnerCard[];
  }) {
    await this.contractPartnerProcessService.processFetchedPartnerCards({
      cards: partnerCards,
      clientSid,
      contractBankDataEntity,
      contractClient: contractClientEntity,
      contractExtendRequestAddressEntity,
      contractOrigin: contractEntity,
      contractOriginExtendRequest: contractExtendRequestEntity,
      manager,
      member: await this.memberRepository.getMember(memberSid, { expand: true })
    });
  }

  async getOneActualTariff(
    tariffOption: U.Nullable<string>,
    tariffName: U.Nullable<string>
  ): Promise<{
    tariffName: string | null;
    tariffEntity: TariffEntity | undefined;
  }> {
    let tariffEntity = await this.tariffRepository.findOneActualTariff({
      code: tariffOption
    });

    if (!tariffEntity && tariffName) {
      tariffEntity = await this.tariffRepository.findOneActualTariff({
        name: tariffName
      });
    }

    return {
      tariffEntity,
      tariffName: tariffName || tariffEntity?.name || null
    };
  }

  public async createContract({
    lastProcessedAt,
    distributor,
    isImported,
    clientEntity,
    leadEntity,
    member,
    requestModel,
    validOptions,
    method,
    manager
  }: {
    lastProcessedAt?: Date;
    distributor?: DistributorModel;
    isImported?: boolean;
    clientEntity?: ClientEntity;
    leadEntity: LeadEntity;
    member: string;
    requestModel: VOResponse;
    validOptions: OmitValidOptionsTeam;
    method: ContractRequestType;
    manager: EntityManager;
  }): Promise<ContractEntity> {
    const vvlNormal = requestModel.contract.subsidyEligibility.find(
      ({ extension }) => extension === ExtensionVariant.normalExtension
    );
    const vvlEarly = requestModel.contract.subsidyEligibility.find(
      ({ extension }) => extension === ExtensionVariant.earlyExtension
    );
    const extensionVariantEntity = await this.extensionVariantRepository.findOne(
      {
        name: getExtensionVariant({
          nextSubsidyDate: requestModel.contract.nextRenewalDate,
          vvlEarlyDate: vvlEarly?.nextRenewalDate,
          vvlNormalDate: vvlNormal?.nextRenewalDate
        })
      }
    );

    if (!extensionVariantEntity) {
      throw new Error('extensionVariantEntity is empty');
    }

    const { tariffName, tariffEntity } = await this.getOneActualTariff(
      requestModel.contract.tariffOption,
      requestModel.contract.tariffName
    );

    const { isHWOmitted, requiredHWCost } = await this.parseTariffNameAndOption(
      tariffName
    );

    await this.parsePhoneNumber(validOptions.phoneNumber);
    const contractBankDataEntity = await this.saveContractBankData(
      requestModel.contract.bankData,
      manager
    );
    const contractEntity = await this.saveContract({
      contract: requestModel.contract,
      contractBankDataEntitySid: contractBankDataEntity.sid,
      earlyExtensionDate: vvlEarly?.nextRenewalDate,
      extensionVariantEntity,
      isHWOmitted,
      lastProcessedAt,
      manager,
      method,
      normalExtensionDate: vvlNormal?.nextRenewalDate,
      password: validOptions.password,
      projectId: distributor?.projectId || '-',
      region: distributor?.region || '-',
      requiredHWCost,
      tariffEntity,
      tariffName,
      voUserId: requestModel.client.voUserId || distributor?.userId
    });

    await this.saveActivityContractCreate({
      contractEntity,
      isImported,
      manager,
      member,
      team: validOptions.team
    });

    const {
      contractExtendRequestEntity,
      contractExtendRequestAddressEntity
    } = await this.saveMailAndInvoiceAddress({
      addressInvoice: requestModel.contract.addressInvoice,
      addressMail: requestModel.contract.addressMail,
      client: requestModel.client,
      contractEntity,
      customerAddress: requestModel.contract.customerAddress,
      manager,
      tariffName,
      tariffOption: requestModel.contract.tariffOption,
      team: validOptions.team
    });

    //Add to orgaization
    await this.addTeamContractXTeam(
      contractEntity.sid,
      validOptions.team,
      manager
    );

    //Add to another team
    await this.addContractToTeamsBy(contractEntity.sid, manager);

    await this.updateLead({
      client: requestModel.client,
      contractEntity,
      leadEntity,
      manager
    });
    if (requestModel.contract.services?.length) {
      await this.saveContractService(
        requestModel.contract.services,
        manager,
        contractEntity,
        requestModel.contract.tariffOption
      );
    }

    const contractClientEntity = await this.updateClient({
      clientEntity,
      contractEntity,
      customerAddress: requestModel.contract.customerAddress,
      leadEntity,
      manager,
      phoneNumber: validOptions.phoneNumber,
      requestClient: requestModel.client,
      requestPhoneNumber: requestModel.contract.phoneNumber
    });
    if (requestModel.contract.partnerCards?.length && clientEntity) {
      await this.savePartnerCards({
        clientSid: clientEntity.sid,
        contractBankDataEntity,
        contractClientEntity,
        contractEntity,
        contractExtendRequestAddressEntity,
        contractExtendRequestEntity,
        manager,
        memberSid: member,
        partnerCards: requestModel.contract.partnerCards
      });
    }
    await this.createNba(requestModel.contract.nba, manager, contractEntity);

    await this.saveAvailableTariffs({
      availableTariffs: requestModel.contract.availableTariffs,
      contractEntity,
      manager
    });
    if (validOptions.password) {
      await this.saveActivityPasswordVerified({
        contractEntity,
        manager,
        member,
        team: validOptions.team
      });
    }
    await this.updateContractExtension({
      contractEntity,
      extensionVariantEntity
    });

    return contractEntity;
  }
}
