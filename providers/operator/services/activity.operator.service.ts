import { Injectable } from '@nestjs/common';

import { ActivityType } from 'Constants';
import {
  ActivityRepository,
  ActivityTypeRepository
} from 'Server/modules/activity/repositoties';

@Injectable()
export class ActivityOperatorService {
  constructor(
    private readonly activityTypeRepository: ActivityTypeRepository,
    private readonly activityRepository: ActivityRepository
  ) {}

  async createActivity(contractSid: string, type: ActivityType) {
    const activityType = await this.activityTypeRepository.getTypeByName(type);
    await this.activityRepository.createActivityWithType({
      contractSid,
      typeSid: activityType.sid
    });
  }
}
