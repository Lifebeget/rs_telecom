import { isNil } from 'remeda';

import { CanBeRenewed } from 'Constants';

export const getRenewMode = (
  value: Record<string, unknown>
): CanBeRenewed | null => {
  if (!isNil(value['Rot_ErwerbNichtMoeglich'])) {
    return CanBeRenewed.Red;
  }

  if (!isNil(value['ErwerbMitAddedRuntimeMoeglich'])) {
    return CanBeRenewed.Yellow;
  }

  if (!isNil(value['Gruen_ErwerbMoeglich'])) {
    return CanBeRenewed.Green;
  }

  if (!isNil(value['Auftragsfehler'])) {
    return CanBeRenewed.Error;
  }

  return CanBeRenewed.Unknown;
};
