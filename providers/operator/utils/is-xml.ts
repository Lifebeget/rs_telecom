import XML from 'fast-xml-parser';

export const isXML = (text: string) => {
  try {
    const xml = text.replace('<?xml version="1.0" encoding="UTF-8"?>', '');
    const result = XML.validate(xml);

    return typeof result === 'boolean' && result;
  } catch (err) {
    return false;
  }
};
