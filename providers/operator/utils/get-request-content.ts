import { AxiosError } from 'axios';

import { VoException } from '../exceptions';

export const getRequestContent = (err: AxiosError<unknown> | VoException) =>
  typeof err.response === 'string'
    ? err.response
    : JSON.stringify(err.response);
