export const isJSON = (json: string) => {
  try {
    JSON.parse(json);

    return true;
  } catch (err) {
    return false;
  }
};
