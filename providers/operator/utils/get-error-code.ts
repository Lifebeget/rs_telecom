/* eslint-disable @typescript-eslint/no-explicit-any -- no need to type every error data type */

export const getErrorCode = (data: any): string | undefined => {
  const voError = data.VoError;
  if (voError) {
    return voError;
  }

  const errCode = data.ErrCode;
  if (errCode) {
    return errCode;
  }

  const limitError = data.limitError;
  if (limitError) {
    return limitError;
  }

  const agentError = data.AgentError;
  if (agentError) {
    return agentError;
  }

  const authError = data.auth?.message;
  if (authError) {
    return authError;
  }

  const errorText = data.vfssdublin?.errorText;
  if (errorText) {
    return errorText;
  }

  const jobError =
    data['Antworten']?.['SubHandyUndOderKundendaten']?.['Antwortstatus']?.[
      'Auftragsfehler'
    ]?.['Fehlertext'];
  if (jobError) {
    return jobError;
  }

  const errorMessage = data['Antworten']?.['Fehler']?.['Fehlertext'];
  if (errorMessage) {
    return errorMessage;
  }

  const message = data.message;
  if (message) {
    return message;
  }

  const error = data.error;
  if (error) {
    return error;
  }

  const easyError = data.EasyError;
  if (easyError) {
    return easyError;
  }
};
