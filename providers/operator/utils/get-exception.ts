import {
  AbortedByTimeoutException,
  AccessBlockedException,
  AccessDeniedException,
  AccountStatusIsIncorrectException,
  ClientBlockedException,
  CodeIsIncorrectException,
  CodeIsNoLongerValidException,
  CouldNotBeAuthenticateException,
  IOErrorToServerException,
  IbanDecodingFailedException,
  IncorrectPasswordException,
  IncorrectPasswordLimitExceededException,
  IncorrectPhoneNumberException,
  IncorrectPhoneOrPasswordException,
  MtanLimitExceededException,
  PasswordValidationFailedException,
  UserNotExistException,
  VODoNotExistException,
  VONotAssignedException,
  VONotAssignedToAgentException,
  VONotSupportedException,
  VOUrlNotAssignedException,
  XmlLimitExceededException,
  XmlRequestSendException
} from '../exceptions';

const exceptions = [
  AbortedByTimeoutException,
  AccountStatusIsIncorrectException,
  AccessBlockedException,
  AccessDeniedException,
  ClientBlockedException,
  CodeIsIncorrectException,
  CodeIsNoLongerValidException,
  CouldNotBeAuthenticateException,
  IOErrorToServerException,
  IbanDecodingFailedException,
  IncorrectPasswordException,
  IncorrectPasswordLimitExceededException,
  IncorrectPhoneNumberException,
  IncorrectPhoneOrPasswordException,
  MtanLimitExceededException,
  PasswordValidationFailedException,
  UserNotExistException,
  VODoNotExistException,
  VONotAssignedException,
  VONotAssignedToAgentException,
  VOUrlNotAssignedException,
  VONotSupportedException,
  XmlLimitExceededException,
  XmlRequestSendException
] as const;

export function getException(message: string) {
  return exceptions.find(exception => exception.instanceOf(message));
}
