import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class AddressDetailModel {
  @Expose()
  public readonly firstName!: string;

  @Expose()
  public readonly lastName1!: string;

  @Expose()
  public readonly lastName2!: string;

  @Expose()
  public readonly lastName3!: string;

  @Expose()
  public readonly streetName!: string;

  @Expose()
  public readonly houseNumber!: string;

  @Expose()
  public readonly countryCode!: string;

  @Expose()
  public readonly postCode!: string;

  @Expose()
  public readonly city!: string;

  @Expose()
  public readonly phoneNumber!: string;

  @Expose()
  public readonly email!: string;
}

@Exclude()
export class AddressModel {
  @Expose()
  @Type(() => AddressDetailModel)
  public readonly address!: AddressDetailModel;
}
