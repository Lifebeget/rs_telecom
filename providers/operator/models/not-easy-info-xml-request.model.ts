import { BadRequestException } from '@nestjs/common';
import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';
import { isNil } from 'remeda';
import { U } from 'ts-toolbelt';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator';

import {
  CONTRACT_END_EXTENSION_OFFSET_ADDITIONAL,
  ExtensionVariant
} from 'Constants';
import { parsePhoneNumber } from 'Utils';
import { ContractError } from 'Constants/errorValues';

import { PartnerCard, VOResponse } from '../interfaces';

import { ClientModelNotEasy } from './client-data-model';
import { ContractServiceModel } from './contract-service.model';
import { BankDataNotEasyModel } from './bank-data-not-easy.model';
import { CustomerAddressNotEasy } from './customer-address-not-easy';
import { AddressNotEasy } from './address-invoice-not-easy.model';
import { ContractNextBestActionModelEasy } from './contract-next-best-action-easy.model';

@Exclude()
class AvailableTariffModel {
  @Expose({ name: 'id' })
  @IsNotEmpty()
  @IsString()
  public readonly code!: string;
}
@Exclude()
class SubsidyEligibilityModel {
  @Expose({ name: 'name' })
  public readonly name!: string | null;

  @Expose()
  public get extension(): ExtensionVariant | null {
    if (this.name === 'Standard Vertragsverlängerung') {
      return ExtensionVariant.normalExtension;
    }

    if (this.name === 'VVL mit Tarifwechsel') {
      return ExtensionVariant.normalExtension;
    }

    if (this.name === 'Vorzeitige Vertragsverlängerung ohne Zuzahlung') {
      return ExtensionVariant.earlyExtension;
    }

    return null;
  }

  @Expose({ name: 'possibleFrom' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromISO(value, { zone: 'utc' }).toJSDate();
    }

    return null;
  })
  public readonly nextRenewalDate!: Date | null;
}
// clone of PartnerCardsXMLResponseCardModel (except `effectiveDate` parsing)
// but it could be temporary so it's stored individually
@Exclude()
export class NotEasyInfoXMLRequestPartnerCardModel implements PartnerCard {
  @Expose({ name: 'ban' })
  public readonly banId!: string;

  @Expose()
  @Transform(({ obj }) => obj['subscriberNumber']['NDC'])
  public readonly ndc!: string;

  @Expose()
  @Transform(({ obj }) => obj['subscriberNumber']['MSISDN'])
  public readonly msisdn!: string;

  @Expose({ name: 'tariffOption' })
  public readonly tariffCode!: string;

  @Expose({ name: 'effectiveDate' })
  @Transform(({ value }) => DateTime.fromISO(value).toJSDate())
  public readonly contractStartDate!: Date;

  @Expose()
  // TODO: move to utility transform decorator
  @Transform(({ obj }) => {
    const ndc = obj['subscriberNumber']['NDC'];
    const msisdn = obj['subscriberNumber']['MSISDN'];

    if (
      (typeof ndc !== 'string' && typeof ndc !== 'number') ||
      (typeof msisdn !== 'string' && typeof msisdn !== 'number')
    ) {
      throw new BadRequestException(ContractError.CONTRACT_NO_PHONE_NUMBER);
    }

    return parsePhoneNumber(`${ndc} ${msisdn}`).formatInternational();
  })
  public readonly phoneNumber!: string;

  @Expose()
  public readonly memberType!: string;

  @Expose()
  @Transform(({ obj }) => obj['memberType'] === 'O')
  public readonly isOwner!: boolean;
}
@Exclude()
class ContractModelNotEasy {
  //@TODO Трансформ делается на первом уровне, изменить в зависимости от вложений
  @Expose({ name: 'ActivationDateString' })
  @IsOptional()
  @IsString()
  readonly updateDate: U.Nullable<Date>;

  @Expose()
  @Transform(({ obj }) => {
    const phoneNumber = obj.contracts?.[0]?.msisdn;
    if (!phoneNumber) {
      throw new Error('No msisdn provided');
    } else if (
      typeof phoneNumber !== 'string' &&
      typeof phoneNumber !== 'number'
    ) {
      throw new BadRequestException(ContractError.CONTRACT_NO_PHONE_NUMBER);
    }

    return parsePhoneNumber(phoneNumber.toString()).formatInternational();
  })
  readonly phoneNumber!: string;

  @Expose()
  @Transform(({ obj }) => obj.loginModel?.ecpassword || null)
  readonly password: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj.contracts?.[0]?.bookedProduct?.name)
  readonly tariffName: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) =>
    obj.contracts?.[0]?.bookedProduct?.id?.concat(
      obj.contracts?.[0]?.bookedProduct?.sub
    )
  )
  readonly tariffOption: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const contractEnd = DateTime.fromISO(obj.contracts?.[0]?.contractEnd, {
      zone: 'utc'
    });

    return contractEnd
      .plus(CONTRACT_END_EXTENSION_OFFSET_ADDITIONAL)
      .toJSDate();
  })
  public readonly nextRenewalDate!: U.Nullable<Date>;

  @Expose()
  @Transform(({ obj }) =>
    DateTime.fromISO(obj.contracts?.[0]?.contractStart, {
      zone: 'utc'
    }).toJSDate()
  )
  public readonly contractStartDate!: Date;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj.contracts?.[0]?.bookableProducts;

    if (!isNil(value)) {
      return plainToClass(
        AvailableTariffModel,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly availableTariffs!: AvailableTariffModel[];

  @Expose()
  @Transform(({ obj }) => {
    const value = obj.contracts?.[0]?.bookedProduct?.services;

    return value.map((res: string) => ({ code: res, name: res }));
  })
  public readonly services!: ContractServiceModel[];

  @Expose()
  @Transform(({ obj }) => plainToClass(BankDataNotEasyModel, obj))
  public readonly bankData!: BankDataNotEasyModel;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressNotEasy, obj))
  public readonly addressInvoice!: AddressNotEasy;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressNotEasy, obj))
  public readonly addressMail!: AddressNotEasy;

  @Expose()
  @Transform(({ obj }) => plainToClass(CustomerAddressNotEasy, obj))
  public readonly customerAddress!: CustomerAddressNotEasy;

  @Expose()
  @Transform(({ obj }) => {
    if (!isNil(obj.contracts?.[0]?.nba)) {
      return plainToClass(ContractNextBestActionModelEasy, obj);
    }

    return null;
  })
  public readonly nba!: ContractNextBestActionModelEasy | null;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.contracts?.[0]?.groups?.[0]?.members;

    if (!value) {
      return [];
    }

    return plainToClass(
      NotEasyInfoXMLRequestPartnerCardModel,
      Array.isArray(value) ? value : [value]
    );
  })
  public readonly partnerCards!: NotEasyInfoXMLRequestPartnerCardModel[];

  @Expose()
  @Transform(({ obj }) => {
    const value = obj.contracts?.[0]?.actions;

    if (!isNil(value)) {
      return plainToClass(
        SubsidyEligibilityModel,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly subsidyEligibility!: SubsidyEligibilityModel[];

  @Expose()
  readonly payment!: null;
}

@Exclude()
export class NotEasyInfoXMLRequestModel implements VOResponse {
  @Expose()
  @Transform(({ obj }) => plainToClass(ClientModelNotEasy, obj))
  @ValidateNested()
  public readonly client!: ClientModelNotEasy;

  @Expose()
  @Transform(({ obj }) => plainToClass(ContractModelNotEasy, obj))
  @ValidateNested()
  public readonly contract!: ContractModelNotEasy;
}
