import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { DateTime } from 'luxon';
import { isNil, isTruthy, values } from 'remeda';
import { U } from 'ts-toolbelt';

import { parsePhoneNumber } from 'Utils';

import {
  Address,
  BankData,
  Client,
  Contract,
  Service,
  VOResponse
} from '../interfaces';

@Exclude()
class ContractServiceModelJson implements Service {
  @Expose({ name: 'description' })
  public readonly name!: U.Nullable<string>;

  @Expose()
  public readonly companyCode!: U.Nullable<string>;

  @Expose()
  public readonly code!: U.Nullable<string>;

  @Expose()
  public readonly linkMod!: U.Nullable<string>;

  @Expose()
  public readonly link!: U.Nullable<string>;

  @Expose()
  public readonly unremovableMessage!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => isNil(obj?.['removable']))
  public readonly removable!: U.Nullable<boolean>;
}
@Exclude()
class AddressInvoice implements Address {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.firstName ||
      obj.vfssdublin?.billingAddress.address.firstName
  )
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.lastName1 ||
      obj.vfssdublin?.billingAddress.address.lastName1
  )
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.streetName ||
      obj.vfssdublin?.billingAddress.address.streetName
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.houseNumber ||
      obj.vfssdublin?.billingAddress.address.houseNumber
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.countryCode ||
      obj.vfssdublin?.billingAddress.address.countryCode
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.postCode ||
      obj.vfssdublin?.billingAddress.address.postCode
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.city ||
      obj.vfssdublin?.billingAddress.address.city
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.billingAddress?.address?.email ||
      obj.vfssdublin?.billingAddress.address.email
  )
  readonly emailAddress: U.Nullable<string>;
}

@Exclude()
class AddressMail implements Address {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.firstName ||
      obj.vfssdublin?.subscriberAddress.address.firstName
  )
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.lastName1 ||
      obj.vfssdublin?.subscriberAddress.address.lastName1
  )
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.streetName ||
      obj.vfssdublin?.subscriberAddress.address.streetName
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.houseNumber ||
      obj.vfssdublin?.subscriberAddress.address.houseNumber
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.countryCode ||
      obj.vfssdublin?.subscriberAddress.address.countryCode
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.postCode ||
      obj.vfssdublin?.subscriberAddress.address.postCode
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.city ||
      obj.vfssdublin?.subscriberAddress.address.city
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.subscriberAddress?.address?.email ||
      obj.vfssdublin?.subscriberAddress.address.email
  )
  readonly emailAddress: U.Nullable<string>;
}

@Exclude()
class BankDataModel implements BankData {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.bankAccount?.bankCode || obj.vfssdublin?.bankAccount.bankCode
  )
  readonly bankCode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.bankAccount?.bankName ||
      obj.vfssdublin?.bankAccount?.bankName
  )
  readonly bankName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.bankAccount?.accountNumber ||
      obj.vfssdublin?.bankAccount?.accountNumber
  )
  readonly iban: U.Nullable<string>;

  @Expose()
  readonly bic!: null;

  @Expose()
  readonly paymentMethod!: null;

  @Expose()
  readonly ownerName!: null;

  @Expose()
  readonly accountNumber!: null;
}
@Exclude()
class CustomerAddress implements Address {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.firstName ||
      obj.vfssdublin?.customerAddress?.address?.firstName
  )
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.lastName1 ||
      obj.vfssdublin?.customerAddress?.address?.lastName1
  )
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.streetName ||
      obj.vfssdublin?.customerAddress?.address?.streetName
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.houseNumber ||
      obj.vfssdublin?.customerAddress?.address?.houseNumber
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.countryCode ||
      obj.vfssdublin?.customerAddress?.address?.countryCode
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.postCode ||
      obj.vfssdublin?.customerAddress?.address?.postCode
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.city ||
      obj.vfssdublin?.customerAddress?.address?.city
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.customerAddress?.address?.email ||
      obj.vfssdublin?.customerAddress?.address?.email
  )
  readonly emailAddress: U.Nullable<string>;
}

@Exclude()
class ContractModel implements Contract {
  @Expose()
  readonly updateDate: U.Nullable<Date>;

  @Expose()
  @Transform(({ obj }) => {
    const phoneNumber = obj?.myData?.msisdn || obj.vfssdublin?.msisdn;

    if (!phoneNumber) {
      throw new Error('No msisdn provided');
    }

    return parsePhoneNumber(phoneNumber).formatInternational();
  })
  readonly phoneNumber!: string;

  @Expose()
  readonly password: U.Nullable<string> = null;

  @Expose()
  readonly contractStartDate: U.Nullable<Date> = null;

  @Expose()
  @Transform(
    ({ obj }) => obj?.myData?.tariffOption || obj.vfssdublin?.tariffOption
  )
  readonly tariffName: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj.vfplan?.vfplan)
  readonly tariffOption: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.myData?.nextSubsidyDate;

    if (value) {
      return DateTime.fromFormat(value, 'dd. LLLL yyyy', {
        locale: 'de',
        zone: 'utc'
      }).toJSDate();
    }

    return null;
  })
  readonly nextRenewalDate: U.Nullable<Date>;

  @Expose()
  public readonly availableTariffs!: null;

  @Expose()
  @Transform(({ obj }) => plainToClass(BankDataModel, obj))
  public readonly bankData!: BankDataModel;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressInvoice, obj))
  public readonly addressInvoice!: AddressInvoice;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressMail, obj))
  public readonly addressMail!: AddressMail;

  @Expose()
  @Transform(({ obj }) => plainToClass(CustomerAddress, obj))
  public readonly customerAddress!: CustomerAddress;

  @Expose()
  @Transform(() => [])
  public readonly subsidyEligibility!: [];

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.myData?.bookedSocs;
    if (!isNil(value)) {
      return plainToClass(
        ContractServiceModelJson,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly services!: ContractServiceModelJson[];
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any -- need to type empty object
function parseClient(obj: any) {
  const nameDataPrimary = {
    firstName:
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.myData?.givenName || obj.vfssdublin?.givenName,
    surname:
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.myData?.surName || obj.vfssdublin?.surName
  };

  const nameDataSecondary = {
    firstName:
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.myData?.customerAddress?.address?.firstName ||
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.vfssdublin?.customerAddress?.address?.firstName,
    surname:
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.myData?.customerAddress?.address?.lastName1 ||
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      obj?.vfssdublin?.customerAddress?.address?.lastName1
  };

  const countNotNullValues = (obj: { [key: string]: unknown }) =>
    values(obj).filter(isTruthy).length;

  // TODO: refactor?
  // use secondary source if only it has more data than primary
  return countNotNullValues(nameDataPrimary) >=
    countNotNullValues(nameDataSecondary)
    ? nameDataPrimary
    : nameDataSecondary;
}

@Exclude()
class ClientModel implements Client {
  @Expose()
  @Transform(({ obj }) => {
    const { firstName } = parseClient(obj);

    return firstName;
  })
  @IsNotEmpty()
  @IsString()
  readonly firstName!: string;

  @Expose()
  @Transform(({ obj }) => {
    const { surname } = parseClient(obj);

    return surname;
  })
  @IsNotEmpty()
  @IsString()
  readonly surname!: string;

  @Expose()
  @Transform(({ obj }) => obj?.myData?.ban || obj.vfssdublin?.ban)
  readonly banId: U.Nullable<string>;

  @Expose()
  readonly clientStatus: U.Nullable<string>;

  @Expose()
  readonly clientStars: U.Nullable<string>;

  @Expose()
  readonly adv: U.Nullable<string>;

  @Expose()
  readonly accountType: U.Nullable<string>;

  @Expose()
  readonly mkek: U.Nullable<string>;

  @Expose()
  readonly birthday: U.Nullable<Date>;

  @Expose()
  readonly appeal: U.Nullable<string>;

  @Expose()
  readonly emailType: U.Nullable<string>;

  @Expose()
  public readonly rank!: null;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.myData?.bankAccount?.accountNumber ||
      obj.vfssdublin?.bankAccount?.accountNumber
  )
  readonly accountNumber: U.Nullable<string>;

  @Expose({ name: 'VOId' })
  public readonly voId!: U.Nullable<string>;
}

export class JSONRequestModel implements VOResponse {
  @Expose()
  @Transform(({ obj }) => plainToClass(ContractModel, obj))
  @ValidateNested()
  public readonly contract!: ContractModel;

  @Expose()
  @Transform(({ obj }) => plainToClass(ClientModel, obj))
  @ValidateNested()
  public readonly client!: ClientModel;
}
