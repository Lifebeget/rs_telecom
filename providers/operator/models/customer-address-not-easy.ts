import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';

import { Address } from '../interfaces';

@Exclude()
export class CustomerAddressNotEasy implements Address {
  @Expose()
  readonly firstName: U.Nullable<string>;

  @Expose()
  readonly surname: U.Nullable<string>;

  @Expose({ name: 'street' })
  @Transform(({ obj }) => obj?.address?.street || null)
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.address?.nr || null)
  readonly house: U.Nullable<string>;

  @Expose()
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.address?.zip || null)
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.address?.city || null)
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.email || obj?.email2 || null)
  readonly emailAddress: U.Nullable<string>;
}
