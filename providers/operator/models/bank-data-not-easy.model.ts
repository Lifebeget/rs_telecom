import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';

import { BankData } from '../interfaces';

@Exclude()
export class BankDataNotEasyModel implements BankData {
  @Expose()
  readonly bankCode: U.Nullable<string>;

  @Expose()
  readonly bankName: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj.loginModel?.iban || null)
  readonly iban: U.Nullable<string>;

  @Expose()
  readonly bic!: null;

  @Expose()
  readonly paymentMethod!: null;

  @Expose()
  readonly ownerName!: null;

  @Expose()
  readonly accountNumber!: null;
}
