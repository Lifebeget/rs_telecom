import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class DistributorModel {
  @ApiProperty({
    description: 'VO user Id',
    example: '0061037563194-0001',
    required: true,
    type: String
  })
  @Expose()
  public readonly userId!: string;

  @ApiProperty({
    description: 'VO project Id',
    example: 'Simwert',
    nullable: true,
    required: false,
    type: String
  })
  @Expose({ name: 'project' })
  public readonly projectId!: string | null;

  @ApiProperty({
    description: 'VO region Id',
    example: 'Nord-Ost',
    nullable: true,
    required: false,
    type: String
  })
  @Expose()
  public readonly region!: string | null;
}
