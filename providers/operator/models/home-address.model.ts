import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class HomeAddressDetailModel {
  @Expose()
  public readonly streetName!: string;

  @Expose()
  public readonly houseNumber!: string;

  @Expose()
  public readonly countryCode!: string;

  @Expose()
  public readonly postCode!: string;

  @Expose()
  public readonly city!: string;
}

@Exclude()
export class HomeAddressModel {
  @Expose()
  public readonly fixedNetNumbers!: string[];

  @Expose()
  @Type(() => HomeAddressDetailModel)
  public readonly address!: HomeAddressDetailModel;
}
