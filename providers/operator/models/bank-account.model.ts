import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class BankAccountModel {
  @Expose()
  public readonly accountNumber!: string; // it's iban

  @Expose()
  public readonly bankCode!: string;

  @Expose()
  public readonly bankName!: string;
}
