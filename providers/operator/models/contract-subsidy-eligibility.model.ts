import { Exclude, Expose, Transform } from 'class-transformer';
import { DateTime } from 'luxon';
import { isNil } from 'remeda';

import { CanBeRenewed, ExtensionVariant } from 'Constants';

import { getRenewMode } from '../utils';

@Exclude()
export class ContractSubsidyEligibilityModel {
  @Expose({ name: 'EligibilityType' })
  public readonly type!: string | null;

  @Expose({ name: 'EligibilityName' })
  public readonly name!: string | null;

  @Expose()
  public get extension(): ExtensionVariant | null {
    if (this.name === 'Standard Vertragsverlängerung') {
      return ExtensionVariant.normalExtension;
    }

    if (this.name === 'Vorzeitige Vertragsverlängerung ohne Zuzahlung') {
      return ExtensionVariant.earlyExtension;
    }

    return null;
  }

  @Expose({ name: 'EligibilityIndex' })
  @Transform(({ value }) =>
    !Number.isNaN(Number(value)) ? Number(value) : null
  )
  public readonly index!: number | null;

  @Expose({ name: 'EligibilityStatus' })
  @Transform(({ value }) => getRenewMode(value))
  public readonly status!: CanBeRenewed;

  @Expose({ name: 'EligibilityNextSubsidyDate' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd', {
        zone: 'utc'
      }).toJSDate();
    }

    return null;
  })
  public readonly nextRenewalDate!: Date | null;

  @Expose({ name: 'NEYInlifeSOCEndDate' })
  // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd', {
        zone: 'utc'
      }).toJSDate();
    }

    return null;
  })
  public readonly expirationDate!: Date | null;
}
