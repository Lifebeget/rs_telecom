import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { isNil, uniq } from 'remeda';
import { U } from 'ts-toolbelt';

import { Nba } from '../interfaces';

@Exclude()
export class ContractNextBestActionTipModelEasy {
  @Expose()
  public readonly rowId!: U.Nullable<string>;

  @Expose()
  public readonly companyCode!: U.Nullable<string>;

  @Expose()
  public readonly cellCode!: U.Nullable<string>;

  @Expose()
  public readonly extractionDate!: U.Nullable<Date>;

  @Expose()
  public readonly flowChartId!: U.Nullable<string>;

  @Expose({ name: 'title' })
  public readonly tipText!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj.services;

    if (!isNil(value)) {
      return Array.isArray(value) ? uniq(value) : [value];
    }

    return [];
  })
  public readonly services!: string[];
}

@Exclude()
export class ContractNextBestActionModelEasy implements Nba {
  @Expose()
  public readonly banId!: U.Nullable<string>;

  @Expose()
  public readonly phoneNumber!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj.contracts?.[0]?.nba;
    if (!isNil(value) && value !== 'null') {
      return plainToClass(
        ContractNextBestActionTipModelEasy,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly tips!: ContractNextBestActionTipModelEasy[];
}
