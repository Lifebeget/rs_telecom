import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';
import { isNil } from 'remeda';

@Exclude()
export class ClientAddressModel {
  @Expose({ name: 'Anrede' })
  public readonly appeal!: string | null;

  @Expose({ name: 'Vorname' })
  public readonly firstName!: string | null;

  @Expose({ name: 'Name' })
  public readonly surname!: string | null;

  @Expose({ name: 'Strasse' })
  public readonly street!: string | null;

  @Expose({ name: 'Hausnummer' })
  public readonly house!: string | null;

  @Expose({ name: 'Postleitzahl' })
  public readonly postcode!: string | null;

  @Expose({ name: 'Ort' })
  public readonly location!: string | null;

  @Expose({ name: 'Land' })
  public readonly land!: string | null;

  @Expose({ name: 'EmailAdresse' })
  public readonly emailAddress!: string | null;

  @Expose({ name: 'EmailAdresseType' })
  public readonly emailType!: string | null;
}

@Exclude()
export class ClientRankModel {
  @Expose({ name: 'AktuellerStatus' })
  public readonly current!: string | null;

  @Expose({ name: 'Statusbeginn' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly startDate!: Date | null;

  @Expose({ name: 'Statusende' })
  // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly endDate!: Date | null;

  @Expose({ name: 'Statuswert' })
  public readonly points!: string | null;

  @Expose({ name: 'ErrechneterStatus' })
  public readonly next!: string | null;
}

@Exclude()
export class ClientModel {
  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Ban'])
  public readonly banId!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['BanKennwort'])
  public readonly banPassword!: string | null;

  @Expose()
  @Transform(({ obj }) =>
    plainToClass(ClientAddressModel, obj?.['Kundendaten']?.['Kundenadresse'])
  )
  public readonly address!: ClientAddressModel;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Teilnehmerstatus'])
  public readonly clientStatus!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['VodafoneStarsRegistriert'])
  public readonly stars!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['AdvertisingIndikator'])
  public readonly adv!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['PersonDataEvalIndikator'])
  public readonly personEvaluation!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['CallDataEvalIndikator'])
  public readonly callEvaluation!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kontotyp'])
  public readonly accountType!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['MkEkIndikator'])
  public readonly mkek!: string | null;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.['Kundendaten']?.['Geburtsdatum'];

    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly birthday!: Date | null;

  @Expose({ name: 'SiGoPla' })
  @Transform(({ obj }) => {
    const value = obj?.['SiGoPla'];

    if (!isNil(value)) {
      return plainToClass(ClientRankModel, obj?.['SiGoPla']);
    }

    return null;
  })
  public readonly rank!: ClientRankModel | null;
}
