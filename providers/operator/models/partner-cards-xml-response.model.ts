import { BadRequestException } from '@nestjs/common';
import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';

import { ContractError } from 'Constants/errorValues';
import { parsePhoneNumber } from 'Utils';

@Exclude()
export class PartnerCardsXMLResponseCardModel {
  @Expose({ name: 'ban' })
  public readonly banId!: string;

  @Expose()
  @Transform(({ obj }) => obj['subscriberNumber']['NDC'])
  public readonly ndc!: string;

  @Expose()
  @Transform(({ obj }) => obj['subscriberNumber']['MSISDN'])
  public readonly msisdn!: string;

  @Expose({ name: 'tariffOption' })
  public readonly tariffCode!: string;

  // TODO: solve `contractStartDate: Invalid Date`
  @Expose({ name: 'effectiveDate' })
  @Transform(({ value }) => DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate())
  public readonly contractStartDate!: Date;

  @Expose()
  // TODO: move to utility transform decorator
  @Transform(({ obj }) => {
    const ndc = obj['subscriberNumber']['NDC'];
    const msisdn = obj['subscriberNumber']['MSISDN'];

    if (
      (typeof ndc !== 'string' && typeof ndc !== 'number') ||
      (typeof msisdn !== 'string' && typeof msisdn !== 'number')
    ) {
      throw new Error(ContractError.CONTRACT_NO_PHONE_NUMBER);
    }

    return parsePhoneNumber(`${ndc} ${msisdn}`).formatInternational();
  })
  public readonly phoneNumber!: string;

  @Expose()
  public readonly memberType!: string;

  @Expose()
  @Transform(({ obj }) => obj['memberType'] === 'O')
  public readonly isOwner!: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any -- parsing data
function parseSharingGroup(root: any) {
  const value = root?.['memberList']?.['member'];

  if (!value) {
    return [];
  }

  const values = Array.isArray(value) ? value : [value];

  return values.map(v => plainToClass(PartnerCardsXMLResponseCardModel, v));
}

@Exclude()
export class PartnerCardsXMLResponseModel {
  @Expose()
  @Transform(({ obj }) => {
    const root =
      obj['responses']?.['syncRetrieveGroupInformation']?.[
        'sharingGroupInformationList'
      ]?.['sharingGroupInformation'];

    if (!root) {
      throw new BadRequestException(
        ContractError.CONTRACT_INVALID_PARTNER_CARD_RESPONSE_NO_PARENT_TAG
      );
    }

    const sharingGroups = Array.isArray(root) ? root : [root];

    return sharingGroups.map(group => parseSharingGroup(group)).flat();
  })
  public readonly cards!: PartnerCardsXMLResponseCardModel[];
}
