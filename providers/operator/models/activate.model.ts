import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';

@Exclude()
export class ActivateModel {
  @ApiProperty({
    description: 'Request Id',
    example: '12468',
    required: true,
    type: String
  })
  @Transform(({ value }) => value.toString()) // number -> string
  @Expose({ name: 'phone_nr_request_id' })
  public readonly requestId!: string;
}
