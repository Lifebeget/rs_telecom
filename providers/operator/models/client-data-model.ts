import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { U } from 'ts-toolbelt';
import { isNil } from 'remeda';
import { DateTime } from 'luxon';
import { IsNotEmpty, IsString } from 'class-validator';

import { Client } from '../interfaces';

import { ClientRankModel } from './client-rank-model';

@Exclude()
export class ClientModel implements Client {
  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kundenadresse']?.Vorname)
  @IsNotEmpty()
  @IsString()
  readonly firstName!: string;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kundenadresse']?.Name)
  @IsNotEmpty()
  @IsString()
  readonly surname!: string;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.Ban)
  readonly banId: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Teilnehmerstatus'])
  public readonly clientStatus!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['VodafoneStarsRegistriert'])
  public readonly clientStars!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['AdvertisingIndikator'])
  public readonly adv!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kontotyp'])
  public readonly accountType!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['MkEkIndikator'])
  public readonly mkek!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.['Kundendaten']?.['Geburtsdatum'];

    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly birthday!: U.Nullable<Date>;

  @Expose({ name: 'Anrede' })
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kundenadresse']?.Anrede)
  public readonly appeal!: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) => obj?.['Kundendaten']?.['Kundenadresse']?.EmailAdresseType
  )
  public readonly emailType!: U.Nullable<string>;

  @Expose({ name: 'SiGoPla' })
  @Transform(({ obj }) => {
    const value = obj?.['SiGoPla'];

    if (!isNil(value)) {
      return plainToClass(ClientRankModel, value);
    }

    return null;
  })
  public readonly rank!: ClientRankModel | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['Kontonummer'])
  public readonly accountNumber!: U.Nullable<string>;

  @Expose({ name: 'VOId' })
  public readonly voId!: U.Nullable<string>;
}
@Exclude()
export class ClientModelNotEasy implements Client {
  @Expose({ name: 'firstname' })
  @IsNotEmpty()
  @IsString()
  readonly firstName!: string;

  @Expose({ name: 'surname' })
  @IsNotEmpty()
  @IsString()
  readonly surname!: string;

  @Expose()
  @Expose({ name: 'ban' })
  readonly banId: U.Nullable<string>;

  @Expose()
  public readonly clientStatus!: U.Nullable<string>;

  @Expose()
  public readonly clientStars!: U.Nullable<string>;

  @Expose()
  public readonly adv!: U.Nullable<string>;

  @Expose({ name: 'customerType' })
  public readonly accountType!: U.Nullable<string>;

  @Expose()
  public readonly mkek!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj.loginModel?.connection?.username)
  @IsNotEmpty()
  public readonly voUserId!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) =>
    DateTime.fromISO(obj.birthday, { zone: 'utc' }).toJSDate()
  )
  public readonly birthday!: U.Nullable<Date>;

  @Expose({ name: 'title' })
  @Transform(({ obj }) => {
    const salutationFormatted = Number(obj.salutation);

    if (salutationFormatted == 1) {
      return 'Herr';
    } else if (salutationFormatted == 2) {
      return 'Frau';
    } else if (salutationFormatted === 3) {
      return 'Firma';
    }

    return null;
  })
  public readonly appeal!: U.Nullable<string>;

  @Expose({ name: 'EmailAdresseType' })
  public readonly emailType!: U.Nullable<string>;

  @Expose({ name: 'SiGoPla' })
  @Transform(({ obj }) => {
    const value = obj?.['SiGoPla'];

    if (!isNil(value)) {
      return plainToClass(ClientRankModel, obj?.['SiGoPla']);
    }

    return null;
  })
  public readonly rank!: ClientRankModel | null;

  @Expose()
  public readonly accountNumber!: U.Nullable<string>;

  @Expose({ name: 'VOId' })
  public readonly voId!: U.Nullable<string>;
}
