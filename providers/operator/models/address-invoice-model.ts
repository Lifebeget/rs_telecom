import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';

@Exclude()
export class AddressInvoice {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Vorname ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Vorname
  )
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Name ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Name
  )
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Strasse ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Strasse
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Hausnummer ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Hausnummer
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Land ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Land
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Postleitzahl ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Postleitzahl
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['RechnungsAdresse']?.['Anrede']?.Ort ||
      obj?.['Kundendaten']?.['Kundenadresse']?.Ort
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['Kundenadresse']?.EmailAdresse)
  readonly emailAddress: U.Nullable<string>;
}
