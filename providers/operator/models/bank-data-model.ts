import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';

import { BankData } from '../interfaces';

@Exclude()
export class BankDataModel implements BankData {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['Bankleitzahl']
  )
  readonly bankCode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['Kreditinstitut']
  )
  readonly bankName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) => obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['IBAN']
  )
  readonly iban: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) => obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['BIC']
  )
  public readonly bic!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['BankverbindungsDaten']?.['Zahlungsmethode'])
  public readonly paymentMethod!: string | null;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['Kontoinhaber']
  )
  public readonly ownerName!: string | null;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.['BankverbindungsDaten']?.['Bankverbindung']?.['Kontonummer']
  )
  public readonly accountNumber!: string | null;
}
