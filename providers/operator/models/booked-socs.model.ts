import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class BookedSocsModel {
  @Expose()
  public readonly code!: string;

  @Expose()
  public readonly description!: string;

  @Expose()
  public readonly linkMod!: string;

  @Expose()
  public readonly link!: string;

  @Expose()
  public readonly removable!: string;
}
