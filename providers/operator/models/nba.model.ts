import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class NBALinkDetailModel {
  @Expose()
  public readonly href!: string;
}

@Exclude()
export class NBALinkModel {
  @Expose()
  @Type(() => NBALinkDetailModel)
  public readonly self!: NBALinkDetailModel;
}

@Exclude()
export class NBAModel {
  @Expose()
  public readonly responseCode!: string;

  @Expose()
  public readonly description!: string;

  @Expose()
  public readonly cf10!: string;

  @Expose()
  @Type(() => NBALinkModel)
  public readonly _links!: NBALinkModel;
}
