import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ContractAddressInvoiceModel {
  @Expose({ name: 'Anrede' })
  public readonly appeal!: string | null;

  @Expose({ name: 'Vorname' })
  public readonly firstName!: string | null;

  @Expose({ name: 'Name' })
  public readonly surname!: string | null;

  @Expose({ name: 'Strasse' })
  public readonly street!: string | null;

  @Expose({ name: 'Hausnummer' })
  public readonly house!: string | null;

  @Expose({ name: 'Postleitzahl' })
  public readonly postcode!: string | null;

  @Expose({ name: 'Ort' })
  public readonly location!: string | null;

  @Expose({ name: 'Land' })
  public readonly land!: string | null;
}
