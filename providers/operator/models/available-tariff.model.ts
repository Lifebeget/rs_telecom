import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

import { AvailableTariffs } from '../interfaces';

@Exclude()
export class AvailableTariffModel implements AvailableTariffs {
  @Expose({ name: 'Tarif' })
  @IsNotEmpty()
  @IsString()
  public readonly code!: string;
}
