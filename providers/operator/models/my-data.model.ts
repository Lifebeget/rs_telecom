import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { DateTime } from 'luxon';

import { parsePhoneNumber } from 'Utils';

import { BookedSocsModel } from './booked-socs.model';
import { HomeAddressModel } from './home-address.model';
import { BankAccountModel } from './bank-account.model';
import { AddressModel } from './address.model';

@Exclude()
export class MyDataModel {
  @Expose()
  public readonly givenName!: string;

  @Expose()
  public readonly surName!: string;

  @Expose()
  public readonly ban!: string;

  @Expose()
  @Transform(({ value }) => {
    try {
      return parsePhoneNumber(value, 'DE').formatInternational();
    } catch (err) {
      return value;
    }
  })
  public readonly msisdn!: string;

  @Expose()
  public readonly marketCode!: string;

  @Expose()
  @Transform(({ value }) => {
    if (value) {
      const date = DateTime.fromFormat(value, 'dd. LLLL yyyy', {
        locale: 'de',
        zone: 'utc'
      }).toJSDate();
      const isValid = date.getTime();

      if (!isValid) {
        /* eslint-disable no-console -- refactor required */
        console.error('Error while parsing date');
        console.error(value, '->', date);
        /* eslint-enable no-console -- refactor required */
      }

      return date;
    }

    return null;
  })
  public readonly nextSubsidyDate!: Date | null;

  @Expose()
  public readonly tariffOption!: string;

  @Expose()
  public readonly hasOnlineBill!: string;

  @Expose()
  @Type(() => BookedSocsModel)
  public readonly bookedSocs!: BookedSocsModel[];

  @Expose()
  @Type(() => HomeAddressModel)
  public readonly homeAddress!: HomeAddressModel;

  @Expose()
  @Type(() => BankAccountModel)
  public readonly bankAccount!: BankAccountModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly customerAddress!: AddressModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly billingAddress!: AddressModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly subscriberAddress!: AddressModel;
}
