import { Exclude, Expose, Transform } from 'class-transformer';

@Exclude()
export class ContractPaymentModel {
  @Expose({ name: 'Zahlungsmethode' })
  public readonly paymentMethod!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['IBAN'])
  public readonly iban!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['BIC'])
  public readonly bic!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['Kontonummer'])
  public readonly accountNumber!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['Bankleitzahl'])
  public readonly bankCode!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['Kreditinstitut'])
  public readonly bankName!: string | null;

  @Expose()
  @Transform(({ obj }) => obj?.['Bankverbindung']?.['Kontoinhaber'])
  public readonly ownerName!: string | null;
}
