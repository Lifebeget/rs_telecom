import { Exclude, Expose } from 'class-transformer';
import { U } from 'ts-toolbelt';

@Exclude()
export class AddressNotEasy {
  @Expose()
  readonly firstName: U.Nullable<string>;

  @Expose()
  readonly surname: U.Nullable<string>;

  @Expose()
  readonly street: U.Nullable<string>;

  @Expose()
  readonly house: U.Nullable<string>;

  @Expose()
  readonly land: U.Nullable<string>;

  @Expose()
  readonly postcode: U.Nullable<string>;

  @Expose()
  readonly location: U.Nullable<string>;

  @Expose()
  readonly emailAddress: U.Nullable<string>;
}
