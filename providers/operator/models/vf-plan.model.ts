import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class VfPlanModel {
  @Expose()
  public readonly msisdn!: string;

  @Expose()
  public readonly vfplan!: string;

  @Expose()
  public readonly vfcid!: string;
}
