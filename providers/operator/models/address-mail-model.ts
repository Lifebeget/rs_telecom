import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';

const customerData = 'Kundendaten';
const customerAddress = 'Kundenadresse';
const billingAddress = 'RechnungsAdresse';
const salutation = 'Anrede';
@Exclude()
export class AddressMail {
  @Expose()
  @Transform(({ obj }) => obj?.[customerData]?.[customerAddress]?.Vorname)
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.[customerData]?.[customerAddress]?.Name)
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Strasse ||
      obj?.[customerData]?.[customerAddress]?.Strasse
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Hausnummer ||
      obj?.[customerData]?.[customerAddress]?.Hausnummer
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Land ||
      obj?.[customerData]?.[customerAddress]?.Land
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Postleitzahl ||
      obj?.[customerData]?.[customerAddress]?.Postleitzahl
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Ort ||
      obj?.[customerData]?.[customerAddress]?.Ort
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.[customerData]?.[customerAddress]?.EmailAdresse)
  readonly emailAddress: U.Nullable<string>;
}
