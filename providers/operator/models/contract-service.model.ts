import { Exclude, Expose, Transform } from 'class-transformer';
import { isNil } from 'remeda';
import { U } from 'ts-toolbelt';

@Exclude()
export class ContractServiceModel {
  @Expose({ name: 'Dienst' })
  public readonly name!: string | null;

  @Expose({ name: 'Code' })
  public readonly code!: string | null;

  @Expose()
  @Transform(({ obj }) => isNil(obj?.['NichtLoeschbar']))
  public readonly removable!: boolean;

  @Expose({ name: 'NichtLoeschbar' })
  @Transform(({ value }) => value ?? null)
  public readonly unremovableMessage!: string | null;

  @Expose()
  public readonly linkMod!: U.Nullable<string>;

  @Expose()
  public readonly link!: U.Nullable<string>;
}
