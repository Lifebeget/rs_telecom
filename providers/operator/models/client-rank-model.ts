import { Exclude, Expose, Transform } from 'class-transformer';
import { U } from 'ts-toolbelt';
import { isNil } from 'remeda';
import { DateTime } from 'luxon';

@Exclude()
export class ClientRankModel {
  @Expose({ name: 'AktuellerStatus' })
  public readonly current!: U.Nullable<string>;

  @Expose({ name: 'Statusbeginn' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toUTC().toJSDate();
    }

    return null;
  })
  public readonly startDate!: U.Nullable<Date>;

  @Expose({ name: 'Statusende' })
  // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toUTC().toJSDate();
    }

    return null;
  })
  public readonly endDate!: U.Nullable<Date>;

  @Expose({ name: 'Statuswert' })
  public readonly points!: U.Nullable<string>;

  @Expose({ name: 'ErrechneterStatus' })
  public readonly next!: U.Nullable<string>;
}
