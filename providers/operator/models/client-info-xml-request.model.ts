import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { U } from 'ts-toolbelt';
import { isNil } from 'remeda';
import { DateTime } from 'luxon';
import { BadRequestException } from '@nestjs/common';
import { IsOptional, IsString, ValidateNested } from 'class-validator';

import { parsePhoneNumber } from '../../../../Utils';
import { Address, Contract, VOResponse } from '../interfaces';
import { ContractError } from '../../../../Constants/errorValues';

import { AvailableTariffModel } from './available-tariff.model';
import { ContractServiceModel } from './contract-service.model';
import { BankDataModel } from './bank-data-model';
import { AddressInvoice } from './address-invoice-model';
import { AddressMail } from './address-mail-model';
import { ClientModel } from './client-data-model';
import { ContractNextBestActionModelEpos } from './contract-next-best-action-epos.model';
import { ContractSubsidyEligibilityModel } from './contract-subsidy-eligibility.model';
import { ContractPaymentModel } from './contract-payment.model';

const customerData = 'Kundendaten';
const customerAddress = 'Kundenadresse';
const billingAddress = 'RechnungsAdresse';
const salutation = 'Anrede';

@Exclude()
export class CustomerAddress implements Address {
  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Vorname ||
      obj?.[customerData]?.[customerAddress]?.Vorname
  )
  readonly firstName: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Name ||
      obj?.[customerData]?.[customerAddress]?.Name
  )
  readonly surname: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Strasse ||
      obj?.[customerData]?.[customerAddress]?.Strasse
  )
  readonly street: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Hausnummer ||
      obj?.[customerData]?.[customerAddress]?.Hausnummer
  )
  readonly house: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Land ||
      obj?.[customerData]?.[customerAddress]?.Land
  )
  readonly land: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Postleitzahl ||
      obj?.[customerData]?.[customerAddress]?.Postleitzahl
  )
  readonly postcode: U.Nullable<string>;

  @Expose()
  @Transform(
    ({ obj }) =>
      obj?.[billingAddress]?.[salutation]?.Ort ||
      obj?.[customerData]?.[customerAddress]?.Ort
  )
  readonly location: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.[customerData]?.[customerAddress]?.EmailAdresse)
  readonly emailAddress: U.Nullable<string>;
}

export class ContractModel implements Contract {
  //@TODO Трансформ делается на первом уровне, изменить в зависимости от вложений
  @Expose({ name: 'Ausfuehrungsdatum' })
  @IsOptional()
  @IsString()
  readonly updateDate: U.Nullable<Date>;

  @Expose()
  @Transform(({ obj }) => {
    const ndc = obj?.['VFRufnummer']?.['NDC'];
    const msisdn = obj?.['VFRufnummer']?.['MSISDN'];

    if (
      (typeof ndc !== 'string' && typeof ndc !== 'number') ||
      (typeof msisdn !== 'string' && typeof msisdn !== 'number')
    ) {
      throw new BadRequestException(ContractError.CONTRACT_NO_PHONE_NUMBER);
    }

    return parsePhoneNumber(`${ndc} ${msisdn}`).formatInternational();
  })
  readonly phoneNumber!: string;

  @Expose()
  @Transform(({ obj }) => obj?.['Kundendaten']?.['BanKennwort'])
  readonly password: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Vertragsdaten']?.['AktuellerTarif'])
  readonly tariffName: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => obj?.['Vertragsdaten']?.['Tarifoption'])
  readonly tariffOption: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.['Vertragsdaten']?.['NaechsterErwerb'];

    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly nextRenewalDate!: U.Nullable<Date>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj['MoeglicheTarife']?.['MoeglicherTarif'];

    if (!isNil(value)) {
      return plainToClass(
        AvailableTariffModel,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly availableTariffs!: AvailableTariffModel[];

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.['Vertragsdaten']?.['LetzterErwerb'];

    return DateTime.fromISO(value, {
      zone: 'utc'
    }).toJSDate();
  })
  public readonly contractStartDate!: Date;

  @Expose()
  @Transform(({ obj }) => {
    const value =
      obj?.['Dienste']?.['Dienste']?.['ePOSDienste']?.['ePOSDienst'];
    if (!isNil(value)) {
      return plainToClass(ContractServiceModel, value);
    }
  })
  public readonly services!: ContractServiceModel[];

  @Expose()
  @Transform(({ obj }) => plainToClass(BankDataModel, obj))
  public readonly bankData!: BankDataModel;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressInvoice, obj))
  public readonly addressInvoice!: AddressInvoice;

  @Expose({ name: 'BankverbindungsDaten' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return plainToClass(ContractPaymentModel, value);
    }

    return null;
  })
  public readonly payment!: ContractPaymentModel | null;

  @Expose()
  @Transform(({ obj }) => plainToClass(AddressMail, obj))
  public readonly addressMail!: AddressMail;

  @Expose()
  @Transform(({ obj }) => plainToClass(CustomerAddress, obj))
  public readonly customerAddress!: CustomerAddress;

  @Expose({ name: 'NBAInformationen' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return plainToClass(ContractNextBestActionModelEpos, value);
    }

    return null;
  })
  public readonly nba!: ContractNextBestActionModelEpos | null;

  @Expose()
  @Transform(({ obj }) => {
    const value =
      obj?.['SubsidyEligibilityStatuses']?.['SubsidyEligibilityStatus'];

    if (!isNil(value)) {
      return plainToClass(
        ContractSubsidyEligibilityModel,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly subsidyEligibility!: ContractSubsidyEligibilityModel[];
}
export class ClientInfoXMLRequestModel implements VOResponse {
  @Expose()
  @Transform(({ obj }) => plainToClass(ContractModel, obj))
  @ValidateNested()
  public readonly contract!: ContractModel;

  @Expose()
  @Transform(({ obj }) => plainToClass(ClientModel, obj))
  @ValidateNested()
  public readonly client!: ClientModel;
}
