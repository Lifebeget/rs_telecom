import { Exclude, Expose, Transform, plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';
import { isNil, uniq } from 'remeda';
import { U } from 'ts-toolbelt';

import { Nba } from '../interfaces';

@Exclude()
export class ContractNextBestActionTipModelEpos {
  @Expose({ name: 'RowId' })
  public readonly rowId!: U.Nullable<string>;

  @Expose({ name: 'RowId' })
  public readonly companyCode!: U.Nullable<string>;

  @Expose({ name: 'ZellCode' })
  public readonly cellCode!: U.Nullable<string>;

  @Expose({ name: 'ExtraktDatum' })
  @Transform(({ value }) => {
    if (!isNil(value)) {
      return DateTime.fromFormat(value, 'yyyy-MM-dd').toJSDate();
    }

    return null;
  })
  public readonly extractionDate!: U.Nullable<Date>;

  @Expose({ name: 'FlowChartID' })
  public readonly flowChartId!: U.Nullable<string>;

  @Expose({ name: 'TipText' })
  public readonly tipText!: U.Nullable<string>;

  @Expose()
  @Transform(({ obj }) => {
    const value = obj?.['Dienste']?.['Dienst'];

    if (!isNil(value)) {
      return Array.isArray(value) ? uniq(value) : [value];
    }

    return [];
  })
  public readonly services!: U.NonNullable<string[]>;
}

@Exclude()
export class ContractNextBestActionModelEpos implements Nba {
  @Expose({ name: 'BAN' })
  public readonly banId!: U.Nullable<string>;

  @Expose({ name: 'TeilnehmerNummer' })
  public readonly phoneNumber!: U.Nullable<string>;

  @Expose({ name: 'NBATip' })
  @Transform(({ value }) => {
    if (!isNil(value) && value !== 'null') {
      return plainToClass(
        ContractNextBestActionTipModelEpos,
        Array.isArray(value) ? value : [value]
      );
    }

    return [];
  })
  public readonly tips!: ContractNextBestActionTipModelEpos[];
}
