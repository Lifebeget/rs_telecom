import { Exclude, Expose, Type } from 'class-transformer';

import { BookedSocsModel } from './booked-socs.model';
import { HomeAddressModel } from './home-address.model';
import { BankAccountModel } from './bank-account.model';
import { AddressModel } from './address.model';

@Exclude()
export class VFSDublinPackageModel {
  @Expose()
  public readonly description!: string;

  @Expose()
  public readonly total!: string;

  @Expose()
  public readonly unit!: string;

  @Expose()
  public readonly used!: string;
}

@Exclude()
export class VFSDublinPackageGroupModel {
  @Expose()
  public readonly description!: string;

  @Expose()
  @Type(() => VFSDublinPackageModel)
  public readonly package!: VFSDublinPackageModel[];
}

@Exclude()
export class VFSDublinChargeModel {
  @Expose()
  public readonly category!: string;

  @Expose()
  public readonly charge!: string;

  @Expose()
  public readonly currency!: string;
}

@Exclude()
export class VFSDublinChargeGroupModel {
  @Expose()
  public readonly description!: string;

  @Expose()
  @Type(() => VFSDublinChargeModel)
  public readonly charge!: VFSDublinChargeModel[];
}

@Exclude()
export class VFSDublinQuickcheckModel {
  @Expose()
  public readonly sum!: string;

  @Expose()
  public readonly currency!: string;

  @Expose()
  public readonly billDate!: string;

  @Expose()
  @Type(() => VFSDublinChargeGroupModel)
  public readonly chargeGroup!: VFSDublinChargeGroupModel[];

  @Expose()
  @Type(() => VFSDublinPackageGroupModel)
  public readonly packageGroup!: VFSDublinPackageGroupModel[];
}

@Exclude()
export class VFSDublinModel {
  @Expose()
  public readonly givenName!: string;

  @Expose()
  public readonly surName!: string;

  @Expose()
  public readonly ban!: string;

  @Expose()
  public readonly msisdn!: string;

  @Expose()
  public readonly marketCode!: string;

  @Expose()
  public readonly tariffOption!: string;

  @Expose()
  public readonly hasOnlineBill!: string;

  @Expose()
  public readonly bookableSocs!: unknown;

  @Expose()
  @Type(() => BookedSocsModel)
  public readonly bookedSocs!: BookedSocsModel[];

  @Expose()
  @Type(() => HomeAddressModel)
  public readonly homeAddress!: HomeAddressModel;

  @Expose()
  @Type(() => BankAccountModel)
  public readonly bankAccount!: BankAccountModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly customerAddress!: AddressModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly billingAddress!: AddressModel;

  @Expose()
  @Type(() => AddressModel)
  public readonly subscriberAddress!: AddressModel;

  @Expose()
  @Type(() => VFSDublinQuickcheckModel)
  public readonly quickcheck!: VFSDublinQuickcheckModel;
}
