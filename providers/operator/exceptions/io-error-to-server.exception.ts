import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IOErrorToServerException
 */
export class IOErrorToServerException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'I/O Error to Server';
  }

  static instanceOf(message: string): boolean {
    return message.includes('I/O Error to Server');
  }
}
