import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#VONotAssignedException
 */
export class VONotAssignedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Not assigned VO';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Not assigned VO');
  }
}
