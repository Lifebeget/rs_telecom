import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IncorrectPasswordLimitExceededException
 */
export class IncorrectPasswordLimitExceededException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Incorrect password limit exceeded';
  }

  static instanceOf(message: string): boolean {
    return message.includes(
      'Sie haben zu oft ein falsches Kundenkennwort angegeben'
    );
  }
}
