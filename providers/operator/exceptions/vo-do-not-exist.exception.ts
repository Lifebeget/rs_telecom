import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#VODoNotExistException
 */
export class VODoNotExistException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Vo do not exist';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Vo do not exist');
  }
}
