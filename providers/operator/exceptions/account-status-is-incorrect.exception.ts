import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#AccountStatusIsIncorrectException
 */
export class AccountStatusIsIncorrectException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = `Client's account status is incorrect`;
  }

  static instanceOf(message: string): boolean {
    return [
      'Kontostatus des Kunden ist nicht in Ordnung',
      'Leider handelt es sich bei diesem Kunden um einen Rahmenvertragskunden'
    ].some(mess => message.includes(mess));
  }
}
