import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#XmlRequestSendException
 */
export class XmlRequestSendException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Request XML send error';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Request XML send error');
  }
}
