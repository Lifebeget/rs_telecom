import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#VONotAssignedToAgentException
 */
export class VONotAssignedToAgentException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Vo not assigned to agent';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Vo not assigned to agent');
  }
}
