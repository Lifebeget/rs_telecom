import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#CodeIsNoLongerValidException
 */
export class CodeIsNoLongerValidException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Code is no longer valid';
  }

  static instanceOf(message: string): boolean {
    return message.includes(
      'Der eingegebene Sicherheitscode ist nicht mehr gültig'
    );
  }
}
