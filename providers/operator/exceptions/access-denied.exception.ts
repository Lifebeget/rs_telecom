import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#AccessDeniedException
 */
export class AccessDeniedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Access denied';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Access denied');
  }
}
