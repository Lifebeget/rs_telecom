import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#CodeIsIncorrectException
 */
export class CodeIsIncorrectException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Code is no longer valid';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Der eingebene Sicherheitscode ist nicht korrekt');
  }
}
