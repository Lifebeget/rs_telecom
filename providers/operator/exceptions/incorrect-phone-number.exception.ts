import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IncorrectPhoneNumberException
 */
export class IncorrectPhoneNumberException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Incorrect phone number';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Ihre Rufnummer ist bei Vodafone unbekannt');
  }
}
