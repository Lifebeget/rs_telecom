import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#AbortedByTimeoutException
 */
export class AbortedByTimeoutException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Request aborted by timeout';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Request aborted by timeout');
  }
}
