import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#CouldNotBeAuthenticateException
 */
export class CouldNotBeAuthenticateException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Could not be authenticate';
  }

  static instanceOf(message: string): boolean {
    return message.includes('konnte nicht authentifiziert werden');
  }
}
