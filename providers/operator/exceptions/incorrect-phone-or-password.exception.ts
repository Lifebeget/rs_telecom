import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IncorrectPhoneOrPasswordException
 */
export class IncorrectPhoneOrPasswordException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Incorrect phone or password';
  }

  static instanceOf(message: string): boolean {
    return message.includes(
      'Das Kennwort oder die Mobilfunknummer ist nicht korrekt'
    );
  }
}
