import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IncorrectPasswordException
 */
export class IncorrectPasswordException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Incorrect password';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Das angegebene Kundenkennwort ist nicht korrekt');
  }
}
