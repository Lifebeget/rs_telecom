export abstract class VoException extends Error {
  public readonly response: string | Record<string, unknown>;
  public readonly status: number;

  protected constructor(
    response: string | Record<string, unknown>,
    status: number
  ) {
    super();
    this.name = 'VOException';
    this.response = response;
    this.status = status;
  }
}
