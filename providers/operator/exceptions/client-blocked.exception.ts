import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#ClientBlockedException
 */
export class ClientBlockedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Client blocked';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Limitanfrage mit Kundenkennwort überschritten');
  }
}
