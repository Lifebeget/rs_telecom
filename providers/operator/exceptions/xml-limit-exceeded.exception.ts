import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#XmlLimitExceededException
 */
export class XmlLimitExceededException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'XML Limit exceeded';
  }

  static instanceOf(message: string): boolean {
    return message.includes('XML Limit exceeded');
  }
}
