import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#MtanLimitExceededException
 */
export class MtanLimitExceededException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Mtan Limit exceeded';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Mtan Limit exceeded');
  }
}
