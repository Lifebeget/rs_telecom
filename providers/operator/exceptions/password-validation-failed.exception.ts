import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#PasswordValidationFailedException
 */
export class PasswordValidationFailedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Password validation failed';
  }

  static instanceOf(message: string): boolean {
    return message.includes('password is incorrect');
  }
}
