import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#IbanDecodingFailedException
 */
export class IbanDecodingFailedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Iban decoding failed';
  }

  static instanceOf(message: string): boolean {
    return message.includes('An error occurred during encrypting');
  }
}
