import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220
 */
export class VONotSupportedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'This VO is not supported';
  }

  static instanceOf(message: string): boolean {
    return message.includes('This VO is not supported');
  }
}
