import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#AccessBlockedException
 */
export class AccessBlockedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Access blocked';
  }

  static instanceOf(message: string): boolean {
    return [
      'Sie haben zu oft ein falsches Kundenkennwort angegeben',
      'Dieser Zugang ist nach drei Fehleingaben des Kennwortes gesperrt'
    ].some(code => message.includes(code));
  }
}
