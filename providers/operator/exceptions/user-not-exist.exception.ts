import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#UserNotExistException
 */
export class UserNotExistException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'User not exist';
  }

  static instanceOf(message: string): boolean {
    return ['User not exist', 'User do not exist'].some(code =>
      message.includes(code)
    );
  }
}
