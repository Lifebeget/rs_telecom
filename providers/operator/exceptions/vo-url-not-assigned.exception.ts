import { VoException } from './vo.exception';

/**
 * @see https://octoerp.atlassian.net/wiki/spaces/TC/pages/300417220/VO+and+Middleware+Exceptions#VOUrlNotAssignedException
 */
export class VOUrlNotAssignedException extends VoException {
  constructor(response: string | Record<string, unknown>, status: number) {
    super(response, status);
    this.message = 'Vo URL not assigned';
  }

  static instanceOf(message: string): boolean {
    return message.includes('Vo URL not assigned');
  }
}
