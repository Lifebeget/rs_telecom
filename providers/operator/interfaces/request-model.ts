export interface RequestModel<T> {
  getResponse(): T;
  getRaw(): string;
}
