export interface SynchronizeJsonRequestOptions {
  phoneNumber: string;
  password?: string | null;
}
