export interface ValidOptions {
  member: string;
  team?: string; // not required for update contract, but required for create contract
  debounce?: boolean; // default - true
  assignToProvidedMember?: boolean; // use assigment member
  phoneNumber: string;
  password?: string | null;
  requestId?: string | null;
  code?: string | null;
}

export interface OmitValidOptionsTeam extends Omit<ValidOptions, 'team'> {
  team: string;
}
