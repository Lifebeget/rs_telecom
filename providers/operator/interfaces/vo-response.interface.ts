import { U } from 'ts-toolbelt';

import { CanBeRenewed, ExtensionVariant } from '../../../../Constants';

export interface BankData {
  bic: U.Nullable<string>;
  paymentMethod: U.Nullable<string>;
  bankCode: U.Nullable<string>;
  bankName: U.Nullable<string>;
  iban: U.Nullable<string>;
  accountNumber: U.Nullable<string>;
  ownerName: U.Nullable<string>;
}
export type BankDataType = keyof BankData;

export interface Address {
  firstName: U.Nullable<string>;
  surname: U.Nullable<string>;
  street: U.Nullable<string>;
  house: U.Nullable<string>;
  land: U.Nullable<string>;
  postcode: U.Nullable<string>;
  location: U.Nullable<string>;
  emailAddress: U.Nullable<string>;
}
export type AddressType = keyof Address;
export interface ContractSubsidyEligibilityModel {
  type?: U.Nullable<string>;
  name: U.Nullable<string>;
  extension: U.Nullable<ExtensionVariant>;
  index?: U.Nullable<number>;
  status?: CanBeRenewed;
  nextRenewalDate: U.Nullable<Date>;
  expirationDate?: U.Nullable<Date>;
}
export interface Contract {
  updateDate: U.Nullable<Date>;
  phoneNumber: string;
  password: U.Nullable<string>;
  tariffName: U.Nullable<string>;
  tariffOption: U.Nullable<string>;
  nextRenewalDate: U.Nullable<Date>;
  contractStartDate: U.Nullable<Date>;
  availableTariffs: U.Nullable<AvailableTariffs[]>;
  services: U.Nullable<Service[]>;
  bankData: BankData;
  addressInvoice: Address;
  addressMail: Address;
  customerAddress: Address;
  nba?: U.Nullable<Nba>;
  partnerCards?: U.Nullable<PartnerCard[]>;
  subsidyEligibility: ContractSubsidyEligibilityModel[];
}

export interface Client {
  firstName: string;
  surname: string;
  banId: U.Nullable<string>;
  clientStatus: U.Nullable<string>;
  clientStars: U.Nullable<string>;
  adv: U.Nullable<string>;
  accountType: U.Nullable<string>;
  mkek: U.Nullable<string>;
  voUserId?: U.Nullable<string>;
  birthday: U.Nullable<Date>;
  appeal: U.Nullable<string>;
  emailType: U.Nullable<string>;
  rank: U.Nullable<Rank>;
  accountNumber: U.Nullable<string>;
  voId: U.Nullable<string>;
}

export interface VOResponse {
  contract: Contract;
  client: Client;
}
interface Rank {
  current: U.Nullable<string>;
  startDate: U.Nullable<Date>;
  endDate: U.Nullable<Date>;
  points: U.Nullable<string>;
  next: U.Nullable<string>;
}
export interface Service {
  name: U.Nullable<string>;
  code: U.Nullable<string>;
  linkMod: U.Nullable<string>;
  removable: U.Nullable<boolean>;
  link: U.Nullable<string>;
  unremovableMessage: U.Nullable<string>;
}
export interface AvailableTariffs {
  code: string;
}

export interface Nba {
  banId: U.Nullable<string>;
  phoneNumber: U.Nullable<string>;
  tips: U.NonNullable<ContractNextBestActionTip[]>;
}
interface ContractNextBestActionTip {
  rowId: U.Nullable<string>;
  companyCode: U.Nullable<string>;
  cellCode: U.Nullable<string>;
  extractionDate: U.Nullable<Date>;
  flowChartId: U.Nullable<string>;
  tipText: U.Nullable<string>;
  services: U.NonNullable<string[]>;
}
export interface PartnerCard {
  banId: string;
  ndc: string;
  msisdn: string;
  tariffCode: string;
  contractStartDate: Date;
  phoneNumber: string;
  memberType: string;
  isOwner: boolean;
}
