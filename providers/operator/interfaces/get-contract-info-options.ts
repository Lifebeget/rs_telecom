export interface GetContractInfoOptions {
  contractSid: string;
  phoneNumber: string;
  password: string;
  postcode: string;
  memberSid: string;
}
