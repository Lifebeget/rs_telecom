export interface SynchronizeXmlRequestOptions {
  phoneNumber: string;
  password?: string | null;
  memberSid: string;
}
