export interface CreateContractEasyXml {
  phoneNumber: string;
  postCode: string;
  member: string;
  password: string;
  userId: string;
  team: string;
  assignToProvidedMember: boolean;
}
