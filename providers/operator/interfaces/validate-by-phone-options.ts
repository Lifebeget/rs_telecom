export interface ValidateByPhoneOptions {
  phoneNumber: string;
  password?: string | null;
  requestId?: string | null;
  code?: string | null;
}
