import {
  ContractExtendRequestAddressEntity,
  ContractExtendRequestEntity
} from '../../../modules/contract/entities';

export interface MailAndInvoiceAddressRequest {
  contractExtendRequestEntity: ContractExtendRequestEntity;
  contractExtendRequestAddressEntity: ContractExtendRequestAddressEntity;
}
