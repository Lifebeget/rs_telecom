import { IsNotEmpty, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
@Exclude()
export class CreateContractOptionDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  public readonly phoneNumber!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  public readonly postCode!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  public readonly userId!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  public readonly password!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  public readonly member!: string;
}
