export class ValidateByPhoneOptionsDto {
  public readonly member!: string;
  public readonly team?: string; // not required for update contract, but required for create contract
  public readonly debounce?: boolean; // default - true
  public readonly assignToProvidedMember?: boolean; // use assigment member
}
