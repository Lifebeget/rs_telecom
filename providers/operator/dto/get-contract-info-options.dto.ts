import { ContractProcessTrigger } from 'Constants';

export class GetContractInfoOptionsDto {
  public readonly trigger!: ContractProcessTrigger;
  public readonly retry?: boolean;
}
