export class GetContractDistributorDto {
  public readonly contractSid!: string;
  public readonly phoneNumber!: string;
  public readonly postcode!: string;
  public readonly memberSid!: string;
}
