import { Agent } from 'http';

import { HttpModule } from '@nestjs/axios';
import { DynamicModule, Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import XML from 'fast-xml-parser';

import { ActivityModule } from 'Server/modules/activity';
import { ClientModule } from 'Server/modules/client';
import { ContractModule } from 'Server/modules/contract';
import { LeadModule } from 'Server/modules/lead';
import { MemberModule } from 'Server/modules/member';
import { ServiceModule } from 'Server/modules/service';
import { TariffModule } from 'Server/modules/tariff';
import { TeamModule } from 'Server/modules/team';
import { DebounceModule } from 'Server/providers';
import { WorkflowModule } from 'Server/modules/workflow/workflow.module';
import { EmptyModule } from 'Server/utils/empty.module';
import { isScheduleProcess } from 'Server/utils/env-params';

import {
  OperatorService,
  SynchronizeCreateContractService,
  SynchronizeService,
  SynchronizeUpdateContractService,
  VOHttpService
} from './services';
import { ActivityOperatorService } from './services/activity.operator.service';
import { isJSON, isXML } from './utils';

@Global()
@Module({})
export class OperatorModule {
  public static forRoot(): DynamicModule {
    return {
      exports: [OperatorService, SynchronizeService],
      imports: [
        isScheduleProcess() ? ScheduleModule.forRoot() : EmptyModule,
        DebounceModule.forRootAsync({
          inject: [ConfigService],
          useFactory: (configService: ConfigService) => ({
            clearDatastore: true,
            clientOptions: {
              host: configService.get('REDIS_HOST'),
              port: Number(configService.get('REDIS_PORT'))
            },
            datastore: 'ioredis',
            maxConcurrent: 1,
            minTime:
              1000 *
              configService.get<number>(
                'JSON_VALIDATION_DAILY_DEBOUNCE_TIMEOUT',
                20
              )
          })
        }),
        HttpModule.registerAsync({
          inject: [ConfigService],
          useFactory: (configService: ConfigService) => ({
            baseURL: configService.get<string>(
              'VO_HOST',
              'http://127.0.0.1:5000/api/vodafone'
            ),
            httpAgent: new Agent({
              keepAlive: true
            }),
            timeout:
              1000 *
              configService.get<number>(
                'JSON_VALIDATION_DAILY_DEBOUNCE_TIMEOUT',
                20
              ),
            transformResponse: [
              (data: string) => {
                if (isJSON(data)) {
                  return JSON.parse(data);
                }

                if (isXML(data)) {
                  return XML.parse(data, { parseNodeValue: false });
                }

                return data;
              }
            ]
          })
        }),
        ActivityModule,
        ClientModule,
        ContractModule,
        LeadModule,
        MemberModule,
        ServiceModule,
        TariffModule,
        TeamModule,
        WorkflowModule
      ],
      module: OperatorModule,
      providers: [
        OperatorService,
        SynchronizeService,
        VOHttpService,
        ActivityOperatorService,
        SynchronizeCreateContractService,
        SynchronizeUpdateContractService
      ]
    };
  }
}
