import { Inject, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { Format, TransformableInfo } from 'logform';
import { DateTime } from 'luxon';
import { omit, pick } from 'remeda';
import winston, { Logger } from 'winston';

import { ValueOf, isErrorLike } from 'Utils';

import { ContextService } from '../context';

import { LOGGER_MODULE_OPTIONS } from './constants';
import { LoggerOptions } from './types';

const ELevel = {
  DEBUG: 'debug',
  ERROR: 'error',
  FATAL: 'fatal',
  INFO: 'info',
  SILLY: 'silly',
  WARNING: 'warning'
} as const;

type ELevel = ValueOf<typeof ELevel>;

type TMetadata = {
  className?: string;
  funcName?: string;
  req?: Request;
  data?: string;
  stacktrace?: string;
  payload?: unknown;
  description?: string;
};

@Injectable()
export class LoggerService {
  private readonly logger: Logger;

  constructor(
    @Inject(LOGGER_MODULE_OPTIONS)
    private readonly options: LoggerOptions,
    private readonly contextService: ContextService
  ) {
    this.logger = winston.createLogger({
      exitOnError: true,
      format: winston.format.combine(
        this.getLoggerFormat(),
        winston.format.errors({ stack: true }),
        winston.format.json(),
        winston.format.prettyPrint()
      ),
      level: options.level,
      levels: {
        [ELevel.FATAL]: 0,
        [ELevel.ERROR]: 1,
        [ELevel.INFO]: 2,
        [ELevel.WARNING]: 3,
        [ELevel.DEBUG]: 4,
        [ELevel.SILLY]: 5
      },
      transports: [new winston.transports.Console()]
    });
  }

  private getLoggerFormat(): Format {
    return {
      transform: (info: TransformableInfo): TransformableInfo => {
        info.asctime = DateTime.local().toISO();

        const context = this.contextService.getStore();
        const requestId = context?.get('request-id');

        if (requestId) {
          info.requestId = requestId;
        }

        if (info.req) {
          info.req = pick(info.req, [
            'body',
            'headers',
            'method',
            'originalUrl',
            'params',
            'query',
            'user'
          ]);

          if (info.req.body) {
            info.req.body = omit(info.req.body, ['password']);
          }
        }

        return info;
      }
    };
  }

  fatal(message: string, meta: TMetadata = {}) {
    this.logger.log(ELevel.FATAL, message, meta);
  }

  error(value: string | Error | unknown, meta: TMetadata = {}) {
    const error = isErrorLike(value) ? value : new Error(String(value));

    if (typeof meta.payload === 'object') {
      Object.assign(error, meta);
    }

    this.logger.log(ELevel.ERROR, error);
  }

  info(message: string, meta: TMetadata = {}) {
    this.logger.log(ELevel.INFO, message, meta);
  }

  warning(message: string, meta: TMetadata = {}) {
    this.logger.log(ELevel.WARNING, message, meta);
  }

  debug(message: string, meta: TMetadata = {}) {
    this.logger.log(ELevel.DEBUG, message, meta);
  }

  silly(message: string, meta: TMetadata = {}) {
    this.logger.log(ELevel.SILLY, message, meta);
  }
}
