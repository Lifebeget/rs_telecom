import { DynamicModule, Global, Module } from '@nestjs/common';

import { LOGGER_MODULE_OPTIONS } from './constants';
import { LoggerModuleOptions } from './types';
import { LoggerService } from './logger.service';

@Global()
@Module({})
export class LoggerModule {
  static forRootAsync(options: LoggerModuleOptions): DynamicModule {
    return {
      exports: [LoggerService],
      module: LoggerModule,
      providers: [
        LoggerService,
        {
          provide: LOGGER_MODULE_OPTIONS,
          ...options
        }
      ]
    };
  }
}
