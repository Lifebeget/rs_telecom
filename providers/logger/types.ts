import { FactoryProvider } from '@nestjs/common';
import { LoggerOptions as WinstonLoggerOptions } from 'winston';

export type LoggerOptions = Pick<WinstonLoggerOptions, 'level'>;

export type LoggerModuleOptions = Pick<
  FactoryProvider<Pick<LoggerOptions, 'level'>>,
  'inject' | 'useFactory'
>;
