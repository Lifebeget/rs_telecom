import * as util from 'util';

import { ConfigService } from '@nestjs/config';
import {
  Inject,
  Injectable,
  ServiceUnavailableException
} from '@nestjs/common';
import initMB, { MessageBird } from 'messagebird';

import { Errors } from 'Constants';

import { LoggerService } from '../logger';

import { SMS_MODULE_OPTIONS } from './constants';
import { SendSmsDto } from './dto';
import { SmsOptions } from './types';

@Injectable()
export class SmsService {
  private readonly messageBird?: MessageBird;
  constructor(
    @Inject(SMS_MODULE_OPTIONS)
    private readonly options: SmsOptions,
    private readonly configService: ConfigService,
    private readonly loggerService: LoggerService
  ) {
    if (options.accessKey) {
      this.messageBird = initMB(options.accessKey);
    }
  }

  async send(sendSmsDto: SendSmsDto) {
    this.loggerService.debug(`Sending sms to '${sendSmsDto.recipients[0]}'`);

    if (process.env.NODE_ENV === 'development') {
      return this.loggerService.info(
        'SMS was not sent in development environment'
      );
    }

    if (this.configService.get<string>('SMS_TEST_MODE', 'false') === 'true') {
      this.loggerService.warning('Sms service is running in test mode');
      this.loggerService.info('Send sms', {
        payload: sendSmsDto
      });

      return true;
    }

    if (!this.messageBird) {
      this.loggerService.error('MessageBird client is not initialized');
      throw new ServiceUnavailableException(Errors.MESSAGE_BIRD);
    }

    const send = util.promisify(this.messageBird.messages.create);

    try {
      await send(sendSmsDto);
    } catch (err) {
      throw err;
    }
  }
}
