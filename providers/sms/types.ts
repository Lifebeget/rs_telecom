import { FactoryProvider } from '@nestjs/common';

export type SmsOptions = {
  accessKey?: string;
};

export type SmsModuleOptions = Pick<
  FactoryProvider<SmsOptions>,
  'useFactory' | 'inject'
>;

export type SMSModuleKeys = Record<'MESSAGE_BIRD_ACCESS_KEY', string>;
