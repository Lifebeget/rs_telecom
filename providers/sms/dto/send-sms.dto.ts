import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  IsString,
  ValidateNested
} from 'class-validator';

@Exclude()
export class SendSmsDto {
  @ApiProperty({
    description: 'Sender of SMS',
    name: 'originator',
    required: true,
    type: String
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  originator!: string;

  @ApiProperty({
    description: 'Array of recipients',
    name: 'recipients',
    required: true,
    type: [String]
  })
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => String)
  @Expose()
  recipients!: string[];

  @ApiProperty({
    description: 'SMS body',
    name: 'body',
    required: true,
    type: String
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  body!: string;
}
