import { DynamicModule, Global, Module } from '@nestjs/common';

import { SMS_MODULE_OPTIONS } from './constants';
import { SmsService } from './sms.service';
import { SmsModuleOptions } from './types';

@Global()
@Module({})
export class SmsModule {
  public static forRootAsync(options: SmsModuleOptions): DynamicModule {
    return {
      exports: [SmsService],
      module: SmsModule,
      providers: [
        {
          provide: SMS_MODULE_OPTIONS,
          ...options
        },
        SmsService
      ]
    };
  }
}
