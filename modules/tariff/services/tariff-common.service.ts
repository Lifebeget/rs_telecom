import { BadRequestException, Injectable } from '@nestjs/common';
import { EventEmitter2 as EventEmitter } from '@nestjs/event-emitter';
import { plainToClass } from 'class-transformer';
import { equals, isEmpty, reject, trim } from 'ramda';

import { Errors } from 'Constants';
import { TariffError } from 'Constants/errorValues';
import { LoggerService } from 'Server/providers';

import { CreateTariffDto, UpdateTariffDto } from '../dto';
import { TariffEntity, TariffXTariffGroupEntity } from '../entities';
import { TariffUpdateEvent } from '../events';
import { TariffGroupModel, TariffModel } from '../models';
import {
  TariffGroupRepository,
  TariffListRepository,
  TariffRepository,
  TariffXTariffGroupRepository
} from '../repositories';

@Injectable()
export class TariffCommonService {
  constructor(
    private readonly loggerService: LoggerService,
    private readonly tariffListRepository: TariffListRepository,
    private readonly tariffRepository: TariffRepository,
    private readonly tariffGroupRepository: TariffGroupRepository,
    private readonly tariffXTariffGroupRepository: TariffXTariffGroupRepository,
    private readonly eventEmitterService: EventEmitter
  ) {}

  async archiveTariff(tariffSid: string) {
    const tariff = await this.tariffRepository.getTariff(tariffSid);

    if (tariff.archived) {
      throw new BadRequestException(TariffError.TARIFF_ALREADY_ARCHIVED);
    }

    await this.tariffRepository.update(tariff.sid, { archived: true });

    const res = await this.tariffRepository.getTariff(tariffSid);

    return plainToClass(TariffModel, res);
  }

  async unarchiveTariff(tariffSid: string) {
    const tariff = await this.tariffRepository.getTariff(tariffSid);

    if (!tariff.archived) {
      throw new BadRequestException(TariffError.TARIFF_NOT_ARCHIVED);
    }

    await this.tariffRepository.update(tariff.sid, { archived: false });

    const res = await this.tariffRepository.getTariff(tariffSid);

    return plainToClass(TariffModel, res);
  }

  async addTariff(createTariffDto: CreateTariffDto) {
    const currentTariffList = await this.tariffListRepository.getCurrentTariffList();

    const { code, price, name } = createTariffDto;
    const tariffEntity = new TariffEntity({
      archived: false,
      code: trim(code),
      name: trim(name),
      price,
      tariffList: currentTariffList.sid
    });

    if (await this.tariffRepository.findOne({ code: trim(code) })) {
      throw new BadRequestException(
        TariffError.TARIFF_CODE_ALREADY_EXISTS,
        `Tariff with code ${name} already exists`
      );
    }

    try {
      return await this.tariffRepository.save(tariffEntity);
    } catch (e) {
      this.loggerService.error('Error while saving the tariff', {
        payload: createTariffDto
      });

      throw e;
    }
  }

  async updateTariffGlobal(
    tariffSid: string,
    updateTariffDto: UpdateTariffDto
  ): Promise<TariffModel> {
    this.loggerService.debug('Update tariff globally', {
      payload: {
        tariffSid,
        updateTariffDto
      }
    });

    const updateObj = reject(
      equals<undefined | string | number | null>(undefined),
      {
        code: updateTariffDto.code === '' ? null : updateTariffDto.code,
        name: updateTariffDto.name,
        price: updateTariffDto.price
      }
    );

    if (isEmpty(updateObj)) {
      throw new BadRequestException(Errors.NOTHING_TO_UPDATE);
    }

    try {
      const tariffBeforeUpdate = await this.tariffRepository.getTariff(
        tariffSid
      );

      await this.tariffRepository.update(tariffSid, updateObj);

      this.eventEmitterService.emit(
        TariffUpdateEvent.eventName,
        new TariffUpdateEvent(tariffSid, updateObj, tariffBeforeUpdate)
      );

      return plainToClass(
        TariffModel,
        await this.tariffRepository.getTariff(tariffSid)
      );
    } catch (e) {
      this.loggerService.error(e, {
        description: 'Error while tariff updating'
      });

      throw new BadRequestException(Errors.TARIFF_CODE_ALREADY_EXISTS);
    }
  }

  async updateTariffInGroup(
    tariffSid: string,
    groupSid: string,
    updateTariffDto: UpdateTariffDto
  ) {
    const groupEntity = await this.tariffGroupRepository.getTariffGroupBySid(
      groupSid
    );

    // check if the tariff exists
    await this.tariffRepository.getTariff(tariffSid);

    this.loggerService.debug('Update tariff for the group', {
      payload: {
        groupName: groupEntity.name,
        tariffSid,
        updateTariffDto
      }
    });

    if (
      updateTariffDto.callCenterCommission === undefined &&
      updateTariffDto.clientCommission === undefined
    ) {
      throw new BadRequestException(Errors.NOTHING_TO_UPDATE);
    }

    if (
      await this.tariffXTariffGroupRepository.findOne({
        tariff: tariffSid,
        tariffGroup: groupSid
      })
    ) {
      await this.tariffXTariffGroupRepository.update(
        { tariff: tariffSid, tariffGroup: groupSid },
        reject(v => v === undefined, {
          callCenterCommission: updateTariffDto.callCenterCommission,
          clientCommission: updateTariffDto.clientCommission
        })
      );
    } else {
      const tariffXTariffGroupEntity = new TariffXTariffGroupEntity({
        callCenterCommission: updateTariffDto.callCenterCommission,
        clientCommission: updateTariffDto.clientCommission,
        tariff: tariffSid,
        tariffGroup: groupSid
      });

      await this.tariffXTariffGroupRepository.save(tariffXTariffGroupEntity);
    }

    this.loggerService.info(
      `Override commission on tariff "${tariffSid}" in group "${groupSid}"`
    );
  }

  async getTariffGroups() {
    return plainToClass(
      TariffGroupModel,
      await this.tariffGroupRepository.find()
    );
  }
}
