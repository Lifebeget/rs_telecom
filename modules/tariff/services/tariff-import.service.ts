import { BadRequestException, Injectable } from '@nestjs/common';
import { QueryRunner } from 'typeorm';
import xlsx from 'xlsx';

import { Errors } from 'Constants';
import { ContractTariffService } from 'Server/modules/contract/services';
import { MemberRepository } from 'Server/modules/member/repositories';
import { LoggerService } from 'Server/providers';

import {
  COLUMN_TYPE_TO_NAME,
  TARIFF_GROUP_NAMES,
  TARIFF_GROUP_TO_NAME_COMMISSION_CALL_CENTER,
  TARIFF_GROUP_TO_NAME_COMMISSION_CLIENT,
  TariffColumnType
} from '../constants';
import { ImportTariffsDto } from '../dto';
import {
  TariffEntity,
  TariffListEntity,
  TariffXTariffGroupEntity
} from '../entities';
import { TariffListModel } from '../models';
import {
  TariffGroupRepository,
  TariffListRepository,
  TariffRepository
} from '../repositories';
import { makeExcelRowsIterator } from '../utils';

import { TariffCommonService } from './tariff-common.service';
import { TariffListService } from './tariff-list.service';

@Injectable()
export class TariffSynchronizeService {
  constructor(
    private readonly tariffCommonService: TariffCommonService,
    private readonly tariffListService: TariffListService,
    private readonly loggerService: LoggerService,
    private readonly contractTariffService: ContractTariffService,
    private readonly memberRepository: MemberRepository,
    private readonly tariffListRepository: TariffListRepository,
    private readonly tariffRepository: TariffRepository,
    private readonly tariffGroupRepository: TariffGroupRepository
  ) {}

  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  async extractTariffsFromImportFile(file: Express.Multer.File) {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
    if (!file) {
      throw new BadRequestException();
    }

    const workbook = xlsx.readFile(file.path);
    const sheet = workbook.Sheets[workbook.SheetNames[0]];

    const rowsIterator = makeExcelRowsIterator(sheet);

    const tariffGroups = await Promise.all(
      TARIFF_GROUP_NAMES.map(
        async name =>
          [
            name,
            await this.tariffGroupRepository.getTariffGroupByName(name)
          ] as const
      )
    );

    const rows = Array.from(rowsIterator);

    const tariffs = rows.map(({ index, rowObj: row }) => {
      const name = row[COLUMN_TYPE_TO_NAME[TariffColumnType.Name]]?.v;
      const code = row[COLUMN_TYPE_TO_NAME[TariffColumnType.Code]]?.v;
      const price = row[COLUMN_TYPE_TO_NAME[TariffColumnType.Price]]?.v ?? null;

      const isHeader = index <= 1;
      const isSubheader = !isHeader && !code;

      if (isHeader || isSubheader) {
        return null;
      }

      if (typeof name !== 'string') {
        this.loggerService.error('Invalid tariff row data type', {
          payload: { index, row }
        });
        this.loggerService.error('Name is not a string', {
          payload: { name }
        });

        throw new BadRequestException(Errors.TARIFF_IMPORT_INVALID_DATA);
      }

      if (typeof code !== 'string') {
        this.loggerService.error('Invalid tariff row data type', {
          payload: { index, row }
        });
        this.loggerService.error('Code is not a string', {
          payload: { code }
        });

        throw new BadRequestException(Errors.TARIFF_IMPORT_INVALID_DATA);
      }

      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      if (price !== undefined && typeof price !== 'number') {
        this.loggerService.error('Invalid tariff row data type', {
          payload: { index, row }
        });
        this.loggerService.error('Price is not a null or number', {
          payload: { price }
        });

        throw new BadRequestException(Errors.TARIFF_IMPORT_INVALID_DATA);
      }

      const tariffEntity = new TariffEntity({
        // will be filled later
        archived: false,

        code: code.trim(),

        name: name.trim(),

        price,
        tariffList: ''
      });

      const tariffXTariffGroupEntities = tariffGroups.map(
        ([groupName, group]) => {
          const groupColumnCommissionCallCenter =
            TARIFF_GROUP_TO_NAME_COMMISSION_CALL_CENTER[groupName];
          const groupColumnCommissionClient =
            TARIFF_GROUP_TO_NAME_COMMISSION_CLIENT[groupName];

          const callCenterCommission =
            row[groupColumnCommissionCallCenter]?.v ?? null;
          const clientCommission = row[groupColumnCommissionClient]?.v ?? null;

          if (
            callCenterCommission !== null &&
            typeof callCenterCommission !== 'number'
          ) {
            this.loggerService.error('Invalid tariff row data type', {
              payload: { index, row }
            });
            this.loggerService.error(
              'CallCenterCommission is not a null or number',
              {
                payload: { callCenterCommission, group }
              }
            );

            throw new BadRequestException(Errors.TARIFF_IMPORT_INVALID_DATA);
          }

          if (
            clientCommission !== null &&
            typeof clientCommission !== 'number'
          ) {
            this.loggerService.error('Invalid tariff row data type', {
              payload: { index, row }
            });
            this.loggerService.error(
              'ClientCommission is not a null or number',
              {
                payload: { clientCommission, group }
              }
            );

            throw new BadRequestException(Errors.TARIFF_IMPORT_INVALID_DATA);
          }

          return new TariffXTariffGroupEntity({
            callCenterCommission,
            clientCommission,
            tariff: '', // will be filled later
            tariffGroup: group.sid
          });
        }
      );

      return { tariffEntity, tariffXTariffGroupEntities };
    });

    return tariffs.filter(v => v !== null) as Exclude<
      typeof tariffs[number],
      null
    >[];
  }

  private async synchronizeImportedTariffs(
    tariffs: TariffEntity[],
    qr: QueryRunner
  ) {
    let synchronized = 0;

    const step = Math.floor(tariffs.length / 10);

    let totalContractsUpdated = 0;
    let totalExtendRequestsUpdated = 0;

    await tariffs.reduce(
      (prev, tariff, i) =>
        prev.then(async () => {
          const syncStat = await this.contractTariffService.synchronizeContractTariffByCode(
            tariff,
            false,
            qr
          );
          synchronized++;
          totalContractsUpdated += syncStat.totalContractsUpdated;
          totalExtendRequestsUpdated += syncStat.totalExtendRequestsUpdated;

          if (i % step === 0 || i === tariffs.length - 1) {
            this.loggerService.info(
              `Synchronized tariffs: ${synchronized}/${tariffs.length}`
            );
          }
        }),

      Promise.resolve()
    );

    this.loggerService.info('All tariffs synchronized', {
      payload: {
        tariffsCount: tariffs.length,
        totalContractsUpdated,
        totalExtendRequestsUpdated
      }
    });
  }

  private async createTariffsInTariffList(
    tariffListSid: string,
    tariffsWithTariffXGroupUnassigned: Array<{
      tariffEntity: TariffEntity;
      tariffXTariffGroupEntities: TariffXTariffGroupEntity[];
    }>,
    qr: QueryRunner
  ) {
    const tariffEntities = tariffsWithTariffXGroupUnassigned.map(
      ({ tariffEntity }) => {
        tariffEntity.tariffList = tariffListSid;

        return tariffEntity;
      }
    );

    const tariffsSaved = await qr.manager.save(TariffEntity, tariffEntities);

    const tariffsWithTariffXGroup = tariffsWithTariffXGroupUnassigned.map(
      ({ tariffXTariffGroupEntities }, i) => {
        const tariffSaved = tariffsSaved[i];

        const tariffXTariffGroupEntitiesAssigned = tariffXTariffGroupEntities.map(
          tariffXGroup => {
            tariffXGroup.tariff = tariffSaved.sid;

            return tariffXGroup;
          }
        );

        return {
          tariffEntity: tariffSaved,
          tariffXTariffGroupEntities: tariffXTariffGroupEntitiesAssigned
        };
      }
    );

    const tariffXTariffGroupEntities = tariffsWithTariffXGroup
      .map(({ tariffXTariffGroupEntities }) => tariffXTariffGroupEntities)
      .flat();

    await qr.manager.save(TariffXTariffGroupEntity, tariffXTariffGroupEntities);

    this.loggerService.info(
      `Added ${tariffsSaved.length} tariffs to tariffList "${tariffListSid}"`
    );

    return this.synchronizeImportedTariffs(tariffsSaved, qr);
  }

  private async updateTariffs(
    tariffsWithTariffXGroupUnassigned: Array<{
      tariffEntity: TariffEntity;
      tariffXTariffGroupEntities: TariffXTariffGroupEntity[];
    }>,
    qr: QueryRunner
  ) {
    const tariffsUpdated: TariffEntity[] = [];

    for await (const {
      tariffEntity,
      tariffXTariffGroupEntities
    } of tariffsWithTariffXGroupUnassigned) {
      await qr.manager.update(
        TariffEntity,
        { code: tariffEntity.code },
        {
          name: tariffEntity.name,
          price: tariffEntity.price
        }
      );

      const tariffUpdated = await this.tariffRepository.findOneOrFail({
        code: tariffEntity.code
      });

      tariffsUpdated.push(tariffUpdated);

      await Promise.all(
        tariffXTariffGroupEntities.map(async tariffXTariffGroup => {
          if (
            await qr.manager.findOne(TariffXTariffGroupEntity, {
              tariff: tariffUpdated.sid,
              tariffGroup: tariffXTariffGroup.tariffGroup
            })
          ) {
            await qr.manager.update(
              TariffXTariffGroupEntity,
              {
                tariff: tariffUpdated.sid,
                tariffGroup: tariffXTariffGroup.tariffGroup
              },
              {
                callCenterCommission: tariffXTariffGroup.callCenterCommission,
                clientCommission: tariffXTariffGroup.clientCommission
              }
            );
          } else {
            await qr.manager.save(
              new TariffXTariffGroupEntity({
                callCenterCommission: tariffXTariffGroup.callCenterCommission,
                clientCommission: tariffXTariffGroup.clientCommission,
                tariff: tariffUpdated.sid,
                tariffGroup: tariffXTariffGroup.tariffGroup
              })
            );
          }
        })
      );
    }

    this.loggerService.info(
      `Updated ${tariffsUpdated.length} tariffs and tariffXGroup`
    );

    return this.synchronizeImportedTariffs(tariffsUpdated, qr);
  }

  async importTariffsCreate(
    createTariffListDto: ImportTariffsDto,
    file: Express.Multer.File
  ): Promise<TariffListModel> {
    const qr = await this.tariffListRepository.startTransaction();

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
    if (!file) {
      throw new BadRequestException();
    }

    try {
      await this.memberRepository.getMember(createTariffListDto.member);

      const tariffList = new TariffListEntity();
      tariffList.member = createTariffListDto.member;

      await qr.manager.save(TariffListEntity, tariffList);

      const tariffsWithTariffXGroupUnassigned = await this.extractTariffsFromImportFile(
        file
      );

      await this.createTariffsInTariffList(
        tariffList.sid,
        tariffsWithTariffXGroupUnassigned,
        qr
      );

      await qr.commitTransaction();

      this.loggerService.info(`Created tariff list "${tariffList.sid}"`);

      return this.tariffListService.getActualTariffListGlobal();
    } catch (err) {
      if (qr.isTransactionActive) {
        await qr.rollbackTransaction();
      }

      throw err;
    } finally {
      if (!qr.isReleased) {
        await qr.release();
      }
    }
  }

  async importTariffsUpdate(
    updateTariffListDto: ImportTariffsDto,
    file: Express.Multer.File
  ): Promise<TariffListModel> {
    const qr = await this.tariffListRepository.startTransaction();

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
    if (!file) {
      throw new BadRequestException();
    }

    try {
      await this.memberRepository.getMember(updateTariffListDto.member);

      const tariffList = await this.tariffListRepository.findOne(undefined, {
        order: { createdAt: 'DESC' }
      });

      if (!tariffList) {
        throw new BadRequestException(Errors.ACTUAL_TARIFF_LIST_DOES_NOT_EXIST);
      }

      const tariffsWithTariffXGroupUnassigned = await this.extractTariffsFromImportFile(
        file
      );

      const tariffListActual = await this.tariffListRepository.getActualTariffListGlobal();

      const {
        toUpdate: tariffsWithTariffXGroupUnassignedToUpdate,
        toCreate: tariffsWithTariffXGroupUnassignedToCreate
      } = tariffsWithTariffXGroupUnassigned.reduce<{
        toUpdate: typeof tariffsWithTariffXGroupUnassigned;
        toCreate: typeof tariffsWithTariffXGroupUnassigned;
      }>(
        (acc, entry) => {
          const tariffExisting = tariffListActual.items.find(
            tariff => tariff.code === entry.tariffEntity.code
          );

          if (tariffExisting) {
            acc.toUpdate.push(entry);
          } else {
            acc.toCreate.push(entry);
          }

          return acc;
        },
        {
          toCreate: [],
          toUpdate: []
        }
      );

      this.loggerService.debug('Tariff extracted', {
        payload: {
          toCreate: tariffsWithTariffXGroupUnassignedToCreate.length,
          toUpdate: tariffsWithTariffXGroupUnassignedToUpdate.length
        }
      });

      if (tariffsWithTariffXGroupUnassignedToCreate.length) {
        await this.createTariffsInTariffList(
          tariffList.sid,
          tariffsWithTariffXGroupUnassignedToCreate,
          qr
        );
      }

      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      if (tariffsWithTariffXGroupUnassignedToUpdate) {
        await this.updateTariffs(tariffsWithTariffXGroupUnassignedToUpdate, qr);
      }

      await qr.commitTransaction();

      this.loggerService.info(`Updated tariff list "${tariffList.sid}"`);

      return this.tariffListService.getActualTariffListGlobal();
    } catch (err) {
      if (qr.isTransactionActive) {
        await qr.rollbackTransaction();
      }

      throw err;
    } finally {
      if (!qr.isReleased) {
        await qr.release();
      }
    }
  }
}
