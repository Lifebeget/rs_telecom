import { BadRequestException, Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

import { TariffError } from 'Constants/errorValues';
import { TeamCommonService } from 'Server/modules/team/services';

import { TariffListModel } from '../models';
import { TariffGroupRepository, TariffListRepository } from '../repositories';

@Injectable()
export class TariffListService {
  constructor(
    private readonly tariffListRepository: TariffListRepository,
    private readonly tariffGroupRepository: TariffGroupRepository,
    private readonly teamCommonService: TeamCommonService
  ) {}

  async getActualTariffListByTeam({
    teamSid,
    ...rest
  }: {
    teamSid: string;
    name?: string;
    excludeArchived?: boolean;
    excludeWithMissingCommission?: boolean;
    contractSid?: string;
  }): Promise<TariffListModel> {
    const team = await this.teamCommonService.getTeam(teamSid);

    if (!team.tariffGroup) {
      throw new BadRequestException(TariffError.TEAM_HAS_NO_TARIFF_GROUP);
    }

    return this.getActualTariffListByGroup({
      ...rest,
      groupSid: team.tariffGroup.sid
    });
  }

  async getActualTariffListByGroup({
    groupSid,
    search,
    excludeArchived,
    excludeWithMissingCommission,
    contractSid
  }: {
    groupSid?: string;
    search?: string;
    excludeArchived?: boolean;
    excludeWithMissingCommission?: boolean;
    contractSid?: string;
  } = {}): Promise<TariffListModel> {
    if (groupSid) {
      await this.tariffGroupRepository.getTariffGroupBySid(groupSid);
    }

    const tariffList = await this.tariffListRepository.getActualTariffListByGroup(
      {
        contractSid,
        excludeArchived,
        excludeWithMissingCommission,
        groupSid,
        search
      }
    );

    return plainToClass(TariffListModel, tariffList);
  }

  async getActualTariffListGlobal({
    name,
    excludeArchived,
    excludeWithMissingCommission
  }: {
    name?: string;
    excludeArchived?: boolean;
    excludeWithMissingCommission?: boolean;
  } = {}): Promise<TariffListModel> {
    const tariffList = await this.tariffListRepository.getActualTariffListGlobal(
      {
        excludeArchived,
        excludeWithMissingCommission,
        name
      }
    );

    return plainToClass(TariffListModel, tariffList);
  }
}
