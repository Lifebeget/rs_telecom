import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { isNil } from 'remeda';

import { MemberModel } from 'Server/modules/member/models';

import { TariffGroupModel } from './tariff-group.model';

@Exclude()
export class TariffModel {
  @ApiProperty({
    description: 'Tariff list item Id',
    example: '987c44fa-54ca-4faa-b718-9d0cb90132c4',
    required: true,
    type: String
  })
  @Expose()
  public readonly sid!: string;

  @ApiProperty({
    description: 'Tariff list item name',
    example: 'Tariff',
    required: true,
    type: String
  })
  @Expose()
  public readonly name!: string;

  @ApiProperty({
    description: 'Tariff list item code',
    example: 'KKI93OO',
    required: true,
    type: String
  })
  @Expose()
  public readonly code!: string;

  @ApiProperty({
    description: 'Tariff list item price',
    example: 100,
    required: true,
    type: Number
  })
  @Expose()
  @Type(() => Number)
  public readonly price!: number;

  @ApiProperty({
    description: 'Tariff archived',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly archived!: boolean;

  @ApiProperty({
    description: 'Tariff group',
    nullable: true,
    required: false,
    type: TariffGroupModel
  })
  @Expose()
  @Type(() => TariffGroupModel)
  public readonly tariffGroup?: TariffGroupModel | null;

  @ApiProperty({
    description: 'Tariff call center commission',
    example: 10,
    nullable: true,
    required: true,
    type: Number
  })
  @Expose()
  @Transform(({ value, obj }) => {
    if (obj.override) {
      return obj.override.callCenterCommission;
    } else if (
      obj.overrideTeam &&
      !isNil(obj.overrideTeam.callCenterCommission)
    ) {
      return obj.overrideTeam.callCenterCommission;
    }

    return value;
  })
  public readonly callCenterCommission!: number | null;

  @ApiProperty({
    description: 'Tariff client commission',
    example: 10,
    nullable: true,
    required: true,
    type: Number
  })
  @Expose()
  @Transform(({ value, obj }) => {
    if (obj.override) {
      return obj.override.clientCommission;
    } else if (obj.overrideTeam && !isNil(obj.overrideTeam.clientCommission)) {
      return obj.overrideTeam.clientCommission;
    }

    return value;
  })
  public readonly clientCommission!: number | null;
}

@Exclude()
export class TariffListModel {
  @ApiProperty({
    description: 'Tariff list Id',
    example: '987c44fa-54ca-4faa-b718-9d0cb90132v4',
    required: true,
    type: String
  })
  @Expose()
  public readonly sid!: string;

  @ApiProperty({
    description: 'List of tariff items',
    required: true,
    type: [TariffModel]
  })
  @Expose()
  @Type(() => TariffModel)
  public readonly items!: TariffModel[];

  @ApiProperty({
    description: 'Creator',
    required: true,
    type: () => MemberModel
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly member!: MemberModel;

  @ApiProperty({
    description: 'Creation date',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly createdAt!: Date;
}
