import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class TariffGroupModel {
  @ApiProperty({
    description: 'Tariff list item Id',
    example: '987c44fa-54ca-4faa-b718-9d0cb90132c4',
    required: true,
    type: String
  })
  @Expose()
  public readonly sid!: string;

  @ApiProperty({
    description: 'Tariff list item name',
    example: 'Tariff',
    required: true,
    type: String
  })
  @Expose()
  public readonly name!: string;

  @ApiProperty({
    description: 'Update date',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly updatedAt!: Date;

  @ApiProperty({
    description: 'Creation date',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly createdAt!: Date;
}
