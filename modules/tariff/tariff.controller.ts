import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UnsupportedMediaTypeException,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import mime from 'mime';

import { Errors, Permission } from 'Constants';
import { Restrict, User } from 'Server/decorators';

import { JwtPayloadModel } from '../auth/models';
import { RoleService } from '../role/role.service';

import {
  CreateTariffDto,
  GetTariffListQueryParamsDto,
  ImportTariffsDto,
  TariffGroupSidDto,
  TariffInGroupSidDto,
  TariffSidDto,
  TeamSidDto,
  UpdateTariffDto
} from './dto';
import { TariffGroupModel, TariffListModel, TariffModel } from './models';
import { TariffCommonService, TariffSynchronizeService } from './services';
import {
  TariffGroupsSwaggerGet,
  TariffSwaggerCommissionPut as TariffItemSwaggerPut,
  TariffSwaggerAddPost,
  TariffSwaggerArchivePost,
  TariffSwaggerGet,
  TariffSwaggerPost,
  TariffSwaggerPut
} from './tariff.swagger';
import { TariffListService } from './services/tariff-list.service';

@Controller('tariffs')
@ApiTags('Tariffs')
export class TariffController {
  constructor(
    private readonly tariffSynchronizeService: TariffSynchronizeService,
    private readonly tariffCommonService: TariffCommonService,
    private readonly tariffListService: TariffListService,
    private readonly roleService: RoleService
  ) {}

  @Post('/import')
  @Restrict()
  @TariffSwaggerPost()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: (_, { mimetype }, cb) => {
        const extension = mime.getExtension(mimetype);

        if (extension !== 'xlsx') {
          return cb(
            new UnsupportedMediaTypeException(
              Errors.TARIFF_IMPORT_FILE_EXTENSION_INVALID
            ),
            false
          );
        }
        cb(null, true);
      },
      limits: { fileSize: 2 * 1024 ** 2, files: 1 }
    })
  )
  async importTariffsCreate(
    @Body() importTariffsDto: ImportTariffsDto,
    @UploadedFile('file') file: Express.Multer.File,
    @User() user: JwtPayloadModel
  ): Promise<TariffListModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.importTariffAndCreate],
      user.sid
    );

    return this.tariffSynchronizeService.importTariffsCreate(
      importTariffsDto,
      file
    );
  }

  @Put('/import')
  @Restrict()
  @TariffSwaggerPut()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('file', {
      // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
      fileFilter: (_, { mimetype }, cb) => {
        const extension = mime.getExtension(mimetype);

        if (extension !== 'xlsx') {
          return cb(
            new UnsupportedMediaTypeException(
              Errors.TARIFF_IMPORT_FILE_EXTENSION_INVALID
            ),
            false
          );
        }
        cb(null, true);
      },

      limits: { fileSize: 2 * 1024 ** 2, files: 1 }
    })
  )
  async importTariffsUpdate(
    @Body() importTariffsDto: ImportTariffsDto,
    @UploadedFile('file') file: Express.Multer.File,
    @User() user: JwtPayloadModel
  ): Promise<TariffListModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.importTariffAndCreate],
      user.sid
    );

    return this.tariffSynchronizeService.importTariffsUpdate(
      importTariffsDto,
      file
    );
  }

  @Get('team/:teamSid')
  @Restrict()
  @TariffSwaggerGet()
  async getActualTariffListByTeam(
    @Param() teamSidDto: TeamSidDto,
    @Query() getTariffListQueryDto: GetTariffListQueryParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffListModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.viewTariffInfo],
      user.sid
    );

    return this.tariffListService.getActualTariffListByTeam({
      contractSid: getTariffListQueryDto.contractSid,
      excludeArchived: getTariffListQueryDto.excludeArchived ?? false,
      excludeWithMissingCommission:
        getTariffListQueryDto.excludeWithMissingCommission ?? false,
      name: getTariffListQueryDto.search,
      teamSid: teamSidDto.teamSid
    });
  }

  @Get('group/:groupSid')
  @Restrict()
  @TariffSwaggerGet()
  async getActualTariffListByGroup(
    @Param() tariffGroupSidDto: TariffGroupSidDto,
    @Query() getTariffListQueryDto: GetTariffListQueryParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffListModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.viewTariffInfo],
      user.sid
    );

    return this.tariffListService.getActualTariffListByGroup({
      excludeArchived: getTariffListQueryDto.excludeArchived ?? false,
      excludeWithMissingCommission:
        getTariffListQueryDto.excludeWithMissingCommission ?? false,
      groupSid: tariffGroupSidDto.groupSid,
      search: getTariffListQueryDto.search
    });
  }

  @Get('global')
  @Restrict()
  @TariffSwaggerGet()
  async getActualTariffListGlobal(
    @Query() getTariffListQueryDto: GetTariffListQueryParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffListModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.viewTariffInfo],
      user.sid
    );

    return this.tariffListService.getActualTariffListGlobal({
      excludeArchived: getTariffListQueryDto.excludeArchived ?? false,
      excludeWithMissingCommission:
        getTariffListQueryDto.excludeWithMissingCommission ?? false,
      name: getTariffListQueryDto.search
    });
  }

  @Put(':tariffSid/global')
  @Restrict()
  @TariffItemSwaggerPut()
  async updateTariffGlobal(
    @Body() updateTariffDto: UpdateTariffDto,
    @Param() tariffSidDto: TariffSidDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.updateTariffGlobal],
      user.sid
    );

    return this.tariffCommonService.updateTariffGlobal(
      tariffSidDto.tariffSid,
      updateTariffDto
    );
  }

  @Put(':tariffSid/group/:groupSid')
  @Restrict()
  @TariffItemSwaggerPut()
  async updateTariffInGroup(
    @Body() updateTariffDto: UpdateTariffDto,
    @Param() tariffInGroupSidDto: TariffInGroupSidDto,
    @User() user: JwtPayloadModel
  ) {
    await this.roleService.checkUserHasPermissions(
      [Permission.updateTariffTeam],
      user.sid
    );

    return this.tariffCommonService.updateTariffInGroup(
      tariffInGroupSidDto.tariffSid,
      tariffInGroupSidDto.groupSid,
      updateTariffDto
    );
  }

  @Post('create')
  @Restrict()
  @TariffSwaggerAddPost()
  async addTariff(
    @Body() createTariffDto: CreateTariffDto,
    @User() user: JwtPayloadModel
  ): Promise<void> {
    await this.roleService.checkUserHasPermissions(
      [Permission.addTariff],
      user.sid
    );

    await this.tariffCommonService.addTariff(createTariffDto);
  }

  @Put(':tariffSid/archive')
  @Restrict()
  @TariffSwaggerArchivePost()
  async archiveTariff(
    @Param() tariffSidDto: TariffSidDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.addTariff],
      user.sid
    );

    return this.tariffCommonService.archiveTariff(tariffSidDto.tariffSid);
  }

  @Put(':tariffSid/unarchive')
  @Restrict()
  @TariffSwaggerArchivePost()
  async unarchiveTariff(
    @Param() tariffSidDto: TariffSidDto,
    @User() user: JwtPayloadModel
  ): Promise<TariffModel> {
    await this.roleService.checkUserHasPermissions(
      [Permission.addTariff],
      user.sid
    );

    return this.tariffCommonService.unarchiveTariff(tariffSidDto.tariffSid);
  }

  @Get('groups')
  @Restrict()
  @TariffGroupsSwaggerGet()
  async getTariffGroups(
    @User() user: JwtPayloadModel
  ): Promise<TariffGroupModel[]> {
    await this.roleService.checkUserHasPermissions(
      [Permission.viewTariffInfo],
      user.sid
    );

    return this.tariffCommonService.getTariffGroups();
  }
}
