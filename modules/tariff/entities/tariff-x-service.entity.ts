import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn
} from 'typeorm';

import { ServiceEntity } from 'Server/modules/service/entities';

import { TariffEntity } from './tariff.entity';

@Entity('tariff_x_service', { schema: 'tariffs' })
export class TariffXServiceEntity {
  @PrimaryColumn({
    name: 'tariff_sid',
    type: 'uuid'
  })
  @OneToOne(() => TariffEntity)
  @JoinColumn({
    name: 'tariff_sid',
    referencedColumnName: 'sid'
  })
  public tariff!: TariffEntity['sid'];

  @PrimaryColumn({
    name: 'service_sid',
    type: 'uuid'
  })
  @OneToOne(() => ServiceEntity)
  @JoinColumn({
    name: 'service_sid',
    referencedColumnName: 'sid'
  })
  public service!: ServiceEntity['sid'];

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  constructor(obj: Omit<TariffXServiceEntity, 'createdAt'>) {
    Object.assign(this, obj);
  }
}
