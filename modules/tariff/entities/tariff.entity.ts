import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

import { TeamEntity } from 'Server/modules/team/entities';

import { TariffListEntity } from './tariff-list.entity';
import { TariffGroupEntity } from './tariff-group.entity';

@Entity('tariff', { schema: 'tariffs' })
export class TariffEntity {
  @PrimaryGeneratedColumn('uuid', {
    name: 'sid'
  })
  public readonly sid!: string;

  @Column({
    name: 'tariff_list_sid',
    type: 'uuid'
  })
  @OneToOne(() => TariffListEntity, tariffList => tariffList.items)
  @JoinColumn({
    name: 'tariff_list_sid',
    referencedColumnName: 'sid'
  })
  public tariffList!: TariffListEntity['sid'];

  @Column({
    length: 255,
    name: 'name',
    type: 'varchar'
  })
  public name!: string;

  @Column({
    length: 255,
    name: 'code',
    nullable: true,
    type: 'varchar'
  })
  public code!: string | null;

  @Column({
    name: 'price',
    type: 'real'
  })
  public price!: number;

  @Column({
    default: false,
    name: 'archived',
    type: 'boolean'
  })
  public archived!: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp with time zone'
  })
  public readonly updatedAt!: Date;

  @OneToMany(() => TeamEntity, team => team.tariffs)
  public readonly teams!: TeamEntity[];

  @OneToMany(() => TariffGroupEntity, group => group.tariffs)
  public readonly tariffGroups!: TariffGroupEntity[];

  constructor(
    obj: Omit<
      TariffEntity,
      'sid' | 'createdAt' | 'updatedAt' | 'teams' | 'tariffGroups'
    >
  ) {
    Object.assign(this, obj);
  }
}
