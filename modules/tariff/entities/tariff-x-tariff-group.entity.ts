import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn
} from 'typeorm';

import { TariffGroupEntity } from './tariff-group.entity';
import { TariffEntity } from './tariff.entity';

@Entity('tariff_x_tariff_group', { schema: 'tariffs' })
export class TariffXTariffGroupEntity {
  @PrimaryColumn({
    name: 'tariff_sid',
    type: 'uuid'
  })
  @OneToOne(() => TariffEntity)
  @JoinColumn({
    name: 'tariff_sid',
    referencedColumnName: 'sid'
  })
  public tariff!: TariffEntity['sid'];

  @PrimaryColumn({
    name: 'tariff_group_sid',
    type: 'uuid'
  })
  @OneToOne(() => TariffGroupEntity)
  @JoinColumn({
    name: 'tariff_group_sid',
    referencedColumnName: 'sid'
  })
  public tariffGroup!: TariffGroupEntity['sid'];

  @Column({
    default: null,
    name: 'call_center_commission',
    nullable: true,
    type: 'real'
  })
  public callCenterCommission?: number | null;

  @Column({
    default: null,
    name: 'client_commission',
    nullable: true,
    type: 'real'
  })
  public clientCommission?: number | null;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp with time zone'
  })
  public readonly updatedAt!: Date;

  constructor(obj: Omit<TariffXTariffGroupEntity, 'createdAt' | 'updatedAt'>) {
    Object.assign(this, obj);
  }
}
