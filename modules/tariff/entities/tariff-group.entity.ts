import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

import { TariffEntity } from './tariff.entity';

@Entity('tariff_group', { schema: 'tariffs' })
export class TariffGroupEntity {
  @PrimaryGeneratedColumn('uuid', {
    name: 'sid'
  })
  public readonly sid!: string;

  @Column({
    length: 255,
    name: 'name',
    type: 'varchar'
  })
  public name!: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp with time zone'
  })
  public readonly updatedAt!: Date;

  @OneToMany(() => TariffEntity, tariff => tariff.tariffGroups)
  public readonly tariffs!: TariffEntity[];

  constructor(obj: Pick<TariffGroupEntity, 'name'>) {
    Object.assign(this, obj);
  }
}
