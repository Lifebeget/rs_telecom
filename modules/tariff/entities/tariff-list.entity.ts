import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { MemberEntity } from 'Server/modules/member/entities';

import { TariffEntity } from './tariff.entity';

@Entity('tariff_list', { schema: 'tariffs' })
export class TariffListEntity {
  @PrimaryGeneratedColumn('uuid', {
    name: 'sid'
  })
  public readonly sid!: string;

  @Column({
    default: null,
    name: 'member_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => MemberEntity)
  @JoinColumn({
    name: 'member_sid',
    referencedColumnName: 'sid'
  })
  public member!: MemberEntity['sid'] | null;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  @OneToMany(() => TariffEntity, tariffListItem => tariffListItem.tariffList)
  public readonly items!: TariffEntity[];
}
