import { Exclude, Expose, Transform } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';

@Exclude()
export class GetTariffListQueryParamsDto {
  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @Expose()
  public readonly search!: string;

  @IsBoolean()
  @IsOptional()
  @Transform(({ value }) => {
    if (value === 'true') {
      return true;
    }

    return false;
  })
  @Expose()
  public readonly excludeArchived?: boolean;

  @IsBoolean()
  @IsOptional()
  // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
  @Transform(({ value }) => {
    if (value === 'true') {
      return true;
    }

    return false;
  })
  @Expose()
  public readonly excludeWithMissingCommission?: boolean;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @Expose()
  public readonly contractSid!: string;
}
