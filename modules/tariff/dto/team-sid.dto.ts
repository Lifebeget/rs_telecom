import { IsUUID } from 'class-validator';

export class TeamSidDto {
  @IsUUID()
  public readonly teamSid!: string;
}
