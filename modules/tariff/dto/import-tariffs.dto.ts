import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsUUID } from 'class-validator';

@Exclude()
export class ImportTariffsDto {
  @ApiProperty({
    description: 'Member Id',
    example: '987c44fa-54ca-4faa-b718-9d0cb90132c4',
    required: true,
    type: String
  })
  @Expose()
  @IsUUID()
  public readonly member!: string;
}
