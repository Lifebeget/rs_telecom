import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

@Exclude()
export class CreateTariffDto {
  @ApiProperty({
    description: 'Tariff name',
    example: 'Alphra Red 2033',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsString()
  public name!: string;

  @ApiProperty({
    description: 'Tariff code',
    example: 'FR544GG',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsString()
  public code!: string;

  @ApiProperty({
    description: 'Tariff price commission',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  @IsNotEmpty()
  @IsNumber()
  public price!: number;
}
