import { IsUUID } from 'class-validator';

export class TariffInGroupSidDto {
  @IsUUID()
  public readonly tariffSid!: string;

  @IsUUID()
  public readonly groupSid!: string;
}
