import { IsUUID } from 'class-validator';

export class TariffSidDto {
  @IsUUID()
  public readonly tariffSid!: string;
}
