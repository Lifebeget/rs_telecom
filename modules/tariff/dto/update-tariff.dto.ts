import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

@Exclude()
export class UpdateTariffDto {
  @ApiProperty({
    description: 'Tariff call center commission',
    example: 10,
    required: false,
    type: Number
  })
  @Expose()
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  callCenterCommission?: number | null;

  @ApiProperty({
    description: 'Tariff client commission',
    example: 10,
    required: false,
    type: Number
  })
  @Expose()
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  clientCommission?: number | null;

  @ApiProperty({
    description: 'Tariff price',
    example: 10,
    required: false,
    type: Number
  })
  @Expose()
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  price?: number | null;

  @ApiProperty({
    description: 'Tariff code',
    example: '9XOXO3',
    nullable: true,
    required: false,
    type: String
  })
  @Expose()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  code?: string | null;

  @ApiProperty({
    description: 'Tariff name',
    example: '9XOXO3',
    required: false,
    type: String
  })
  @Expose()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name?: string | null;
}
