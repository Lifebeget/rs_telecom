import { IsUUID } from 'class-validator';

export class TariffGroupSidDto {
  @IsUUID()
  public readonly groupSid!: string;
}
