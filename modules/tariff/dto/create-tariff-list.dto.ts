import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested
} from 'class-validator';

@Exclude()
class TariffListItem {
  @ApiProperty({
    description: 'Tariff list item name',
    example: 'Tariff',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  name!: string;

  @ApiProperty({
    description: 'Tariff list item price',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  @IsNotEmpty()
  @IsNumber()
  price!: number;

  @ApiProperty({
    description: 'Tariff list item code',
    example: 'KKI93OO',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsString()
  code!: string;

  @ApiProperty({
    description: 'Tariff call center commission',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  @IsNotEmpty()
  @IsNumber()
  callCenterCommission!: number;

  @ApiProperty({
    description: 'Tariff client commission',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  @IsNotEmpty()
  @IsNumber()
  clientCommission!: number;
}

@Exclude()
export class CreateTariffListDto {
  @ApiProperty({
    description: 'Member Id',
    example: '987c44fa-54ca-4faa-b718-9d0cb90132c4',
    required: true,
    type: String
  })
  @Expose()
  @IsUUID()
  public readonly member!: string;

  @ApiProperty({
    description: 'List of tariff items',
    isArray: true,
    required: true,
    type: TariffListItem
  })
  @Expose()
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => TariffListItem)
  public readonly items!: TariffListItem[];
}
