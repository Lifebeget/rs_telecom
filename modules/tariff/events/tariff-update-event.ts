import { TariffEntity } from '../entities';

export class TariffUpdateEvent {
  public static eventName = Symbol('tariff.updated');

  constructor(
    public readonly tariffSid: TariffEntity['sid'],
    public readonly updateObj: Partial<TariffEntity>,
    public readonly tariffBeforeUpdate: TariffEntity
  ) {}
}
