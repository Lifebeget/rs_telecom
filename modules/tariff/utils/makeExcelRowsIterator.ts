import xlsx from 'xlsx';

export function* makeExcelRowsIterator(sheet: xlsx.WorkSheet) {
  const columnNamesSet = new Set<string>();

  let rowsCount = 0;
  for (const p in sheet) {
    const isCellName = !p.startsWith('!');
    if (!isCellName) {
      continue;
    }

    // double-named columns are not supported
    const rowColumnName = p[0];
    const rowIndexRaw = p.slice(1, p.length);
    const rowIndex = parseInt(rowIndexRaw);

    if (!columnNamesSet.has(rowColumnName)) {
      columnNamesSet.add(rowColumnName);
    }

    rowsCount = Math.max(rowsCount, rowIndex);
  }

  const columnNames = Array.from(columnNamesSet);
  for (let row = 0; row < rowsCount; row++) {
    const rowObj: Record<string, xlsx.CellObject | undefined> = {};

    for (const column of columnNames) {
      const cellName = column + (row + 1);
      rowObj[column] = sheet[cellName];
    }

    yield { index: row, rowObj };
  }
}
