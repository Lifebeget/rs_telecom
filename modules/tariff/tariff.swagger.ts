import { applyDecorators } from '@nestjs/common';
import {
  ApiBody,
  ApiConsumes,
  ApiParam,
  ApiQuery,
  ApiResponse
} from '@nestjs/swagger';

import { CreateTariffDto } from './dto';
import { TariffListModel } from './models';
import { TariffGroupModel } from './models/tariff-group.model';

export function TariffSwaggerPost() {
  return applyDecorators(
    ApiResponse({
      description: 'Tariff list',
      status: 200,
      type: TariffListModel
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

// eslint-disable-next-line sonarjs/no-identical-functions -- swagger equal scheme
export function TariffSwaggerPut() {
  return applyDecorators(
    ApiResponse({
      description: 'Tariff list',
      status: 200,
      type: TariffListModel
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffImportSwaggerPost() {
  return applyDecorators(
    ApiConsumes('multipart/form-data'),
    ApiResponse({
      description: 'Tariff list',
      status: 200,
      type: TariffListModel
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffSwaggerGet() {
  return applyDecorators(
    ApiResponse({
      description: 'Tariff list',
      status: 200,
      type: TariffListModel
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    }),
    ApiQuery({ name: 'search', required: false })
  );
}

export function TariffSwaggerCommissionPut() {
  return applyDecorators(
    ApiResponse({
      description: 'Tariff item',
      status: 200
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffSwaggerCommissionDelete() {
  return applyDecorators(
    ApiResponse({
      description: 'Status',
      status: 200
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffSwaggerAddPost() {
  return applyDecorators(
    ApiBody({
      type: CreateTariffDto
    }),
    ApiResponse({
      description: 'Status',
      status: 200
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffSwaggerArchivePost() {
  return applyDecorators(
    ApiParam({
      description: 'TariffSid',
      name: 'tariffSid',
      type: String
    }),
    ApiResponse({
      description: 'Status',
      status: 200
    }),
    ApiResponse({
      description: 'Not found',
      status: 404
    })
  );
}

export function TariffGroupsSwaggerGet() {
  return applyDecorators(
    ApiResponse({
      description: 'Tariff groups',
      status: 200,
      type: [TariffGroupModel]
    })
  );
}
