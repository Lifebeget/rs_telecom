import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MemberModule } from '../member';
import { TeamModule } from '../team';
import { ContractModule } from '../contract';

import { TariffController } from './tariff.controller';
import {
  TariffGroupRepository,
  TariffListRepository,
  TariffRepository,
  TariffXServiceRepository,
  TariffXTariffGroupRepository
} from './repositories';
import {
  TariffCommonService,
  TariffListService,
  TariffSynchronizeService
} from './services';

@Module({
  controllers: [TariffController],
  exports: [
    TariffCommonService,
    TariffListService,
    TariffSynchronizeService,
    TypeOrmModule
  ],
  imports: [
    TypeOrmModule.forFeature([
      TariffListRepository,
      TariffRepository,
      TariffXServiceRepository,
      TariffGroupRepository,
      TariffXTariffGroupRepository
    ]),
    MemberModule,
    TeamModule,
    forwardRef(() => ContractModule)
  ],
  providers: [TariffCommonService, TariffListService, TariffSynchronizeService]
})
export class TariffModule {}
