import { NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { EntityRepository } from 'typeorm';

import { Errors } from 'Constants';
import { TeamEntity } from 'Server/modules/team/entities';
import { BaseRepository } from 'Server/repositories';

import {
  TariffEntity,
  TariffListEntity,
  TariffXTariffGroupEntity
} from '../entities';
import { TariffModel } from '../models';

@EntityRepository(TariffEntity)
export class TariffRepository extends BaseRepository<TariffEntity> {
  async getTariff(sid: string): Promise<TariffEntity> {
    const tariff = await this.createQueryBuilder('t')
      .leftJoin(TariffListEntity, 'tl', 't.tariff_list_sid = tl.sid')
      .where('t.sid = :sid', { sid })
      .orderBy('tl.created_at', 'DESC')
      .getOne();

    if (!tariff) {
      throw new NotFoundException(Errors.TARIFF_DOES_NOT_EXIST);
    }

    return tariff;
  }

  async getTariffOverrided(sid: string, teamSid: string): Promise<TariffModel> {
    const tariff = await this.createQueryBuilder('t')
      .leftJoin(TariffListEntity, 'tl', 't.tariff_list_sid = tl.sid')
      .leftJoin(TeamEntity, 'team', 'team.sid = :teamSid', { teamSid })
      .leftJoinAndMapOne(
        't.override',
        TariffXTariffGroupEntity,
        'txg',
        'txg.tariff_sid = t.sid AND txg.tariff_group_sid = team.tariff_group_sid'
      )
      .where('t.sid = :sid', { sid })
      .orderBy('tl.created_at', 'DESC')
      .getOne();

    if (!tariff) {
      throw new NotFoundException(Errors.TARIFF_DOES_NOT_EXIST);
    }

    return plainToClass(TariffModel, tariff);
  }

  async findOneActualTariff(
    params: Partial<Pick<TariffEntity, 'code' | 'name'>>
  ): Promise<TariffEntity | undefined> {
    const qb = this.createQueryBuilder('t')
      .leftJoin(TariffListEntity, 'tl', 't.tariff_list_sid = tl.sid')
      .orderBy('tl.created_at', 'DESC');

    if ('code' in params) {
      qb.andWhere('t.code = :code', { code: params.code });
    }

    if ('name' in params) {
      qb.andWhere('t.name = :name', { name: params.name });
    }

    return qb.getOne();
  }
}
