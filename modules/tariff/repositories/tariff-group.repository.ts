import { EntityRepository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';

import { BaseRepository } from 'Server/repositories';
import { TariffError } from 'Constants/errorValues';

import { TariffGroupEntity } from '../entities';

@EntityRepository(TariffGroupEntity)
export class TariffGroupRepository extends BaseRepository<TariffGroupEntity> {
  async getTariffGroupByName(name: string) {
    const group = await this.findOne({ name });

    if (!group) {
      throw new NotFoundException(TariffError.NO_SUCH_TARIFF_GROUP);
    }

    return group;
  }

  async getTariffGroupBySid(sid: string) {
    const group = await this.findOne(sid);

    if (!group) {
      throw new NotFoundException(TariffError.NO_SUCH_TARIFF_GROUP);
    }

    return group;
  }
}
