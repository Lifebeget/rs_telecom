import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { TariffXTariffGroupEntity } from '../entities';

@EntityRepository(TariffXTariffGroupEntity)
export class TariffXTariffGroupRepository extends BaseRepository<TariffXTariffGroupEntity> {}
