import { NotFoundException } from '@nestjs/common';
import { Brackets, EntityRepository, SelectQueryBuilder } from 'typeorm';

import {
  ContractRequestStatus,
  ContractRequestType,
  Errors,
  ExtensionVariant
} from 'Constants';
import {
  ContractAvailableTariffEntity,
  ContractEntity,
  ContractRequestHistoryEntity
} from 'Server/modules/contract/entities';
import { BaseRepository } from 'Server/repositories';

import { TariffListEntity, TariffXTariffGroupEntity } from '../entities';

@EntityRepository(TariffListEntity)
export class TariffListRepository extends BaseRepository<TariffListEntity> {
  async getActualTariffListByGroup({
    groupSid,
    search,
    excludeArchived,
    excludeWithMissingCommission,
    contractSid
  }: {
    groupSid?: string;
    search?: string;
    excludeArchived?: boolean;
    excludeWithMissingCommission?: boolean; // exclude null and negative
    contractSid?: string; // return only with tariff codes able to extend with
  } = {}): Promise<TariffListEntity> {
    const tariffListLast = await this.findOneOrFail(undefined, {
      order: { createdAt: 'DESC' }
    });

    const qb = this.createQueryBuilder('t')
      .leftJoinAndSelect('t.member', 'm')
      .leftJoinAndSelect('m.user', 'u')
      .leftJoinAndSelect('t.items', 'i')
      .leftJoinAndMapOne(
        'i.override',
        TariffXTariffGroupEntity,
        'txg',
        'txg.tariff_sid = i.sid AND txg.tariff_group_sid = :groupSid',
        { groupSid }
      )
      .where('t.sid = :tariffListLastSid', {
        tariffListLastSid: tariffListLast.sid
      });

    if (search) {
      qb.andWhere(
        `LOWER(i.name) like LOWER(:search) OR LOWER(i.code) like LOWER(:search)`,
        {
          search: `%${search}%`
        }
      );
    }

    if (excludeArchived) {
      qb.andWhere('i.archived = false');
    }

    if (excludeWithMissingCommission) {
      qb.andWhere(`
        txg.tariff_sid is null
        OR (
          txg.call_center_commission is not null and txg.call_center_commission >= 0
          AND txg.client_commission is not null and txg.client_commission >= 0
        )
      `);
    }

    if (contractSid) {
      qb.leftJoin(ContractEntity, 'contract', 'contract.sid = :contractSid', {
        contractSid
      })
        .leftJoin('contract.extension', 'extensionVariant')
        .leftJoin('contract.tariff', 'contractTariff')
        .leftJoin(
          ContractAvailableTariffEntity,
          'availableTariff',
          'availableTariff.contract_sid = :contractSid',
          { contractSid }
        )
        .andWhere(
          new Brackets(sqb =>
            sqb
              .andWhere(
                new Brackets(ssqb => {
                  ssqb
                    .where((sssqb: SelectQueryBuilder<ContractEntity>) => {
                      const countSubQuery = this.countSuccessXMLRequestsSubQuery(
                        sssqb,
                        contractSid
                      );

                      return `(${countSubQuery}) = 0`;
                    })
                    .orWhere('i.code = availableTariff.code');
                })
              )
              .andWhere(
                new Brackets(ssqb =>
                  ssqb
                    .where((sssqb: SelectQueryBuilder<ContractEntity>) => {
                      const countSubQuery = this.countSuccessXMLRequestsSubQuery(
                        sssqb,
                        contractSid
                      );

                      return `(${countSubQuery}) > 0`;
                    })
                    .orWhere('contractTariff.sid is null')
                    .orWhere('contractTariff.price < i.price')
                    .orWhere(
                      'extensionVariant.name != :contractStatusEarlyExtension',
                      {
                        contractStatusEarlyExtension:
                          ExtensionVariant.earlyExtension
                      }
                    )
                )
              )
              .orWhere('i.sid = contract.tariff_sid')
          )
        );
    }

    qb.orderBy('i.name', 'ASC').addOrderBy('i.createdAt', 'ASC');

    const tariffList = await qb.getOne();

    if (!tariffList) {
      throw new NotFoundException(Errors.ACTUAL_TARIFF_LIST_DOES_NOT_EXIST);
    }

    return tariffList;
  }

  private countSuccessXMLRequestsSubQuery(
    sqb: SelectQueryBuilder<ContractEntity>,
    contractSid: string
  ) {
    const qbCount = sqb
      .addSelect('COUNT(*)')
      .from(ContractRequestHistoryEntity, 'xmlRequestHistory')
      .where('xmlRequestHistory.contract_sid = :contractSid', {
        contractSid
      })
      .andWhere('xmlRequestHistory.request_type = :requestType', {
        requestType: ContractRequestType.Xml
      })
      .andWhere('xmlRequestHistory.request_status = :requestStatus', {
        requestStatus: ContractRequestStatus.Successful
      });

    return qbCount.getQuery();
  }

  async getActualTariffListGlobal({
    name,
    excludeArchived,
    excludeWithMissingCommission
  }: {
    name?: string;
    excludeArchived?: boolean;
    excludeWithMissingCommission?: boolean; // exclude null and negative
  } = {}): Promise<TariffListEntity> {
    const qb = this.createQueryBuilder('t')
      .leftJoinAndSelect('t.member', 'm')
      .leftJoinAndSelect('m.user', 'u')
      .leftJoinAndSelect('t.items', 'i');

    if (name) {
      qb.where('LOWER(i.name) like LOWER(:name)', {
        name: '%' + name + '%'
      });
      qb.orWhere('LOWER(i.code) like LOWER(:name)', {
        name: '%' + name + '%'
      });
    }

    if (excludeArchived) {
      qb.andWhere('i.archived = false');
    }

    if (excludeWithMissingCommission) {
      // tariff.commission is not null and not negative
      qb.andWhere(`(        
        i.client_commission is not null or i.client_commission >= 0
        OR i.client_commission is not null or i.client_commission >= 0
      )`);
    }

    qb.orderBy('t.createdAt', 'DESC')
      .addOrderBy('i.name', 'ASC')
      .addOrderBy('i.createdAt', 'ASC');

    const tariffList = await qb.getOne();

    if (!tariffList) {
      throw new NotFoundException(Errors.ACTUAL_TARIFF_LIST_DOES_NOT_EXIST);
    }

    return tariffList;
  }

  async getCurrentTariffList(): Promise<TariffListEntity> {
    const qb = this.createQueryBuilder('t').orderBy('t.createdAt', 'DESC');

    const tariffList = await qb.getOne();

    if (!tariffList) {
      throw new NotFoundException(Errors.ACTUAL_TARIFF_LIST_DOES_NOT_EXIST);
    }

    return tariffList;
  }
}
