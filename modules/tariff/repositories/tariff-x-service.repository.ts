import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { TariffXServiceEntity } from '../entities';

@EntityRepository(TariffXServiceEntity)
export class TariffXServiceRepository extends BaseRepository<TariffXServiceEntity> {}
