import { ValueOf } from 'Utils';
import { TariffGroupName } from 'Constants';

export const TariffColumnType = {
  Code: 'Code',
  CommissionCallCenter: 'CommissionCallCenter',
  CommissionClient: 'CommissionClient',
  Name: 'Name',
  Price: 'Price'
} as const;

export type TariffColumnType = ValueOf<typeof TariffColumnType>;

export const TariffColumnName = {
  A: 'A',
  B: 'B',
  C: 'C',
  D: 'D',
  E: 'E',
  F: 'F',
  G: 'G',
  H: 'H',
  I: 'I',
  J: 'J',
  K: 'K',
  L: 'L',
  M: 'M',
  N: 'N',
  O: 'O',
  P: 'P',
  Q: 'Q'
} as const;

export type TariffColumnName = ValueOf<typeof TariffColumnName>;

export const TARIFF_GROUP_NAMES: TariffGroupName[] = Object.values(
  TariffGroupName
);

export const COLUMN_TYPE_TO_NAME = {
  [TariffColumnType.Name]: TariffColumnName.A,
  [TariffColumnType.Code]: TariffColumnName.B,
  [TariffColumnType.Price]: TariffColumnName.C
} as const;

export const TARIFF_GROUP_TO_NAME_COMMISSION_CALL_CENTER: Record<
  TariffGroupName,
  TariffColumnName
> = {
  [TariffGroupName.Spezial]: TariffColumnName.D,
  [TariffGroupName.S]: TariffColumnName.E,
  [TariffGroupName.S1]: TariffColumnName.F,
  [TariffGroupName.S15]: TariffColumnName.G,
  [TariffGroupName.S2]: TariffColumnName.H,
  [TariffGroupName.S3]: TariffColumnName.I,
  [TariffGroupName.ICC]: TariffColumnName.J
};

export const TARIFF_GROUP_TO_NAME_COMMISSION_CLIENT: Record<
  TariffGroupName,
  TariffColumnName
> = {
  [TariffGroupName.Spezial]: TariffColumnName.K,
  [TariffGroupName.S]: TariffColumnName.L,
  [TariffGroupName.S1]: TariffColumnName.M,
  [TariffGroupName.S15]: TariffColumnName.N,
  [TariffGroupName.S2]: TariffColumnName.O,
  [TariffGroupName.S3]: TariffColumnName.P,
  [TariffGroupName.ICC]: TariffColumnName.Q
};
