import fs from 'fs';
import path from 'path';

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { RedisModule } from 'nestjs-redis';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BullModule } from '@nestjs/bull';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { getConnectionOptions } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

import { RedisClientType } from 'Server/constants';
import { ExceptionsFilter } from 'Server/filters';
import { RequestMiddleware } from 'Server/middlewares';
import {
  ContextModule,
  LoggerModule,
  MailerModule,
  MulterModule,
  OperatorModule,
  RabbitMQModule,
  SmsModule,
  StorageModule,
  TelegramModule,
  TwilioModule
} from 'Server/providers';

import { ActivityModule } from './activity';
import { AuthModule } from './auth';
import { BookingModule } from './booking';
import { ClientModule } from './client';
import { ContractModule } from './contract';
import { CronModule } from './cron';
import { DashboardModule } from './dashboard';
import { FlowModule } from './flow';
import { HardwareModule } from './hardware';
import { HealthCheckModule } from './healthcheck';
import { LeadModule } from './lead';
import { MemberModule } from './member';
import { PhoneModule } from './phone';
import { PromoterScheduleModule } from './promoter-shedule';
import { RoleModule } from './role';
import { ServiceModule } from './service';
import { ShopModule } from './shop';
import { TariffModule } from './tariff';
import { TeamModule } from './team';
import { TelephonyModule } from './telephony';
import { UploadModule } from './upload';
import { UserModule } from './user';
import { UserTrackingModule } from './user-tracking';
import { VpknModule } from './vpkn';
import { PropertiesModule } from './properties';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    // core modules
    BullModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        redis: {
          host: configService.get('REDIS_HOST'),
          port: Number(configService.get('REDIS_PORT'))
        },
        settings: {
          lockDuration: 1000 * 60 * 6,
          maxStalledCount: 5, // 6 minutes
          stalledInterval: 1000 * 60 * 6 // 6 minutes
        }
      })
    }),
    ContextModule.forRoot(),
    EventEmitterModule.forRoot({
      global: true,
      ignoreErrors: true,
      verboseMemoryLeak: true
    }),
    MulterModule,
    RedisModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const commonConfig = {
          host: configService.get('REDIS_HOST'),
          port: Number(configService.get('REDIS_PORT'))
        };

        return [
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.accessToken}:`,
            name: RedisClientType.accessToken
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.fileToken}:`,
            name: RedisClientType.fileToken
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.refreshToken}:`,
            name: RedisClientType.refreshToken
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.verifyToken}:`,
            name: RedisClientType.verifyToken
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.globalLimit}:`,
            name: RedisClientType.globalLimit
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.teamLimit}:`,
            name: RedisClientType.teamLimit
          },
          {
            ...commonConfig,
            keyPrefix: `${RedisClientType.memberLimit}:`,
            name: RedisClientType.memberLimit
          }
        ];
      }
    }),
    ThrottlerModule.forRoot({
      limit: 100,
      ttl: 60
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const options = (await getConnectionOptions()) as PostgresConnectionOptions;
        const ssl =
          configService.get<string>('POSTGRES_SSL', 'false') === 'true';

        return {
          ...options,
          autoLoadEntities: true,
          keepConnectionAlive: true,
          migrationsTransactionMode: 'each',
          ssl: ssl && {
            ca: fs
              .readFileSync(path.join(process.cwd(), 'ca-certificate.crt'))
              .toString()
          }
        };
      }
    }),

    // common modules
    LoggerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        level: configService.get<string>('LOGGER_LEVEL', 'error')
      })
    }),
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        auth: {
          pass: configService.get('MAILER_PASSWORD'),
          user: configService.get('MAILER_USER')
        },
        service: configService.get('MAILER_SERVICE')
      })
    }),
    SmsModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        accessKey: configService.get('MESSAGE_BIRD_ACCESS_KEY')
      })
    }),
    TelegramModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        apiKey: configService.get('TELEGRAM_API_KEY'),
        chatId: configService.get('TELEGRAM_CHANNEL_CHAT_ID')
      })
    }),
    OperatorModule.forRoot(),
    RabbitMQModule,
    StorageModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        accessKeyId: configService.get<string>('AWS_ACCESS_KEY_ID', ''),
        endpoint: configService.get<string>('AWS_ENDPOINT', ''),
        secretAccessKey: configService.get<string>('AWS_SECRET_ACCESS_KEY', '')
      })
    }),
    TelephonyModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        applicationId: configService.get<number>('VOXIMPLANT_APPLICATION_ID'),
        credentialsPath: configService.get('VOXIMPLANT_CREDENTIALS_PATH')
      })
    }),
    TwilioModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        accountSid: configService.get('TWILIO_ACCOUNT_SID'),
        authToken: configService.get('TWILIO_AUTH_TOKEN')
      })
    }),
    TelegramModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        apiKey: configService.get('TELEGRAM_API_KEY'),
        chatId: configService.get('TELEGRAM_CHANNEL_CHAT_ID')
      })
    }),

    // application modules
    ActivityModule,
    AuthModule,
    BookingModule,
    ClientModule,
    ContractModule,
    CronModule,
    DashboardModule,
    FlowModule,
    HardwareModule,
    HealthCheckModule,
    LeadModule,
    MemberModule,
    PhoneModule,
    PromoterScheduleModule,
    PropertiesModule,
    RoleModule,
    ServiceModule,
    ShopModule,
    TariffModule,
    TeamModule,
    UploadModule,
    UserModule,
    UserTrackingModule,
    VpknModule
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: ExceptionsFilter
    },
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard
    }
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestMiddleware).forRoutes('*');
  }
}
