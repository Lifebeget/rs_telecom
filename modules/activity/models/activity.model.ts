import { ApiExtraModels, ApiProperty, getSchemaPath } from '@nestjs/swagger';
import {
  Exclude,
  Expose,
  Transform,
  Type,
  plainToClass
} from 'class-transformer';
import { IsUUID } from 'class-validator';

import { ActivityType } from 'Constants';
import { ClientNoteModel } from 'Server/modules/client/models';
import { MemberModel } from 'Server/modules/member/models';
import { TeamModel } from 'Server/modules/team/models';
import { UserModel } from 'Server/modules/user/models';
import { notReachable } from 'Utils';

@Exclude()
export class ActivityClientCreatedPayloadModel {
  @ApiProperty({
    description: 'Client sid',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true,
    type: String
  })
  @Expose()
  public readonly client!: string;
}

@Exclude()
export class ActivityNoteCreatedPayloadModel {
  @ApiProperty({
    description: 'Note',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true,
    type: ClientNoteModel
  })
  @Expose()
  @Type(() => ClientNoteModel)
  public readonly note!: ClientNoteModel;
}

@Exclude()
export class ActivityContractPayloadModel {
  @ApiProperty({
    description: 'Contract Id',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true,
    type: String
  })
  @Expose()
  public readonly contract!: string;
}

@Exclude()
export class ActivityContractMovedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'From team',
    required: true
  })
  @Expose()
  public readonly teamFrom!: TeamModel;

  @ApiProperty({
    description: 'To team',
    required: true
  })
  @Expose()
  @Type(() => TeamModel)
  public readonly teamTo!: TeamModel;
}

@Exclude()
export class ActivityContractAssignedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'Assigned to member',
    required: true
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly memberTo!: MemberModel;
}

@Exclude()
export class ActivityContractAssigneeChangedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'Assigned to member',
    required: true
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly memberTo!: MemberModel;

  @ApiProperty({
    description: 'Unassigned from member',
    required: true
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly memberFrom!: MemberModel;
}

@Exclude()
export class ActivityContractStatusChangedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'From status sid',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    nullable: true,
    required: true,
    type: String
  })
  @Expose()
  public readonly statusFrom!: string | null;

  @ApiProperty({
    description: 'To status sid',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true,
    type: String
  })
  @Expose()
  public readonly statusTo!: string;
}

@Exclude()
export class ActivityContractCreatedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'Status sid',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true,
    type: String
  })
  @Expose()
  public readonly status!: string;

  @ApiProperty({
    description: 'Contract is imported',
    example: 'true',
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isImported!: boolean;
}

@Exclude()
export class ActivityLeadCreatedPayloadModel {
  @ApiProperty({
    description: 'Lead sid',
    example: '2d17c8eb-c820-4313-a1ac-1c741282ce9a',
    required: true
  })
  @Expose()
  public readonly lead!: string;
}

@Exclude()
export class ActivityContractExtensionVariantChangedPayloadModel extends ActivityContractPayloadModel {
  @ApiProperty({
    description: 'From extension variant sid',
    example: '6323ba58-45d8-11ec-832a-7bb73410ed83',
    required: true,
    type: String
  })
  @Expose()
  public readonly extensionVariantFrom!: string;

  @ApiProperty({
    description: 'To extension variant sid',
    example: '78b43d2a-45d8-11ec-9b1e-93b81552d0b1',
    required: true,
    type: String
  })
  @Expose()
  public readonly extensionVariantTo!: string;
}

@Exclude()
@ApiExtraModels(
  ActivityClientCreatedPayloadModel,
  ActivityContractAssignedPayloadModel,
  ActivityContractCreatedPayloadModel,
  ActivityContractExtensionVariantChangedPayloadModel,
  ActivityContractMovedPayloadModel,
  ActivityContractPayloadModel,
  ActivityContractStatusChangedPayloadModel,
  ActivityLeadCreatedPayloadModel,
  ActivityNoteCreatedPayloadModel
)
export class ActivityModel {
  @ApiProperty({
    description: 'Activity Id',
    example: 'feffe0d3-68d7-4d08-ac9f-cd231cddc093',
    required: true,
    type: String
  })
  @Expose()
  public readonly sid!: string;

  @ApiProperty({
    description: 'Activity type',
    enum: ActivityType,
    example: ActivityType.ContractCreated,
    required: true
  })
  @Expose()
  @Transform(({ value }) => value.name)
  public readonly type!: ActivityType;

  @ApiProperty({
    description: 'Member Id',
    example: 'b083b3ed-8a44-4ac2-bb95-877f4d127144',
    required: true,
    type: String
  })
  @Expose()
  @IsUUID()
  public readonly member!: string;

  @ApiProperty({
    description: 'Team Id',
    example: 'b083b3ed-8a44-4ac2-bb95-877f4d127144',
    required: true,
    type: String
  })
  @Expose()
  @IsUUID()
  public readonly team!: string;

  @ApiProperty({
    description: 'Activity payload',
    example: {
      contract: '23ba9bf2-8104-4d3b-b2c3-a5978e1d7ed0'
    },
    oneOf: [
      { $ref: getSchemaPath(ActivityClientCreatedPayloadModel) },
      { $ref: getSchemaPath(ActivityContractAssignedPayloadModel) },
      { $ref: getSchemaPath(ActivityContractCreatedPayloadModel) },
      {
        $ref: getSchemaPath(ActivityContractExtensionVariantChangedPayloadModel)
      },
      { $ref: getSchemaPath(ActivityContractMovedPayloadModel) },
      { $ref: getSchemaPath(ActivityContractPayloadModel) },
      { $ref: getSchemaPath(ActivityContractStatusChangedPayloadModel) },
      { $ref: getSchemaPath(ActivityLeadCreatedPayloadModel) },
      { $ref: getSchemaPath(ActivityNoteCreatedPayloadModel) }
    ],
    required: true
  })
  @Expose()
  @Transform(({ obj }) => {
    const type = obj.type.name as ActivityType;
    switch (type) {
      case ActivityType.ClientCreated:
        return plainToClass(ActivityClientCreatedPayloadModel, obj);
      case ActivityType.ContractNoteAdded:
        return plainToClass(ActivityNoteCreatedPayloadModel, obj);
      case ActivityType.ContractMoved:
        return plainToClass(ActivityContractMovedPayloadModel, {
          ...obj,
          ...obj.moved
        });
      case ActivityType.ContractNoteAdded:
        return plainToClass(ActivityNoteCreatedPayloadModel, obj);
      case ActivityType.ContractFlowFinished:
      case ActivityType.ContractStatusChanged:
        return plainToClass(ActivityContractStatusChangedPayloadModel, {
          ...obj,
          ...obj.contract_status_changed
        });
      case ActivityType.ContractAssigned:
        return plainToClass(ActivityContractAssignedPayloadModel, {
          ...obj,
          ...obj.assigned
        });
      case ActivityType.LeadCreated:
        return plainToClass(ActivityLeadCreatedPayloadModel, obj);

      case ActivityType.ContractCreated:
        return plainToClass(ActivityContractCreatedPayloadModel, {
          ...obj,
          ...obj.contract_created
        });

      case ActivityType.ContractPasswordVerified:
      case ActivityType.ContractUpdatedXML:
      case ActivityType.ContractUpdatedPassword:
      case ActivityType.ContractWrongPassword:
        return plainToClass(ActivityContractPayloadModel, obj);

      case ActivityType.ContractExtensionVariantChanged:
        return plainToClass(
          ActivityContractExtensionVariantChangedPayloadModel,
          {
            ...obj,
            ...obj.contract_extension_changed
          }
        );

      case ActivityType.ContractReturnedForClarification:
      case ActivityType.ContractFlagRemoved:
      case ActivityType.ContractFlagAdded:
        throw new Error('not implemented');
      default:
        return notReachable(type);
    }
  })
  public readonly payload!:
    | ActivityClientCreatedPayloadModel
    | ActivityContractAssignedPayloadModel
    | ActivityContractCreatedPayloadModel
    | ActivityContractExtensionVariantChangedPayloadModel
    | ActivityContractMovedPayloadModel
    | ActivityContractPayloadModel
    | ActivityContractStatusChangedPayloadModel
    | ActivityLeadCreatedPayloadModel
    | ActivityNoteCreatedPayloadModel;

  @ApiProperty({
    description: 'Created date',
    example: '2000-01-31T21:00:00.000Z',
    nullable: true,
    required: false,
    type: Date
  })
  @Expose()
  public readonly createdAt!: Date;

  @ApiProperty({
    description: 'Assigned User',
    nullable: true,
    required: true,
    type: UserModel
  })
  @Expose({ name: 'user' })
  @Type(() => UserModel)
  public readonly assignedUser!: UserModel | null;

  @ApiProperty({
    nullable: true,
    required: true,
    type: TeamModel
  })
  @Expose({ name: 'teamRel' })
  @Type(() => TeamModel)
  public readonly teamRel!: TeamModel | null;
}
