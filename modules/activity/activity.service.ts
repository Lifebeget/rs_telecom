import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';

import {
  ACTIVITY_CREATED_OFFSET_CONTRACT_ASSIGNMENT,
  Role,
  TeamType
} from 'Constants';

import { ClientNoteRepository, ClientRepository } from '../client/repositories';
import { ContractRepository } from '../contract/repositories';
import { ContractAssignmentService } from '../contract/services';
import { MemberService } from '../member/member.service';
import { TeamEntity } from '../team/entities';

import { ActivityModel } from './models';
import { ActivityRepository } from './repositoties';

@Injectable()
export class ActivityService {
  constructor(
    private readonly activityRepository: ActivityRepository,
    private readonly clientNoteRepository: ClientNoteRepository,
    private readonly clientRepository: ClientRepository,
    private readonly contractRepository: ContractRepository,
    private readonly memberService: MemberService,
    private readonly contractAssignmentService: ContractAssignmentService
  ) {}

  private async getMemberTeamCC(userSid: string): Promise<TeamEntity | null> {
    const member = await this.memberService.getMemberByUserSid(userSid);

    return member.teamRel?.type === TeamType.CC ? member.teamRel : null;
  }

  async getActivityByClient(
    clientSid: string,
    userSid: string
  ): Promise<ActivityModel[]> {
    await this.clientRepository.getClient(clientSid);

    const team = await this.getMemberTeamCC(userSid);

    return plainToClass(
      ActivityModel,
      await this.activityRepository.getActivityListByClient(
        clientSid,
        team?.sid
      )
    );
  }

  async getActivityByNote(
    noteSid: string,
    userSid: string
  ): Promise<ActivityModel[]> {
    await this.clientNoteRepository.getClientNote(noteSid);

    const team = await this.getMemberTeamCC(userSid);

    return plainToClass(
      ActivityModel,
      await this.activityRepository.getActivityListByNote(noteSid, team?.sid)
    );
  }

  private async getActivityListByContractModel(
    ...params: Parameters<ActivityRepository['getActivityListByContract']>
  ) {
    return plainToClass(
      ActivityModel,
      await this.activityRepository.getActivityListByContract(...params)
    );
  }

  async getActivityByContract(
    contractSid: string,
    userSid: string
  ): Promise<ActivityModel[]> {
    await this.contractRepository.getContract(contractSid);

    const member = await this.memberService.getMemberByUserSid(userSid);

    if (member.teamRel?.type !== TeamType.CC) {
      return this.getActivityListByContractModel({ contractSid });
    }

    const rolesToGetByTeam: string[] = [
      Role['callcenter admin'],
      Role['callcenter teamlead']
    ];
    if (rolesToGetByTeam.includes(member.role.name)) {
      return this.getActivityListByContractModel({
        contractSid,
        teamSid: member.team
      });
    }

    const isLocked = await this.contractAssignmentService.isLockedByMember({
      contractSid,
      memberSid: member.sid
    });
    if (!isLocked) {
      return this.getActivityListByContractModel({
        contractSid,
        memberSid: member.sid
      });
    }

    const assignmentEntity = await this.contractAssignmentService.getAssignmentMember(
      { contractSid, memberSid: member.sid }
    );

    const createdFrom = DateTime.fromJSDate(assignmentEntity.createdAt, {
      zone: 'utc'
    })
      .minus({ second: ACTIVITY_CREATED_OFFSET_CONTRACT_ASSIGNMENT })
      .toISO();

    return this.getActivityListByContractModel({
      contractSid,
      createdAtFrom: createdFrom
    });
  }
}
