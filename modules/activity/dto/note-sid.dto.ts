import { IsUUID } from 'class-validator';

export class NoteSidDto {
  @IsUUID()
  public readonly noteSid!: string;
}
