import { IsUUID } from 'class-validator';

export class ContractSidDto {
  @IsUUID()
  public readonly contractSid!: string;
}
