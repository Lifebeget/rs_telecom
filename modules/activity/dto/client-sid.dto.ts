import { IsUUID } from 'class-validator';

export class ClientSidDto {
  @IsUUID()
  public readonly clientSid!: string;
}
