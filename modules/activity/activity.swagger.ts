import { applyDecorators } from '@nestjs/common';
import { ApiParam, ApiResponse } from '@nestjs/swagger';

import { ActivityModel } from './models';

export function ActivitySwaggerClient() {
  return applyDecorators(
    ApiParam({
      description: 'Note Id',
      example: 'c6d82ef9-aaf8-4947-9731-a5f2ef3d56df',
      name: 'noteSid',
      required: true,
      type: String
    }),
    ApiResponse({
      description: 'Note activity',
      status: 200,
      type: [ActivityModel]
    }),
    ApiResponse({
      description: 'Not Found',
      status: 404
    })
  );
}

export function ActivitySwaggerNote() {
  return applyDecorators(
    ApiParam({
      description: 'Note Id',
      example: 'c6d82ef9-aaf8-4947-9731-a5f3ef3d56df',
      name: 'noteSid',
      required: true,
      type: String
    }),
    ApiResponse({
      description: 'Note activity',
      status: 200,
      type: [ActivityModel]
    }),
    ApiResponse({
      description: 'Not Found',
      status: 404
    })
  );
}

export function ActivitySwaggerContract() {
  return applyDecorators(
    ApiParam({
      description: 'Contract Id',
      example: '8d709c75-afee-46fe-91d8-422dc5c9214b',
      name: 'contractSid',
      required: true,
      type: String
    }),
    ApiResponse({
      description: 'Contract activity',
      status: 200,
      type: [ActivityModel]
    }),
    ApiResponse({
      description: 'Not Found',
      status: 404
    })
  );
}
