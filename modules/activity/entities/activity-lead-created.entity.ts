import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { EnumValues } from 'enum-values';

import { LeadCreationMethod } from 'Constants/LeadCreationMethod';

import { ActivityEntity } from './activity.entity';

@Entity('activity_lead_created', { schema: 'activities' })
export class ActivityLeadCreatedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    enum: EnumValues.getValues(LeadCreationMethod),
    name: 'method',
    type: 'enum'
  })
  public method!: LeadCreationMethod;
}
