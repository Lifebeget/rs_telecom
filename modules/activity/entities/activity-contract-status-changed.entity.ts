import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { ContractStatusEntity } from 'Server/modules/workflow/entities';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_status_changed', { schema: 'activities' })
export class ActivityContractStatusChangedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    name: 'status_from_sid',
    nullable: true,
    type: 'varchar'
  })
  @OneToOne(() => ContractStatusEntity)
  @JoinColumn({
    name: 'status_from_sid',
    referencedColumnName: 'sid'
  })
  public statusFrom!: string | null;

  @Column({
    name: 'status_to_sid',
    type: 'varchar'
  })
  @OneToOne(() => ContractStatusEntity)
  @JoinColumn({
    name: 'status_to_sid',
    referencedColumnName: 'sid'
  })
  public statusTo!: string;
}
