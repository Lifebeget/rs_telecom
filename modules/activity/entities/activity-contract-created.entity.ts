import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_created', { schema: 'activities' })
export class ActivityContractCreatedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    length: 255,
    name: 'status_sid',
    type: 'varchar'
  })
  public status!: string;

  @Column({
    name: 'is_imported',
    type: 'boolean'
  })
  public isImported!: boolean;
}
