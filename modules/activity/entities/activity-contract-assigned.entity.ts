import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_assigned', { schema: 'activities' })
export class ActivityContractAssignedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    length: 255,
    name: 'member_to_sid',
    type: 'varchar'
  })
  public memberTo!: string;
}
