import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { MemberEntity } from 'Server/modules/member/entities';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_unassigned', { schema: 'activities' })
export class ActivityContractUnassignedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    name: 'member_from_sid',
    type: 'varchar'
  })
  @OneToOne(() => MemberEntity)
  @JoinColumn({
    name: 'member_from_sid',
    referencedColumnName: 'sid'
  })
  public memberFrom!: string;
}
