import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

import { ActivityType } from 'Constants';

@Entity('dict_activity_type', { schema: 'activities' })
export class ActivityTypeEntity {
  @PrimaryGeneratedColumn('uuid', {
    name: 'sid'
  })
  public readonly sid!: string;

  @Column({
    length: 255,
    name: 'name',
    type: 'varchar',
    unique: true
  })
  public name!: ActivityType;

  @Column({
    name: 'description',
    nullable: true,
    type: 'text'
  })
  public description!: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp with time zone'
  })
  public readonly updatedAt!: Date;
}
