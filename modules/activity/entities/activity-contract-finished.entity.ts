import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn
} from 'typeorm';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_finished', { schema: 'activities' })
export class ActivityContractFinished {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @CreateDateColumn({
    name: 'finish_at',
    type: 'timestamp with time zone'
  })
  public readonly finishAt!: Date;
}
