import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { TeamEntity } from 'Server/modules/team/entities';

import { ActivityEntity } from './activity.entity';

@Entity('activity_contract_moved', { schema: 'activities' })
export class ActivityContractMovedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    name: 'from_team_sid',
    type: 'uuid'
  })
  @OneToOne(() => TeamEntity)
  @JoinColumn({
    name: 'from_team_sid',
    referencedColumnName: 'sid'
  })
  public fromTeam!: TeamEntity['sid'];

  @Column({
    name: 'to_team_sid',
    type: 'uuid'
  })
  @OneToOne(() => TeamEntity)
  @JoinColumn({
    name: 'to_team_sid',
    referencedColumnName: 'sid'
  })
  public toTeam!: TeamEntity['sid'];
}
