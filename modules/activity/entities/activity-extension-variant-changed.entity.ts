import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { ExtensionVariantEntity } from 'Server/modules/contract/entities';

import { ActivityEntity } from './activity.entity';

@Entity('activity_extension_variant_changed', { schema: 'activities' })
export class ActivityExtensionVariantChangedEntity {
  @PrimaryColumn({
    name: 'activity_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityEntity)
  @JoinColumn({
    name: 'activity_sid',
    referencedColumnName: 'sid'
  })
  public activity!: ActivityEntity['sid'];

  @Column({
    name: 'exchange_variant_from_sid',
    type: 'varchar'
  })
  @OneToOne(() => ExtensionVariantEntity)
  @JoinColumn({
    name: 'exchange_variant_from_sid',
    referencedColumnName: 'sid'
  })
  public extensionVariantFrom!: string;

  @Column({
    name: 'exchange_variant_to_sid',
    type: 'varchar'
  })
  @OneToOne(() => ExtensionVariantEntity)
  @JoinColumn({
    name: 'exchange_variant_to_sid',
    referencedColumnName: 'sid'
  })
  public extensionVariantTo!: string;
}
