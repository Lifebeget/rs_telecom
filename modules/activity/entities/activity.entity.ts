import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { ClientEntity, ClientNoteEntity } from 'Server/modules/client/entities';
import { ContractEntity } from 'Server/modules/contract/entities';
import { LeadEntity } from 'Server/modules/lead/entities';
import { MemberEntity } from 'Server/modules/member/entities';
import { TeamEntity } from 'Server/modules/team/entities';
import { UserEntity } from 'Server/modules/user/entities';

import { ActivityTypeEntity } from './activity-type.entity';
import { ActivityContractMovedEntity } from './activity-contract-moved.entity';
import { ActivityContractAssignedEntity } from './activity-contract-assigned.entity';
import { ActivityContractUnassignedEntity } from './activity-contract-unassigned.entity';
import { ActivityLeadCreatedEntity } from './activity-lead-created.entity';
import { ActivityContractStatusChangedEntity } from './activity-contract-status-changed.entity';
import { ActivityContractCreatedEntity } from './activity-contract-created.entity';

@Entity('activity', { schema: 'activities' })
export class ActivityEntity {
  @PrimaryGeneratedColumn('uuid', {
    name: 'sid'
  })
  public readonly sid!: string;

  @Column({
    name: 'activity_type_sid',
    type: 'uuid'
  })
  @OneToOne(() => ActivityTypeEntity)
  @JoinColumn({
    name: 'activity_type_sid',
    referencedColumnName: 'sid'
  })
  public type!: ActivityTypeEntity['sid'];

  @Column({
    default: null,
    name: 'client_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => ClientEntity)
  @JoinColumn({
    name: 'client_sid',
    referencedColumnName: 'sid'
  })
  public client?: ClientEntity['sid'] | null;

  @Column({
    default: null,
    name: 'note_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => ClientNoteEntity)
  @JoinColumn({
    name: 'note_sid',
    referencedColumnName: 'sid'
  })
  public note?: ClientNoteEntity['sid'] | null;

  @Column({
    default: null,
    name: 'contract_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => ContractEntity)
  @JoinColumn({
    name: 'contract_sid',
    referencedColumnName: 'sid'
  })
  public contract?: ContractEntity['sid'] | null;

  @Column({
    default: null,
    name: 'lead_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => LeadEntity)
  @JoinColumn({
    name: 'lead_sid',
    referencedColumnName: 'sid'
  })
  public lead?: LeadEntity['sid'] | null;

  @Column({
    default: null,
    name: 'member_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => MemberEntity)
  @JoinColumn({
    name: 'member_sid',
    referencedColumnName: 'sid'
  })
  public member?: MemberEntity['sid'] | null;

  @Column({
    comment: 'location of the entity when the activity happens',
    default: null,
    name: 'team_sid',
    nullable: true,
    type: 'uuid'
  })
  @OneToOne(() => TeamEntity)
  @JoinColumn({
    name: 'team_sid',
    referencedColumnName: 'sid'
  })
  public team?: TeamEntity['sid'] | null;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp with time zone'
  })
  public readonly createdAt!: Date;

  public readonly moved!: ActivityContractMovedEntity;

  public readonly assigned!: ActivityContractAssignedEntity;

  public readonly unassigned!: ActivityContractUnassignedEntity;

  public readonly leadCreated!: ActivityLeadCreatedEntity;

  public readonly contractStatusChanged!: ActivityContractStatusChangedEntity;

  public readonly contractCreated!: ActivityContractCreatedEntity;

  public user?: UserEntity;

  public teamRel?: TeamEntity;
}
