import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityContractMovedEntity } from '../entities';

@EntityRepository(ActivityContractMovedEntity)
export class ActivityContractMovedRepository extends BaseRepository<ActivityContractMovedEntity> {}
