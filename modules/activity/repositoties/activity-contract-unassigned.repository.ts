import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityContractUnassignedEntity } from '../entities';

@EntityRepository(ActivityContractUnassignedEntity)
export class ActivityContractUnassignedRepository extends BaseRepository<ActivityContractUnassignedEntity> {}
