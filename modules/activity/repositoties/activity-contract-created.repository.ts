import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityContractCreatedEntity } from '../entities';

@EntityRepository(ActivityContractCreatedEntity)
export class ActivityContractCreatedRepository extends BaseRepository<ActivityContractCreatedEntity> {}
