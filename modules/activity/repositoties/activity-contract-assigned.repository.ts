import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityContractAssignedEntity } from '../entities';

@EntityRepository(ActivityContractAssignedEntity)
export class ActivityContractAssignedRepository extends BaseRepository<ActivityContractAssignedEntity> {}
