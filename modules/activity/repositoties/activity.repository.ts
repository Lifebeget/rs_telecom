import { Brackets, EntityRepository } from 'typeorm';

import { ContractXLeadEntity } from 'Server/modules/contract/entities';
import { BaseRepository } from 'Server/repositories';
import { ClientXContractEntity } from 'Server/modules/client/entities';
import { MemberEntity } from 'Server/modules/member/entities';
import { UserEntity } from 'Server/modules/user/entities';
import { TeamEntity } from 'Server/modules/team/entities';

import {
  ActivityContractAssignedEntity,
  ActivityContractCreatedEntity,
  ActivityContractFinished,
  ActivityContractMovedEntity,
  ActivityContractStatusChangedEntity,
  ActivityContractUnassignedEntity,
  ActivityEntity,
  ActivityExtensionVariantChangedEntity,
  ActivityLeadCreatedEntity
} from '../entities';

@EntityRepository(ActivityEntity)
export class ActivityRepository extends BaseRepository<ActivityEntity> {
  private createActivityQueryBuilder() {
    return this.createQueryBuilder('a')
      .leftJoinAndSelect('a.type', 't')
      .leftJoinAndMapOne(
        'a.contract_status_changed',
        ActivityContractStatusChangedEntity,
        'acsc',
        'acsc.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'a.finished',
        ActivityContractFinished,
        'acf',
        'acf.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'a.lead_created',
        ActivityLeadCreatedEntity,
        'alc',
        'alc.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'a.unassigned',
        ActivityContractUnassignedEntity,
        'acu',
        'acu.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'a.assigned',
        ActivityContractAssignedEntity,
        'aca',
        'aca.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'aca.memberTo',
        MemberEntity,
        'aca_member',
        'aca_member.sid = aca.member_to_sid'
      )
      .leftJoinAndMapOne(
        'aca_member.user',
        UserEntity,
        'aca_user',
        'aca_user.sid = aca_member.user_sid'
      )
      .leftJoinAndMapOne(
        'a.moved',
        ActivityContractMovedEntity,
        'acm',
        'acm.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'acm.teamTo',
        TeamEntity,
        'acm_teamTo',
        'acm_teamTo.sid = acm.to_team_sid'
      )
      .leftJoinAndMapOne(
        'acm.teamFrom',
        TeamEntity,
        'acm_teamFrom',
        'acm_teamFrom.sid = acm.from_team_sid'
      )
      .leftJoinAndMapOne(
        'a.contract_created',
        ActivityContractCreatedEntity,
        'acc',
        'acc.activity_sid = a.sid'
      )
      .leftJoinAndMapOne(
        'a.contract_extension_changed',
        ActivityExtensionVariantChangedEntity,
        'aevc',
        'aevc.activity_sid = a.sid'
      )
      .orderBy('a.created_at');
  }

  async getActivityListByContract({
    contractSid,
    teamSid,
    memberSid,
    createdAtFrom
  }: {
    contractSid: string;
    teamSid?: string | null;
    memberSid?: string | null;
    createdAtFrom?: string | null;
  }): Promise<ActivityEntity[]> {
    const qb = this.createActivityQueryBuilder()
      .leftJoin(ContractXLeadEntity, 'cxl', 'cxl.contract_sid = :contractSid')
      .leftJoin(ClientXContractEntity, 'cxc', 'cxc.contract_sid = :contractSid')
      .leftJoin(MemberEntity, 'm', 'm.sid = a.member_sid')
      .leftJoinAndMapOne('a.user', UserEntity, 'u', 'u.sid = m.user_sid')
      .leftJoin(TeamEntity, 'tt', 'tt.sid = a.team_sid')
      .leftJoinAndMapOne('a.teamRel', TeamEntity, 'tr', 'tr.sid = tt.sid')
      .where(
        new Brackets(sqb => {
          sqb
            .where('a.contract_sid = :contractSid')
            .orWhere('a.client_sid = cxc.client_sid')
            .orWhere('a.lead_sid = cxl.lead_sid');
        })
      )
      .setParameters({ contractSid });

    if (teamSid) {
      qb.andWhere('a.team_sid = :teamSid', { teamSid });
    }

    if (memberSid) {
      qb.andWhere('a.member_sid = :memberSid', { memberSid });
    }

    if (createdAtFrom) {
      qb.andWhere('a.created_at >= :createdAtFrom', {
        createdAtFrom
      });
    }

    return qb.getMany();
  }

  async getActivityListByNote(
    noteSid: string,
    teamSid?: string | null
  ): Promise<ActivityEntity[]> {
    const qb = this.createActivityQueryBuilder().where(
      'a.note_sid = :noteSid',
      { noteSid }
    );

    if (teamSid) {
      qb.andWhere('a.team_sid = :teamSid', { teamSid });
    }

    return qb.getMany();
  }

  async getActivityListByClient(
    clientSid: string,
    teamSid?: string | null
  ): Promise<ActivityEntity[]> {
    const qb = this.createActivityQueryBuilder()
      .leftJoin(ClientXContractEntity, 'cxc', 'cxc.client_sid = :clientSid')
      .leftJoin(
        ContractXLeadEntity,
        'cxl',
        'cxl.contract_sid = cxc.contract_sid'
      )
      .where(
        new Brackets(sqb => {
          sqb
            .where('a.client_sid = :clientSid')
            .orWhere('a.contract_sid = cxc.contract_sid')
            .orWhere('a.lead_sid = cxl.lead_sid');
        })
      )
      .setParameters({ clientSid });

    if (teamSid) {
      qb.andWhere('a.team_sid = :teamSid', { teamSid });
    }

    return qb.getMany();
  }

  async createActivityWithType({
    contractSid,
    memberSid,
    teamSid,
    typeSid,
    clientSid
  }: {
    contractSid: string;
    memberSid?: string;
    teamSid?: string;
    clientSid?: string;
    typeSid: string;
  }) {
    return this.save(
      this.create({
        client: clientSid,
        contract: contractSid,
        member: memberSid,
        team: teamSid,
        type: typeSid
      })
    );
  }
}
