import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityLeadCreatedEntity } from '../entities';

@EntityRepository(ActivityLeadCreatedEntity)
export class ActivityLeadCreatedRepository extends BaseRepository<ActivityLeadCreatedEntity> {}
