import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityContractStatusChangedEntity } from '../entities';

@EntityRepository(ActivityContractStatusChangedEntity)
export class ActivityContractStatusChangedRepository extends BaseRepository<ActivityContractStatusChangedEntity> {}
