import { EntityRepository } from 'typeorm';

import { BaseRepository } from 'Server/repositories';

import { ActivityExtensionVariantChangedEntity } from '../entities';

@EntityRepository(ActivityExtensionVariantChangedEntity)
export class ActivityExtensionVariantChangedRepository extends BaseRepository<ActivityExtensionVariantChangedEntity> {}
