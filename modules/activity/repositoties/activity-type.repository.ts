import { EntityRepository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';

import { ActivityType, Errors } from 'Constants';
import { BaseRepository } from 'Server/repositories';

import { ActivityTypeEntity } from '../entities';

@EntityRepository(ActivityTypeEntity)
export class ActivityTypeRepository extends BaseRepository<ActivityTypeEntity> {
  async getTypeByName(name: ActivityType): Promise<ActivityTypeEntity> {
    const activityType = await this.createQueryBuilder()
      .where('name = :name', { name })
      .getOne();

    if (!activityType) {
      throw new InternalServerErrorException(
        Errors.SOMETHING_WENT_WRONG,
        `Activity type ${name} doesn't exist`
      );
    }

    return activityType;
  }
}
