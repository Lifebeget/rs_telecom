import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ClientModule } from '../client';
import { ContractModule } from '../contract';
import { MemberModule } from '../member';

import { ActivityController } from './activity.controller';
import { ActivityService } from './activity.service';
import {
  ActivityContractAssignedRepository,
  ActivityContractCreatedRepository,
  ActivityContractMovedRepository,
  ActivityContractStatusChangedRepository,
  ActivityContractUnassignedRepository,
  ActivityExtensionVariantChangedRepository,
  ActivityLeadCreatedRepository,
  ActivityRepository,
  ActivityTypeRepository
} from './repositoties';

@Module({
  controllers: [ActivityController],
  exports: [TypeOrmModule, ActivityService],
  imports: [
    TypeOrmModule.forFeature([
      ActivityContractAssignedRepository,
      ActivityContractCreatedRepository,
      ActivityContractMovedRepository,
      ActivityContractStatusChangedRepository,
      ActivityContractUnassignedRepository,
      ActivityExtensionVariantChangedRepository,
      ActivityLeadCreatedRepository,
      ActivityRepository,
      ActivityTypeRepository,
      ActivityContractAssignedRepository
    ]),
    forwardRef(() => ClientModule),
    forwardRef(() => ContractModule),
    MemberModule
  ],
  providers: [ActivityService]
})
export class ActivityModule {}
