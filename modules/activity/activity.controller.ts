import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { Permission } from 'Constants';
import { Restrict, User } from 'Server/decorators';

import { JwtPayloadModel } from '../auth/models';
import { ClientService } from '../client/client.service';
import { RoleService } from '../role/role.service';

import { ActivityService } from './activity.service';
import {
  ActivitySwaggerClient,
  ActivitySwaggerContract,
  ActivitySwaggerNote
} from './activity.swagger';
import { ClientSidDto, ContractSidDto, NoteSidDto } from './dto';
import { ActivityModel } from './models';

@Controller('activity')
@ApiTags('Activity')
export class ActivityController {
  constructor(
    private readonly activityService: ActivityService,
    private readonly clientService: ClientService,
    private readonly roleService: RoleService
  ) {}

  @Get('client/:clientSid')
  @Restrict()
  @ActivitySwaggerClient()
  async getActivityByClient(
    @Param() clientSidDto: ClientSidDto,
    @User() user: JwtPayloadModel
  ): Promise<ActivityModel[]> {
    await this.roleService.checkPermissionsByClient(
      [Permission.viewTeamContracts],
      user.sid,
      clientSidDto.clientSid
    );

    return this.activityService.getActivityByClient(
      clientSidDto.clientSid,
      user.sid
    );
  }

  @Get('note/:noteSid')
  @Restrict()
  @ActivitySwaggerNote()
  async getActivityByNote(
    @Param() noteSidDto: NoteSidDto,
    @User() user: JwtPayloadModel
  ): Promise<ActivityModel[]> {
    const clientEntity = await this.clientService.getClientByNote(
      noteSidDto.noteSid
    );
    await this.roleService.checkPermissionsByClient(
      [Permission.viewClientCard],
      user.sid,
      clientEntity.sid
    );

    return this.activityService.getActivityByNote(noteSidDto.noteSid, user.sid);
  }

  @Get('contract/:contractSid')
  @Restrict()
  @ActivitySwaggerContract()
  async getActivityByContract(
    @Param() contractSidDto: ContractSidDto,
    @User() user: JwtPayloadModel
  ): Promise<ActivityModel[]> {
    await this.roleService.checkPermissionsByContract(
      [Permission.viewTeamContracts],
      user.sid,
      contractSidDto.contractSid
    );

    return this.activityService.getActivityByContract(
      contractSidDto.contractSid,
      user.sid
    );
  }
}
