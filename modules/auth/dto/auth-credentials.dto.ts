import { IsEmail, IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class AuthCredentialsDto {
  @ApiProperty({
    description: 'Email',
    example: 'example@gmail.com',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(255)
  public readonly email!: string;

  @ApiProperty({
    description: 'Password',
    example: 'password',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @MaxLength(255)
  public readonly password!: string;
}
