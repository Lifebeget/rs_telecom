import { IsEmail, IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ResetPasswordDto {
  @ApiProperty({
    description: 'Email',
    example: 'example@gmail.com',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(255)
  public readonly email!: string;
}
