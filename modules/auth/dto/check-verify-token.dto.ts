import { IsNotEmpty, Length } from 'class-validator';

export class CheckVerifyTokenDto {
  @IsNotEmpty()
  @Length(32)
  public readonly verifyToken!: string;
}
