import { IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RefreshTokenDto {
  @ApiProperty({
    description: 'Refresh token',
    example: 'dcdb9411-9ace-4222-95c5-eb83406bf537',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @MaxLength(255)
  public readonly refreshToken!: string;
}
