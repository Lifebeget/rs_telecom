import { IsNotEmpty, Length, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UpdatePasswordDto {
  @ApiProperty({
    description: 'Verify token',
    example:
      'e4ef3a38d03a7cee47028a934a2c42ee431c4be0ae8e96a540b91f5066b611b820f5098b116d7be6c4654b00f825c21cfb141bd1b7243d1df9f830e9edb04e6f',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @Length(32)
  public readonly verifyToken!: string;

  @ApiProperty({
    description: 'New password',
    example: 'password',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @MaxLength(255)
  public readonly password!: string;
}
