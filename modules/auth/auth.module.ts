import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { AuthService, TokenService } from './services';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies';

@Module({
  controllers: [AuthController],
  exports: [AuthService, TokenService],
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: {
          expiresIn: '1d'
        }
      })
    })
  ],
  providers: [AuthService, JwtStrategy, TokenService]
})
export class AuthModule {}
