import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { RedisService } from 'nestjs-redis';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';

import { Errors } from 'Constants';
import { RedisClientType } from 'Server/constants';
import { UserRepository } from 'Server/modules/user/repositories';

import { JwtPayloadModel } from '../models';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private readonly configService: ConfigService,
    private readonly redisService: RedisService
  ) {
    super({
      ignoreExpiration: false,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET')
    });
  }

  private readonly accessTokenRedisClient = this.redisService.getClient(
    RedisClientType.accessToken
  );

  async validate(payload: JwtPayloadModel): Promise<JwtPayloadModel> {
    const user = await this.userRepository.findOne({
      isBlockedFlg: false,
      isInvalidatedFlg: false,
      isVerifiedEmailFlg: true,
      sid: payload.sid
    });
    if (
      !user ||
      (await this.accessTokenRedisClient.exists(payload.jti)) !== 1
    ) {
      throw new UnauthorizedException(Errors.WRONG_AUTH_DATA);
    }

    if (user.validatedAt) {
      const validatedAt = DateTime.fromJSDate(user.validatedAt).toSeconds();

      if (validatedAt > payload.iat) {
        throw new UnauthorizedException(Errors.WRONG_AUTH_DATA);
      }
    }

    return payload;
  }
}
