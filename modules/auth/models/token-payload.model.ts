import { ApiProperty } from '@nestjs/swagger';

export class TokenPayloadModel {
  @ApiProperty({
    description: 'Access token',
    required: true,
    type: String
  })
  public readonly accessToken!: string;

  @ApiProperty({
    description: 'Refresh token',
    required: true,
    type: String
  })
  public readonly refreshToken!: string;
}
