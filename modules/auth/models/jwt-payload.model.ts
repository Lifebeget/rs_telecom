import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class JwtPayloadModel {
  @Expose()
  @ApiProperty({
    description: 'sid',
    example: 'b76b8972-1001-41b8-a506-00331231ab53',
    required: true,
    type: String
  })
  public readonly sid!: string;

  public readonly iat!: number;

  public readonly exp!: number;

  public readonly jti!: string;
}
