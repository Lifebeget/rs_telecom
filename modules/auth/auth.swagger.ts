import { applyDecorators } from '@nestjs/common';
import { ApiParam, ApiResponse } from '@nestjs/swagger';

import { UserModel } from '../user/models';

import { TokenPayloadModel } from './models';

export function AuthSwaggerClient() {
  return applyDecorators(
    ApiResponse({
      status: 200,
      type: TokenPayloadModel
    }),
    ApiResponse({
      description: 'Forbidden',
      status: 403
    })
  );
}

export function AuthSwaggerRefreshToken() {
  return applyDecorators(
    ApiResponse({
      status: 200,
      type: TokenPayloadModel
    }),
    ApiResponse({
      description: 'Unauthorized',
      status: 401
    })
  );
}

export function AuthSwaggerResetPassword() {
  return applyDecorators(
    ApiResponse({
      status: 200
    })
  );
}

export function AuthSwaggerVerify() {
  return applyDecorators(
    ApiParam({
      name: 'verifyToken',
      type: String
    }),
    ApiResponse({
      status: 200,
      type: UserModel
    }),
    ApiResponse({
      status: 400
    })
  );
}

export function AuthSwaggerUpdatePassword() {
  return applyDecorators(
    ApiResponse({
      status: 200
    }),
    ApiResponse({
      status: 400
    })
  );
}
