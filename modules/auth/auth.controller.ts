import {
  Body,
  Controller,
  Get,
  Headers,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { Restrict, User } from 'Server/decorators';

import { UserModel } from '../user/models';

import { AuthService, TokenService } from './services';
import {
  AuthCredentialsDto,
  CheckVerifyTokenDto,
  RefreshTokenDto,
  ResetPasswordDto,
  UpdatePasswordDto
} from './dto';
import { JwtPayloadModel, TokenPayloadModel } from './models';
import {
  AuthSwaggerClient,
  AuthSwaggerRefreshToken,
  AuthSwaggerResetPassword,
  AuthSwaggerUpdatePassword,
  AuthSwaggerVerify
} from './auth.swagger';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly tokenService: TokenService
  ) {}

  @Post('/login')
  @AuthSwaggerClient()
  login(
    @Body() authCredentialsDto: AuthCredentialsDto
  ): Promise<TokenPayloadModel> {
    return this.authService.login(authCredentialsDto);
  }

  @Post('/logout')
  @Restrict()
  logout(@User() user: JwtPayloadModel) {
    return this.authService.logout(user);
  }

  @Post('/refresh-token')
  @AuthSwaggerRefreshToken()
  refreshToken(
    @Body() refreshTokenDto: RefreshTokenDto,
    @Headers('authorization') accessToken?: string
  ): Promise<TokenPayloadModel> {
    return this.tokenService.createAccessTokenFromRefreshToken(
      refreshTokenDto.refreshToken,
      accessToken
    );
  }

  @Post('/reset-password')
  @AuthSwaggerResetPassword()
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    await this.authService.resetPassword(resetPasswordDto.email);
  }

  @Get('/verify/:verifyToken')
  @AuthSwaggerVerify()
  checkVerifyToken(@Param() params: CheckVerifyTokenDto): Promise<UserModel> {
    return this.tokenService.checkVerifyToken(params.verifyToken);
  }

  @Put('/update-password')
  @AuthSwaggerUpdatePassword()
  async updatePassword(@Body() updatePasswordDto: UpdatePasswordDto) {
    await this.authService.updatePassword(updatePasswordDto);
  }
}
