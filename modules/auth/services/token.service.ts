import crypto from 'crypto';
import { URL } from 'url';

import { plainToClass } from 'class-transformer';
import {
  BadRequestException,
  Injectable,
  UnauthorizedException
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { RedisService } from 'nestjs-redis';
import { v4 as uuid } from 'uuid';
import { DateTime } from 'luxon';
import { pick } from 'remeda';

import { Errors } from 'Constants';
import { LoggerService, MailerService } from 'Server/providers';
import { RedisClientType } from 'Server/constants';
import { UserRepository } from 'Server/modules/user/repositories';
import { UserEntity } from 'Server/modules/user/entities';
import { UserModel } from 'Server/modules/user/models';

import { JwtPayloadModel, TokenPayloadModel } from '../models';

type AccessTokenData = {
  accessToken: string;
  jti: string;
  iat: number;
};

@Injectable()
export class TokenService {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly loggerService: LoggerService,
    private readonly mailerService: MailerService,
    private readonly redisService: RedisService,
    private readonly userRepository: UserRepository
  ) {}

  private readonly refreshTokenRedisClient = this.redisService.getClient(
    RedisClientType.refreshToken
  );

  private readonly accessTokenRedisClient = this.redisService.getClient(
    RedisClientType.accessToken
  );

  private readonly verifyTokenRedisClient = this.redisService.getClient(
    RedisClientType.verifyToken
  );

  async generateAccessToken(sid: string): Promise<AccessTokenData> {
    const jti = uuid();
    const accessToken = this.jwtService.sign({ sid }, { jwtid: jti });
    const { iat, exp } = this.jwtService.decode(accessToken) as JwtPayloadModel;

    await this.accessTokenRedisClient.set(jti, accessToken, 'EX', exp);

    return { accessToken, iat, jti };
  }

  async generateRefreshToken(jti: string): Promise<string> {
    const refreshToken = uuid();

    await this.refreshTokenRedisClient.set(
      jti,
      refreshToken,
      'PX',
      DateTime.local().plus({ days: 7 }).valueOf()
    );

    return refreshToken;
  }

  async destroyAuthTokens(jti: string) {
    await this.accessTokenRedisClient.del(jti);
    await this.refreshTokenRedisClient.del(jti);
  }

  async createAccessTokenFromRefreshToken(
    prevRefreshToken: string,
    prevAccessToken?: string
  ): Promise<TokenPayloadModel> {
    if (!prevAccessToken) {
      throw new UnauthorizedException(Errors.WRONG_AUTH_TOKEN);
    }

    const decodedAccessToken = this.jwtService.decode(
      prevAccessToken
    ) as JwtPayloadModel | null;

    if (!decodedAccessToken) {
      throw new UnauthorizedException(Errors.WRONG_AUTH_TOKEN);
    }

    const user = await this.userRepository.findOne({
      isBlockedFlg: false,
      isInvalidatedFlg: false,
      sid: decodedAccessToken.sid
    });

    if (!user) {
      throw new UnauthorizedException(Errors.WRONG_AUTH_TOKEN);
    }

    if (user.validatedAt) {
      const validatedAt = DateTime.fromJSDate(user.validatedAt).toSeconds();

      if (validatedAt > decodedAccessToken.iat) {
        throw new UnauthorizedException(Errors.WRONG_AUTH_TOKEN);
      }
    }

    const refreshToken = await this.refreshTokenRedisClient.get(
      decodedAccessToken.jti
    );

    if (refreshToken !== prevRefreshToken) {
      throw new UnauthorizedException(Errors.WRONG_AUTH_TOKEN);
    }

    const {
      accessToken: updatedAccessToken,
      jti
    } = await this.generateAccessToken(decodedAccessToken.sid);
    const updatedRefreshToken = await this.generateRefreshToken(jti);

    await this.destroyAuthTokens(decodedAccessToken.jti);

    return {
      accessToken: updatedAccessToken,
      refreshToken: updatedRefreshToken
    };
  }

  //Думаю этот метод можно вынести в отдельный модуль рассылок
  async sendResetPassword(email: string, verifyToken: string) {
    try {
      const resetPasswordLink = `/reset-password/${verifyToken}`;

      const url = new URL(resetPasswordLink, this.configService.get('DOMAIN'));

      await this.mailerService.send(email, 'Verify your email', url.toString());
    } catch (err) {
      this.loggerService.error('Error while sending the email', {
        payload: err instanceof Error ? pick(err, ['message']) : err
      });
    }
  }

  async generateVerifyToken(userSid: string): Promise<string> {
    const verifyToken = crypto.randomBytes(64).toString('hex');

    await this.verifyTokenRedisClient.set(
      verifyToken,
      userSid,
      'PX',
      DateTime.local().plus({ days: 1 }).valueOf()
    );

    return verifyToken;
  }

  async generateVerifyTokenAndSendEmail(userSid: string, email: string) {
    const verifyToken = await this.generateVerifyToken(userSid);
    await this.sendResetPassword(email, verifyToken);
    this.loggerService.info(
      `Created verify token "${verifyToken}" for user "${userSid}"`
    );
  }

  async getUserFromVerifyToken(verifyToken: string): Promise<UserEntity> {
    const sid = await this.verifyTokenRedisClient.get(verifyToken);

    if (!sid) {
      throw new BadRequestException(
        Errors.WRONG_VERIFY_TOKEN,
        'Verification link is invalid or has expired'
      );
    }

    const user = await this.userRepository.findOne({
      isBlockedFlg: false,
      isDeleted: false,
      sid
    });

    if (!user) {
      throw new BadRequestException(
        Errors.WRONG_VERIFY_TOKEN,
        `Active user ${sid} not found`
      );
    }

    return user;
  }

  async checkVerifyToken(verifyToken: string) {
    const user = await this.getUserFromVerifyToken(verifyToken);

    return plainToClass(UserModel, user);
  }
}
