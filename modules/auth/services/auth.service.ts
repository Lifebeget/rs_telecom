import { ForbiddenException, Injectable } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import bcrypt from 'bcrypt';
import { DateTime } from 'luxon';

import { Errors } from 'Constants';
import { RedisClientType } from 'Server/constants';
import { UserRepository } from 'Server/modules/user/repositories';
import { LoggerService } from 'Server/providers';

import { JwtPayloadModel, TokenPayloadModel } from '../models';
import { AuthCredentialsDto, UpdatePasswordDto } from '../dto';

import { TokenService } from './token.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly loggerService: LoggerService,
    private readonly redisService: RedisService,
    private readonly tokenService: TokenService,
    private readonly userRepository: UserRepository
  ) {}

  private readonly verifyTokenRedisClient = this.redisService.getClient(
    RedisClientType.verifyToken
  );

  async login(
    authCredentialsDto: AuthCredentialsDto
  ): Promise<TokenPayloadModel> {
    const { email, password } = authCredentialsDto;
    const user = await this.userRepository.getActiveUserByParam({
      email,
      isBlockedFlg: false,
      isVerifiedEmailFlg: true
    });
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
    if (!user?.password || !(await bcrypt.compare(password, user.password))) {
      throw new ForbiddenException(Errors.WRONG_AUTH_DATA, 'User not found');
    }

    const {
      accessToken,
      jti,
      iat
    } = await this.tokenService.generateAccessToken(user.sid);
    const refreshToken = await this.tokenService.generateRefreshToken(jti);

    if (user.isInvalidatedFlg) {
      await this.userRepository.update(
        { sid: user.sid },
        {
          isInvalidatedFlg: false,
          validatedAt: DateTime.fromSeconds(iat).toJSDate()
        }
      );
    }

    await this.userRepository.update(
      { sid: user.sid },
      {
        lastLoginAt: new Date()
      }
    );

    this.loggerService.info(`User "${user.sid}" successfully sing in`);

    return {
      accessToken,
      refreshToken
    };
  }

  async logout(user: JwtPayloadModel) {
    await this.tokenService.destroyAuthTokens(user.jti);

    this.loggerService.info(`User "${user.sid}" successfully logout`);
  }

  async resetPassword(email: string) {
    const user = await this.userRepository.getActiveUserByParam({
      email,
      isBlockedFlg: false
    });
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
    if (user) {
      await this.tokenService.generateVerifyTokenAndSendEmail(
        user.sid,
        user.email
      );
    }
  }

  async generatePassword(password: string) {
    return bcrypt.hash(password, 10);
  }

  async updatePassword(updatePasswordDto: UpdatePasswordDto) {
    const { verifyToken, password } = updatePasswordDto;
    const {
      sid,
      isVerifiedEmailFlg,
      isInvalidatedFlg
    } = await this.tokenService.getUserFromVerifyToken(verifyToken);
    const updatedPassword = await this.generatePassword(password);

    await this.verifyTokenRedisClient.del(verifyToken);

    await this.userRepository.update(
      { sid },
      {
        password: updatedPassword
      }
    );

    if (!isInvalidatedFlg) {
      await this.userRepository.update(
        { sid },
        {
          isInvalidatedFlg: true
        }
      );
    }

    if (!isVerifiedEmailFlg) {
      await this.userRepository.update(
        { sid },
        {
          isVerifiedEmailFlg: true,
          verifiedAt: new Date()
        }
      );
    }

    this.loggerService.info(`Update password for user "${sid}"`);
  }
}
