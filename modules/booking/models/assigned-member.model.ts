import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { EnumValues } from 'enum-values';

import { ScheduleStatus } from 'Constants';
import { MemberModel } from 'Server/modules/member/models';
import { TimeBlockModel } from 'Server/modules/shop/models';

@Exclude()
export class AssignedMember extends MemberModel {
  @ApiProperty({
    description: 'Schedule status',
    // example: ScheduleStatus.Assigned,
    enum: EnumValues.getValues(ScheduleStatus),

    required: true
  })
  @Expose()
  public readonly scheduleStatus!: ScheduleStatus;

  @ApiProperty({
    description: 'Is work day canceled',
    required: true,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!: TimeBlockModel;
}
