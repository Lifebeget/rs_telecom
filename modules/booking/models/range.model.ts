import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RangeModel {
  @ApiProperty({
    description: 'Start range',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly from!: Date;

  @ApiProperty({
    description: 'End range',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly to!: Date;
}
