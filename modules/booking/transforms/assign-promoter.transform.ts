import { MemberEntity } from 'Server/modules/member/entities';
import {
  PromoterScheduleEntity,
  ShopScheduleEntity
} from 'Server/modules/shop/entities';

export const assignPromoterTransform = (
  shopScheduleEntities: ShopScheduleEntity[]
) =>
  shopScheduleEntities.map(({ promoterSchedule, scheduleStatus }) => {
    const {
      member: memberEntity,
      timeBlock
    } = (promoterSchedule as unknown) as PromoterScheduleEntity;
    const member = (memberEntity as unknown) as MemberEntity;

    return {
      ...member,
      scheduleStatus,
      timeBlock
    };
  });
