import { DateTime } from 'luxon';

import { ScheduleType } from 'Constants';
import { MemberEntity } from 'Server/modules/member/entities';
import {
  PromoterScheduleEntity,
  ShopDayEntity,
  ShopEntity,
  ShopScheduleEntity
} from 'Server/modules/shop/entities';
import { SplitDateOnRange } from 'Server/utils';

export const shopPlanningTransform = (
  shops: ShopEntity[],
  shopScheduleEntities: ShopScheduleEntity[],
  shopDayEntities: ShopDayEntity[],
  promoters: MemberEntity[],
  promoterSchedules: PromoterScheduleEntity[],
  weeks: ReturnType<SplitDateOnRange['weeks']>,
  mainMonth: string
) =>
  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  shops.map(shop => {
    const shopSchedules = shopScheduleEntities
      .filter(({ shop: shopSid }) => shopSid && shopSid === shop.sid)
      .map(shopScheduleEntity => ({
        ...shopScheduleEntity,
        shop
      }));

    return {
      schedule: weeks.map(({ days, range }) => ({
        days: days.map(({ date }) => {
          const isMainMonth =
            new Date(date).getMonth() === new Date(mainMonth).getMonth();

          const isCancelled = Boolean(
            shopDayEntities.find(({ shop: shopSid, date: day }) => {
              if (shopSid === shop.sid) {
                return (
                  date ===
                  DateTime.fromISO(day, { zone: 'utc' }).startOf('day').toISO()
                );
              }
            })
          );

          const shopSchedule = shopSchedules.filter(
            ({ promoterSchedule, shop }) => {
              if (!promoterSchedule) {
                return false;
              }
              const {
                workedAt
              } = (promoterSchedule as unknown) as PromoterScheduleEntity;
              // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
              if (workedAt && shop) {
                return (
                  date ===
                  DateTime.fromJSDate(workedAt, { zone: 'utc' })
                    .startOf('day')
                    .toISO()
                );
              }
            }
          );

          if (shopSchedule.length) {
            return {
              date,
              isCancelled,
              isMainMonth,
              members: shopSchedule.map(
                ({ promoterSchedule, scheduleStatus }) => {
                  const {
                    member,
                    timeBlock
                  } = (promoterSchedule as unknown) as PromoterScheduleEntity;
                  const memberEntity = (member as unknown) as MemberEntity;

                  return {
                    ...memberEntity,
                    scheduleStatus,
                    timeBlock
                  };
                }
              ),
              scheduleType: ScheduleType.Schedule
            };
          }

          const promoterSchedule = promoterSchedules.filter(
            ({ workedAt, member }) => {
              const memberEntity = promoters.find(
                ({ sid: memberSid }) => memberSid === member
              );

              if (memberEntity) {
                const shopEntity = memberEntity.shops.find(
                  ({ sid }) => sid === shop.sid
                );

                if (shopEntity) {
                  return (
                    date ===
                    DateTime.fromJSDate(workedAt, { zone: 'utc' })
                      .startOf('day')
                      .toISO()
                  );
                }
              }
            }
          );

          if (promoterSchedule.length) {
            return {
              date,
              isCancelled,
              isMainMonth,
              members: promoterSchedule.map(({ member, timeBlock }) => {
                const memberEntity = promoters.find(
                  ({ sid: memberSid }) => memberSid === member
                );

                return {
                  ...memberEntity,
                  scheduleStatus: null,
                  timeBlock
                };
              }),
              scheduleType: ScheduleType.Planning
            };
          }

          return {
            date,
            isCancelled,
            isMainMonth,
            members: [],
            scheduleType: ScheduleType.Schedule
          };
        }),
        range
      })),
      store: shop
    };
  });
