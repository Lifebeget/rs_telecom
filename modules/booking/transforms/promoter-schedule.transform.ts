import { DateTime } from 'luxon';

import { MemberEntity } from 'Server/modules/member/entities';
import { TIME_ERROR } from 'Server/modules/shop/constants';
import {
  PromoterScheduleEntity,
  ShopDayEntity,
  ShopScheduleEntity,
  ShopTimeBlockEntity
} from 'Server/modules/shop/entities';
import { WorkingDayEntity } from 'Server/modules/user-tracking/entities';
import { SplitDateOnRange } from 'Server/utils';

export const promoterScheduleTransform = (
  members: MemberEntity[],
  shopScheduleEntities: ShopScheduleEntity[],
  workingDays: WorkingDayEntity[],
  weeks: ReturnType<SplitDateOnRange['weeks']>,
  mainMonth: string
) =>
  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  members.map(member => {
    const schedule = shopScheduleEntities
      .filter(({ promoterSchedule }) => {
        const {
          member: memberSid
        } = (promoterSchedule as unknown) as PromoterScheduleEntity;

        return memberSid === member.sid;
      })
      .map(shopScheduleEntity => ({
        ...shopScheduleEntity,
        member
      }));

    return {
      member,
      schedule: weeks.map(({ days, range }) => ({
        days: days.map(({ date }) => {
          const shopSchedule = schedule.find(({ promoterSchedule, day }) => {
            const {
              workedAt
            } = (promoterSchedule as unknown) as PromoterScheduleEntity;
            const { shop } = (day as unknown) as ShopDayEntity;

            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
            if (workedAt && shop) {
              return (
                date ===
                DateTime.fromJSDate(workedAt, { zone: 'utc' })
                  .startOf('day')
                  .toISO()
              );
            }
          });

          const isMainMonth =
            new Date(date).getMonth() === new Date(mainMonth).getMonth();

          if (!shopSchedule) {
            return {
              date,
              isCancelled: null,
              isMainMonth,
              scheduleStatus: null,
              startTimeLate: null,
              store: null,
              timeBlock: null,
              workingDayEnd: null,
              workingDayStart: null,
              workingTimeNotEnough: null
            };
          }

          const day = (shopSchedule.day as unknown) as ShopDayEntity;
          const promoterSchedule = (shopSchedule.promoterSchedule as unknown) as PromoterScheduleEntity;
          const timeBlock = promoterSchedule.timeBlock as ShopTimeBlockEntity | null;

          const workDay = workingDays.find(
            ({ user, startedAt }) =>
              user === member.user &&
              DateTime.fromJSDate(startedAt, { zone: 'utc' })
                .startOf('day')
                .toISO() === date
          );

          const startedAt = workDay?.startedAt
            ? DateTime.fromJSDate(workDay.startedAt, { zone: 'utc' })
            : null;

          const endedAt = workDay?.endedAt
            ? DateTime.fromJSDate(workDay.endedAt, { zone: 'utc' })
            : null;

          const startTimeLate =
            startedAt && timeBlock
              ? TIME_ERROR <
                startedAt.diff(startedAt.startOf('day')).valueOf() -
                  timeBlock.startTime
              : false;

          const timeBlockLength = timeBlock
            ? timeBlock.endTime - timeBlock.startTime
            : null;

          const workingTimeNotEnough =
            startedAt && endedAt && timeBlockLength
              ? TIME_ERROR > timeBlockLength - endedAt.diff(startedAt).valueOf()
              : false;

          return {
            date,

            isCancelled: day.isCanceled,

            isMainMonth,
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
            scheduleStatus: shopSchedule.scheduleStatus ?? null,
            startTimeLate,
            store: day.shop,
            timeBlock: timeBlock ?? null,
            workingDayEnd: workDay?.endedAt ?? null,
            workingDayStart: workDay?.startedAt ?? null,
            workingTimeNotEnough
          };
        }),
        range
      }))
    };
  });
