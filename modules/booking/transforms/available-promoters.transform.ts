import { uniqBy } from 'remeda';

import { PromoterScheduleStatus } from 'Constants';
import { MemberModel } from 'Server/modules/member/models';
import {
  PromoterScheduleEntity,
  ShopScheduleEntity,
  ShopTimeBlockEntity
} from 'Server/modules/shop/entities';

import { GetAvailablePromoterByDateRow } from '../responses';

export const availablePromotersTransform = (
  promoters: MemberModel[],
  selected: ShopScheduleEntity[],
  available: PromoterScheduleEntity[]
) => {
  const availablePromoters = promoters.reduce<GetAvailablePromoterByDateRow[]>(
    (acc, member) => {
      const schedule = available.find(
        ({ member: memberSid }) => memberSid === member.sid
      );

      if (schedule) {
        const availableTimeBlock = (schedule.timeBlock as unknown) as ShopTimeBlockEntity;
        /**
         * @todo Mark
         */
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
        const currentaAvailableTimeBlock = availableTimeBlock
          ? availableTimeBlock
          : { blockType: schedule.timeBlockType, sid: null };

        acc.push({
          ...member,
          availableTimeBlock: currentaAvailableTimeBlock,
          declinedTimeBlock: null,
          isAvailable: true,
          isDeclined: false,
          isSelected: false,
          timeBlock: null
        });
      }

      return acc;
    },
    []
  );

  const selectedPromoters = promoters.reduce<GetAvailablePromoterByDateRow[]>(
    (acc, member) => {
      const schedule = selected.find(({ promoterSchedule }) => {
        const {
          member: memberSid
        } = (promoterSchedule as unknown) as PromoterScheduleEntity;

        return memberSid === member.sid;
      });

      if (schedule) {
        const availableSchedule = available.find(
          ({ member: memberSid, promoterStatus }) =>
            memberSid === member.sid &&
            promoterStatus === PromoterScheduleStatus.Available
        );

        const promoterSchedule = (schedule.promoterSchedule as unknown) as PromoterScheduleEntity;
        const timeBlock = (promoterSchedule.timeBlock as unknown) as ShopTimeBlockEntity;
        const availableTimeBlock = (availableSchedule?.timeBlock as unknown) as ShopTimeBlockEntity;
        const isDeclined =
          promoterSchedule.promoterStatus === PromoterScheduleStatus.Cancel;
        const declinedTimeBlock = isDeclined ? timeBlock : null;

        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
        const currentaAvailableTimeBlock = availableTimeBlock
          ? availableTimeBlock
          : { blockType: promoterSchedule.timeBlockType, sid: null };

        /**
         * @TODO
         */
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
        const currentTimeBlock = timeBlock
          ? timeBlock
          : { blockType: promoterSchedule.timeBlockType, sid: null };
        acc.push({
          ...member,
          // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
          availableTimeBlock: currentaAvailableTimeBlock ?? null,

          declinedTimeBlock,

          isAvailable:
            promoterSchedule.promoterStatus ===
            PromoterScheduleStatus.Available,

          isDeclined,

          isSelected: !isDeclined,

          timeBlock: currentTimeBlock
        });
      }

      return acc;
    },
    []
  );

  const inaccessiblePromoters = promoters.reduce<
    GetAvailablePromoterByDateRow[]
  >((acc, member) => {
    const sids = selectedPromoters
      .map(({ sid }) => sid)
      .concat(availablePromoters.map(({ sid }) => sid));

    if (!sids.includes(member.sid)) {
      acc.push({
        ...member,
        availableTimeBlock: null,
        declinedTimeBlock: null,
        isAvailable: false,
        isDeclined: false,
        isSelected: false,
        timeBlock: null
      });
    }

    return acc;
  }, []);

  return uniqBy(
    [...selectedPromoters, ...availablePromoters, ...inaccessiblePromoters],
    ({ sid: memberSid }) => memberSid
  );
};
