import { DateTime } from 'luxon';

import { PromoterScheduleStatus } from 'Constants';
import { MemberEntity } from 'Server/modules/member/entities';
import { PromoterScheduleEntity } from 'Server/modules/shop/entities';
import { SplitDateOnRange } from 'Server/utils';

export const promoterPlanningTransform = (
  members: MemberEntity[],
  promoterScheduleEntities: PromoterScheduleEntity[],
  weeks: ReturnType<SplitDateOnRange['weeks']>,
  mainMonth: string
) =>
  members.map(member => {
    const schedule = promoterScheduleEntities
      .filter(({ member: memberSid }) => memberSid === member.sid)
      .map(shopScheduleEntity => ({
        ...shopScheduleEntity,
        member
      }));

    return {
      member,
      schedule: weeks.map(({ days, range }) => ({
        days: days.map(({ date }) => {
          const promoterSchedule = schedule.find(({ workedAt }) => {
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
            if (workedAt) {
              return (
                date ===
                DateTime.fromJSDate(workedAt, { zone: 'utc' })
                  .startOf('day')
                  .toISO()
              );
            }
          });

          const isMainMonth =
            new Date(date).getMonth() === new Date(mainMonth).getMonth();
          /**
           * @TODO Mark
           */
          const currentTimeBlock = promoterSchedule?.timeBlock
            ? promoterSchedule.timeBlock
            : { blockType: promoterSchedule?.timeBlockType, sid: null };

          return {
            date,
            isMainMonth,
            timeBlock:
              promoterSchedule?.promoterStatus !==
                PromoterScheduleStatus.Cancel && promoterSchedule?.timeBlock
                ? currentTimeBlock
                : null
          };
        }),
        range
      }))
    };
  });
