import { DateTime } from 'luxon';

import { MemberEntity } from 'Server/modules/member/entities';
import { TIME_ERROR } from 'Server/modules/shop/constants';
import {
  PromoterScheduleEntity,
  ShopDayEntity,
  ShopEntity,
  ShopScheduleEntity,
  ShopTimeBlockEntity
} from 'Server/modules/shop/entities';
import { WorkingDayEntity } from 'Server/modules/user-tracking/entities';
import { SplitDateOnRange } from 'Server/utils';

export const shopScheduleTransform = (
  shops: ShopEntity[],
  shopScheduleEntities: ShopScheduleEntity[],
  shopDayEntities: ShopDayEntity[],
  workingDays: WorkingDayEntity[],
  weeks: ReturnType<SplitDateOnRange['weeks']>,
  mainMonth: string
) =>
  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  shops.map(shop => {
    const schedule = shopScheduleEntities
      .filter(({ shop: shopSid }) => shopSid && shopSid === shop.sid)
      .map(shopScheduleEntity => ({
        ...shopScheduleEntity,
        shop
      }));

    return {
      schedule: weeks.map(({ days, range }) => ({
        days: days.map(({ date }) => {
          const shopSchedule = schedule
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
            .filter(el => el.promoterSchedule && el.promoterSchedule !== null)
            .filter(({ promoterSchedule, shop }) => {
              const {
                workedAt
              } = (promoterSchedule as unknown) as PromoterScheduleEntity;
              // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
              if (workedAt && shop) {
                return (
                  date ===
                  DateTime.fromJSDate(workedAt, { zone: 'utc' })
                    .startOf('day')
                    .toISO()
                );
              }
            });

          const isMainMonth =
            new Date(date).getMonth() === new Date(mainMonth).getMonth();

          const isCancelled = Boolean(
            shopDayEntities.find(({ shop: shopSid, date: day }) => {
              if (shopSid === shop.sid) {
                return (
                  date ===
                  DateTime.fromISO(day, { zone: 'utc' }).startOf('day').toISO()
                );
              }
            })
          );

          return {
            date,
            isCancelled,
            isEditable:
              DateTime.fromISO(date, { zone: 'utc' })
                .diffNow('days')
                .as('days') > 0,
            isMainMonth,
            members: shopSchedule.map(
              ({ promoterSchedule, scheduleStatus }) => {
                const promoterScheduleEntity = (promoterSchedule as unknown) as PromoterScheduleEntity;
                const memberEntity = (promoterScheduleEntity.member as unknown) as MemberEntity;
                const timeBlockEntity = (promoterScheduleEntity.timeBlock as unknown) as ShopTimeBlockEntity | null;

                const workDay = workingDays.find(
                  ({ user, startedAt }) =>
                    user === memberEntity.user &&
                    DateTime.fromJSDate(startedAt, { zone: 'utc' })
                      .startOf('day')
                      .toISO() === date
                );

                const startedAt = workDay?.startedAt
                  ? DateTime.fromJSDate(workDay.startedAt, { zone: 'utc' })
                  : null;

                const endedAt = workDay?.endedAt
                  ? DateTime.fromJSDate(workDay.endedAt, { zone: 'utc' })
                  : null;

                const startTimeLate =
                  startedAt && timeBlockEntity
                    ? TIME_ERROR <
                      startedAt.diff(startedAt.startOf('day')).valueOf() -
                        timeBlockEntity.startTime
                    : false;

                const timeBlockLength = timeBlockEntity
                  ? timeBlockEntity.endTime - timeBlockEntity.startTime
                  : null;

                const workingTimeNotEnough =
                  startedAt && endedAt && timeBlockLength
                    ? TIME_ERROR >
                      timeBlockLength - endedAt.diff(startedAt).valueOf()
                    : false;

                return {
                  ...memberEntity,
                  scheduleStatus,
                  startTimeLate,
                  timeBlock: timeBlockEntity,
                  workingDayEnd: workDay?.endedAt ?? null,
                  workingDayStart: workDay?.startedAt ?? null,
                  workingTimeNotEnough
                };
              }
            )
          };
        }),
        range
      })),
      store: shop
    };
  });
