import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { DateTime } from 'luxon';

import { BERLIN_TIME_ZONE } from 'Constants';
import { ShopScheduleRepository } from 'Server/modules/shop/repositories';
import { LoggerService } from 'Server/providers';

@Injectable()
export class PromoterConfirmationJobService {
  constructor(
    private readonly loggerService: LoggerService,
    private readonly shopScheduleRepository: ShopScheduleRepository
  ) {}

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT, { timeZone: BERLIN_TIME_ZONE })
  private async declineConfirmation() {
    this.loggerService.info('Initialize decline confirmation');

    const date = DateTime.utc().startOf('day').toISO();

    const confirmation = await this.shopScheduleRepository.getConfirmationByDate(
      date
    );

    if (confirmation.length) {
      const {
        affected
      } = await this.shopScheduleRepository.declineConfirmationByDate(
        confirmation.map(({ sid }) => sid)
      );

      if (affected) {
        this.loggerService.info(`${affected} confirmation declined`);
      }
    }

    this.loggerService.info('Decline confirmation complete');
  }
}
