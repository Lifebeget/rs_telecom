import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';
import { Any } from 'typeorm';

import {
  Errors,
  ScheduleStatus,
  SchedulesDoneDay,
  ShopScheduleStateStatus
} from 'Constants';
import { PaginationModel } from 'Server/models';
import { MemberEntity } from 'Server/modules/member/entities';
import { MemberService } from 'Server/modules/member/member.service';
import { PromoterScheduleEntity } from 'Server/modules/shop/entities';
import {
  PromoterScheduleRepository,
  ShopDayRepository,
  ShopRepository,
  ShopScheduleRepository,
  ShopScheduleStateRepository,
  ShopXMemberRepository
} from 'Server/modules/shop/repositories';
import { ShopService } from 'Server/modules/shop/shop.service';
import { WorkingDaysRepository } from 'Server/modules/user-tracking/repositories';
import { SplitDateOnRange, fromToDate } from 'Server/utils';
import { LoggerService } from 'Server/providers';

import { PromoterNotificationService } from '../../promoter-shedule/notification';
import { GetShopScheduleParamsDto } from '../dto';
import {
  GetShopPlanningListResponse,
  GetShopPlanningResponse,
  GetShopScheduleListResponse,
  GetShopScheduleResponse,
  ShopPlanningStateResponse
} from '../responses';
import { shopPlanningTransform, shopScheduleTransform } from '../transforms';

@Injectable()
export class ShopBookingService {
  constructor(
    private readonly loggerService: LoggerService,
    private readonly promoterNotificationService: PromoterNotificationService,
    private readonly promoterScheduleRepository: PromoterScheduleRepository,
    private readonly shopDayRepository: ShopDayRepository,
    private readonly shopRepository: ShopRepository,
    private readonly shopScheduleRepository: ShopScheduleRepository,
    private readonly shopScheduleStateRepository: ShopScheduleStateRepository,
    private readonly shopService: ShopService,
    private readonly shopXMemberRepository: ShopXMemberRepository,
    private readonly workingDaysRepository: WorkingDaysRepository,
    private readonly memberService: MemberService
  ) {}

  async getShopScheduleList(
    userSid: string,
    filter: GetShopScheduleParamsDto,
    pagination: PaginationModel
  ): Promise<GetShopScheduleListResponse> {
    const { from, to } = fromToDate(filter.from);
    const weeks = SplitDateOnRange.fromState(from, to);

    const shopIds = await this.shopService.getShopByUser(userSid);
    const [shops, total] = await this.shopRepository.getShopListByIds(
      shopIds,
      pagination
    );
    const shopScheduleEntities = await this.shopScheduleStateRepository.getShopScheduleList(
      shopIds,
      from,
      to
    );
    const workingDayEntities = await this.workingDaysRepository.getPromotersWorkingDays(
      shopScheduleEntities.map(({ promoterSchedule }) => {
        const {
          member
        } = (promoterSchedule as unknown) as PromoterScheduleEntity;
        const memberEntity = (member as unknown) as MemberEntity;

        return memberEntity.sid;
      }),
      from,
      to
    );
    const shopDayEntities = await this.shopDayRepository.getShopCancelledDays(
      shops.map(({ sid }) => sid)
    );

    return plainToClass(GetShopScheduleListResponse, {
      rows: shopScheduleTransform(
        shops,
        shopScheduleEntities,
        shopDayEntities,
        workingDayEntities,
        weeks,
        filter.from
      ),
      total
    });
  }

  async getShopSchedule(
    userSid: string,
    shopSid: string,
    filter: GetShopScheduleParamsDto
  ): Promise<GetShopScheduleResponse> {
    const { from, to } = fromToDate(filter.from);
    const weeks = SplitDateOnRange.fromState(from, to);

    const member = await this.memberService.getMemberByUserSid(userSid);
    const isSupervisor = this.memberService.isMemberSupervisor(member);

    if (!isSupervisor) {
      const shopIds = await this.shopService.getShopByUser(userSid);

      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      if (shopIds && shopIds !== null && !shopIds.includes(shopSid)) {
        throw new ForbiddenException(Errors.SOMETHING_WENT_WRONG);
      }
    }

    const shop = await this.shopRepository.getShopEntity(shopSid);
    const shops = [shop]; // TODO: fix this workaround

    const shopScheduleEntities = await this.shopScheduleStateRepository.getShopScheduleList(
      shops.map(s => s.sid),
      from,
      to
    );

    const workingDayEntities = await this.workingDaysRepository.getPromotersWorkingDays(
      shopScheduleEntities
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
        .filter(el => el.promoterSchedule && el.promoterSchedule !== null)
        // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
        .map(({ promoterSchedule }) => {
          const {
            member
          } = (promoterSchedule as unknown) as PromoterScheduleEntity;
          const memberEntity = (member as unknown) as MemberEntity;

          return memberEntity.sid;
        }),
      from,
      to
    );
    const shopDayEntities = await this.shopDayRepository.getShopCancelledDays(
      shops.map(({ sid }) => sid)
    );

    const list = plainToClass(GetShopScheduleListResponse, {
      rows: shopScheduleTransform(
        shops,
        shopScheduleEntities,
        shopDayEntities,
        workingDayEntities,
        weeks,
        filter.from
      ),
      total: 1
    });

    return list.rows[0];
  }

  async getShopPlanning(
    userSid: string,
    shopSid: string
  ): Promise<GetShopPlanningResponse> {
    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);
    const weeks = SplitDateOnRange.fromState(from, to);

    const member = await this.memberService.getMemberByUserSid(userSid);
    const isSupervisor = this.memberService.isMemberSupervisor(member);

    if (!isSupervisor) {
      const shopIds = await this.shopService.getShopByUser(userSid);

      if (!shopIds?.includes(shopSid)) {
        throw new ForbiddenException(Errors.SOMETHING_WENT_WRONG);
      }
    }

    const shop = await this.shopRepository.getShopEntity(shopSid);
    const shops = [shop]; // TODO: fix this workaround

    const shopScheduleEntities = await this.shopScheduleStateRepository.getShopPlanningList(
      shops.map(s => s.sid),
      from,
      to
    );
    const shopDayEntities = await this.shopDayRepository.getShopCancelledDays(
      shops.map(({ sid }) => sid)
    );
    const promoters = await this.shopXMemberRepository.getPromotersByShop(
      shops.map(({ sid }) => sid)
    );
    const promoterSchedule = await this.promoterScheduleRepository.getAvailablePromoterByShop(
      promoters.map(({ sid }) => sid),
      DateTime.fromISO(nextMonth, { zone: 'utc' }).startOf('month').toISO(),
      DateTime.fromISO(nextMonth, { zone: 'utc' }).endOf('month').toISO()
    );

    const response = plainToClass(GetShopPlanningListResponse, {
      rows: shopPlanningTransform(
        shops,
        shopScheduleEntities,
        shopDayEntities,
        promoters,
        promoterSchedule,
        weeks,
        nextMonth
      ),
      total: 1
    });

    return response.rows[0];
  }

  async getShopPlanningList(
    userSid: string,
    pagination: PaginationModel
  ): Promise<GetShopPlanningListResponse> {
    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);
    const weeks = SplitDateOnRange.fromState(from, to);

    const shopIds = await this.shopService.getShopByUser(userSid);
    const [shops, total] = await this.shopRepository.getShopListByIds(
      shopIds,
      pagination
    );
    const shopScheduleEntities = await this.shopScheduleStateRepository.getShopPlanningList(
      shopIds,
      from,
      to
    );
    const shopDayEntities = await this.shopDayRepository.getShopCancelledDays(
      shops.map(({ sid }) => sid)
    );
    const promoters = await this.shopXMemberRepository.getPromotersByShop(
      shops.map(({ sid }) => sid)
    );
    const promoterSchedule = await this.promoterScheduleRepository.getAvailablePromoterByShop(
      promoters.map(({ sid }) => sid),
      DateTime.fromISO(nextMonth, { zone: 'utc' }).startOf('month').toISO(),
      DateTime.fromISO(nextMonth, { zone: 'utc' }).endOf('month').toISO()
    );

    return plainToClass(GetShopPlanningListResponse, {
      rows: shopPlanningTransform(
        shops,
        shopScheduleEntities,
        shopDayEntities,
        promoters,
        promoterSchedule,
        weeks,
        nextMonth
      ),
      total
    });
  }

  async checkShopAccess(userSid: string, shops: string[]) {
    const shopIds = await this.shopService.getShopByUser(userSid);
    let res = false;
    if (shopIds) {
      shopIds.forEach(shopId => {
        if (shops.find(shop => shop === shopId)) {
          res = true;
        }
      });
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- refactor
      if (!res) {
        throw new ForbiddenException(Errors.FORBIDDEN);
      }
    }
  }

  async getShopPlanConfirmationByUser(
    userSid: string
  ): Promise<ShopPlanningStateResponse> {
    this.loggerService.debug('Get shop plan confirmation by user', {
      payload: { userSid }
    });

    const shopIds = await this.shopService.getShopByUser(userSid);

    if (!shopIds || !shopIds.length) {
      this.loggerService.error('No shops for user');

      throw new BadRequestException(Errors.SOMETHING_WENT_WRONG);
    }

    return this.getShopPlanConfirmation(userSid, shopIds, false);
  }

  async getShopPlanConfirmation(
    userSid: string,
    shops: string[],
    checkAccess = true
  ): Promise<ShopPlanningStateResponse> {
    if (checkAccess) {
      await this.checkShopAccess(userSid, shops);
    }

    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);

    const shopScheduleStateEntities = await this.shopScheduleStateRepository.find(
      {
        from,
        shop: Any(shops),
        to
      }
    );

    if (shopScheduleStateEntities.length) {
      const shopScheduleEntities = await this.shopScheduleRepository.find({
        scheduleStatus: ScheduleStatus.Confirmation,
        shopScheduleState: Any(shopScheduleStateEntities.map(({ sid }) => sid))
      });

      shopScheduleStateEntities.forEach(shopScheduleStateEntity => {
        shopScheduleStateEntity.shopSchedules = shopScheduleEntities.filter(
          ({ shop }) => shopScheduleStateEntity.shop === shop
        );
      });
    }

    return plainToClass(ShopPlanningStateResponse, {
      states: shopScheduleStateEntities
    });
  }

  async getShopScheduleListFromNotification(
    shopSids: string[]
  ): Promise<GetShopScheduleListResponse> {
    //@TODO
    const filterFrom = DateTime.utc().startOf('day').toISO();
    const { from, to } = fromToDate(filterFrom);
    const weeks = SplitDateOnRange.fromState(from, to);

    const [shops, total] = await this.shopRepository.getAllShopsByIds(shopSids);
    const shopScheduleEntities = await this.shopScheduleStateRepository.getShopScheduleList(
      shopSids,
      from,
      to
    );
    const shopDayEntities = await this.shopDayRepository.getShopCancelledDays(
      shops.map(({ sid }) => sid)
    );
    const workingDays = await this.workingDaysRepository.getPromotersWorkingDays(
      // eslint-disable-next-line sonarjs/no-identical-functions -- refactor
      shopScheduleEntities.map(({ promoterSchedule }) => {
        const {
          member
        } = (promoterSchedule as unknown) as PromoterScheduleEntity;
        const memberEntity = (member as unknown) as MemberEntity;

        return memberEntity.sid;
      }),
      from,
      to
    );

    return plainToClass(GetShopScheduleListResponse, {
      rows: shopScheduleTransform(
        shops,
        shopScheduleEntities,
        shopDayEntities,
        workingDays,
        weeks,
        filterFrom
      ),
      total
    });
  }

  async confirmShopPlan(
    userSid: string,
    shops: string[]
  ): Promise<ShopPlanningStateResponse> {
    await this.checkShopAccess(userSid, shops);

    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);

    const shopScheduleStateEntity = await this.shopScheduleStateRepository.findOne(
      {
        from,
        to
      }
    );

    if (!shopScheduleStateEntity) {
      throw new InternalServerErrorException(Errors.SOMETHING_WENT_WRONG);
    }

    if (
      await this.shopScheduleRepository.findOne({
        isArchived: false,
        scheduleStatus: ScheduleStatus.Confirmation,
        shopScheduleState: shopScheduleStateEntity.sid
      })
    ) {
      throw new BadRequestException(Errors.HAS_CONFIRMING_IN_SCHEDULE);
    }

    await this.shopScheduleStateRepository.update(
      {
        from,
        shop: Any(shops),
        to
      },
      {
        status: ShopScheduleStateStatus.Confirm
      }
    );

    //@TODO: Верстка еще не готова
    // const data = await this.getShopScheduleListFromNotification(shops);
    // const preparedNotificationData = this.promoterNotificationService.prepareEmailShop(data);

    return this.getShopPlanConfirmation(userSid, shops, false);
  }

  async cancelShopPlan(
    userSid: string,
    shops: string[]
  ): Promise<ShopPlanningStateResponse> {
    const currentDay = new Date().getDate();

    if (currentDay > SchedulesDoneDay.SHOP) {
      throw new BadRequestException(Errors.FORBIDDEN);
    }

    await this.checkShopAccess(userSid, shops);

    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);

    await this.shopScheduleStateRepository.update(
      {
        from,
        shop: Any(shops),
        to
      },
      {
        status: ShopScheduleStateStatus.Draft
      }
    );

    return this.getShopPlanConfirmation(userSid, shops, false);
  }

  async cleanSchedule() {
    await this.shopScheduleStateRepository.query(
      `
      truncate shops.shop_schedule cascade;
      truncate shops.shop_schedule_state cascade;
      truncate shops.shop_day cascade;
      truncate shops.promoter_schedule cascade;
      truncate shops.promoter_schedule_state cascade;
    `
    );
  }
}
