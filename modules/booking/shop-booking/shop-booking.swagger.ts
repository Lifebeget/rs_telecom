import { HttpStatus, applyDecorators } from '@nestjs/common';
import {
  ApiExtraModels,
  ApiParam,
  ApiQuery,
  ApiResponse,
  getSchemaPath
} from '@nestjs/swagger';

import { ApiPagination } from 'Server/decorators';

import { ConfirmPlanDto } from '../dto';
import {
  GetShopPlanningListResponse,
  GetShopPlanningResponse,
  GetShopScheduleListResponse,
  GetShopScheduleResponse,
  ShopPlanningStateResponse
} from '../responses';

export function ShopBookingSwaggerGetShopScheduleList() {
  return applyDecorators(
    ApiPagination(),
    ApiQuery({
      description: 'From worked at in seconds',
      example: '1607731200',
      name: 'from',
      required: false,
      type: String
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetShopScheduleListResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function ShopBookingSwaggerGetShopSchedule() {
  return applyDecorators(
    ApiPagination(),
    ApiQuery({
      description: 'From worked at in seconds',
      example: '1607731200',
      name: 'from',
      required: false,
      type: String
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetShopScheduleResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function ShopBookingSwaggerGetShopPlanningList() {
  return applyDecorators(
    ApiPagination(),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetShopPlanningListResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function ShopBookingSwaggerGetShopPlanning() {
  return applyDecorators(
    ApiParam({
      name: 'shopSid',
      required: true,
      type: String
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetShopPlanningResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function ShopBookingSwaggerGetShopPlanConfirmation() {
  return applyDecorators(
    ApiExtraModels(ConfirmPlanDto),
    ApiQuery({
      schema: {
        $ref: getSchemaPath(ConfirmPlanDto)
      }
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: ShopPlanningStateResponse
    }),
    ApiResponse({
      description: 'Forbidden',
      status: HttpStatus.FORBIDDEN
    })
  );
}

export function ShopBookingSwaggerConfirmShopPlan() {
  return applyDecorators(
    ApiResponse({
      status: HttpStatus.OK,
      type: ShopPlanningStateResponse
    }),
    ApiResponse({
      description: 'Forbidden',
      status: HttpStatus.FORBIDDEN
    })
  );
}

// eslint-disable-next-line sonarjs/no-identical-functions -- swagger equal scheme
export function ShopBookingSwaggerCancelShopPlan() {
  return applyDecorators(
    ApiResponse({
      status: HttpStatus.OK,
      type: ShopPlanningStateResponse
    }),
    ApiResponse({
      description: 'Forbidden',
      status: HttpStatus.FORBIDDEN
    })
  );
}
