import { Body, Controller, Get, Param, Put, Query } from '@nestjs/common';
import { ApiExcludeEndpoint, ApiTags } from '@nestjs/swagger';

import { Permission } from 'Constants';
import { Pagination, Restrict, User } from 'Server/decorators';
import { PaginationModel } from 'Server/models';
import { JwtPayloadModel } from 'Server/modules/auth/models';
import { RoleService } from 'Server/modules/role/role.service';

import { ConfirmPlanDto, GetShopScheduleParamsDto } from '../dto';
import {
  GetShopPlanningListResponse,
  GetShopPlanningResponse,
  GetShopScheduleListResponse,
  GetShopScheduleResponse,
  ShopPlanningStateResponse
} from '../responses';

import { ShopBookingService } from './shop-booking.service';
import {
  ShopBookingSwaggerCancelShopPlan,
  ShopBookingSwaggerConfirmShopPlan,
  ShopBookingSwaggerGetShopPlanConfirmation,
  ShopBookingSwaggerGetShopPlanning,
  ShopBookingSwaggerGetShopPlanningList,
  ShopBookingSwaggerGetShopSchedule,
  ShopBookingSwaggerGetShopScheduleList
} from './shop-booking.swagger';

@Controller('booking/shop')
@ApiTags('Booking')
export class ShopBookingController {
  constructor(
    private readonly shopBookingService: ShopBookingService,
    private readonly roleService: RoleService
  ) {}

  @Get('schedule')
  @Restrict()
  @ShopBookingSwaggerGetShopScheduleList()
  async getShopScheduleList(
    @Query() query: GetShopScheduleParamsDto,
    @Pagination() pagination: PaginationModel,
    @User() user: JwtPayloadModel
  ): Promise<GetShopScheduleListResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getShopSchedule],
      user.sid
    );

    return this.shopBookingService.getShopScheduleList(
      user.sid,
      query,
      pagination
    );
  }

  @Get('schedule/:shopSid')
  @Restrict()
  @ShopBookingSwaggerGetShopSchedule()
  async getShopSchedule(
    @Param('shopSid') shopSid: string,
    @Query() query: GetShopScheduleParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetShopScheduleResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getShopSchedule],
      user.sid
    );

    return this.shopBookingService.getShopSchedule(user.sid, shopSid, query);
  }

  @Get('planning')
  @Restrict()
  @ShopBookingSwaggerGetShopPlanningList()
  async getShopPlanningList(
    @Pagination() pagination: PaginationModel,
    @User() user: JwtPayloadModel
  ): Promise<GetShopPlanningListResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getShopPlanning],
      user.sid
    );

    return this.shopBookingService.getShopPlanningList(user.sid, pagination);
  }

  @Get('planning/:shopSid')
  @Restrict()
  @ShopBookingSwaggerGetShopPlanning()
  async getShopPlanning(
    @Param('shopSid') shopSid: string,
    @User() user: JwtPayloadModel
  ): Promise<GetShopPlanningResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getShopPlanning],
      user.sid
    );

    return this.shopBookingService.getShopPlanning(user.sid, shopSid);
  }

  @Get('confirm-plan')
  @Restrict()
  @ShopBookingSwaggerConfirmShopPlan()
  async getShopPlanConfirmation(
    @Query() confirmPlanDto: ConfirmPlanDto,
    @User() user: JwtPayloadModel
  ): Promise<ShopPlanningStateResponse> {
    return this.shopBookingService.getShopPlanConfirmation(
      user.sid,
      confirmPlanDto.shops
    );
  }

  @Get('confirm-plan/my')
  @Restrict()
  @ShopBookingSwaggerConfirmShopPlan() // TODO: separated swagger decorator (who cares abount swagger?)
  async getShopPlanConfirmationMy(
    @User() user: JwtPayloadModel
  ): Promise<ShopPlanningStateResponse> {
    return this.shopBookingService.getShopPlanConfirmationByUser(user.sid);
  }

  @Put('confirm-plan')
  @Restrict()
  @ShopBookingSwaggerGetShopPlanConfirmation()
  async confirmShopPlan(
    @Body() confirmPlanDto: ConfirmPlanDto,
    @User() user: JwtPayloadModel
  ): Promise<ShopPlanningStateResponse> {
    return this.shopBookingService.confirmShopPlan(
      user.sid,
      confirmPlanDto.shops
    );
  }

  @Put('cancel-plan')
  @Restrict()
  @ShopBookingSwaggerCancelShopPlan()
  async cancelShopPlan(
    @Body() confirmPlanDto: ConfirmPlanDto,
    @User() user: JwtPayloadModel
  ): Promise<ShopPlanningStateResponse> {
    return this.shopBookingService.cancelShopPlan(
      user.sid,
      confirmPlanDto.shops
    );
  }

  @Get('clean')
  @ApiExcludeEndpoint()
  async cleanSchedule() {
    await this.shopBookingService.cleanSchedule();
  }
}
