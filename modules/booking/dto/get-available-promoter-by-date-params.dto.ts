import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsNotEmpty, IsUUID } from 'class-validator';
import { DateTime } from 'luxon';

@Exclude()
export class GetAvailablePromoterByDateParamsDto {
  @ApiProperty({
    description: 'Date in seconds',
    example: '1607731200',
    name: 'date',
    required: true,
    type: Number
  })
  @Expose()
  @IsNotEmpty()
  @Transform(({ value }) => {
    const from = parseInt(value);

    if (!isNaN(from)) {
      return DateTime.fromSeconds(from, { zone: 'utc' })
        .startOf('day')
        .toSeconds();
    }
  })
  public readonly date!: number;

  @ApiProperty({
    description: 'Store Id',
    example: '1143aae9-beed-46b1-bf65-f2ff87499bbc',
    name: 'store',
    required: true,
    type: String
  })
  @Expose({ name: 'store' })
  @IsUUID()
  public readonly shop!: string;
}
