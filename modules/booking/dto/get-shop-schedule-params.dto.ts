import { Exclude, Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { DateTime } from 'luxon';

@Exclude()
export class GetShopScheduleParamsDto {
  @Expose()
  @IsOptional()
  @Transform(({ value }) => {
    if (value) {
      const from = parseInt(value);

      if (!isNaN(from)) {
        return DateTime.fromSeconds(from, { zone: 'utc' })
          .startOf('day')
          .toISO();
      }
    }

    return DateTime.utc().startOf('day').toISO();
  })
  public readonly from!: string;
}
