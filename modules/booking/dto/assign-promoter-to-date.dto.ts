import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  IsUUID,
  ValidateNested
} from 'class-validator';
import { DateTime } from 'luxon';
import { uniq } from 'remeda';

import { AssignPromoterDto } from './assign-promoter.dto';

@Exclude()
export class AssignPromoterToDateDto {
  @ApiProperty({
    description: 'Assign date in ISO',
    example: 0,
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @Transform(({ value }) =>
    DateTime.fromISO(value, { zone: 'utc' }).startOf('day').toISO()
  )
  public readonly date!: string;

  @ApiProperty({
    description: 'Shop Id',
    example: 'd67b4798-01da-4366-8333-78b18bcee376',
    required: true,
    type: String
  })
  @Expose()
  @IsNotEmpty()
  @IsUUID()
  public readonly shop!: string;

  @ApiProperty({
    description: 'Members',
    required: true,
    type: [AssignPromoterDto]
  })
  @Expose()
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(0)
  @ArrayMaxSize(2)
  @ValidateNested({ each: true })
  @Transform(({ value, obj }) => {
    if (obj.isCancelled) {
      return [];
    }

    return value ? uniq(value) : value;
  })
  public readonly members!: AssignPromoterDto[];

  @ApiProperty({
    description: 'Is work day cancelled',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  @IsNotEmpty()
  public readonly isCancelled!: boolean;
}
