import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { ArrayMinSize, IsArray, IsOptional, IsUUID } from 'class-validator';
import { uniq } from 'remeda';

@Exclude()
export class ConfirmPlanDto {
  @ApiProperty({
    description: 'Shop Ids',
    example: ['4477d15e-2fb7-46c6-a65e-e47ece726399'],
    isArray: true,
    required: true,
    type: String
  })
  @Expose()
  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  @IsUUID(4, { each: true })
  @Transform(({ value }) => (value ? uniq(value) : value))
  public readonly shops!: string[];
}
