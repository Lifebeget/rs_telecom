import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { DateTime } from 'luxon';

@Exclude()
export class GetPromoterScheduleParamsDto {
  @ApiProperty({
    description: 'Search by promoter first name, last name',
    example: 'Jack',
    required: false,
    type: String
  })
  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(255)
  public readonly search?: string;

  @ApiProperty({
    description: 'From worked at in seconds',
    example: '1607731200',
    required: false,
    type: String
  })
  @Expose()
  @IsOptional()
  @Transform(({ value }) => {
    if (value) {
      const from = parseInt(value);

      if (!isNaN(from)) {
        return DateTime.fromSeconds(from, { zone: 'utc' })
          .startOf('day')
          .toISO();
      }
    }

    return DateTime.utc().startOf('day').toISO();
  })
  public readonly from!: string;
}
