import { Exclude, Expose } from 'class-transformer';
import { IsOptional, IsString, MaxLength } from 'class-validator';

@Exclude()
export class GetPromoterPlanningParamsDto {
  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(255)
  public readonly search?: string;
}
