import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsUUID } from 'class-validator';
import { EnumValues } from 'enum-values';

import { BookingType } from 'Constants';

@Exclude()
export class AssignPromoterDto {
  @ApiProperty({
    description: 'Member Id',
    example: '36c16c49-5f05-41f6-a781-1586e301e2f4',
    required: true,
    type: String
  })
  @Expose()
  @IsUUID()
  public readonly memberSid!: string;

  @ApiProperty({
    description: 'Booking type',
    enum: BookingType,
    example: BookingType.K,
    required: true
  })
  @Expose()
  @IsIn(EnumValues.getValues(BookingType))
  public readonly timeBlockType!: BookingType;
}
