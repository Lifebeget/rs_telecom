import { HttpStatus, applyDecorators } from '@nestjs/common';
import {
  ApiExtraModels,
  ApiQuery,
  ApiResponse,
  getSchemaPath
} from '@nestjs/swagger';

import { ApiPagination } from 'Server/decorators';

import {
  AssignPromoterToDateResponse,
  GetAvailablePromoterByDateResponse,
  GetPromoterPlanningListResponse,
  GetPromoterPlanningResponse,
  GetPromoterScheduleListResponse,
  GetPromoterScheduleResponse
} from '../responses';
import {
  GetAvailablePromoterByDateParamsDto,
  GetPromoterScheduleParamsDto
} from '../dto';
import { GetPromotersByMyStoresListResponse } from '../responses/get-promoters-by-my-store-list.response';

export function BookingSwaggerGetPromoterScheduleList() {
  return applyDecorators(
    ApiPagination(),
    ApiExtraModels(GetPromoterScheduleParamsDto),
    ApiQuery({
      schema: {
        $ref: getSchemaPath(GetPromoterScheduleParamsDto)
      }
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetPromoterScheduleListResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function BookingSwaggerGetPromoterSchedule() {
  return applyDecorators(
    ApiExtraModels(GetPromoterScheduleParamsDto),
    ApiQuery({
      schema: {
        $ref: getSchemaPath(GetPromoterScheduleParamsDto)
      }
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetPromoterScheduleResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function BookingSwaggerGetPromoterPlanningList() {
  return applyDecorators(
    ApiPagination(),
    ApiQuery({
      description: 'Search by promoter first name, last name',
      example: 'Jack',
      name: 'search',
      required: false,
      type: String
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetPromoterPlanningListResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function BookingSwaggerGetPromotersByMyStoresList() {
  return applyDecorators(
    ApiPagination(),
    ApiQuery({
      description: 'Search by promoter first name, last name',
      example: 'Jack',
      name: 'search',
      required: false,
      type: String
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetPromotersByMyStoresListResponse
    })
  );
}

export function BookingSwaggerGetPromoterPlanning() {
  return applyDecorators(
    ApiPagination(),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetPromoterPlanningResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function BookingSwaggerGetAvailablePromoterByDate() {
  return applyDecorators(
    ApiExtraModels(GetAvailablePromoterByDateParamsDto),
    ApiQuery({
      schema: {
        $ref: getSchemaPath(GetAvailablePromoterByDateParamsDto)
      }
    }),
    ApiResponse({
      status: HttpStatus.OK,
      type: GetAvailablePromoterByDateResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}

export function BookingSwaggerAssignPromoterToDate() {
  return applyDecorators(
    ApiResponse({
      status: HttpStatus.OK,
      type: AssignPromoterToDateResponse
    }),
    ApiResponse({
      description: 'Not Found',
      status: HttpStatus.NOT_FOUND
    })
  );
}
