import {
  BadRequestException,
  ForbiddenException,
  Injectable
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { DateTime } from 'luxon';

import {
  Errors,
  PromoterScheduleStateStatus,
  PromoterScheduleStatus,
  ScheduleStatus,
  ShopScheduleStateStatus
} from 'Constants';
import { PaginationModel } from 'Server/models';
import { MemberEntity } from 'Server/modules/member/entities';
import { MemberService } from 'Server/modules/member/member.service';
import { MemberModel } from 'Server/modules/member/models';
import {
  PromoterScheduleEntity,
  ShopScheduleEntity
} from 'Server/modules/shop/entities';
import {
  PromoterScheduleRepository,
  PromoterScheduleStateRepository,
  ShopDayRepository,
  ShopRepository,
  ShopScheduleRepository,
  ShopScheduleStateRepository,
  ShopTimeBlockRepository,
  ShopXMemberRepository
} from 'Server/modules/shop/repositories';
import { ShopService } from 'Server/modules/shop/shop.service';
import { WorkingDaysRepository } from 'Server/modules/user-tracking/repositories';
import { LoggerService } from 'Server/providers';
import { SplitDateOnRange, fromToDate } from 'Server/utils';

import {
  AssignPromoterDto,
  AssignPromoterToDateDto,
  GetPromoterPlanningParamsDto,
  GetPromoterScheduleParamsDto
} from '../dto';
import {
  AssignPromoterToDateResponse,
  GetAvailablePromoterByDateResponse,
  GetPromoterPlanningListResponse,
  GetPromoterPlanningResponse,
  GetPromoterScheduleListResponse,
  GetPromoterScheduleResponse
} from '../responses';
import {
  assignPromoterTransform,
  availablePromotersTransform,
  promoterPlanningTransform,
  promoterScheduleTransform
} from '../transforms';
import { GetPromotersByMyStoresListResponse } from '../responses/get-promoters-by-my-store-list.response';

@Injectable()
export class PromoterBookingService {
  constructor(
    private readonly loggerService: LoggerService,
    private readonly promoterScheduleRepository: PromoterScheduleRepository,
    private readonly promoterScheduleStateRepository: PromoterScheduleStateRepository,
    private readonly shopDayRepository: ShopDayRepository,
    private readonly shopRepository: ShopRepository,
    private readonly shopScheduleRepository: ShopScheduleRepository,
    private readonly shopScheduleStateRepository: ShopScheduleStateRepository,
    private readonly shopService: ShopService,
    private readonly shopTimeBlockRepository: ShopTimeBlockRepository,
    private readonly shopXMemberRepository: ShopXMemberRepository,
    private readonly workingDaysRepository: WorkingDaysRepository,
    private readonly memberService: MemberService
  ) {}

  async getPromoterScheduleList(
    userSid: string,
    filter: GetPromoterScheduleParamsDto,
    pagination: PaginationModel
  ): Promise<GetPromoterScheduleListResponse> {
    const { from, to } = fromToDate(filter.from);
    const weeks = SplitDateOnRange.fromState(from, to);

    const shops = await this.shopService.getShopByUser(userSid);
    const [members, total] = await this.shopRepository.getPromoterListByShops(
      shops,
      filter.search ?? null,
      pagination
    );
    const shopScheduleEntities = await this.shopScheduleStateRepository.getPromoterScheduleList(
      members.map(({ sid }) => sid),
      from,
      to
    );
    const workingDays = await this.workingDaysRepository.getPromotersWorkingDays(
      members.map(({ sid }) => sid),
      from,
      to
    );

    return plainToClass(GetPromoterScheduleListResponse, {
      rows: promoterScheduleTransform(
        members,
        shopScheduleEntities,
        workingDays,
        weeks,
        filter.from
      ),
      total
    });
  }

  async getPromoterSchedule(
    userSid: string,
    promoterMemberSid: string,
    filter: GetPromoterScheduleParamsDto
  ): Promise<GetPromoterScheduleResponse> {
    const { from, to } = fromToDate(filter.from);
    const weeks = SplitDateOnRange.fromState(from, to);

    const member = await this.memberService.getMemberByUserSid(userSid);
    const isSupervisor = this.memberService.isMemberSupervisor(member);

    const shopIds = isSupervisor
      ? (await this.shopService.getShopList({})).rows.map(r => r.sid)
      : await this.shopService.getShopByUser(userSid);

    const [members] = await this.shopRepository.getPromoterListByShops(
      shopIds,
      filter.search ?? null,
      {}
    );
    const shopScheduleEntities = await this.shopScheduleStateRepository.getPromoterScheduleList(
      [promoterMemberSid],
      from,
      to
    );
    const workingDays = await this.workingDaysRepository.getPromotersWorkingDays(
      [promoterMemberSid],
      from,
      to
    );

    const rows = promoterScheduleTransform(
      members,
      shopScheduleEntities,
      workingDays,
      weeks,
      filter.from
    );

    return plainToClass(GetPromoterScheduleResponse, rows[0]);
  }

  async getPromoterPlanningList(
    userSid: string,
    filter: GetPromoterPlanningParamsDto,
    pagination: PaginationModel
  ): Promise<GetPromoterPlanningListResponse> {
    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);
    const weeks = SplitDateOnRange.fromState(from, to);

    const shops = await this.shopService.getShopByUser(userSid);
    const [members, total] = await this.shopRepository.getPromoterListByShops(
      shops,
      filter.search ?? null,
      pagination
    );
    const promoterScheduleEntities = await this.promoterScheduleStateRepository.getPromoterPlanningList(
      members.map(({ sid }) => sid)
    );

    return plainToClass(GetPromoterPlanningListResponse, {
      rows: promoterPlanningTransform(
        members,
        promoterScheduleEntities,
        weeks,
        nextMonth
      ),
      total
    });
  }

  async getPromotersByMyStores(
    userSid: string,
    filter: GetPromoterPlanningParamsDto
  ): Promise<GetPromotersByMyStoresListResponse> {
    const member = await this.memberService.getMemberByUserSid(userSid);
    const isSupervisor = this.memberService.isMemberSupervisor(member);

    const shopIds = isSupervisor
      ? (await this.shopService.getShopList({})).rows.map(r => r.sid)
      : await this.shopService.getShopByUser(userSid);

    const [members, total] = await this.shopRepository.getPromoterListByShops(
      shopIds,
      filter.search ?? null,
      {}
    );

    return plainToClass(GetPromotersByMyStoresListResponse, {
      rows: members,
      total
    });
  }

  async getPromoterPlanning(
    userSid: string,
    promoterMemberSid: string,
    filter: GetPromoterPlanningParamsDto
  ): Promise<GetPromoterPlanningResponse> {
    const nextMonth = DateTime.utc().plus({ month: 1 }).toISO();
    const { from, to } = fromToDate(nextMonth);
    const weeks = SplitDateOnRange.fromState(from, to);

    const member = await this.memberService.getMemberByUserSid(userSid);
    const isSupervisor = this.memberService.isMemberSupervisor(member);

    const shopIds = isSupervisor
      ? (await this.shopService.getShopList({})).rows.map(r => r.sid)
      : await this.shopService.getShopByUser(userSid);

    const [members] = await this.shopRepository.getPromoterListByShops(
      shopIds,
      filter.search ?? null,
      {}
    );

    if (!members.find(m => m.sid === promoterMemberSid)) {
      throw new ForbiddenException(Errors.SOMETHING_WENT_WRONG);
    }

    const promoterScheduleEntities = await this.promoterScheduleStateRepository.getPromoterPlanningList(
      [promoterMemberSid]
    );

    const rows = promoterPlanningTransform(
      members,
      promoterScheduleEntities,
      weeks,
      nextMonth
    );

    const memberPlanning = rows.find(
      row => row.member.sid === promoterMemberSid
    );

    if (!memberPlanning) {
      this.loggerService.error('Missing member planning', {
        payload: { promoterMemberSid }
      });
      throw new ForbiddenException(Errors.SOMETHING_WENT_WRONG);
    }

    return plainToClass(GetPromoterPlanningResponse, memberPlanning);
  }

  async getAvailablePromoterByDate(
    shopSid: string,
    date: number
  ): Promise<GetAvailablePromoterByDateResponse> {
    await this.shopRepository.getShopEntity(shopSid);

    const promoters = await this.shopXMemberRepository.getPromotersByShop([
      shopSid
    ]);

    // selected
    const selected = await this.shopScheduleRepository.getSelectedPromoterByShop(
      promoters.map(({ sid }) => sid),
      shopSid,
      DateTime.fromSeconds(date, { zone: 'utc' }).toISO()
    );

    // available and not selected
    const available = await this.promoterScheduleRepository.getAvailablePromoterByShop(
      promoters.map(({ sid }) => sid),
      DateTime.fromSeconds(date, { zone: 'utc' }).toISO()
    );

    return plainToClass(GetAvailablePromoterByDateResponse, {
      rows: availablePromotersTransform(
        plainToClass(MemberModel, promoters),
        selected,
        available
      ),
      total: promoters.length
    });
  }

  async checkPlanIsDoneAndConfirm(date: string, shop: string): Promise<void> {
    /**
     * @TODO Mark
     */
    const actualDate = DateTime.utc().startOf('day').toISO();
    const actualFromAndToDate = fromToDate(actualDate);

    const from = DateTime.fromISO(date)
      .toUTC()
      .startOf('month')
      .startOf('week')
      .startOf('day')
      .toUTC()
      .toISO();
    const to = DateTime.fromISO(date)
      .toUTC()
      .endOf('month')
      .endOf('week')
      .endOf('day')
      .toUTC()
      .toISO();
    if (actualFromAndToDate.from !== from && actualFromAndToDate.to !== to) {
      const state = await this.shopScheduleStateRepository
        .createQueryBuilder('shopSchedule')
        .where('shopSchedule.from = :from', { from })
        .andWhere('shopSchedule.to = :to', { to })
        .andWhere('shopSchedule.shop_sid = :shop', { shop })
        .getOne();
      if (state && state.status !== ShopScheduleStateStatus.Draft) {
        throw new BadRequestException(
          Errors.PLAN_CANNOT_BE_EDITED,
          `The plan cannot be edited, current status: ${state.status}`
        );
      }
    }
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity -- refactor
  async assignPromoterToDate(
    assignPromoterToDateDto: AssignPromoterToDateDto
  ): Promise<AssignPromoterToDateResponse> {
    const shopEntity = await this.shopRepository.getShopEntity(
      assignPromoterToDateDto.shop
    );

    await this.checkPlanIsDoneAndConfirm(
      assignPromoterToDateDto.date,
      shopEntity.sid
    );

    // if (assignPromoterToDateDto.members.length) {
    //   await this.checkAssignablePromoters(assignPromoterToDateDto.members);
    //   const promoters = await this.shopXMemberRepository.findFromAvailablePromoter(
    //     shopEntity.sid,
    //     assignPromoterToDateDto.date,
    //     assignPromoterToDateDto.members.map(({ memberSid }) => memberSid)
    //   );
    //
    //   if (promoters.length !== assignPromoterToDateDto.members.length) {
    //     throw new BadRequestException(Errors.PROMOTER_DOES_NOT_AVAILABLE);
    //   }
    // }

    const scheduleEntities = await this.shopScheduleRepository.getPromoterScheduleByDate(
      shopEntity.sid,
      assignPromoterToDateDto.date
    );

    //Есть проверка выше, данный блок уже не актуален
    // if (
    //   scheduleEntities.some(
    //     ({ shopScheduleStates }) =>
    //       shopScheduleStates?.status === ShopScheduleStateStatus.Done
    //   )
    // ) {
    //   throw new BadRequestException(Errors.FORBIDDEN);
    // }

    const shopTimeBlocks = await this.shopTimeBlockRepository.getShopTimeBlocks(
      assignPromoterToDateDto.shop
    );

    const qr = await this.shopScheduleRepository.startTransaction();

    try {
      let shopDayEntity = await this.shopDayRepository.findOne({
        date: assignPromoterToDateDto.date,
        shop: assignPromoterToDateDto.shop
      });

      if (!shopDayEntity) {
        shopDayEntity = await this.shopDayRepository.createShopDay(
          assignPromoterToDateDto.shop,
          DateTime.fromISO(assignPromoterToDateDto.date)
            .toUTC()
            .toFormat('yyyy-LL-dd'),
          assignPromoterToDateDto.isCancelled,
          qr
        );
      }

      await this.shopDayRepository.cancelShopDay(
        shopDayEntity.sid,
        assignPromoterToDateDto.isCancelled,
        qr
      );

      const insertSids = assignPromoterToDateDto.members.filter(
        ({ memberSid }) => {
          if (!scheduleEntities.length) {
            return true;
          }

          return !scheduleEntities.find(({ promoterSchedule }) => {
            const {
              member
            } = (promoterSchedule as unknown) as PromoterScheduleEntity;
            const memberEntity = (member as unknown) as MemberEntity;

            return memberEntity.sid === memberSid;
          });
        }
      );

      const updateSid = scheduleEntities.reduce<
        Array<[ShopScheduleEntity, AssignPromoterDto]>
      >((acc, shopScheduleEntity) => {
        const {
          member
        } = (shopScheduleEntity.promoterSchedule as unknown) as PromoterScheduleEntity;
        /**
         * @todo Mark
         * timeBlockTypePromoter
         */
        const promoterSchedule = (shopScheduleEntity.promoterSchedule as unknown) as PromoterScheduleEntity;
        const timeBlockTypePromoter = promoterSchedule.timeBlockType;

        const memberEntity = (member as unknown) as MemberEntity;

        const members = assignPromoterToDateDto.members.find(
          ({ memberSid, timeBlockType }) =>
            memberEntity.sid === memberSid &&
            timeBlockType !== timeBlockTypePromoter
        );

        if (members) {
          acc.push([shopScheduleEntity, members]);
        }

        return acc;
      }, []);

      const deleteSids = scheduleEntities.filter(({ promoterSchedule }) => {
        const {
          member
        } = (promoterSchedule as unknown) as PromoterScheduleEntity;
        const memberEntity = (member as unknown) as MemberEntity;

        return !assignPromoterToDateDto.members.find(
          ({ memberSid }) => memberEntity.sid === memberSid
        );
      });

      const isMainMonth =
        new Date().getMonth() ===
        new Date(assignPromoterToDateDto.date).getMonth();

      if (insertSids.length) {
        await Promise.all(
          insertSids.map(async ({ memberSid, timeBlockType }) => {
            const promoterScheduleEntities = await this.promoterScheduleRepository.find(
              {
                member: memberSid,
                workedAt: assignPromoterToDateDto.date
              }
            );

            await this.promoterScheduleRepository.archivePromoterSchedule(
              promoterScheduleEntities.map(({ sid }) => sid),
              true,
              qr
            );

            /**
             * @TODO Mark
             */
            let promoterScheduleEntity = promoterScheduleEntities.find(
              el =>
                el.timeBlock === shopTimeBlocks[timeBlockType] ||
                el.timeBlockType === timeBlockType
            );

            if (!promoterScheduleEntity) {
              promoterScheduleEntity = await this.promoterScheduleRepository.createPromoterSchedule(
                assignPromoterToDateDto.date,
                memberSid,
                shopTimeBlocks[timeBlockType],
                PromoterScheduleStatus.Pending,
                isMainMonth
                  ? PromoterScheduleStateStatus.Done
                  : PromoterScheduleStateStatus.Draft,
                qr
              );
            } else {
              await this.promoterScheduleRepository.archivePromoterSchedule(
                [promoterScheduleEntity.sid],
                false,
                qr
              );
            }

            await qr.manager.update(
              PromoterScheduleEntity,
              { sid: promoterScheduleEntity.sid },
              { timeBlock: shopTimeBlocks[timeBlockType] }
            );

            await this.shopScheduleRepository.createShopSchedule(
              assignPromoterToDateDto.date,
              assignPromoterToDateDto.shop,
              promoterScheduleEntity.sid,
              promoterScheduleEntity.promoterStatus ===
                PromoterScheduleStatus.Available
                ? ScheduleStatus.Assigned
                : ScheduleStatus.Confirmation,
              isMainMonth
                ? ShopScheduleStateStatus.Done
                : ShopScheduleStateStatus.Draft,
              qr
            );
          })
        );
      }

      if (updateSid.length) {
        await this.promoterScheduleRepository.archivePromoterSchedule(
          updateSid.map(([{ promoterSchedule }]) => {
            const { sid } = promoterSchedule as PromoterScheduleEntity;

            return sid;
          }),
          true,
          qr
        );

        await Promise.all(
          updateSid.map(async ([{ sid }, { memberSid, timeBlockType }]) => {
            let promoterScheduleEntity = await this.promoterScheduleRepository.findOne(
              {
                member: memberSid,
                promoterStatus: PromoterScheduleStatus.Available,
                timeBlockType: timeBlockType,
                workedAt: assignPromoterToDateDto.date
              }
            );

            if (!promoterScheduleEntity) {
              promoterScheduleEntity = await this.promoterScheduleRepository.createPromoterSchedule(
                assignPromoterToDateDto.date,
                memberSid,
                shopTimeBlocks[timeBlockType],
                PromoterScheduleStatus.Pending,
                isMainMonth
                  ? PromoterScheduleStateStatus.Done
                  : PromoterScheduleStateStatus.Draft,
                qr
              );
            } else {
              await this.promoterScheduleRepository.archivePromoterSchedule(
                [promoterScheduleEntity.sid],
                false,
                qr
              );
            }

            await qr.manager.update(
              PromoterScheduleEntity,
              { sid: promoterScheduleEntity.sid },
              { timeBlock: shopTimeBlocks[timeBlockType] }
            );

            await this.shopScheduleRepository.updateShopSchedule(
              sid,
              promoterScheduleEntity.sid,
              promoterScheduleEntity.promoterStatus ===
                PromoterScheduleStatus.Available
                ? ScheduleStatus.Assigned
                : ScheduleStatus.Confirmation,
              qr
            );
          })
        );
      }

      if (deleteSids.length) {
        await this.shopScheduleRepository.archiveShopSchedule(
          deleteSids.map(({ sid }) => sid),
          true,
          qr
        );

        /**
         * @Todo Mark
         */
        const prmoterSchedulesRestore = deleteSids.map(
          ({ promoterSchedule }) =>
            (promoterSchedule as unknown) as PromoterScheduleEntity
        );
        await this.promoterScheduleRepository.archivePromoterScheduleAndRestoreAvailable(
          prmoterSchedulesRestore,
          qr
        );
      }

      await qr.commitTransaction();

      return plainToClass(AssignPromoterToDateResponse, {
        date: assignPromoterToDateDto.date,
        members: assignPromoterTransform(
          await this.shopScheduleRepository.getPromoterScheduleByDate(
            shopEntity.sid,
            assignPromoterToDateDto.date
          )
        )
      });
    } catch (err) {
      if (qr.isTransactionActive) {
        await qr.rollbackTransaction();
      }

      throw err;
    } finally {
      if (!qr.isReleased) {
        await qr.release();
      }
    }
  }
}
