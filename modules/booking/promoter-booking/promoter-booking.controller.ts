import { Body, Controller, Get, Param, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { Permission } from 'Constants';
import { Pagination, Restrict, User } from 'Server/decorators';
import { PaginationModel } from 'Server/models';
import { JwtPayloadModel } from 'Server/modules/auth/models';
import { RoleService } from 'Server/modules/role/role.service';

import {
  AssignPromoterToDateDto,
  GetAvailablePromoterByDateParamsDto,
  GetPromoterPlanningParamsDto,
  GetPromoterScheduleParamsDto
} from '../dto';
import {
  AssignPromoterToDateResponse,
  GetAvailablePromoterByDateResponse,
  GetPromoterPlanningListResponse,
  GetPromoterPlanningResponse,
  GetPromoterScheduleListResponse,
  GetPromoterScheduleResponse
} from '../responses';
import { GetPromotersByMyStoresListResponse } from '../responses/get-promoters-by-my-store-list.response';

import { PromoterBookingService } from './promoter-booking.service';
import {
  BookingSwaggerAssignPromoterToDate,
  BookingSwaggerGetAvailablePromoterByDate,
  BookingSwaggerGetPromoterPlanning,
  BookingSwaggerGetPromoterPlanningList,
  BookingSwaggerGetPromoterSchedule,
  BookingSwaggerGetPromoterScheduleList,
  BookingSwaggerGetPromotersByMyStoresList
} from './promoter-booking.swagger';

@Controller('booking/promoter')
@ApiTags('Booking')
export class PromoterBookingController {
  constructor(
    private readonly promoterBookingService: PromoterBookingService,
    private readonly roleService: RoleService
  ) {}

  @Get('schedule')
  @Restrict()
  @BookingSwaggerGetPromoterScheduleList()
  async getPromoterScheduleList(
    @Query() query: GetPromoterScheduleParamsDto,
    @Pagination() pagination: PaginationModel,
    @User() user: JwtPayloadModel
  ): Promise<GetPromoterScheduleListResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getPromoterSchedule],
      user.sid
    );

    return this.promoterBookingService.getPromoterScheduleList(
      user.sid,
      query,
      pagination
    );
  }

  @Get('schedule/:promoterMemberSid')
  @Restrict()
  @BookingSwaggerGetPromoterSchedule()
  async getPromoterSchedule(
    @Param('promoterMemberSid') promoterMemberSid: string,
    @Query() query: GetPromoterScheduleParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetPromoterScheduleResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getPromoterSchedule],
      user.sid
    );

    return this.promoterBookingService.getPromoterSchedule(
      user.sid,
      promoterMemberSid,
      query
    );
  }

  @Get('planning')
  @Restrict()
  @BookingSwaggerGetPromoterPlanningList()
  async getPromoterPlanningList(
    @Pagination() pagination: PaginationModel,
    @Query() query: GetPromoterPlanningParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetPromoterPlanningListResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getPromoterPlanning],
      user.sid
    );

    return this.promoterBookingService.getPromoterPlanningList(
      user.sid,
      query,
      pagination
    );
  }

  @Get('planning/:promoterMemberSid')
  @Restrict()
  @BookingSwaggerGetPromoterPlanning()
  async getPromoterPlanning(
    @Param('promoterMemberSid') promoterMemberSid: string,
    @Query() query: GetPromoterPlanningParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetPromoterPlanningResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getPromoterPlanning],
      user.sid
    );

    return this.promoterBookingService.getPromoterPlanning(
      user.sid,
      promoterMemberSid,
      query
    );
  }

  @Get('list')
  @Restrict()
  @BookingSwaggerGetPromotersByMyStoresList()
  async getPromotersByMyStoresList(
    @Query() query: GetPromoterPlanningParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetPromotersByMyStoresListResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.getPromoterPlanning],
      user.sid
    );

    return this.promoterBookingService.getPromotersByMyStores(user.sid, query);
  }

  @Get('available')
  @Restrict()
  @BookingSwaggerGetAvailablePromoterByDate()
  async getAvailablePromoterByDate(
    @Query() query: GetAvailablePromoterByDateParamsDto,
    @User() user: JwtPayloadModel
  ): Promise<GetAvailablePromoterByDateResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.assignPromoters],
      user.sid
    );

    return this.promoterBookingService.getAvailablePromoterByDate(
      query.shop,
      query.date
    );
  }

  @Put('assign')
  @Restrict()
  @BookingSwaggerAssignPromoterToDate()
  async assignPromoterToDate(
    @Body() assignPromoterToDateDto: AssignPromoterToDateDto,
    @User() user: JwtPayloadModel
  ): Promise<AssignPromoterToDateResponse> {
    await this.roleService.checkPermissionsByMemberOrigin(
      [Permission.assignPromoters],
      user.sid
    );

    return this.promoterBookingService.assignPromoterToDate(
      assignPromoterToDateDto
    );
  }
}
