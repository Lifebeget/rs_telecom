import { Module } from '@nestjs/common';

import { MemberModule } from '../member';
import { PromoterScheduleModule } from '../promoter-shedule';
import { PromoterNotificationService } from '../promoter-shedule/notification/promoter-notification.service';
import { ShopModule } from '../shop';
import { UserTrackingModule } from '../user-tracking';

import { PromoterConfirmationJobService } from './jobs';
import {
  PromoterBookingController,
  PromoterBookingService
} from './promoter-booking';
import { ShopBookingController, ShopBookingService } from './shop-booking';

@Module({
  controllers: [PromoterBookingController, ShopBookingController],
  exports: [PromoterBookingService, ShopBookingService],
  imports: [
    MemberModule,
    ShopModule,
    UserTrackingModule,
    PromoterScheduleModule
  ],
  providers: [
    PromoterBookingService,
    PromoterConfirmationJobService,
    PromoterNotificationService,
    ShopBookingService
  ]
})
export class BookingModule {}
