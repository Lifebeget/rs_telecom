import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';

import { BookingType } from 'Constants';
import { MemberModel } from 'Server/modules/member/models';
import { TimeBlockModel } from 'Server/modules/shop/models';

@Exclude()
export class GetAvailablePromoterByDateRow extends MemberModel {
  @ApiProperty({
    description: 'Is available',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isAvailable!: boolean;

  @ApiProperty({
    description: 'Is assigned',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isSelected!: boolean;

  @ApiProperty({
    description: 'Is declined',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isDeclined!: boolean;

  /**
   * @TODO
   * { sid: null, blockType: BookingType | null }
   */
  @ApiProperty({
    description: 'Selected time block',
    nullable: true,
    required: false,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!:
    | TimeBlockModel
    | { sid?: null; blockType?: BookingType | null }
    | null;

  /**
   * @TODO
   * { sid: null, blockType: BookingType | null }
   */
  @ApiProperty({
    description: 'Available time block',
    nullable: true,
    required: false,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly availableTimeBlock!:
    | TimeBlockModel
    | { sid?: null; blockType?: BookingType | null }
    | null;

  @ApiProperty({
    description: 'Declined time block',
    nullable: true,
    required: false,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly declinedTimeBlock?:
    | TimeBlockModel
    | { sid?: null; blockType?: BookingType | null }
    | null;
}

@Exclude()
export class GetAvailablePromoterByDateResponse {
  @ApiProperty({
    description: 'Total count of available promoter',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of available promoter',
    required: true,
    type: [GetAvailablePromoterByDateRow]
  })
  @Expose()
  @Type(() => GetAvailablePromoterByDateRow)
  public readonly rows!: GetAvailablePromoterByDateRow[];
}
