import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { EnumValues } from 'enum-values';

import { ScheduleStatus } from 'Constants';
import { RangeModel } from 'Server/modules/booking/models';
import { MemberModel } from 'Server/modules/member/models';
import { ShopModel, TimeBlockModel } from 'Server/modules/shop/models';

@Exclude()
class PromoterScheduleDayModel {
  @ApiProperty({
    description: 'Day',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly date!: Date;

  @ApiProperty({
    description: 'Time block',
    nullable: true,
    required: false,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!: TimeBlockModel | null;

  @ApiProperty({
    description: 'Schedule status',
    // example: ScheduleStatus.Assigned,
    enum: EnumValues.getValues(ScheduleStatus),

    nullable: true,

    required: false
  })
  @Expose()
  public readonly scheduleStatus!: ScheduleStatus | null;

  @ApiProperty({
    description: 'Start working date',
    example: '2000-01-31T21:00:00.000Z',
    nullable: true,
    required: false,
    type: Date
  })
  @Expose()
  public readonly workingDayStart!: Date | null;

  @ApiProperty({
    description: 'End working date',
    example: '2000-01-31T21:00:00.000Z',
    nullable: true,
    required: false,
    type: Date
  })
  @Expose()
  public readonly workingDayEnd!: Date | null;

  @ApiProperty({
    description: 'Is work day cancelled',
    example: false,
    nullable: true,
    required: false,
    type: Boolean
  })
  @Expose()
  public readonly isCancelled!: boolean | null;

  @ApiProperty({
    description: 'Start time late flag',
    example: false,
    nullable: true,
    required: false,
    type: Boolean
  })
  @Expose()
  public readonly startTimeLate!: boolean | null;

  @ApiProperty({
    description: 'Not enough of working time flag',
    example: false,
    nullable: true,
    required: false,
    type: Boolean
  })
  @Expose()
  public readonly workingTimeNotEnough!: boolean | null;

  @ApiProperty({
    description: 'Store',
    nullable: true,
    required: false,
    type: ShopModel
  })
  @Expose()
  @Type(() => ShopModel)
  public readonly store!: ShopModel | null;

  @Expose()
  @ApiProperty({
    description: 'Is actual planing month',
    example: true,
    required: false,
    type: Boolean
  })
  public readonly isMainMonth?: boolean;
}

@Exclude()
class PromoterScheduleItem {
  @ApiProperty({
    description: 'Time range',
    required: true,
    type: RangeModel
  })
  @Expose()
  @Type(() => RangeModel)
  public readonly range!: RangeModel;

  @ApiProperty({
    description: 'Days of range',
    required: true,
    type: [PromoterScheduleDayModel]
  })
  @Expose()
  @Type(() => PromoterScheduleDayModel)
  public readonly days!: PromoterScheduleDayModel[];
}

@Exclude()
class GetPromoterScheduleRow {
  @ApiProperty({
    description: 'Member',
    required: true,
    type: MemberModel
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly member!: MemberModel;

  @ApiProperty({
    description: 'Member schedule',
    required: true,
    type: [PromoterScheduleItem]
  })
  @Expose()
  @Type(() => PromoterScheduleItem)
  public readonly schedule!: PromoterScheduleItem[];
}

@Exclude()
export class GetPromoterScheduleResponse extends GetPromoterScheduleRow {}

@Exclude()
export class GetPromoterScheduleListResponse {
  @ApiProperty({
    description: 'Total count of shops schedule',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of promoter schedule',
    required: true,
    type: [GetPromoterScheduleRow]
  })
  @Expose()
  @Type(() => GetPromoterScheduleRow)
  public readonly rows!: GetPromoterScheduleRow[];
}
