import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { EnumValues } from 'enum-values';

import { ScheduleStatus, ScheduleType } from 'Constants';
import { MemberModel } from 'Server/modules/member/models';
import { ShopModel, TimeBlockModel } from 'Server/modules/shop/models';

import { RangeModel } from '../models';

@Exclude()
class AssignedMemberModel extends MemberModel {
  @ApiProperty({
    description: 'Schedule status',
    // example: ScheduleStatus.Assigned,
    enum: EnumValues.getValues(ScheduleStatus),

    nullable: true,

    required: false
  })
  @Expose()
  public readonly scheduleStatus!: ScheduleStatus | null;

  @ApiProperty({
    description: 'Is work day canceled',
    required: true,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!: TimeBlockModel;
}

@Exclude()
class ShopPlanningDayModel {
  @ApiProperty({
    description: 'Day',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly date!: Date;

  @ApiProperty({
    description: 'Schedule type',
    enum: ScheduleType,
    example: ScheduleType.Schedule,
    required: true
  })
  @Expose()
  public readonly scheduleType!: ScheduleType;

  @ApiProperty({
    description: 'Is work day cancelled',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isCancelled!: boolean;

  @ApiProperty({
    description: 'List of assigned members',
    required: true,
    type: [AssignedMemberModel]
  })
  @Expose()
  @Type(() => AssignedMemberModel)
  public readonly members!: AssignedMemberModel[];

  @Expose()
  @ApiProperty({
    description: 'Is actual planing month',
    example: true,
    required: false,
    type: Boolean
  })
  isMainMonth?: boolean;
}

@Exclude()
class ShopPlanningScheduleModel {
  @ApiProperty({
    description: 'Time range',
    required: true,
    type: RangeModel
  })
  @Expose()
  @Type(() => RangeModel)
  public readonly range!: RangeModel;

  @ApiProperty({
    description: 'Days of range',
    required: true,
    type: [ShopPlanningDayModel]
  })
  @Expose()
  @Type(() => ShopPlanningDayModel)
  public readonly days!: ShopPlanningDayModel[];

  @ApiProperty({
    description: 'Amount of unset days',
    example: 2,
    required: true,
    type: Number
  })
  @Expose()
  public readonly summary!: number;
}

@Exclude()
export class GetShopPlanningRow {
  @ApiProperty({
    description: 'Shop',
    required: true,
    type: ShopModel
  })
  @Expose()
  @Type(() => ShopModel)
  public readonly store!: ShopModel;

  @ApiProperty({
    description: 'Shop schedule',
    required: true,
    type: [ShopPlanningScheduleModel]
  })
  @Expose()
  @Type(() => ShopPlanningScheduleModel)
  public readonly schedule!: ShopPlanningScheduleModel[];
}

@Exclude()
export class GetShopPlanningResponse extends GetShopPlanningRow {}

@Exclude()
export class GetShopPlanningListResponse {
  @ApiProperty({
    description: 'Total count of shops schedule',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of shops planning',
    required: true,
    type: [GetShopPlanningRow]
  })
  @Expose()
  @Type(() => GetShopPlanningRow)
  public readonly rows!: GetShopPlanningRow[];
}
