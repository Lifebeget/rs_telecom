import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';

import { MemberModel } from 'Server/modules/member/models';

@Exclude()
export class GetPromotersByMyStoresListResponse {
  @ApiProperty({
    description: 'Total count of membeers',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of members',
    required: true,
    type: [MemberModel]
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly rows!: MemberModel[];
}
