import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { EnumValues } from 'enum-values';

import { ScheduleStatus } from 'Constants';
import { RangeModel } from 'Server/modules/booking/models';
import { MemberModel } from 'Server/modules/member/models';
import { ShopModel, TimeBlockModel } from 'Server/modules/shop/models';

@Exclude()
class WorkedMemberModel extends MemberModel {
  @ApiProperty({
    description: 'Schedule status',
    // example: ScheduleStatus.Assigned,
    enum: EnumValues.getValues(ScheduleStatus),

    nullable: true,

    required: false
  })
  @Expose()
  public readonly scheduleStatus!: ScheduleStatus | null;

  @ApiProperty({
    description: 'Is work day canceled',
    required: true,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!: TimeBlockModel;

  @ApiProperty({
    description: 'Not enough of working time flag',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly workingTimeNotEnough!: boolean;

  @ApiProperty({
    description: 'Start time late flag',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly startTimeLate!: boolean;

  @ApiProperty({
    description: 'Start working date',
    example: '2000-01-31T21:00:00.000Z',
    nullable: true,
    required: false,
    type: Date
  })
  @Expose()
  public readonly workingDayStart!: Date | null;

  @ApiProperty({
    description: 'End working date',
    example: '2000-01-31T21:00:00.000Z',
    nullable: true,
    required: false,
    type: Date
  })
  @Expose()
  public readonly workingDayEnd!: Date | null;
}

@Exclude()
class ShopScheduleDayModel {
  @ApiProperty({
    description: 'Day',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly date!: Date;

  @ApiProperty({
    description: 'Is work day cancelled',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isCancelled!: boolean;

  @ApiProperty({
    description: 'Can the day be edited',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  public readonly isEditable!: boolean;

  @ApiProperty({
    description: 'List of worked members',
    required: true,
    type: [WorkedMemberModel]
  })
  @Expose()
  @Type(() => WorkedMemberModel)
  public readonly members!: WorkedMemberModel[];

  @Expose()
  @ApiProperty({
    description: 'Is actual planing month',
    example: true,
    required: false,
    type: Boolean
  })
  isMainMonth?: boolean;
}

@Exclude()
class ShopScheduleModel {
  @ApiProperty({
    description: 'Time range',
    required: true,
    type: RangeModel
  })
  @Expose()
  @Type(() => RangeModel)
  public readonly range!: RangeModel;

  @ApiProperty({
    description: 'Days of range',
    required: true,
    type: [ShopScheduleDayModel]
  })
  @Expose()
  @Type(() => ShopScheduleDayModel)
  public readonly days!: ShopScheduleDayModel[];
}

@Exclude()
class GetShopScheduleRow {
  @ApiProperty({
    description: 'Shop',
    required: true,
    type: ShopModel
  })
  @Expose()
  @Type(() => ShopModel)
  public readonly store!: ShopModel;

  @ApiProperty({
    description: 'Shop schedule',
    required: true,
    type: [ShopScheduleModel]
  })
  @Expose()
  @Type(() => ShopScheduleModel)
  public readonly schedule!: ShopScheduleModel[];
}

@Exclude()
export class GetShopScheduleResponse extends GetShopScheduleRow {}

@Exclude()
export class GetShopScheduleListResponse {
  @ApiProperty({
    description: 'Total count of shops schedule',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of shops schedule',
    required: true,
    type: [GetShopScheduleRow]
  })
  @Expose()
  @Type(() => GetShopScheduleRow)
  public readonly rows!: GetShopScheduleRow[];
}
