import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform, Type } from 'class-transformer';

import { ShopScheduleStateStatus } from 'Constants';

@Exclude()
class ShopPlanningState {
  @ApiProperty({
    description: 'Shop Id',
    example: 'f1d03163-5929-48e5-b9ee-03c3af10ed68',
    required: true,
    type: String
  })
  @Expose()
  shop!: string;

  @ApiProperty({
    description: 'Shop plan status',
    enum: ShopScheduleStateStatus,
    example: ShopScheduleStateStatus.Draft,
    required: true
  })
  @Expose()
  status!: ShopScheduleStateStatus;

  @ApiProperty({
    description: 'Shop has pending confirmed',
    example: false,
    required: true,
    type: Boolean
  })
  @Expose()
  @Transform(({ obj }) => {
    if (Array.isArray(obj.shopSchedules)) {
      return !!obj.shopSchedules.length;
    }

    return false;
  })
  pendingConfirmed!: boolean;
}

@Exclude()
export class ShopPlanningStateResponse {
  @ApiProperty({
    description: 'Shop states',
    isArray: true,
    required: true,
    type: ShopPlanningState
  })
  @Expose()
  @Type(() => ShopPlanningState)
  states!: ShopPlanningState[];
}
