import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';

import { RangeModel } from 'Server/modules/booking/models';
import { MemberModel } from 'Server/modules/member/models';
import { TimeBlockModel } from 'Server/modules/shop/models';

@Exclude()
class PromoterPlanningDayModel {
  @ApiProperty({
    description: 'Day',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly date!: Date;

  @ApiProperty({
    description: 'Time block',
    nullable: true,
    required: false,
    type: TimeBlockModel
  })
  @Expose()
  @Type(() => TimeBlockModel)
  public readonly timeBlock!: TimeBlockModel | null;

  @Expose()
  @ApiProperty({
    description: 'Is actual planing month',
    example: true,
    required: false,
    type: Boolean
  })
  public readonly isMainMonth?: boolean;
}

@Exclude()
class PromoterPlanningModel {
  @ApiProperty({
    description: 'Time range',
    required: true,
    type: RangeModel
  })
  @Expose()
  @Type(() => RangeModel)
  public readonly range!: RangeModel;

  @ApiProperty({
    description: 'Days of range',
    required: true,
    type: [PromoterPlanningDayModel]
  })
  @Expose()
  @Type(() => PromoterPlanningDayModel)
  public readonly days!: PromoterPlanningDayModel[];
}

@Exclude()
class GetPromoterPlanningRow {
  @ApiProperty({
    description: 'Member',
    required: true,
    type: MemberModel
  })
  @Expose()
  @Type(() => MemberModel)
  public readonly member!: MemberModel;

  @ApiProperty({
    description: 'Member schedule',
    required: true,
    type: [PromoterPlanningModel]
  })
  @Expose()
  @Type(() => PromoterPlanningModel)
  public readonly schedule!: PromoterPlanningModel[];
}

@Exclude()
export class GetPromoterPlanningResponse extends GetPromoterPlanningRow {}

@Exclude()
export class GetPromoterPlanningListResponse {
  @ApiProperty({
    description: 'Total count of promoter planning list',
    example: 10,
    required: true,
    type: Number
  })
  @Expose()
  public readonly total!: number;

  @ApiProperty({
    description: 'List of promoter planning',
    required: true,
    type: [GetPromoterPlanningRow]
  })
  @Expose()
  @Type(() => GetPromoterPlanningRow)
  public readonly rows!: GetPromoterPlanningRow[];
}
