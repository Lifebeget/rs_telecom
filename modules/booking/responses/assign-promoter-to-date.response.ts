import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';

import { AssignedMember } from '../models';

@Exclude()
export class AssignPromoterToDateResponse {
  @ApiProperty({
    description: 'Day',
    example: '2000-01-31T21:00:00.000Z',
    required: true,
    type: Date
  })
  @Expose()
  public readonly date!: Date;

  @ApiProperty({
    description: 'List of assigned members',
    required: true,
    type: [AssignedMember]
  })
  @Expose()
  @Type(() => AssignedMember)
  public readonly members!: AssignedMember[];
}
