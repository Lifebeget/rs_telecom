import * as WebSocket from 'ws';
import { INestApplicationContext, WebSocketAdapter } from '@nestjs/common';
import { MessageMappingProperties } from '@nestjs/websockets';
import { EMPTY, Observable, fromEvent } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';

export class WsAdapter implements WebSocketAdapter {
  constructor(private app: INestApplicationContext) {}
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
  create(port: number, options: any = {}): any {
    let params = {
      ...options
    };
    if (!port || port === 0) {
      params = { ...params, noServer: true, port: 0 };
    }

    return new WebSocket.Server({ ...params });
  }

  // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
  bindClientConnect(server: WebSocket.Server, cb: any) {
    server.on('connection', cb);
  }

  bindMessageHandlers(
    client: WebSocket,
    handlers: MessageMappingProperties[],
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
    process: (data: any) => Observable<any>
  ) {
    fromEvent(client, 'message')
      .pipe(
        mergeMap(data => this.bindMessageHandler(data, handlers, process)),
        filter(result => result)
      )
      .subscribe(response => client.send(JSON.stringify(response)));
  }

  bindMessageHandler(
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
    buffer: any,
    handlers: MessageMappingProperties[],
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
    process: (data: any) => Observable<any>
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any -- scheduled for refactor
  ): Observable<any> {
    const message = JSON.parse(buffer.data);
    const messageHandler = handlers.find(
      handler => handler.message === message.event
    );
    if (!messageHandler) {
      return EMPTY;
    }

    return process(messageHandler.callback(message.data));
  }

  close(server: WebSocket.Server) {
    server.close();
  }
}
