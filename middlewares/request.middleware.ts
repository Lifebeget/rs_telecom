import { v4 as uuid } from 'uuid';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

import { ContextService } from '../providers';

@Injectable()
export class RequestMiddleware implements NestMiddleware {
  constructor(private readonly contextService: ContextService) {}

  use(req: Request, res: Response, next: NextFunction) {
    const context = new Map<string, string>();
    context.set('request-id', uuid());

    this.contextService.run(context, () => {
      next();
    });
  }
}
