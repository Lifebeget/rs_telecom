import { NextFunction, Request, Response } from 'express';
import busy from 'toobusy-js';
import { ServiceUnavailableException } from '@nestjs/common';

import { Errors } from 'Constants';

export function tooBusy(req: Request, res: Response, next: NextFunction): void {
  if (busy()) {
    throw new ServiceUnavailableException(Errors.SOMETHING_WENT_WRONG);
  }

  next();
}
